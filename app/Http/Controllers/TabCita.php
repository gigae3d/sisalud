<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use DB;
use Validator;
use Validate;
use Illuminate\Validation\Rule;

use App\Models\tab_cita;

class TabCita extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm1()
    {
        $existe = false;
        return view('citas.registerCarolina', ['validar_registro' => $existe]);
    }

    public function showForm2()
    {
        $existe = false;
        return view('citas.registerItalo', ['validar_registro' => $existe]);
    }

    public function new(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'fecha' => 'required',
            'hora' => 'required',
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'fecha.required' => 'Por favor ingrese una fecha',
                'hora.required' => 'Por favor ingrese una hora'
            ]
        );

        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');

        $resultado = DB::select("SELECT id, fecha, hora, numero_de_identificacion, motivo_consulta FROM tab_citas WHERE fecha ='" . $date . "' AND hora = '" . $request->hora . "'");

        if (count($resultado) > 0) {
            $existe = true;
        } else {
            $existe = false;
            tab_cita::create([
                'fecha' => $date,
                'hora' => $request->hora,
                'numero_de_identificacion' => $request->numero_de_identificacion,
                'motivo_consulta' => empty($request->motivo_consulta) ? '' : $request->motivo_consulta,
                'medico' => $request->medico
            ]);

            return back()->with('exito', 'El registro ha sido creado exitosamente');
        }

        if ($request->medico == 'Ana Carolina Garcia') {
            return view('citas.registerCarolina', ['validar_registro' => $existe]);
        } else {
            return view('citas.registerItalo', ['validar_registro' => $existe]);
        }
    }

    public function eliminarCita($id)
    {
        tab_cita::where('id', $id)->delete();
        return back()->with('exito', 'El registro ha sido eliminado exitosamente');
    }

    public function getCitasMedicoAnaAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("tab_citas.id", "tab_citas.fecha", "tab_citas.hora", "tab_citas.numero_de_identificacion", "tab_citas.motivo_consulta", 'tab_citas.medico', 'tab_pacientes.nombres', 'tab_pacientes.apellidos');

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "tab_citas.id";
        elseif ($sByColumn == 1)
            $bY = "tab_citas.fecha";
        elseif ($sByColumn == 2)
            $bY = "tab_citas.hora";
        elseif ($sByColumn == 3 || $sByColumn == 5)
            $bY = "tab_citas.numero_de_identificacion";
        elseif ($sByColumn == 4)
            $bY = "tab_citas.motivo_consulta";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $citas = DB::select("SELECT
                    tab_pacientes.nombres,
                    tab_pacientes.apellidos,
                    tab_citas.id,
                    tab_citas.fecha,
                    tab_citas.hora,
                    tab_citas.numero_de_identificacion,
                    tab_citas.motivo_consulta,
                    tab_citas.medico
                    FROM
                    tab_citas
                    INNER JOIN tab_pacientes ON tab_pacientes.numero_de_identificacion = tab_citas.numero_de_identificacion
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);


        $citas2 = DB::select("SELECT
                    tab_pacientes.nombres,
                    tab_pacientes.apellidos,
                    tab_citas.id,
                    tab_citas.fecha,
                    tab_citas.hora,
                    tab_citas.numero_de_identificacion,
                    tab_citas.motivo_consulta,
                    tab_citas.medico
                    FROM
                    tab_citas
                    INNER JOIN tab_pacientes ON tab_pacientes.numero_de_identificacion = tab_citas.numero_de_identificacion
                        " . $sWhere . "
                        " . $sOrder);

        $filteredcitas = count($citas);
        $totalcitas = count($citas2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredcitas,
            "recordsFiltered" => $totalcitas,
            "data" => array(),
        );

        foreach ($citas as $inv) {
            $options = '<p hidden>' . $inv->id . '</p><a class="btn btn-warning" href="' . route('delete_cita', ['id' => $inv->id]) . '"><i class="fa fa-trash"></i></a>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->fecha;
            $row[] = $inv->hora;
            $row[] = $inv->numero_de_identificacion.' :: '.$inv->nombres.' '.$inv->apellidos;
            $row[] = $inv->motivo_consulta;
            $row[] = $inv->medico;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);
    }
}
