<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Redirect;
use Validate;
use DB;

use App\Models\tab_equipos_medico;

class TabEquiposMedicos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('equipos medicos.register');
    }

    public function showEditForm($id)
    {
        $equipo_medico = tab_equipos_medico::where('id', $id)->first();

        return view('equipos medicos.edit', compact('equipo_medico'));
    }

    public function new(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'tipo' => 'required',
            'existencia' => 'required|numeric',
            'numero_de_serie' => 'required',
        ],
            [
                'nombre.required' => 'Por favor ingrese un nombre',
                'marca.required' => 'Por favor ingrese una marca',
                'modelo.required' => 'Por favor ingrese un modelo',
                'tipo.required' => 'Por favor ingrese un tipo',
                'existencia.required' => 'Por favor ingrese una existencia',
                'numero_de_serie.required' => 'Por favor ingrese un numero de serie'
            ]
        );

        $resultado = tab_equipos_medico::create([
            'nombre' => $request->nombre,
            'marca' => $request->marca,
            'modelo' => $request->modelo,
            'tipo' => $request->tipo,
            'existencia' => $request->existencia,
            'numero_de_serie' => $request->numero_de_serie
        ]);

        return redirect()->route('view_equipos_medicos')->with('exito', 'El Equipo Médico ha sido agregado exitosamente');
    }

    public function registrarEquipoMedico(Request $request){

        $resultado = tab_equipos_medico::create([
            'nombre' => $request->input('equipo_medico.nombre'),
            'marca' => $request->input('equipo_medico.marca'),
            'modelo' => $request->input('equipo_medico.modelo'),
            'tipo' => $request->input('equipo_medico.tipo'),
            'existencia' => $request->input('equipo_medico.existencia'),
            'numero_de_serie' => $request->input('equipo_medico.numero_serie')
        ]);

        return response()->json(["respuesta"=> "Registro exitoso"]);

    }

    public function list()
    {
        return view('equipos medicos.list2');
    }

    public function obtenerEquiposMedicos(Request $request) {
        $equipos_medicos = tab_equipos_medico::where('nombre','like','%'.$request->input('palabra').'%')->orderBy('existencia', 'desc')->paginate(5);
        $nequipos_medicos = tab_equipos_medico::where('nombre','like','%'.$request->input('palabra').'%')->count();
        return response()->json(["equipos_medicos" => $equipos_medicos, "nequipos_medicos" => $nequipos_medicos]);
    }

    public function getEquiposMedicosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "marca", "modelo", "tipo", "existencia", "numero_de_serie");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];

        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 7)
            $bY = "nombre";
        elseif ($sByColumn == 2)
            $bY = "marca";
        elseif ($sByColumn == 3)
            $bY = "modelo";
        elseif ($sByColumn == 4)
            $bY = "tipo";
        elseif ($sByColumn == 5)
            $bY = "existencia";
        elseif ($sByColumn == 6)
            $bY = "numero_de_serie";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $equipos_medicos = DB::select("SELECT id, nombre, marca, modelo, tipo, existencia, numero_de_serie FROM tab_equipos_medicos
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);


        $equipos_medicos2 = DB::select("SELECT id, nombre, marca, modelo, tipo, existencia, numero_de_serie FROM tab_equipos_medicos
                        " . $sWhere . "
                        " . $sOrder);

        $filteredEquiposMedicos = count($equipos_medicos);
        $totalEquiposMedicos = count($equipos_medicos2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredEquiposMedicos,
            "recordsFiltered" => $totalEquiposMedicos,
            "data" => array(),
        );

        foreach ($equipos_medicos as $inv) {
            $options = '<a class="btn btn-warning" href="' . route('edit_form_equipo_medico', ['id' => $inv->id]) . '">
                                <i class="fa fa-edit"></i> Editar
                              </a>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->marca;
            $row[] = $inv->modelo;
            $row[] = $inv->tipo;
            $row[] = $inv->existencia;
            $row[] = $inv->numero_de_serie;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);
    }

    public function update(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'marca' => 'required',
            'modelo' => 'required',
            'tipo' => 'required',
            'existencia' => 'required|numeric',
            'numero_de_serie' => 'required',
        ]);

        tab_equipos_medico::where('id', $request->id)
            ->update(['nombre' => $request->nombre, 'marca' => $request->marca, 'modelo' => $request->modelo, 'tipo' => $request->tipo, 'existencia' => $request->existencia, 'numero_de_serie' => $request->numero_de_serie]);

        return redirect()->route('view_equipos_medicos')->with('exito', 'El Equipo Médico ha sido modificado exitosamente');
    }
}
