<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\tab_atencion_en_salud;
use App\Models\tab_tpresion_arterial;
use App\Models\tab_glucometria;
use App\Models\tab_inyectologia;
use App\Models\tab_entrega_medicamento;

use Redirect;
use Validate;
use DB;

class AtencionEnEnfermeria extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function form_atencion_en_enfermeria() {
        return view('atencion en salud.atencion_de_enfermeria');
    }

    public function showMenu($id){

        $numero_de_identificacion = $id;

        return view('atencion en salud.menu_atencion_de_enfermeria', ['numero_de_identificacion'=>$numero_de_identificacion]);

    }

    public function showFormInyectologia(){
        return view('atencion en salud.atencion_en_salud');
    }

    public function showFormEntregaDeMedicamento(){
        return view('entrega de medicamentos.register');
    }

    public function new(Request $request){

    }


    

}
