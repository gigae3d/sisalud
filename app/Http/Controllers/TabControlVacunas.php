<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

use App\Models\tab_est_control_v;
use App\Models\tab_archivos_est_vac;
use App\Models\tab_esquema_vacuna;
use App\Models\tab_titulacion_antic;
use App\Models\User;
use App\Models\tab_mensajes_cv;
use App\Notifications\MensajeEnviado;
use App\Models\tab_programas;

use DB;
use Redirect;
use PDF;
use Auth;

class TabControlVacunas extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        $programas = tab_programas::whereIn('id', [4,5])->get();

        return view('control de vacunas.register', compact('programas'));
    }

    public function showFormEnviarMensaje($id)
    {
        return view('control de vacunas.enviar_mensaje', ['id' => $id]);
    }

    public function showFormEnviarMensajeFuncionario()
    {
        $funcionarios = User::where('role', 'UControlVacunas')->get();
        return view('control de vacunas.enviar_mensaje_funcionario', compact('funcionarios'));
    }

    public function new(Request $request)
    {

        $request->validate([
            'numero_de_identificacion'   =>  ['required', 'unique:tab_est_control_vs,numero_de_identificacion'],
            'exped_identificacion'       =>  'required',
            'tipo_identificacion'        =>  'required',
            'apellidos'                  =>  'required',
            'nombres'                    =>  'required',
            'pregrado'                   =>  'required',
            'semestre'                   =>  'required',
            'fecha_de_nacimiento'        =>  'required',
            'lugar_nacimiento'           =>  'required',
            'genero'                     =>  'required',
            'grupo_sanguineo'            =>  'required',
            'rh'                         =>  'required',
            'direccion'                  =>  'required',
            'barrio'                     =>  'required',
            'ciudad'                     =>  'required',
            'telefono_fijo'              =>  'required|numeric',
            'celular'                    =>  'required',
            'correo_electronico'         =>  'required',
            'enfermedades_previas'       =>  'required',
            'ant_alergicos'              =>  'required',
            'med_tomados'                =>  'required',
            'resultado_ppd'              =>  'required',
            'fecha_ppd'                  =>  'required',
            'laboratorio_ppd'            =>  'required',
            'eps'                        =>  'required',
            'conoce_reglamento'          =>  'required',
            'nomape_eme'                 =>  'required',
            'direccion_eme'              =>  'required',
            'telefono_eme'               =>  'required',
            'archivo'                    =>  'required',
            'n_dosis_aplicadas'          =>  'nullable|array',
            'n_dosis_aplicadas.*'        =>  'nullable'

        ],
        [
            'numero_de_identificacion.required'   =>  'Por favor ingrese un numero de identificacion',
            'numero_de_identificacion.unique'     =>  'El número de identificación ya está registrado. Debe ser único',
            'exped_identificacion.required'       =>  'Por favor ingrese donde fue expedida su cedula',
            'tipo_identificacion.required'        =>  'Por favor ingrese el tipo de identificación',
            'apellidos.required'                  =>  'Por favor ingrese sus apellidos',
            'nombres.required'                    =>  'Por favor ingrese su nombre',
            'pregrado.required'                   =>  'Por favor especifique su programa',
            'semestre.required'                   =>  'Por favor ingrese su semestre',
            'fecha_de_nacimiento.required'        =>  'Por favor ingrese su fecha de nacimiento',
            'lugar_nacimiento.required'           =>  'Por favor ingrese su lugar de nacimiento',
            'genero.required'                     =>  'Por favor ingrese un genero',
            'grupo_sanguineo.required'            =>  'Por favor ingrese un grupo sanguineo',
            'rh.required'                         =>  'Por favor ingrese un RH',
            'direccion.required'                  =>  'Por favor ingrese una direccion',
            'barrio.required'                     =>  'Por favor ingrese un barrio',
            'ciudad.required'                     =>  'Por favor ingrese una ciudad',
            'telefono_fijo.required'              =>  'Por favor ingrese un telefono',
            'celular.required'                    =>  'Por favor ingrese un celular',
            'correo_electronico.required'         =>  'Por favor ingrese un correo electrónico',
            'enfermedades_previas.required'       =>  'Por favor especifique si tiene enfermedades previas',
            'ant_alergicos.required'              =>  'Por favor especifique si tiene antecedentes alérgicos',
            'med_tomados.required'                =>  'Por favor especifique si toma medicamentos',
            'resultado_ppd.required'              =>  'Por favor especifique un resultado para PPD',
            'fecha_ppd.required'                  =>  'Por favor especifique una fecha de PPD',
            'laboratorio_ppd.required'            =>  'Por favor especifique un laboratorio donde se realizo PPD',
            'eps.required'                        =>  'Por favor especifique una EPS',
            'conoce_reglamento.required'          =>  'Por favor especifique si conoce el reglamento',
            'nomape_eme.required'                 =>  'Por favor especifique el nombre del contacto de emergencia',
            'direccion_eme.required'              =>  'Por favor especifique una direccion del contacto de emergencia',
            'telefono_eme.required'                =>  'Por favor especifique un teléfono',
            'archivo.required'                    =>  'Por favor ingrese un archivo junto con los anexos respectivos'
        ]
        );

    $date_fecha_d   =   date("Y-m-d");

    $date_nacimiento   =   date_create($request->fecha_de_nacimiento);
    $date_nacimiento   =   date_format($date_nacimiento,'Y-m-d');

    $date_fecha_ppd = date_create($request->fecha_ppd);
    $date_fecha_ppd = date_format($date_fecha_ppd, 'Y-m-d');

        $estudiante = tab_est_control_v::create([
            'usuario' => Auth::id(),
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'pregrado' => $request->pregrado,
            'semestre' => $request->semestre,
            'fecha_diligenciamiento' => $date_fecha_d,
            'numero_de_identificacion' => $request->numero_de_identificacion,
            'tipo' => $request->tipo_identificacion,
            'd_expedida_en' => $request->exped_identificacion,
            'fecha_de_nacimiento' => $date_nacimiento,
            'lugar_de_nacimiento' => $request->lugar_nacimiento,
            'genero' => $request->genero,
            'grupo_sanguineo' => $request->grupo_sanguineo,
            'RH' => $request->rh,
            'direccion' => $request->direccion,
            'barrio' => $request->barrio,
            'ciudad' => $request->ciudad,
            'telefono_fijo' => $request->telefono_fijo,
            'celular' => $request->celular,
            'correo_electronico' => $request->correo_electronico,
            'enfermedades_previas' => $request->enfermedades_previas,
            'antecedentes_alergicos' => $request->ant_alergicos,
            'medicamentos_tomados' => $request->med_tomados,
            'pr_ppd_resultado' => $request->resultado_ppd,
            'pr_ppd_fecha' => $date_fecha_ppd,
            'pr_ppd_laboratorio' => $request->laboratorio_ppd,
            'EPS' => $request->eps,
            'conoce_regl_pr' => $request->conoce_reglamento,
            'eme_nomyape' => $request->nomape_eme,
            'eme_direccion' => $request->direccion_eme,
            'eme_telefono' => $request->telefono_eme
        ]);

        //Guardar vacunas aplicadas
        for ($i = 0; $i < 9; $i++) {
            $date;
            if(isset($request->fechas_ultima_dosis[$i])){
            $date = date_create($request->fechas_ultima_dosis[$i]);
            $date = date_format($date, 'Y-m-d');

            } else{
                $date = NULL;
                
            }
            tab_esquema_vacuna::create([
                'est_control_v' => $estudiante->id,
                'vacuna' => $request->vacuna[$i],
                'n_dosis_aplicadas' => (isset($request->n_dosis_aplicadas[$i]) ? $request->n_dosis_aplicadas[$i]:NULL),
                'fecha_ultima_dosis' => $date
            ]);
        }

        //Guardar vacunas de anticuerpos
        for ($i = 0; $i < 3; $i++) {
            $date;
            if(isset($request->fecha_ant[$i])){
                $date = date_create($request->fecha_ant[$i]);
                $date = date_format($date, 'Y-m-d');
    
            } else{
                $date = NULL;
                    
            }
            tab_titulacion_antic::create([
                'est_control_v' => $estudiante->id,
                'anticuerpos' => $request->anticuerpos[$i],
                'resultado' => (isset($request->resultados_ant[$i]) ? $request->resultados_ant[$i]:NULL),
                'laboratorio' => (isset($request->laboratorio_ant[$i]) ? $request->laboratorio_ant[$i]:NULL),
                'fecha' => $date
            ]);
        }

        //Guardar archivo anexo
        $nombre_archivo = $request->archivo->store('anexos_control_vacunas');

        tab_archivos_est_vac::create([
            'est_control_v' => $estudiante->id,
            'archivo' => $nombre_archivo
        ]);

        //Establecer el numero de registros como 1 para el usuario que inició sesión
        $usuario = User::find(Auth::id());
        $usuario->num_records = 1;
        $usuario->save();

        return redirect()->route('view_registro_est')->with('exito', 'El registro ha sido agregado exitosamente');
    }

    public function borrarRegistroEst(Request $request, $id)
    {
        //Borrar vacunas aplicadas
        tab_esquema_vacuna::where('est_control_v', $id)->delete();
        //Borrar titulación de anticuerpos
        tab_titulacion_antic::where('est_control_v', $id)->delete();
        //Borrar archivo
        $archivo = tab_archivos_est_vac::where('est_control_v', $id)->first();
        //echo("archivo ".storage_path()."/".$archivo->archivo);
        Storage::delete($archivo->archivo);
        //Borrar registro del nombre del archivo en la base de datos
        tab_archivos_est_vac::where('est_control_v', $id)->delete();
        //Borrar registro del estudiante
        tab_est_control_v::where('id', $id)->delete();

      //Establecer el numero de registros como 0 para el usuario que inició sesión
      $usuario = User::find(Auth::id());
      $usuario->num_records = 0;
      $usuario->save();

      //return Redirect::to(route('view_registro_est'));
      return redirect()->route('view_registro_est')->with('exito', 'El registro fue eliminado exitosamente');

    }

    public function list()
    {
        return view('control de vacunas.list');
    }

    public function getRegistrosControlVacunas(Request $request)
    {
        ini_set('max_execution_time', 0);

        $f_i = $request->fecha_i;
        $f_f = $request->fecha_f;

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $iSortCol_0 = $request->iSortCol_0;
        $iSortingCols = $request->iSortingCols;
        $aColumns = array("id", "nombres", "apellidos", "pregrado", "semestre", "fecha_diligenciamiento", "celular", "correo_electronico");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0) {

            $bY = "id";

        } elseif ($sByColumn == 1) {

            $bY = "nombres";

        } elseif ($sByColumn == 2) {

            $bY = "apellidos";

        } elseif ($sByColumn == 3) {

            $bY = "pregrado";

        } elseif ($sByColumn == 4) {

            $bY = "semestre";

        } elseif ($sByColumn == 5) {

            $bY = "fecha_diligenciamiento";

        } elseif ($sByColumn == 6) {

            $bY = "celular";

        } elseif ($sByColumn == 7) {

            $bY = "correo_electronico";

        }

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;


        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $estudiantes = DB::select("SELECT id, usuario, nombres, apellidos, pregrado, semestre, fecha_diligenciamiento, celular, correo_electronico FROM tab_est_control_vs
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart . "");


        $estudiantes2 = DB::select("SELECT id, usuario, nombres, apellidos, pregrado, semestre, fecha_diligenciamiento, celular, correo_electronico FROM tab_est_control_vs
                        " . $sWhere . "
                        " . $sOrder);

        $filteredEstudiantes = count($estudiantes);
        $totalEstudiantes = count($estudiantes2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredEstudiantes,
            "recordsFiltered" => $totalEstudiantes,
            "data" => array(),
        );

        foreach ($estudiantes as $inv) {
            //$archivos = DB::select("SELECT archivo FROM tab_archivos_est_vacs WHERE est_control_v = ".$inv->id);


            //$archivo = $archivos[0]->archivo;

            $options = '<div class="btn-toolbar">
            <a class="btn btn-success" href="' . route('generarpdf', ['id' => $inv->id]) . '"><i class="fa fa-file-pdf-o"></i></a>
            <a class="btn btn-warning"'
                . 'href="' . route('show_file', ['id' => $inv->id]) . '">'
                . '<i class="fa fa-files-o"></i>'
                . '</a>
                <a class="btn btn-primary" href="' . route('view_reg_control_vac', ['id' => $inv->id]) . '"><i class="fa fa-eye"></i></a>
                <a class="btn btn-default" href="' . route('new_mensaje', ['usuario' => $inv->usuario]) . '"><i class="fa fa-comment-o"></i></a>
                              </div>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombres;
            $row[] = $inv->apellidos;
            $row[] = $inv->pregrado;
            $row[] = $inv->semestre;
            $row[] = $inv->fecha_diligenciamiento;
            $row[] = $inv->celular;
            $row[] = $inv->correo_electronico;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);
    }

    public function filtrarEstudiantes(Request $request)
    {

        $estudiantes2 = tab_est_control_v::where('pregrado', $request->programa)->where('semestre', $request->semestre)->paginate(10);

        $estudiantes = $estudiantes2;

        $options = array();

        foreach ($estudiantes2 as $est) {


            array_push($options, '<div class="btn-toolbar">
            <a class="btn btn-success" href="' . route('generarpdf', ['id' => $est->id]) . '"><i class="fa fa-file-pdf-o"></i></a>
            <a class="btn btn-warning"'
                . 'href="' . route('show_file', ['id' => $est->id]) . '">'
                . '<i class="fa fa-files-o"></i>'
                . '</a>
                <a class="btn btn-primary" href="' . route('view_reg_control_vac', ['id' => $est->id]) . '"><i class="fa fa-eye"></i></a>
                <a class="btn btn-default" href="' . route('new_mensaje', ['usuario' => $est->usuario]) . '"><i class="fa fa-comment-o"></i></a>
                              </div>');


        }

        //dd($estudiantes);
        return view('control de vacunas.lista_estudiantes_filtrados', compact('estudiantes', 'options'));

    }

    public function mostrarArchivoAnexo(Request $request, $id)
    {
        $archivo = tab_archivos_est_vac::where('est_control_v', $id)->first();
        return response()->file(storage_path("app/" . $archivo["archivo"]));
    }

    public function showRegistroControlVacunas(Request $request, $id)
    {
        $estudiante = tab_est_control_v::where('id', $id)->first();
        $esquemas_vacunas = tab_est_control_v::find($estudiante->id)->esquema_vacunas;
        $titulacion_anticuerpos = tab_est_control_v::find($estudiante->id)->titulacion_anticuerpos;
        $informacion = compact('estudiante', 'esquemas_vacunas', 'titulacion_anticuerpos');

        return view('control de vacunas.showRegistroControlVacunas', $informacion);
    }

    public function generarPDFHojaDeVida(Request $request, $id)
    {
        ini_set('max_execution_time', 0);
        $estudiante = tab_est_control_v::where('id', $id)->first();
        $esquemas_vacunas = tab_est_control_v::find($estudiante->id)->esquema_vacunas;
        $titulacion_anticuerpos = tab_est_control_v::find($estudiante->id)->titulacion_anticuerpos;

        PDF::SetTitle('Documento');
        PDF::AddPage();
        $path = public_path('imgs/siacu_logo.svg');

        $html = '
            <table style="width: 100%;" border="1">
                <tr>
                    <td rowspan="2"> <img src="' . $path . '" style="width:110px;height:50px;"></td>
                    <td colspan="2"><h4 style="text-align: center;">BIENESTAR INSTITUCIONAL Y GESTIÓN HUMANA</h4></td>
                    <td><h5>Código: 1025-39.1-108-F</h5></td>
                </tr>
                <tr>
                    <td colspan="2"><h4 style="text-align: center;">HOJA DE VIDA ESTUDIANTE</h4></td>
                    <td><h5>Versión:01</h5></td>
                </tr>
            </table>
            <br/><br/>
  
            <div>
                <table style="width: 100%;" border="0" >
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Numero de identificación:</label><br><label style="font-size:9px;">' . $estudiante->numero_de_identificacion . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Expedida en:</label><br><label style="font-size:9px;">' . $estudiante->d_expedida_en . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Tipo:</label><br><label style="font-size:9px;">' . $estudiante->tipo . '</label></td>
                    </tr>
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Apellidos:</label><br><label style="font-size:9px;">' . $estudiante->apellidos . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Nombres:</label><br><label style="font-size:9px;">' . $estudiante->nombres . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Pregrado de:</label><br><label style="font-size:9px;">' . $estudiante->pregrado . '</label></td>
                    </tr>
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Semestre:</label><br><label style="font-size:9px;">' . $estudiante->semestre . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha de nacimiento:</label><br><label style="font-size:9px;">' . $estudiante->fecha_de_nacimiento . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Lugar de nacimiento:</label><br><label style="font-size:9px;">' . $estudiante->lugar_de_nacimiento . '</label></td>
                    </tr>
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Género:</label><br><label style="font-size:9px;">' . $estudiante->genero . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Grupo sanguíneo:</label><br><label style="font-size:9px;">' . $estudiante->grupo_sanguineo . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">RH:</label><br><label style="font-size:9px;">' . $estudiante->RH . '</label></td>
                    </tr>
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Dirección:</label><br><label style="font-size:9px;">' . $estudiante->direccion . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Barrio:</label><br><label style="font-size:9px;">' . $estudiante->barrio . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Ciudad:</label><br><label style="font-size:9px;">' . $estudiante->ciudad . '</label></td>
                    </tr>
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Teléfono fijo:</label><br><label style="font-size:9px;">' . $estudiante->telefono_fijo . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Celular:</label><br><label style="font-size:9px;">' . $estudiante->celular . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Correo electrónico:</label><br><label style="font-size:9px;">' . $estudiante->correo_electronico . '</label></td>
                    </tr>
                </table>
            </div>
            
            <div>
                <label style="font-size:12px; font-weight: bold;">Antecedentes de salud personal</label>
            </div>
            
            <div>
                <table style="width: 100%;" border="0" >
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Enfermedades previas:</label><br><label style="font-size:9px;">' . $estudiante->enfermedades_previas . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Antecedentes alérgicos:</label><br><label style="font-size:9px;">' . $estudiante->antecedentes_alergicos . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Medicamentos que toma:</label><br><label style="font-size:9px;">' . $estudiante->medicamentos_tomados . '</label></td>
                    </tr>
                </table>  
            </div>
            
            <div>
                <label style="font-size:12px; font-weight: bold;">Esquema de vacunas</label>
            </div>

            <div>
                <table style="width: 100%;" border="0">
                    <tr>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Vacuna</th>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Número de dosis aplicadas</th>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Fecha de la última dosis</th>
                    </tr>';

                    foreach ($esquemas_vacunas as $vacuna) {
                        $html .= '
                            <tr>
                                <td><label style="font-size:9px;">' . $vacuna->vacuna . '</label></td>
                                <td><label style="font-size:9px;">' . $vacuna->n_dosis_aplicadas . '</label></td>
                                <td><label style="font-size:9px;">' . $vacuna->fecha_ultima_dosis . '</label></td>
                            </tr>';
                    }

                $html .= '</table> 
            </div>
            
            <div><label style="font-size:12px; font-weight: bold;">Prueba de PPD</label></div>
            
            <div>
                <table style="width: 100%;" border="0">
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Resultado:</label><br><label style="font-size:9px;">' . $estudiante->pr_ppd_resultado . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha:</label><br><label style="font-size:9px;">' . $estudiante->pr_ppd_fecha . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Laboratorio:</label><br><label style="font-size:9px;">' . $estudiante->pr_ppd_laboratorio . '</label></td>
                    </tr>
                </table>
            </div>

            <div><label style="font-size:12px;font-weight: bold;">Titulación de anticuerpos</label></div>
            
            <div>
                <table style="width: 100%;" border="0">
                    <tr>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Anticuerpos</th>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Resultado</th>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Laboratorio</th>
                        <th style="font-size:10px; font-style: italic; font-weight: bold;">Fecha</th>
                    </tr>';

                    foreach ($titulacion_anticuerpos as $titant) {
                        $html .= '
                            <tr>
                                <td><label style="font-size:9px;">' . $titant->anticuerpos . '</label></td>
                                <td><label style="font-size:9px;">' . $titant->resultado . '</label></td>
                                <td><label style="font-size:9px;">' . $titant->laboratorio . '</label></td>
                                <td><label style="font-size:9px;">' . $titant->fecha . '</label></td>
                            </tr>';
                    }

                $html .= '
                </table>
            </div>
            
            <div><label style="font-size:12px; font-weight: bold;">Sistema de aseguramiento</label></div>

            <div>
                <table style="width: 100%;" border="0">
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Seguridad social (EPS):</label><br><label style="font-size:9px;">' . $estudiante->EPS . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">¿Conoce el reglamento de prácticas?:</label><br><label style="font-size:9px;">' . $estudiante->conoce_regl_pr . '</label></td>
                    </tr>
                </table>
            </div>
            
            <div><label style="font-size:12px; font-weight: bold;">En caso de emergencia avisar a:</label></div>

            <div>
                <table style="width: 100%;" border="0">
                    <tr>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Nombres y apellidos:</label><br><label style="font-size:9px;">' . $estudiante->eme_nomyape . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Dirección:</label><br><label style="font-size:9px;">' . $estudiante->eme_direccion . '</label></td>
                        <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Teléfono:</label><br><label style="font-size:9px;">' . $estudiante->eme_telefono . '</label></td>
                    </tr>
                </table>
            </div>';

        PDF::WriteHTML($html);
        PDF::Output('hello_world.pdf');
    }

    public function viewRegistro(Request $request)
    {
        return view('control de vacunas.showRegistroEstudiante', ['usuario'=>Auth::id()]);
    }

    public function viewRegistroAjax(Request $request, $id)
    {
        ini_set('max_execution_time', 0);

        $registro = DB::select("SELECT ecv.id, ecv.nombres, ecv.apellidos, p.name as pregrado, ecv.semestre, ecv.fecha_diligenciamiento, ecv.celular, ecv.correo_electronico 
                                FROM tab_est_control_vs ecv
                                JOIN tab_programas p ON p.id = ecv.pregrado
                                WHERE ecv.usuario = " . $id);

        $output = array();


        foreach ($registro as $inv) {
            $options = '<div class="btn-toolbar"><a class="btn btn-warning"'
                . 'href="' . route('show_file', ['id' => $inv->id]) . '" target="_blank">'
                . '<i class="fa fa-picture-o" aria-hidden="true"></i>'
                . '</a>'
                . '<a class="btn btn-primary"'
                . 'href="' . route('view_reg_control_vac', ['id' => $inv->id]) . '">'
                . '<i class="fa fa-address-card"></i>'
                . '</a>'
                . '<a class="btn btn-success"'
                . 'href="' . route('generarpdf', ['id' => $inv->id]) . '" target="_blank">'
                . '<i class="fa fa-file-pdf-o" aria-hidden="true"></i>'
                . '</a>'
                . '<a class="btn btn-danger"'
                . ' href="' . route('borrarR', ['id' => $inv->id]) . '">'
                . '<i class="fa fa-trash-o" aria-hidden="true"></i>'
                . '</a>'
                . '</div>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombres;
            $row[] = $inv->apellidos;
            $row[] = $inv->pregrado;
            $row[] = $inv->semestre;
            $row[] = $inv->fecha_diligenciamiento;
            $row[] = $inv->celular;
            $row[] = $inv->correo_electronico;
            $row[] = $options;

            array_push($output, $row);
        }

        return response()->json($output);
    }

    public function newMensaje(Request $request)
    {
        $request->validate([
            'mensaje' => 'required'
        ],
            [
                'mensaje.required' => 'Por favor escriba un mensaje'
            ]);

        tab_mensajes_cv::create([
            'destinatario' => $request->estudiante,
            'remitente' => Auth::id(),
            'mensaje' => $request->mensaje
        ]);

        User::find($request->estudiante)->notify(new MensajeEnviado);

        return view('control de vacunas.list');
    }

    public function newMensajeFuncionario(Request $request)
    {
        $request->validate([
            'funcionario' => 'required',
            'mensaje' => 'required'
        ],
            [
                'funcionario.required' => 'Por favor especifique el destinatario',
                'mensaje.required' => 'Por favor escriba un mensaje'
            ]);

        tab_mensajes_cv::create([
            'destinatario' => $request->funcionario,
            'remitente' => Auth::id(),
            'mensaje' => $request->mensaje
        ]);

        User::find($request->funcionario)->notify(new MensajeEnviado);

        return view('control de vacunas.list_mensajes', ['usuario' => Auth::id()]);
    }

    public function listMensajes()
    {
        return view('control de vacunas.list_mensajes', ['usuario' => Auth::id()]);
    }

    public function listMensajesAjax(Request $request, $id)
    {
        $mensajes = DB::select("SELECT id, mensaje, remitente, created_at FROM tab_mensajes_cvs WHERE destinatario = " . $id . " ORDER BY created_at DESC");

        $output = array();

        foreach ($mensajes as $inv) {

            //Obtener nombres y apellidos del usuario remitente
            $remitente = User::where('id', $inv->remitente)->first();

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->remitente . ' : ' . $remitente->name;
            $row[] = $inv->mensaje;
            $row[] = $inv->created_at;

            array_push($output, $row);
            //$output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function marcarNotificacion(Request $request, $id)
    {
        auth()->user()->notifications()->findOrFail($id)->markAsRead();

        return view('control de vacunas.list_mensajes', ['usuario' => Auth::id()]);
    }
}
