<?php

namespace App\Http\Controllers;

use App\Models\tab_equipos_medico;
use App\Models\tab_estudiante;
use App\Models\tab_insumos_medico;
use App\Models\tab_medic_planific;
use App\Models\tab_medicamento;
use Doctrine\Common\Collections\Collection;
use Illuminate\Http\Request;
use Excel;

use App\Models\tab_est_control_v;
use App\Models\tab_archivos_est_vac;
use App\Models\tab_esquema_vacuna;
use App\Models\tab_titulacion_antic;
use App\Models\tab_paciente;
use DB;

class TabReportsExcel extends Controller
{
    public function viewReports()
    {
        return view('reportes.reports');
    }

    public function reporteCuadroVacunas(Request $request)
    {
        $programa = $request->request->get('programa');
        $semestre = $request->request->get('semestre');

        $estudiantes = tab_est_control_v::
        select(
            'id as Identificador',
            'tipo as Tipo de Doc',
            'numero_de_identificacion as Documento',
            'apellidos',
            'nombres',
            'pregrado',
            'semestre',
            'fecha_diligenciamiento',
            'd_expedida_en as Documento expedido en',
            'fecha_de_nacimiento',
            'lugar_de_nacimiento',
            'genero',
            'grupo_sanguineo',
            'RH',
            'direccion',
            'barrio',
            'ciudad',
            'telefono_fijo',
            'celular',
            'correo_electronico',
            'enfermedades_previas',
            'antecedentes_alergicos',
            'medicamentos_tomados',
            'pr_ppd_resultado as PPD Resultado',
            'pr_ppd_fecha as PPD Fecha',
            'pr_ppd_laboratorio as PPD Laboratorio',
            'EPS',
            'conoce_regl_pr as Conoce reglamento',
            'eme_nomyape as Nombre del contacto de emergencia',
            'eme_direccion as Direccion del contacto de emergencia',
            'eme_telefono as Telefono del contacto de emergencia');

        if ($semestre != '0') {
            $estudiantes->where('semestre', (integer)$semestre);
        }

        if ($programa != 'all') {
            $identificaciones = tab_est_control_v::select('numero_de_identificacion')->where('pregrado',$programa)->get();
            $estudiantes->whereIn('numero_de_identificacion',$identificaciones);
        }

        $estudiantes = $estudiantes->get()->toArray();

        $dataExcel = [];
        foreach ($estudiantes as $key => $valor) {
            $data = $valor;
            $vacunas = tab_esquema_vacuna::select('vacuna', 'n_dosis_aplicadas')
                ->where('est_control_v', $valor['Identificador'])
                ->get()->toArray();

            $titulaciones = tab_titulacion_antic::select('anticuerpos', 'resultado')
                ->where('est_control_v', $valor['Identificador'])
                ->get()->toArray();

            $colVac = [
                'vac_Varicela' => '',
                'vac_Hepatitis A' => '',
                'vac_Hepatitis B' => '',
                'vac_MMR (Papera, rubeola, sarampion)' => '',
                'vac_Tétanos-Difteria' => '',
                'vac_Influenza' => '',
                'vac_Neumococo' => '',
                'vac_Meningitis' => '',
                'vac_DPTa' => ''
            ];
            if (count($vacunas)) {
                foreach ($vacunas as $vacuna) {
                    $colVac['vac_' . $vacuna['vacuna']] = $vacuna['n_dosis_aplicadas'] != 0 ? $vacuna['n_dosis_aplicadas'] : '0';
                    $data = array_merge($data, $colVac);
                }
            } else {
                $data = array_merge($data, $colVac);
            }

            $colAnt = [
                'Anticuerpos_Varicela' => '',
                'Anticuerpos_Hepatitis A' => '',
                'Anticuerpos_Hepatitis B' => ''
            ];
            if (!empty($titulaciones)) {
                foreach ($titulaciones as $titulacion) {
                    $colAnt['Anticuerpos_' . $titulacion['anticuerpos']] = $titulacion['resultado'];
                    $data = array_merge($data, $colAnt);
                }
            } else {
                $data = array_merge($data, $colAnt);
            }
            array_push($dataExcel, $data);
        }

        Excel::create('Cuadro_Vacunas', function ($excel) use ($dataExcel) {
            $excel->sheet('Cuadro_Vacunas', function ($sheet) use ($dataExcel) {
                $sheet->fromArray($dataExcel);
            });
        })->export('xls');
    }

    public function reporteMedicamentos()
    {
        $medicamentos = tab_medicamento::select(
            'nombre AS Nombre',
            'laboratorio AS Laboratorio',
            'fecha_vencimiento AS Fecha de vencimiento',
            'existencia AS Existencia',
            'presentacion AS Presentacion',
            'lote AS Lote',
            'invima AS Invima',
            'observaciones AS Observaciones'
        )->orderBy('Nombre', 'ASC')->get()->toArray();

        Excel::create('Cuadro_Medicamentos', function ($excel) use ($medicamentos) {
            $excel->sheet('Cuadro_Medicamentos', function ($sheet) use ($medicamentos) {
                 
                $sheet->fromArray($medicamentos);
                $sheet->prependRow( array(
                    'Fecha de generación de reporte:', "".date("d/m/Y")
                ));
            });
        })->export('xls');
    }

    public function reporteAnticonceptivos()
    {
        $medicamentos = tab_medic_planific::select(
            'nombre',
            'laboratorio',
            'fecha_vencimiento',
            'existencia',
            'presentacion',
            'lote',
            'invima',
            'observaciones'
        )->get()->toArray();

        Excel::create('Cuadro_Anticonceptivos', function ($excel) use ($medicamentos) {
            $excel->sheet('Cuadro_Anticonceptivos', function ($sheet) use ($medicamentos) {
                $sheet->fromArray($medicamentos);
            });
        })->export('xls');
    }

    public function reportInsumosMedicos()
    {
        $medicamentos = tab_insumos_medico::select(
            'nombre',
            'marca',
            'fecha_de_vencimiento',
            'existencia',
            'lote',
            'Registro_INVIMA'
        )->get()->toArray();

        Excel::create('Insumos_Medicos', function ($excel) use ($medicamentos) {
            $excel->sheet('Insumos_Medicos', function ($sheet) use ($medicamentos) {
                $sheet->fromArray($medicamentos);
            });
        })->export('xls');
    }

    public function reportEquiposMedicos()
    {
        $medicamentos = tab_equipos_medico::select(
            'nombre',
            'marca',
            'modelo',
            'tipo',
            'numero_de_serie'
        )->get()->toArray();

        Excel::create('Equipo_Medico', function ($excel) use ($medicamentos) {
            $excel->sheet('Equipo_Medico', function ($sheet) use ($medicamentos) {
                $sheet->fromArray($medicamentos);
            });
        })->export('xls');
    }
}
