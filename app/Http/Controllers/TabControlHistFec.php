<?php

namespace App\Http\Controllers;

use App\Models\tab_controles_fec;
use DB;
use Illuminate\Http\Request;
use Redirect;
use Validate;

class TabControlHistFec extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('historia seg control fecundidad.register_control_historia');
    }

    public function showFormWithId($id)
    {
        $numero_de_identificacion = $id;
        return view('historia seg control fecundidad.register_control_historia', ['id' => $numero_de_identificacion]);
    }

    public function new(Request $request)
    {

        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');

        $resultado = tab_controles_fec::create([
            'historia_control_fec' => $request->historia,
            'fecha' => $date,
            'peso' => $request->peso,
            'TA' => $request->tension_arterial,
            'sangrado_intermenstrual' => $request->sangrado_intermenstrual,
            'efectos_secundarios' => $request->efectos_secundarios,
            'falla_metodo' => $request->falla_del_metodo,
            'signo_infeccion_pelvica' => $request->signos_infeccion_pel,
            'cambio_anticonceptivo' => $request->cambio_anticonceptivo,
            'anexos' => $request->anexos,
            'observaciones' => $request->observaciones
        ]);

        return Redirect::to(route('view_historia_con_fec'));

    }

    public function list($id)
    {
        return view('historia seg control fecundidad.list_controles_hist', ['historia' => 1]);
    }


    public function getControlesHistoriaAjax(Request $request, $historia)
    {
        ini_set('max_execution_time', 0);
        /*$controles_historia = DB::select("SELECT id,fecha, peso, TA, sangrado_intermenstrual,efectos_secundarios,falla_metodo,signo_infeccion_pelvica,anexos FROM tab_controles_fecs");*/
        $controles_historia = DB::select("SELECT id,fecha, peso, TA, sangrado_intermenstrual,efectos_secundarios,falla_metodo,signo_infeccion_pelvica, cambio_anticonceptivo,anexos FROM tab_controles_fecs");

        $output = array();

        foreach ($controles_historia as $inv) {

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->fecha;
            $row[] = $inv->peso;
            $row[] = $inv->TA;
            $row[] = $inv->sangrado_intermenstrual;
            $row[] = $inv->efectos_secundarios;
            $row[] = $inv->falla_metodo;
            $row[] = $inv->signo_infeccion_pelvica;
            $row[] = $inv->anexos;

            array_push($output, $row);


        }

        return response()->json($output);


    }

}
