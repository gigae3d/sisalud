<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;
use Redirect;
use Validate;
use DB;

use App\Models\tab_paciente;
use App\Models\tab_estudiante;
use App\Models\tab_docente;
use App\Models\tab_funcionario;
use App\Models\tab_glucometria;
use App\Models\tab_atencion_en_salud;
use App\Models\tab_tpresion_arterial;
use App\Models\tab_inyectologia;
use App\Models\tab_entrega_medicamento;
use App\Models\tab_programas;

class TabPacientes extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm($nidentificacion = null)
    {
        $programas = tab_programas::all();
        return view('clientes.register', compact('programas', 'nidentificacion'));
    }

    public function showValidate()
    {
        $resultado = array(
            'entro' => false,
            'datos' => array()
        );

        return view('paciente.validar', compact('resultado'));
    }

    public function valCedula(Request $request)
    {
        $resultado = array(
            'entro' => true,
            'datos' => array(),
            'cedula' => $request->get('input_cedula')
        );

        if ($request->isMethod("post")) {
            $request->validate([
                'input_cedula' => 'required|numeric',
            ]);

            $cedula = $request->get('input_cedula');
        }

        if ($request->isMethod("get")) {
            $cedula = session('input_cedula');
        }

        $paciente = tab_paciente::where('numero_de_identificacion', $cedula)->get();
        $paciente = json_decode(json_encode($paciente), true);

        if (!empty($paciente))
            $resultado['datos'] = $paciente;

        $glucometrias = tab_glucometria::where('numero_de_identificacion', $cedula)->get()->count();
        $atenciones_en_salud = tab_atencion_en_salud::where('numero_de_identificacion', $cedula)->get()->count();
        $toma_presiones_arteriales = tab_tpresion_arterial::where('numero_de_identificacion', $cedula)->get()->count();
        $inyectologias = tab_inyectologia::where('numero_de_identificacion', $cedula)->get()->count();
        $entregas_de_medicamento = tab_entrega_medicamento::where('numero_de_identificacion', $cedula)->get()->count();


        return view('paciente.validar', compact('resultado', 'glucometrias', 'atenciones_en_salud', 'toma_presiones_arteriales', 'inyectologias', 'entregas_de_medicamento'));
    }

    public function showEditForm(Request $request, $id)
    {
        $programas = tab_programas::all();
        $pacientes = tab_paciente::where('numero_de_identificacion', $id)->first();
        $estudiantes = tab_estudiante::where('numero_de_identificacion', $id)->first();
        $docente = tab_docente::where('numero_de_identificacion', $id)->first();
        $funcionario = tab_funcionario::where('numero_de_identificacion', $id)->first();

        return view('clientes.edit', compact(
            'pacientes',
            'programas',
            'estudiantes',
            'docente',
            'funcionario'
        ));
    }

    public function new(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required|numeric|unique:tab_pacientes,numero_de_identificacion',
            'nombres' => 'required',
            'apellidos' => 'required',
            'telefono' => 'required',
            'tipo_de_usuario' => 'required',
        ], [
            'nombres.required' => 'Por favor ingrese su Nombre',
            'apellidos.required' => 'Por favor ingrese su Apellido',
            'numero_de_identificacion.required' => 'Por favor ingrese Número de identificación',
            'numero_de_identificacion.unique' => 'Este Número de identificación, ya esta registrado intente con otro',
            'telefono.required' => 'Por favor ingrese su Telefono',
            'tipo_de_usuario.required' => 'Por favor ingrese un Tipo de Usuario'
        ]);

        tab_paciente::create([
            'numero_de_identificacion' => $request->numero_de_identificacion,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'telefono' => $request->telefono,
            'tipo_de_usuario' => $request->tipo_de_usuario,
        ]);

        switch ($request->tipo_de_usuario) {
            case "Estudiante":
                $nameProgram = tab_programas::where('id', $request->programa)->get();
                tab_estudiante::create([
                    'numero_de_identificacion' => $request->numero_de_identificacion,
                    'programa' => $nameProgram[0]->name,
                    'id_programa' => $request->programa,
                    'semestre' => $request->semestre
                ]);
                break;

            case "Docente":
                tab_docente::create([
                    'numero_de_identificacion' => $request->numero_de_identificacion,
                    'facultad' => $request->facultad
                ]);
                break;

            case "Funcionario":
                tab_funcionario::create([
                    'numero_de_identificacion' => $request->numero_de_identificacion,
                    'departamento' => $request->departamento
                ]);
                break;
        }

        return redirect()->route('view_pacientes')->with('exito', 'Paciente creado Exitosamente!');
    }

    public function list()
    {
        $programas = tab_programas::all();
        return view('clientes.list', compact('programas'));
    }

    public function getPacientesAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("numero_de_identificacion", "nombres", "apellidos", "telefono", "tipo_de_usuario");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "numero_de_identificacion";
        elseif ($sByColumn == 1)
            $bY = "nombres";
        elseif ($sByColumn == 2)
            $bY = "apellidos";
        elseif ($sByColumn == 3)
            $bY = "telefono";
        elseif ($sByColumn == 4 || $sByColumn == 5)
            $bY = "tipo_de_usuario";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '')
                $sWhere .= 'WHERE (';
            else
                $sWhere .= 'AND (';

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $pacientes = DB::select("SELECT numero_de_identificacion, nombres, apellidos, telefono, tipo_de_usuario FROM tab_pacientes " . $sWhere . " " . $sOrder . " LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $pacientes2 = DB::select("SELECT numero_de_identificacion, nombres, apellidos, telefono, tipo_de_usuario FROM tab_pacientes " . $sWhere . " " . $sOrder);

        $filteredPacientes = count($pacientes);
        $totalPacientes = count($pacientes2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredPacientes,
            "recordsFiltered" => $totalPacientes,
            "data" => array(),
        );

        foreach ($pacientes as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>' . $inv->numero_de_identificacion . '</p><b hidden>' . $inv->nombres . ' ' . $inv->apellidos . '</b><a class="btn btn-warning"'
                . 'href="' . route('edit_form_paciente', ['id' => $inv->numero_de_identificacion]) . '">'
                . '<i class="fa fa-edit"></i>'
                . '</a>'
                . '<button class="btn btn-success">'
                . '<i class="fa fa-address-card"></i>'
                . '</button>'
                . '</div>';

            $row = array();

            $row[] = $inv->numero_de_identificacion;
            $row[] = $inv->nombres;
            $row[] = $inv->apellidos;
            $row[] = $inv->telefono;
            $row[] = $inv->tipo_de_usuario;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);

    }

    public function update(Request $request)
    {
        $request->validate([
            'nombres' => 'required',
            'apellidos' => 'required',
            'telefono' => 'required'
        ], [
            'nombres.required' => 'Por favor ingrese su Nombre',
            'apellidos.required' => 'Por favor ingrese su Apellido',
            'telefono.required' => 'Por favor ingrese su Telefono'
        ]);

        tab_paciente::where('numero_de_identificacion', $request->numero_de_identificacion)
            ->update(['nombres' => $request->nombres, 'apellidos' => $request->apellidos, 'telefono' => $request->telefono]);

        switch ($request->tipo_de_usuario) {
            case 'Estudiante':
                $nameProgram = tab_programas::where('id', $request->programa)->get();
                tab_estudiante::where('numero_de_identificacion', $request->numero_de_identificacion)
                    ->update(['programa' => $nameProgram[0]->name,
                        'id_programa' => $request->programa,
                        'semestre' => $request->semestre
                    ]);
                break;

            case 'Docente':
                tab_docente::where('numero_de_identificacion', $request->numero_de_identificacion)
                    ->update(['facultad' => $request->facultad]);
                break;

            case 'Funcionario':
                tab_funcionario::where('numero_de_identificacion', $request->numero_de_identificacion)
                    ->update(['departamento' => $request->departamento]);
                break;
        }

        return redirect()->route('view_pacientes')->with('exito', 'Paciente actualizado Exitosamente!');
    }

    public function obtenerEstadisticasPaciente(Request $request)
    {
        $glucometrias = tab_glucometria::where('numero_de_identificacion', $request->numero_de_identificacion)->get()->count();
        $atenciones_en_salud = tab_atencion_en_salud::where('numero_de_identificacion', $request->numero_de_identificacion)->get()->count();
        $toma_presiones_arteriales = tab_tpresion_arterial::where('numero_de_identificacion', $request->numero_de_identificacion)->get()->count();
        $inyectologias = tab_inyectologia::where('numero_de_identificacion', $request->numero_de_identificacion)->get()->count();
        $entregas_de_medicamento = tab_entrega_medicamento::where('numero_de_identificacion', $request->numero_de_identificacion)->get()->count();

        return response()->json(['glucometrias' => $glucometrias, 'entregas_de_medicamento' => $entregas_de_medicamento, 'atenciones_en_salud' => $atenciones_en_salud, 'toma_presiones_arteriales' => $toma_presiones_arteriales, 'inyectologias' => $inyectologias]);
    }

    public function obtenerEstadisticasPorPrograma(Request $request)
    {
        $nreg_entrega_med = DB::select("SELECT * FROM tab_estudiantes,tab_entrega_medicamentos WHERE tab_estudiantes.numero_de_identificacion = tab_entrega_medicamentos.numero_de_identificacion AND tab_estudiantes.id_programa = '" . $request->programa . "' AND tab_entrega_medicamentos.fecha BETWEEN CAST('" . $request->fecha_inicio . "' AS DATE) AND CAST('" . $request->fecha_fin . "' AS DATE)");

        $nreg_glucometrias = DB::select("SELECT * FROM tab_estudiantes,tab_glucometrias WHERE tab_estudiantes.numero_de_identificacion = tab_glucometrias.numero_de_identificacion AND tab_estudiantes.id_programa = '" . $request->programa . "' AND tab_glucometrias.fecha BETWEEN CAST('" . $request->fecha_inicio . "' AS DATE) AND CAST('" . $request->fecha_fin . "' AS DATE)");

        $nreg_atenciones_en_salud = DB::select("SELECT * FROM tab_estudiantes,tab_atencion_en_saluds WHERE tab_estudiantes.numero_de_identificacion = tab_atencion_en_saluds.numero_de_identificacion AND tab_estudiantes.id_programa = '" . $request->programa . "' AND tab_atencion_en_saluds.fecha BETWEEN CAST('" . $request->fecha_inicio . "' AS DATE) AND CAST('" . $request->fecha_fin . "' AS DATE)");

        $nreg_toma_presion_arterial = DB::select("SELECT * FROM tab_estudiantes,tab_tpresion_arterials WHERE tab_estudiantes.numero_de_identificacion = tab_tpresion_arterials.numero_de_identificacion AND tab_estudiantes.id_programa = '" . $request->programa . "' AND tab_tpresion_arterials.fecha BETWEEN CAST('" . $request->fecha_inicio . "' AS DATE) AND CAST('" . $request->fecha_fin . "' AS DATE)");

        $nreg_inyectologia = DB::select("SELECT * FROM tab_estudiantes,tab_inyectologias WHERE tab_estudiantes.numero_de_identificacion = tab_inyectologias.numero_de_identificacion AND tab_estudiantes.id_programa = '" . $request->programa . "' AND tab_inyectologias.fecha BETWEEN CAST('" . $request->fecha_inicio . "' AS DATE) AND CAST('" . $request->fecha_fin . "' AS DATE)");

        $np_entregas_de_medicamento = count($nreg_entrega_med);
        $np_glucometrias = count($nreg_glucometrias);
        $np_toma_presion_arterial = count($nreg_toma_presion_arterial);
        $np_atenciones_en_salud = count($nreg_atenciones_en_salud);
        $np_inyectologias = count($nreg_inyectologia);

        return response()->json(['np_entregas_de_medicamento' => $np_entregas_de_medicamento, 'np_glucometrias' => $np_glucometrias, 'np_toma_presion_arterial' => $np_toma_presion_arterial, 'np_atenciones_en_salud' => $np_atenciones_en_salud, 'np_inyectologias' => $np_inyectologias]);
    }

}
