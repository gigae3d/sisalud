<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;
use Validate;
use DB;

use App\Models\tab_historia_control_fec;

class TabHistoriaClinicaFec extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm(){
       return view('historia seg control fecundidad.register_historia_clinica');
    }

    public function showFormWithId($id){
    	$numero_de_identificacion = $id;
        return view('historia seg control fecundidad.register_historia_clinica',['numero_de_identificacion'=>$numero_de_identificacion]);
    }

    public function new(Request $request){
        
        $fum  =   date_create($request->fum);
        $fum  =   date_format($fum,'Y-m-d');

        $fup = date_create($request->fecha_ultimo_parto);
        $fup = date_format($fup,'Y-m-d');

        $fecha_citologia = date_create($request->fecha_citologia);
        $fecha_citologia = date_format($fecha_citologia,'Y-m-d');

    	$resultado      =   tab_historia_control_fec::create([
        'paciente_control_fec'  => $request->numero_de_identificacion,
        'menarquia'             => $request->menarquia,
        'ciclos_menstruales'    => $request->ciclos_menstruales,
        'FUM'                   => $fum,
        'paridad_g'             => $request->G,
        'paridad_p'             => $request->P,
        'paridad_a'             => $request->A,
        'edad_inicio_rel_sex'   => $request->edad_in_rel_se,
        'FUP'                   => $fup,
        'numero_comp_sexuales'  => $request->num_com_se,
        'cirugia_ginecologica'  => $request->cirugia_ginecologica,
        'fecha_citologia'       => $fecha_citologia,
        'resultado_citologia'   => $request->resultado_citologia,
        'metodo_deseado'        => $request->metodo_deseado,
        'metodo_adoptado'       => $request->metodo_adoptado
        ]);

        return Redirect::to(route('view_historia_con_fec'));

    }

    public function list(){
      return view('historia seg control fecundidad.list_hist_control');
    }

    public function getHistoriasControlFecAjax(Request $request){
    	ini_set('max_execution_time',0);

        $f_i = $request->fecha_i;
        $f_f = $request->fecha_f;
    
        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $iSortCol_0 = $request->iSortCol_0;
        $iSortingCols = $request->iSortingCols;
        $aColumns = array("id","paciente_control_fec","menarquia","ciclos_menstruales","FUM","cirugia_ginecologica");
        
        $sWhere = '';
        
        //Searching
        $sSearch = $request->search['value'];        
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if($sByColumn == 0){

            $bY="id";

        }elseif($sByColumn == 1){

            $bY="paciente_control_fec";

        }elseif($sByColumn == 2){

            $bY="menarquia";

        }elseif($sByColumn == 3){

            $bY="ciclos_menstruales";

        }elseif($sByColumn == 4){

            $bY="FUM";

        }elseif($sByColumn == 5){

            $bY="cirugia_ginecologica";

        }

        $sOrder = "ORDER BY ".$bY." ".$OrderD;
        
        
        if ($sSearch != null && $sSearch != "")
        {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $historias_control = DB::select("SELECT id,paciente_control_fec,menarquia,ciclos_menstruales,FUM,cirugia_ginecologica FROM tab_historia_control_fecs
                        ". $sWhere . "
                        ". $sOrder . "
                        LIMIT ". $iDisplayLength . " OFFSET " . $iDisplayStart . "");


        $historias_control2 = DB::select("SELECT id,paciente_control_fec,menarquia,ciclos_menstruales,FUM,cirugia_ginecologica FROM tab_historia_control_fecs
                        ". $sWhere . "
                        ". $sOrder);

        $filteredHistorias_control = count($historias_control);
        $totalHistorias_control    = count($historias_control2);

        $output = array(
            "draw"            => $sEcho,
            "recordsTotal"    => $filteredHistorias_control,
            "recordsFiltered" => $totalHistorias_control,
            "data"            => array(),
        );

        foreach ($historias_control as $inv)
        {
            $options    =   '<div class="btn-toolbar"><a class="btn btn-success" href="#">'
                                .'<i class="fa fa-edit"></i>'
                              .'</a>'
                              .'<a class="btn btn-success"'
                              .'href="'.route('new_control_historia', ['numero_de_identificacion'=>$inv->id]).'">'
                                .'<i class="fa fa-plus-square"></i>'
                              .'</a>'
                              .'<a class="btn btn-success" href="'.route('list_controles_hist', ['historia'=>$inv->id]).'">'
                                .'<i class="fa fa-list"></i>'
                              .'</a>'
                              .'</div>';

            $row = array();          

            $row[] = $inv->id;
            $row[] = $inv->paciente_control_fec;
            $row[] = $inv->menarquia;
            $row[] = $inv->ciclos_menstruales;
            $row[] = $inv->FUM;
            $row[] = $inv->cirugia_ginecologica;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);

    }

}
