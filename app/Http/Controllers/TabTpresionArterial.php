<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validate;
use DB;
use Auth;

use App\Models\tab_paciente;
use App\Models\tab_tpresion_arterial;
use App\Models\tab_afinamiento_toma_presion;
use App\Models\User;

class TabTpresionArterial extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('toma de presion arterial.register');
    }

    public function showFormWithId($id)
    {
        $numero_de_identificacion = $id;
        return view('toma de presion arterial.register', ['numero_de_identificacion' => $numero_de_identificacion]);
    }

    public function new(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required',
            'equipo_medico' => 'required',
            'fecha' => 'required',
            'tipo_toma_presion' => 'required',
            'resultado' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'equipo_medico.required' => 'Por favor ingrese un equipo medico',
                'fecha.required' => 'Por favor ingrese una fecha',
                'tipo_toma_presion.required' => 'Por favor seleccione un tipo',
                'resultado.required' => 'Por favor escriba un resultado'
            ]
        );

        $date = date_create($request->input('fecha'));
        $date = date_format($date, 'Y-m-d');
        $usuario = Auth::id();

        $nuevo_registro = tab_tpresion_arterial::create([
            'numero_de_identificacion' => $request->input('numero_de_identificacion'),
            'equipo_medico' => $request->input('equipo_medico'),
            'usuario' => $usuario,
            'fecha' => $date,
            'tipo' => $request->input('tipo_toma_presion'),
            'posicion' => $request->input('resultado.posicion'),
            'resultado' => $request->input('resultado.resultado')
        ]);

        //Registrar afinamiento para la toma de presión arterial si lo hay
        if($request->input('tipo_toma_presion') == 'Afinamiento'){
        $afinamientos = $request->input('resultado');

        if($afinamientos["de_pie"]["MSD"] != "" && $afinamientos["de_pie"]["MSI"] != ""){
            $resultado = tab_afinamiento_toma_presion::create([
                'toma_presion_arterial' => $nuevo_registro->id,
                'postura' => "De pie",
                'MSD' => $afinamientos["de_pie"]["MSD"] . " mmHg",
                'MSI' => $afinamientos["de_pie"]["MSI"] . " mmHg"
            ]);
        }

        if($afinamientos["sentado"]["MSD"] != "" && $afinamientos["sentado"]["MSI"] != ""){
            $resultado = tab_afinamiento_toma_presion::create([
                'toma_presion_arterial' => $nuevo_registro->id,
                'postura' => "Sentado",
                'MSD' => $afinamientos["sentado"]["MSD"] . " mmHg",
                'MSI' => $afinamientos["sentado"]["MSI"] . " mmHg"
            ]);
        }

        if($afinamientos["acostado"]["MSD"] != "" && $afinamientos["acostado"]["MSI"] != ""){
            $resultado = tab_afinamiento_toma_presion::create([
                'toma_presion_arterial' => $nuevo_registro->id,
                'postura' => "Acostado",
                'MSD' => $afinamientos["acostado"]["MSD"] . " mmHg",
                'MSI' => $afinamientos["acostado"]["MSI"] . " mmHg"
            ]);
        }

        /*$N = count($afinamientos);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_afinamiento_toma_presion::create([
                    'toma_presion_arterial' => $resultado->id,
                    'postura' => $request->afinamientos[$i],
                    'MSD' => $request->msd_tp[$i] . " mmHg",
                    'MSI' => $request->msi_tp[$i] . " mmHg"
                ]);
            }
        }*/
        }

        //return redirect()->route('view_tpresion_arteriales')->with('exito', 'El registro ha sido creado exitosamente');
        return response()->json(["respuesta"=> "Registro exitoso"]);
    }

    /*public function new(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required',
            'equipo_medico_as' => 'required',
            'fecha' => 'required',
            'tipo_toma_presion' => 'required',
            'resultado' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'equipo_medico_as.required' => 'Por favor ingrese un equipo medico',
                'fecha.required' => 'Por favor ingrese una fecha',
                'tipo_toma_presion.required' => 'Por favor seleccione un tipo',
                'resultado.required' => 'Por favor escriba un resultado'
            ]
        );

        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');
        $usuario = Auth::id();

        $resultado = tab_tpresion_arterial::create([
            'numero_de_identificacion' => $request->numero_de_identificacion,
            'equipo_medico' => $request->equipo_medico_as,
            'usuario' => $usuario,
            'fecha' => $date,
            'tipo' => $request->tipo_toma_presion,
            'posicion' => $request->posicion_tp,
            'resultado' => $request->resultado
        ]);

        //Registrar afinamiento para la toma de presión arterial si lo hay
        if(isset($request->afinamientos) && ($request->tipo_toma_presion == 'Afinamiento')){
        $afinamientos = $request->afinamientos;

        $N = count($afinamientos);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_afinamiento_toma_presion::create([
                    'toma_presion_arterial' => $resultado->id,
                    'postura' => $request->afinamientos[$i],
                    'MSD' => $request->msd_tp[$i] . " mmHg",
                    'MSI' => $request->msi_tp[$i] . " mmHg"
                ]);
            }
        }
        }

        return redirect()->route('view_tpresion_arteriales')->with('exito', 'El registro ha sido creado exitosamente');
    }*/

    public function list()
    {
        return view('toma de presion arterial.list');
    }

    public function obtenerTPresionArterial(Request $request) {
        $tpresion_arteriales = tab_tpresion_arterial::where('numero_de_identificacion',$request->input('paciente'))->paginate(5);
        $ntpresion_arteriales = tab_tpresion_arterial::where('numero_de_identificacion',$request->input('paciente'))->count();
        return response()->json(["tpresion_arteriales" => $tpresion_arteriales, "ntpresion_arteriales" => $ntpresion_arteriales]);
    }


    public function getTomaDePresionArterialAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("tab_tpresion_arterials.id", "tab_tpresion_arterials.numero_de_identificacion", "tab_equipos_medicos.nombre", "tab_tpresion_arterials.usuario", "tab_tpresion_arterials.fecha", "tab_tpresion_arterials.tipo", "tab_tpresion_arterials.posicion", "tab_tpresion_arterials.resultado");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "tab_tpresion_arterials.id";
        elseif ($sByColumn == 1)
            $bY = "tab_tpresion_arterials.numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "tab_equipos_medicos.nombre";
        elseif ($sByColumn == 3)
            $bY = "tab_tpresion_arterials.usuario";
        elseif ($sByColumn == 4)
            $bY = "tab_tpresion_arterials.fecha";
        elseif ($sByColumn == 5)
            $bY = "tab_tpresion_arterials.tipo";
        elseif ($sByColumn == 6)
            $bY = "tab_tpresion_arterials.posicion";
        elseif ($sByColumn == 7)
            $bY = "tab_tpresion_arterials.resultado";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $tpresionArterial = DB::select("SELECT
                            tab_tpresion_arterials.id,
                            tab_tpresion_arterials.numero_de_identificacion,
                            tab_equipos_medicos.nombre AS equipo_medico,
                            tab_tpresion_arterials.usuario,
                            tab_tpresion_arterials.fecha,
                            tab_tpresion_arterials.tipo,
                            tab_tpresion_arterials.posicion,
                            tab_tpresion_arterials.resultado
                            FROM
                            tab_tpresion_arterials
                            INNER JOIN tab_equipos_medicos ON tab_equipos_medicos.id = tab_tpresion_arterials.equipo_medico 
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);


        $tpresionArterial2 = DB::select("SELECT
                            tab_tpresion_arterials.id,
                            tab_tpresion_arterials.numero_de_identificacion,
                            tab_equipos_medicos.nombre AS equipo_medico,
                            tab_tpresion_arterials.usuario,
                            tab_tpresion_arterials.fecha,
                            tab_tpresion_arterials.tipo,
                            tab_tpresion_arterials.posicion,
                            tab_tpresion_arterials.resultado
                            FROM
                            tab_tpresion_arterials
                            INNER JOIN tab_equipos_medicos ON tab_equipos_medicos.id = tab_tpresion_arterials.equipo_medico
                        " . $sWhere . "
                        " . $sOrder);

        $filteredtpresionArterial = count($tpresionArterial);
        $totaltpresionArterial = count($tpresionArterial2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredtpresionArterial,
            "recordsFiltered" => $totaltpresionArterial,
            "data" => array(),
        );

        foreach ($tpresionArterial as $inv) {
            $row = array();

            //Obtener nombres y apellidos del paciente
            $paciente = tab_paciente::where('numero_de_identificacion', $inv->numero_de_identificacion)->first();

            $usuario = User::where('id', Auth::id())->first();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion . ': ' . $paciente->nombres . ' ' . $paciente->apellidos;
            $row[] = $inv->equipo_medico;
            $row[] = $inv->usuario . ': ' . $usuario->name;
            $row[] = $inv->fecha;
            $row[] = $inv->tipo;
            $row[] = $inv->posicion;
            $row[] = $inv->resultado;

            $output['data'][] = $row;
        }

        return response()->json($output);

    }

    public function listarAfinamientosPaciente($id)
    {
        $tpresion_arterials = tab_tpresion_arterial::where('numero_de_identificacion', $id)->where('tipo', 'Afinamiento')->paginate(20);

        $tpresion_arterials2 = $tpresion_arterials;

        $options = array();

        foreach ($tpresion_arterials2 as $tpre) {
            array_push($options, '<div class="btn-toolbar"><p hidden>' . $tpre->id . '</p><button class="btn btn-success" type="button"><i class="fa fa-address-card"></i></button></div>');
        }

        return view('toma de presion arterial.list_afinamientos', compact('tpresion_arterials', 'options'));
    }

    public function obtenerInfoAdicionalAfinamiento(Request $request)
    {
        //Obtener los afinamientos registrados para el registro en particular seleccionado
        //$afinamientos = tab_tpresion_arterial::find($request->id)->afinamientos;
        $afinamientos = tab_tpresion_arterial::find($request->input('id'))->afinamientos;

        return response()->json(['afinamientos' => $afinamientos]);
    }
}
