<?php

namespace App\Http\Controllers;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;
use Redirect;
use validate;
use DB;

class SistemaController extends Controller
{
    public function showLoginForm()
    {
        if (Auth::check())
            return Redirect::to(route('Home'));
        else
            return view('auth.login');
    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6',
        ]);

        $email = $request->email;
        $password = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            User::where('id', Auth::user()->id)->update(['status' => 'Online']);
            if (Auth::user()->estado_usuario == 'Desactivado') {
                Auth::logout();
                return Redirect::to(route('login'));
            } else {
                return Redirect::to(route('Home'));
            }
        } else {
            return Redirect::to(route('login'))->with('error_log', 'Credenciales incorrectas!');
        }
    }

    public function logout()
    {
        if (Auth::user() != null) {
            User::where('id', Auth::user()->id)->update(['status' => 'Offline']);
            Auth::logout();
        }

        return Redirect::to(route('login'));
    }

    public function showCreateUserForm()
    {
        return view('auth.register_user');
    }
    public function showRegistrationForm()
    {
        if (Auth::check())
        {
            return Redirect::to(route('Home'));
        }
        else
        {
            return view('auth.register', compact('catsexo'));
        }
    }

    public function register(Request $request)
    {

        $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|min:6|confirmed',
            'name'     => 'required',
        ]);

        //creacion de usuario
        $usuario                = new User;
        $usuario->name          = $request->name;
        $usuario->email         = $request->email;
        $usuario->password      = bcrypt($request->password);
        $usuario->role          = 'Estudiante';
        $usuario->num_records   = 0;
        $usuario->save();

        // Asigno Rol Visitante
        /*$usuario->roles()->attach('2');
        $informacion = $request->email;

        Mail::send('auth.emails.bienvenido', ['user' => $request->name, 'contrasena' => $request->password, 'usuario' => $request->email, 'url' => route('login')], function ($m) use ($informacion)
        {

            $m->to($informacion)->subject('Bienvenido a Bubo Solutions !');

        });*/

        return Redirect::to(route('login'))->with('mensaje', 'Se ha enviado un Email con la información para el ingreso al sistema!');
    }

    public function newUser(Request $request)
    {
        $request->validate([
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => 'required|min:6|confirmed',
            'name' => 'required',
            'rol' => 'required'
        ],
            [
                'name.required' => 'Por favor ingrese su Nombre Completo',
                'email.required' => 'Por favor ingrese un Email',
                'email.email' => 'Por favor el Email debe tener un formato adecuado',
                'email.unique' => 'Este Email, ya esta registrado intente con otro',
                'rol.required' => 'Por favor ingrese un Rol',
                'password.required' => 'Por favor ingrese una Contraseña',
                'password.min' => 'La Contraseña debe tener minimo seis caracteres'
            ]
        );

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password),
            'role' => $request->rol,
            'num_records' => 0
        ]);

        return redirect()->route('view_usuarios')->with('exito', 'Usuario creado Exitosamente!');
    }

    public function borrarUsuario($id)
    {
        User::where('id', $id)->delete();
        return redirect()->back();
    }

    public function listUsers()
    {
        return view('auth.list_users');
    }

    public function listUsersAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("name", "email", "role");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];

        if ($sByColumn == 0)
            $bY = "name";
        else if ($sByColumn == 1)
            $bY = "email";
        else if ($sByColumn == 2 or $sByColumn == 3)
            $bY = "role";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '')
                $sWhere .= 'WHERE (';
            else
                $sWhere .= 'AND (';

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $usuarios = DB::select("SELECT id, name, email, role FROM users " . $sWhere . " " . $sOrder . " LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $usuarios2 = DB::select("SELECT id, name, email, role FROM users " . $sWhere . " " . $sOrder);

        $filteredUsuarios = count($usuarios);
        $totalUsuarios = count($usuarios2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredUsuarios,
            "recordsFiltered" => $totalUsuarios,
            "data" => array(),
        );

        foreach ($usuarios as $inv) {
            if (($inv->role) != 'Admin') {
                $options = '<div class="btn-toolbar"><a class="btn btn-warning"'
                    . 'href="' . route('borrar_usuario', ['id' => $inv->id]) . '" title="Eliminar Usuario">'
                    . '<i class="fa fa-trash" aria-hidden="true"></i>'
                    . '</a>'
                    . '</div>';
            } else {
                $options = '<a class="btn btn-default"'
                    . 'href="javascript:void(0);">'
                    . '<i class="fa fa-lock"></i>'
                    . '</a>';
            }

            $row = array();

            $row[] = $inv->name;
            $row[] = $inv->email;
            $row[] = $inv->role;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);
    }
}
