<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validate;
use DB;

use App\Models\tab_medicamento;

class TabMedicamentos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('medicamentos.form');
    }

    public function showEditForm(Request $request, $id)
    {
        $medicamentos = tab_medicamento::where('id', $id)->first();

        return view('medicamentos.edit', compact('medicamentos'));

    }

    public function new(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'laboratorio' => 'required',
            'fecha_vencimiento' => 'required',
            'existencia' => 'required|numeric',
            'presentacion' => 'required',
            'lote' => 'required',
            'invima' => 'required',
        ],
            [
                'nombre.required' => 'Por favor ingrese un nombre.',
                'laboratorio.required' => 'Por favor ingrese un laboratorio.',
                'fecha_vencimiento.required' => 'Por favor ingrese una fecha de vencimiento.',
                'existencia.required' => 'Por favor ingrese una existencia.',
                'presentacion.required' => 'Por favor ingrese una presentación.',
                'lote.required' => 'Por favor ingrese un lote',
                'invima.required' => 'Por favor ingrese un valor INVIMA',
                'nombre.unique' => 'Este Medicamento, ya esta registrado intenta con otro'
            ]
        );

        $date = date_create($request->fecha_vencimiento);
        $date = date_format($date, 'Y-m-d');


        tab_medicamento::create([
            'nombre' => $request->nombre,
            'laboratorio' => $request->laboratorio,
            'fecha_vencimiento' => $date,
            'existencia' => $request->existencia,
            'presentacion' => $request->presentacion,
            'lote' => $request->lote,
            'invima' => $request->invima,
            'observaciones' => $request->observaciones,
        ]);

        return redirect()->route('view_medicamentos')->with('exito', 'El medicamento ha sido agregado exitosamente');
    }

    public function list()
    {
        $medicamentos = tab_medicamento::all();
        //return view('medicamentos.list', compact('medicamentos'));
        return view('medicamentos.list');
    }

    public function getMedicamentosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "laboratorio", "fecha_vencimiento", "existencia", "presentacion", "lote", "invima");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];

        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 8)
            $bY = "nombre";
        elseif ($sByColumn == 2)
            $bY = "laboratorio";
        elseif ($sByColumn == 3)
            $bY = "fecha_vencimiento";
        elseif ($sByColumn == 4)
            $bY = "existencia";
        elseif ($sByColumn == 5)
            $bY = "presentacion";
        elseif ($sByColumn == 6)
            $bY = "lote";
        elseif ($sByColumn == 7)
            $bY = "invima";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '')
                $sWhere .= 'WHERE (';
            else
                $sWhere .= 'AND (';

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inventario = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medicamentos " . $sWhere . " " . $sOrder . " LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $inventario2 = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medicamentos " . $sWhere . " " . $sOrder);

        $filteredInventario = count($inventario);
        $totalInventario = count($inventario2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInventario,
            "recordsFiltered" => $totalInventario,
            "data" => array(),
        );

        foreach ($inventario as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>' . $inv->id . '</p><b hidden>' . $inv->nombre . '</b>'
                . '<a class="btn btn-warning" href="' . route('edit_form_medicamento', ['id' => $inv->id]) . '">'
                . '<i class="fa fa-edit"></i></a>'
                . '<button class="btn btn-success" type="button"><i class="fa fa-address-card"></i></button></div>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->laboratorio;
            if ($inv->fecha_vencimiento < date('Y-m-d', strtotime("+3 months"))) {
                $row[] = '<p style="color:red">' . $inv->fecha_vencimiento . '</p>';
            } elseif ($inv->fecha_vencimiento < date('Y-m-d', strtotime("+6 months")) && $inv->fecha_vencimiento >= date('Y-m-d', strtotime("+3 months"))) {
                $row[] = '<p style="color:orange">' . $inv->fecha_vencimiento . '</p>';
            } elseif ($inv->fecha_vencimiento < date('Y-m-d', strtotime("+12 months")) && $inv->fecha_vencimiento >= date('Y-m-d', strtotime("+6 months"))) {
                $row[] = '<p style="color:#c0c009">' . $inv->fecha_vencimiento . '</p>';
            } elseif ($inv->fecha_vencimiento >= date('Y-m-d', strtotime("+12 months"))) {
                $row[] = '<p style="color:green">' . $inv->fecha_vencimiento . '</p>';
            }

            $row[] = $inv->existencia;
            $row[] = $inv->presentacion;
            $row[] = $inv->lote;
            $row[] = $inv->invima;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function registrarMedicamento(Request $request) {
        //$medicamento = $request->all();
        $date = date_create($request->input('medicamento.fecha_vencimiento'));
        $date = date_format($date, 'Y-m-d');
        //dd($request->input('medicamento.fecha_vencimiento'));

        tab_medicamento::create([
            'nombre' => $request->input('medicamento.nombre'),
            'laboratorio' => $request->input('medicamento.laboratorio'),
            'fecha_vencimiento' => $date,
            'existencia' => $request->input('medicamento.existencia'),
            'presentacion' => $request->input('medicamento.presentacion'),
            'lote' => $request->input('medicamento.lote'),
            'invima' => $request->input('medicamento.invima'),
            'observaciones' => $request->input('medicamento.observaciones')
        ]);

        return response()->json(["respuesta"=> "Registro exitoso"]);
    }

    public function obtenerMedicamento(Request $request)
    {
        //$medicamento = tab_medicamento::where('id', $request->input('id'))->first();
        $medicamento = DB::table('tab_medicamentos')->select('id','nombre','laboratorio','fecha_vencimiento','existencia','presentacion', 'lote', 'invima','observaciones')->where('id', $request->input('id'))->first();
        return response()->json(['medicamento' => $medicamento]);
    }

    public function obtenerMedicamentos(Request $request) {
        $medicamentos = tab_medicamento::where('nombre','like','%'.$request->input('palabra').'%')->orWhere('laboratorio', 'like', '%'.$request->input('palabra').'%')->orderBy('existencia', 'desc')->paginate(5);
        $nmedicamentos = tab_medicamento::where('nombre','like','%'.$request->input('palabra').'%')->orWhere('laboratorio', 'like', '%'.$request->input('palabra').'%')->count();
        return response()->json(["medicamentos" => $medicamentos, "nmedicamentos" => $nmedicamentos]);
    }

    public function editarMedicamento(Request $request){
        $date = date_create($request->input('medicamento.fecha_vencimiento'));
        $date = date_format($date, 'Y-m-d');
        
        tab_medicamento::where('id', $request->input('medicamento.id'))->update([
            'nombre' => $request->input('medicamento.nombre'),
            'laboratorio' => $request->input('medicamento.laboratorio'),
            'fecha_vencimiento' => $date,
            'existencia' => $request->input('medicamento.existencia'),
            'presentacion' => $request->input('medicamento.presentacion'),
            'lote' => $request->input('medicamento.lote'),
            'invima' => $request->input('medicamento.invima'),
            'observaciones' => $request->input('medicamento.observaciones')
        ]);

        return response()->json(["respuesta"=> "Actualizacion exitosa"]);

    }

    public function update(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'laboratorio' => 'required',
            'fecha_vencimiento' => 'required',
            'existencia' => 'required|numeric',
            'presentacion' => 'required',
            'lote' => 'required',
            'invima' => 'required',
        ]);

        $date = date_create($request->fecha_vencimiento);
        $date = date_format($date, 'Y-m-d');

        tab_medicamento::where('id', $request->id)
            ->update(['nombre' => $request->nombre, 'laboratorio' => $request->laboratorio, 'fecha_vencimiento' => $date, 'existencia' => $request->existencia, 'presentacion' => $request->presentacion, 'lote' => $request->lote, 'invima' => $request->invima]);

        return redirect()->route('view_medicamentos')->with('exito', 'Medicamento actualizado Exitosamente!');
    }

    public function obtenerInfoAdicionalMed(Request $request)
    {
        $medicamento = tab_medicamento::where('id', $request->id)->first();
        return response()->json(['observaciones' => $medicamento->observaciones]);
    }

    public function obtenerEstadisticaMed(Request $request)
    {
        $nreg_entrega_med = DB::table("tab_entrega_medicamentos")
            ->join("tab_medicamento_entregados", "tab_entrega_medicamentos.id", "=", "tab_medicamento_entregados.reg_entrega_medicamento")
            ->where("tab_medicamento_entregados.medicamento",  "=", $request->medicamento)
            ->whereBetween("tab_entrega_medicamentos.fecha", array($request->fecha_inicio, $request->fecha_fin))
            ->select(DB::raw("Sum(tab_medicamento_entregados.cantidad) AS cantidad"))
            ->get();

        foreach ($nreg_entrega_med as $value){
            $cantidad = $value->cantidad;break;
        }

        return response()->json(['nentregas_med' => count($nreg_entrega_med), 'cantidad' => $cantidad]);
    }
}
