<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\tab_inyectologia;
use App\Models\tab_insumo_inyectologia;
use App\Models\tab_insumos_medico;
use App\Models\User;
use App\Models\tab_paciente;

use Redirect;
use Validate;
use DB;

class TabInyectologia extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        $error = false;
        return view('inyectologias.register', ['error_existencia' => $error]);
    }

    public function showFormWithId($id)
    {
        $numero_de_identificacion = $id;
        return view('inyectologias.register', ['numero_de_identificacion' => $numero_de_identificacion]);
    }

    public function new(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'medicamento_inyectologia' => 'required',
            'fecha' => 'required',
            'orden_medica_iny' => 'required',
            'insumos_medicos' => 'required',
            'cantidad_im_iny' => 'required',
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion.',
                'medicamento_inyectologia.required' => 'Por favor ingrese un medicamento.',
                'fecha.required' => 'Por favor ingrese una fecha.',
                'orden_medica_iny.required' => 'Por favor ingrese la orden medica.',
                'insumos_medicos.required' => 'Por favor ingrese un insumo medico.',
                'cantidad_im_iny.required' => 'Por favor ingrese una cantidad para los insumos médicos'
            ]
        );

        //Registrar inyectologia
        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');

        $resultado = tab_inyectologia::create([
            'numero_de_identificacion' => $request->numero_de_identificacion,
            'medicamento' => $request->medicamento_inyectologia,
            'fecha' => $date,
            'orden_medica' => $request->orden_medica_iny
        ]);

        //Verificar si la cantidad a descontar al insumo medico es menor o igual a la existencia actual en inventario
        $error = false; //Variable para indicar si hay un error al tratar de descontar de la existencia una cantidad a la existente

        //Registrar insumos usados en la inyectologia
        $N = count($request->insumos_medicos);
        //Verificar si la cantidad ingresada del insumo medico usado en el inventario tiene un menor valor que el existente en el inventario
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $insumo_medico = tab_insumos_medico::where('id', $request->insumos_medicos[$i])->first();
                if ($insumo_medico->existencia < $request->cantidad_im_iny[$i]) {
                    $error = true;
                    return back()->with(['error_existencia' => $error])->withInput();
                }
            }

            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_insumo_inyectologia::create([
                    'inyectologia' => $resultado->id,
                    'insumo_medico' => $request->insumos_medicos[$i],
                    'cantidad' => $request->cantidad_im_iny[$i]
                ]);

                //Obtener valor actual existencia del insumo médico
                $insumo_medico = tab_insumos_medico::where('id', $request->insumos_medicos[$i])->first();
                //Calcular el valor nuevo en la existencia
                $existencia_actualizada = $insumo_medico->existencia - $request->cantidad_im_iny[$i];
                //Actualizar la existencia del insumo medico
                tab_insumos_medico::where('id', $request->insumos_medicos[$i])->update(['existencia' => $existencia_actualizada]);
            }
        }

        return redirect()->route('view_inyectologias')->with('exito', 'El registro ha sido agregado exitosamente');
    }

    public function list()
    {
        return view('inyectologias.list');
    }

    public function obtenerInfoAdicionalIny(Request $request)
    {
        $insumos_medicos = tab_insumo_inyectologia::where('id', $request->id)->get();
        //Obtener nombres de insumos médicos
        $nombres = array();
        foreach ($insumos_medicos as $ins) {
            $registro_ins = tab_insumos_medico::where('id', $ins->insumo_medico)->first();
            array_push($nombres, $registro_ins->nombre);
        }
        return response()->json(['insumos_medicos' => $insumos_medicos, 'nombres' => $nombres]);
    }

    public function getInyectologiasAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("tab_inyectologias.id", "tab_inyectologias.numero_de_identificacion", "tab_inyectologias.fecha", "tab_medicamentos.medicamento", "tab_inyectologias.orden_medica");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "tab_inyectologias.id";
        elseif ($sByColumn == 1 || $sByColumn == 5)
            $bY = "tab_inyectologias.numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "tab_inyectologias.fecha";
        elseif ($sByColumn == 3)
            $bY = "tab_medicamentos.medicamento";
        elseif ($sByColumn == 4)
            $bY = "tab_inyectologias.orden_medica";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inyectologia = DB::select("SELECT
                    tab_inyectologias.id,
                    tab_inyectologias.numero_de_identificacion,
                    tab_inyectologias.fecha,
                    tab_medicamentos.nombre AS medicamento,
                    tab_inyectologias.orden_medica
                    
                    FROM
                    tab_inyectologias
                    INNER JOIN tab_medicamentos ON tab_medicamentos.id = tab_inyectologias.medicamento
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart . "");

        $inyectologia2 = DB::select("SELECT
                    tab_inyectologias.id,
                    tab_inyectologias.numero_de_identificacion,
                    tab_inyectologias.fecha,
                    tab_medicamentos.nombre AS medicamento,
                    tab_inyectologias.orden_medica
                    
                    FROM
                    tab_inyectologias
                    INNER JOIN tab_medicamentos ON tab_medicamentos.id = tab_inyectologias.medicamento
                        " . $sWhere . "
                        " . $sOrder);

        $filteredInyectologia = count($inyectologia);
        $totalInyectologia = count($inyectologia2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInyectologia,
            "recordsFiltered" => $totalInyectologia,
            "data" => array(),
        );

        foreach ($inyectologia as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>' . $inv->id . '</p><button class="btn btn-warning" type="button">
                                <i class="fa fa-address-card"></i>
                              </button></div>';

            $row = array();

            //Obtener nombres y apellidos del paciente
            $paciente = tab_paciente::where('numero_de_identificacion', $inv->numero_de_identificacion)->first();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion . ': ' . $paciente->nombres . ' ' . $paciente->apellidos;
            $row[] = $inv->fecha;
            $row[] = $inv->medicamento;
            $row[] = $inv->orden_medica;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

}
