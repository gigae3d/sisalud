<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;
use Validate;
use DB;
use Auth;

use App\Models\tab_paciente;
use App\Models\tab_glucometria;
use App\Models\User;

class TabGlucometria extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('glucometrias.register');
    }

    public function showFormWithId($id)
    {
        $numero_de_identificacion = $id;
        return view('glucometrias.register', ['numero_de_identificacion' => $numero_de_identificacion]);
    }

    public function new(Request $request)
    {

        $request->validate([
            'numero_de_identificacion' => 'required',
            'equipo_medico' => 'required',
            'fecha' => 'required',
            'glucometria' => 'required'
        ],
            ['numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'equipo_medico.required' => 'Por favor ingrese un equipo médico',
                'fecha.required' => 'Por favor especifique una fecha',
                'glucometria.required' => 'Por favor ingrese una glucometria'
            ]
        );

        $date = date_create($request->input('fecha'));
        $date = date_format($date, 'Y-m-d');
        print_r($date);
        $usuario = Auth::id();

        $resultado = tab_glucometria::create([

            'numero_de_identificacion' => $request->input('numero_de_identificacion'),
            'equipo_medico' => $request->input('equipo_medico'),
            'usuario' => $usuario,
            'fecha' => $date,
            'patologia_base' => $request->input('glucometria.patologia'),
            'resultado' => $request->input('glucometria.resultado'),
            'tirillas' => $request->input('glucometria.tirillas'),
            'lancetas' => $request->input('glucometria.lancetas'),
            'guantes' => $request->input('glucometria.guantes')
        ]);

        return response()->json(["respuesta"=> "Registro exitoso"]);
        //return redirect()->route('view_glucometrias')->with('exito', 'El registro ha sido agregado exitosamente');

        //return Redirect::to(route('view_glucometrias'));

    }

    public function obtenerGlucometrias(Request $request) {
        $glucometrias = tab_glucometria::where('numero_de_identificacion',$request->input('paciente'))->paginate(5);
        $nglucometrias = tab_glucometria::where('numero_de_identificacion',$request->input('paciente'))->count();
        return response()->json(["glucometrias" => $glucometrias, "nglucometrias" => $nglucometrias]);
    }

    /*public function new(Request $request)
    {

        $request->validate([
            'numero_de_identificacion' => 'required',
            'equipo_medico' => 'required',
            'fecha' => 'required',
            'patologia_base' => 'required',
            'resultado' => 'required',
            'tirillas_gl' => 'required',
            'lancetas_gl' => 'required',
            'guantes_gl' => 'required'
        ],
            ['numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'equipo_medico.required' => 'Por favor ingrese un equipo médico',
                'fecha.required' => 'Por favor especifique una fecha',
                'patologia_base.required' => 'Por favor ingrese una patología',
                'resultado.required' => 'Por favor ingrese un resultado',
                'tirillas_gl.required' => 'Por favor ingrese el valor de las tirillas',
                'lancetas_gl.required' => 'Por favor ingrese el valor de las lancetas',
                'guantes_gl.required' => 'Por favor ingrese el valor para los guantes'
            ]
        );

        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');
        print_r($date);
        $usuario = Auth::id();

        $resultado = tab_glucometria::create([

            'numero_de_identificacion' => $request->numero_de_identificacion,
            'equipo_medico' => $request->equipo_medico,
            'usuario' => $usuario,
            'fecha' => $date,
            'patologia_base' => $request->patologia_base,
            'resultado' => $request->resultado,
            'tirillas' => $request->tirillas_gl,
            'lancetas' => $request->lancetas_gl,
            'guantes' => $request->guantes_gl
        ]);

        return redirect()->route('view_glucometrias')->with('exito', 'El registro ha sido agregado exitosamente');

        //return Redirect::to(route('view_glucometrias'));

    }*/

    public function list()
    {
        return view('glucometrias.list');
    }

    public function getGlucometriasAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("tab_glucometrias.id", "tab_glucometrias.numero_de_identificacion", "tab_equipos_medicos.nombre", "tab_glucometrias.usuario", "tab_glucometrias.fecha", "tab_glucometrias.patologia_base", "tab_glucometrias.resultado");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "tab_glucometrias.id";
        elseif ($sByColumn == 1)
            $bY = "tab_glucometrias.numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "tab_equipos_medicos.nombre";
        elseif ($sByColumn == 3)
            $bY = "tab_glucometrias.usuario";
        elseif ($sByColumn == 4)
            $bY = "tab_glucometrias.fecha";
        elseif ($sByColumn == 5)
            $bY = "tab_glucometrias.patologia_base";
        elseif ($sByColumn == 6)
            $bY = "tab_glucometrias.resultados";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $glucometrias = DB::select("SELECT
                    tab_glucometrias.id,
                    tab_glucometrias.numero_de_identificacion,
                    tab_equipos_medicos.nombre AS equipo_medico,
                    tab_glucometrias.usuario,
                    tab_glucometrias.fecha,
                    tab_glucometrias.patologia_base,
                    tab_glucometrias.resultado
                    FROM
                    tab_glucometrias
                    INNER JOIN tab_equipos_medicos ON tab_equipos_medicos.id = tab_glucometrias.equipo_medico
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart . "");


        $glucometrias2 = DB::select("SELECT
                    tab_glucometrias.id,
                    tab_glucometrias.numero_de_identificacion,
                    tab_equipos_medicos.nombre AS equipo_medico,
                    tab_glucometrias.usuario,
                    tab_glucometrias.fecha,
                    tab_glucometrias.patologia_base,
                    tab_glucometrias.resultado
                    FROM
                    tab_glucometrias
                    INNER JOIN tab_equipos_medicos ON tab_equipos_medicos.id = tab_glucometrias.equipo_medico
                        " . $sWhere . "
                        " . $sOrder);

        $filteredglucometrias = count($glucometrias);
        $totalglucometrias = count($glucometrias2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredglucometrias,
            "recordsFiltered" => $totalglucometrias,
            "data" => array(),
        );

        foreach ($glucometrias as $inv) {

            $row = array();

            //Obtener nombres y apellidos del paciente
            $paciente = tab_paciente::where('numero_de_identificacion', $inv->numero_de_identificacion)->first();

            $usuario = User::where('id', Auth::id())->first();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion . ': ' . $paciente->nombres . ' ' . $paciente->apellidos;
            $row[] = $inv->equipo_medico;
            $row[] = $inv->usuario . ': ' . $usuario->name;
            $row[] = $inv->fecha;
            $row[] = $inv->patologia_base;
            $row[] = $inv->resultado;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function getEquiposMedicosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "modelo", "tipo", "numero_de_serie");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1)
            $bY = "nombre";
        elseif ($sByColumn == 2 || $sByColumn == 5)
            $bY = "modelo";
        elseif ($sByColumn == 3)
            $bY = "tipo";
        elseif ($sByColumn == 4)
            $bY = "numero_de_serie";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $equipos_medicos = DB::select("SELECT id, nombre, marca, modelo, tipo, existencia, numero_de_serie FROM tab_equipos_medicos
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);


        $equipos_medicos2 = DB::select("SELECT id, nombre, marca, modelo, tipo, existencia, numero_de_serie FROM tab_equipos_medicos
                        " . $sWhere . "
                        " . $sOrder);

        $filteredEquiposMedicos = count($equipos_medicos);
        $totalEquiposMedicos = count($equipos_medicos2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredEquiposMedicos,
            "recordsFiltered" => $totalEquiposMedicos,
            "data" => array(),
        );

        foreach ($equipos_medicos as $inv) {
            $options = '<p hidden>' . $inv->id . '</p><label hidden>' . $inv->nombre . '</label><button id="' . $inv->id . '" class="btn btn-sm btn-success" type="button"><i class="fa fa-check"></i></button>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->modelo;
            $row[] = $inv->tipo;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }
}
