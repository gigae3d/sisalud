<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validate;
use DB;
use Auth;

use App\Models\tab_paciente;
use App\Models\tab_entrega_medicamento;
use App\Models\tab_medicamento;
use App\Models\tab_medicamento_entregado;
use App\Models\User;

class TabEntregaMedicamentos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
        $this->middleware('web');
    }

    public function showForm()
    {
        $error = false;
        return view('entrega de medicamentos.register', ['error_existencia' => $error]);
    }

    public function showFormWithId($id)
    {
        $numero_de_identificacion = $id;
        return view('entrega de medicamentos.register', ['numero_de_identificacion' => $numero_de_identificacion]);
    }

    public function new(Request $request)
    {
        /*$request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'medicamento' => 'required',
            'fecha_de_entrega' => 'required',
            'cantidad_med' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'medicamento.required' => 'Por favor ingrese un medicamento',
                'fecha_de_entrega.required' => 'Por favor ingrese una fecha de entrega',
                'cantidad_med.required' => 'Por favor ingrese una cantidad'
            ]
        );*/

        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'medicamentos' => 'required',
            'fecha_de_entrega' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'medicamentos.required' => 'Por favor ingrese un medicamento',
                'fecha_de_entrega.required' => 'Por favor ingrese una fecha de entrega'
            ]
        );

        $date = date_create($request->input('fecha_de_entrega'));
        $date = date_format($date, 'Y-m-d');
        $usuario = Auth::id();

        //Verificar si la cantidad a descontar es menor o igual a la existencia actual en inventario
        $error = false; //Variable para indicar si hay un error al tratar de descontar de la existencia una cantidad a la existente

        $N = count($request->input('medicamentos'));
        $medicamentos_seleccionados = $request->input('medicamentos');
        //dd($medicamentos_seleccionados);
        //Verificar si la cantidad ingresada del medicamento a entregar en el inventario tiene un menor valor que el existente en el inventario
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $med_act = tab_medicamento::where('id', $medicamentos_seleccionados[$i]["id"])->first();
                if ($med_act->existencia < $medicamentos_seleccionados[$i]["cantidad"]) {
                    $error = true;
                    return response()->json(['respuesta' => "error_existencia"]);
                }
            }

            if (!$error) {
                $entrega_medicamento = tab_entrega_medicamento::create([
                    'numero_de_identificacion' => $request->input('numero_de_identificacion'),
                    'usuario' => $usuario,
                    'fecha' => $date
                ]);
            }
            for ($i = 0; $i < $N; $i++) {
                //Registrar en la tabla de medicamento entregado asociada con la tabla entrega de medicamento
                $tab_medicamento_entregado = tab_medicamento_entregado::create([
                    'reg_entrega_medicamento' => $entrega_medicamento->id,
                    'medicamento' => $medicamentos_seleccionados[$i]["id"],
                    'cantidad' => $medicamentos_seleccionados[$i]["cantidad"]
                ]);

                //Obtener valor actual existencia del medicamento
                $med_act = tab_medicamento::where('id', $medicamentos_seleccionados[$i]["id"])->first();
                //Calcular el valor nuevo en la existencia
                $existencia_actualizada = $med_act->existencia - $medicamentos_seleccionados[$i]["cantidad"];
                //Actualizar la existencia del medicamento
                tab_medicamento::where('id', $medicamentos_seleccionados[$i]["id"])->update(['existencia' => $existencia_actualizada]);
            }

            return response()->json(["respuesta"=> "Registro exitoso"]);
        }
    }

    /*public function new(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'medicamento' => 'required',
            'fecha_de_entrega' => 'required',
            'cantidad_med' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'medicamento.required' => 'Por favor ingrese un medicamento',
                'fecha_de_entrega.required' => 'Por favor ingrese una fecha de entrega',
                'cantidad_med.required' => 'Por favor ingrese una cantidad'
            ]
        );

        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'medicamento' => 'required',
            'fecha_de_entrega' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'medicamento.required' => 'Por favor ingrese un medicamento',
                'fecha_de_entrega.required' => 'Por favor ingrese una fecha de entrega'
            ]
        );

        $date = date_create($request->fecha_de_entrega);
        $date = date_format($date, 'Y-m-d');
        $usuario = Auth::id();

        //Verificar si la cantidad a descontar es menor o igual a la existencia actual en inventario
        $error = false; //Variable para indicar si hay un error al tratar de descontar de la existencia una cantidad a la existente

        $N = count($request->medicamento);
        //Verificar si la cantidad ingresada del medicamento a entregar en el inventario tiene un menor valor que el existente en el inventario
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $med_act = tab_medicamento::where('id', $request->medicamento[$i])->first();
                if ($med_act->existencia < $request->cantidad_med[$i]) {
                    $error = true;
                    return back()->with(['error_existencia' => $error])->withInput();
                }
            }

            if (!$error) {
                $entrega_medicamento = tab_entrega_medicamento::create([
                    'numero_de_identificacion' => $request->numero_de_identificacion,
                    'usuario' => $usuario,
                    'fecha' => $date
                ]);
            }
            for ($i = 0; $i < $N; $i++) {
                //Registrar en la tabla de medicamento entregado asociada con la tabla entrega de medicamento
                $tab_medicamento_entregado = tab_medicamento_entregado::create([
                    'reg_entrega_medicamento' => $entrega_medicamento->id,
                    'medicamento' => $request->medicamento[$i],
                    'cantidad' => $request->cantidad_med[$i]
                ]);

                //Obtener valor actual existencia del medicamento
                $med_act = tab_medicamento::where('id', $request->medicamento[$i])->first();
                //Calcular el valor nuevo en la existencia
                $existencia_actualizada = $med_act->existencia - $request->cantidad_med[$i];
                //Actualizar la existencia del medicamento
                tab_medicamento::where('id', $request->medicamento[$i])->update(['existencia' => $existencia_actualizada]);
            }

            //return redirect()->route('view_entrega_de_medicamentos')->with('exito', 'El registro ha sido creado exitosamente');
            return redirect()->json(["respuesta" => "El registro ha sido creado exitosamente"]);
        }
    }*/

    public function obtenerInfoPaciente(Request $request)
    {
        $paciente = tab_paciente::where('numero_de_identificacion', $request->numero_de_identificacion)->first();

        if(!empty($paciente->nombres) && !empty($paciente->apellidos)){
            return response()->json(['nombres' => $paciente->nombres, 'apellidos' => $paciente->apellidos]);
        } else {
            return response()->json(['nombres' => '-', 'apellidos' => '-']);
        }
    }

    public function obtenerInfoAdicionalEntrega(Request $request)
    {
        $medicamentos = tab_medicamento_entregado::where('reg_entrega_medicamento', $request->input('id'))->get();

        //Obtener nombres de medicamentos
        $nombres = array();
        foreach ($medicamentos as $med) {
            $registro_med = tab_medicamento::where('id', $med->medicamento)->first();
            array_push($nombres, $registro_med->nombre);
        }
        return response()->json(['medicamentos' => $medicamentos, 'nombres' => $nombres]);
    }

    public function list()
    {
        return view('entrega de medicamentos.list');
    }

    //Obtener entregas de medicamento para un paciente
    public function obtenerEntregasMedicamentos(Request $request){
        $entregas_medicamentos = tab_entrega_medicamento::where('numero_de_identificacion',$request->input('paciente'))->paginate(5);
        $nentregas_medicamentos = tab_entrega_medicamento::where('numero_de_identificacion',$request->input('paciente'))->count();
        return response()->json(["entregas_medicamentos" => $entregas_medicamentos, "nentregas_medicamentos" => $nentregas_medicamentos]);
    }

    public function getEntregaDeMedicamentosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "numero_de_identificacion", "usuario", "fecha");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 4)
            $bY = "numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "usuario";
        elseif ($sByColumn == 3)
            $bY = "fecha";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inventario = DB::select("SELECT id, numero_de_identificacion, usuario, fecha FROM tab_entrega_medicamentos
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart . "");


        $inventario2 = DB::select("SELECT id, numero_de_identificacion, usuario, fecha FROM tab_entrega_medicamentos 
                        " . $sWhere . "
                        " . $sOrder);

        $filteredInventario = count($inventario);
        $totalInventario = count($inventario2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInventario,
            "recordsFiltered" => $totalInventario,
            "data" => array(),
        );

        foreach ($inventario as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>' . $inv->id . '</p><button class="btn btn-warning" type="button"><i class="fa fa-address-card"></i></button></div>';

            $row = array();

            //Obtener nombres y apellidos del paciente
            $paciente = tab_paciente::where('numero_de_identificacion', $inv->numero_de_identificacion)->first();

            $usuario = User::where('id', Auth::id())->first();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion . ': ' . $paciente->nombres . ' ' . $paciente->apellidos;
            $row[] = $inv->usuario . ': ' . $usuario->name;
            $row[] = $inv->fecha;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function getMedicamentosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "laboratorio", "fecha_vencimiento", "existencia", "presentacion");

        $sWhere = ' WHERE existencia > 0 ';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];

        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 5)
            $bY = "nombre";
        elseif ($sByColumn == 2)
            $bY = "laboratorio";
        elseif ($sByColumn == 3)
            $bY = "fecha_vencimiento";
        elseif ($sByColumn == 4)
            $bY = "existencia";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            $sWhere .= 'AND (';

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inventario = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medicamentos 
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $inventario2 = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medicamentos 
                        " . $sWhere . "
                        " . $sOrder);

        $filteredInventario = count($inventario);
        $totalInventario = count($inventario2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInventario,
            "recordsFiltered" => $totalInventario,
            "data" => array(),
        );

        foreach ($inventario as $inv) {
            $options = '<p hidden>' . $inv->id . '</p><label hidden>' . $inv->nombre . '</label><button id="' . $inv->id . '" class="btn btn-sm btn-success" type="button"><i class="fa fa-check-square" aria-hidden="true"></i></button>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->laboratorio;
            $row[] = $inv->existencia;
            $row[] = $inv->fecha_vencimiento;
            $row[] = $inv->presentacion;
            $row[] = $inv->lote;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }
}
