<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validate;
use DB;
use Auth;

use App\Models\tab_paciente;
use App\Models\tab_entrega_med_anti;
use App\Models\tab_medic_planific;
use App\Models\tab_medic_entregado_anti;

class TabEntregaMedAntic extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        $error = false;
        return view('entrega de medicamentos antic.register', ['error_existencia' => $error]);

    }

    public function new(Request $request)
    {

        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'medicamento' => 'required',
            'fecha_de_entrega' => 'required',
            'cantidad_med' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor, ingrese un numero de identificacion',
                'medicamento.required' => 'Por favor, ingrese un medicamento',
                'fecha_de_entrega.required' => 'Por favor ingrese una fecha de entrega',
                'cantidad_med.required' => 'Por favor ingrese una cantidad'
            ]
        );

        $date = date_create($request->fecha_de_entrega);
        $date = date_format($date, 'Y-m-d');
        $usuario = Auth::id();

        //Verificar si la cantidad a descontar es menor o igual a la existencia actual en inventario

        $error = false; //Variable para indicar si hay un error al tratar de descontar de la existencia una cantidad a la existente

        $N = count($request->medicamento);
        //Verificar si la cantidad ingresada del medicamento a entregar en el inventario tiene un menor valor que el existente en el inventario
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $med_act = tab_medic_planific::where('id', $request->medicamento[$i])->first();
                if ($med_act->existencia < $request->cantidad_med[$i]) {
                    $error = true;
                    return view('entrega de medicamentos antic.register', ['error_existencia' => $error]);
                }
            }

            if (!$error) {
                $entrega_medicamento = tab_entrega_med_anti::create([
                    'numero_de_identificacion' => $request->numero_de_identificacion,
                    'usuario' => $usuario,
                    'fecha' => $date
                ]);
            }
            for ($i = 0; $i < $N; $i++) {
                //Registrar en la tabla de medicamento entregado asociada con la tabla entrega de medicamento
                $tab_medicamento_entregado = tab_medic_entregado_anti::create([
                    'reg_entrega_medicamento' => $entrega_medicamento->id,
                    'medicamento' => $request->medicamento[$i],
                    'cantidad' => $request->cantidad_med[$i]
                ]);

                //Obtener valor actual existencia del medicamento
                $med_act = tab_medic_planific::where('id', $request->medicamento[$i])->first();
                //Calcular el valor nuevo en la existencia
                $existencia_actualizada = $med_act->existencia - $request->cantidad_med[$i];
                //Actualizar la existencia del medicamento
                tab_medic_planific::where('id', $request->medicamento[$i])->update(['existencia' => $existencia_actualizada]);

            }

            return redirect()->route('view_reg_entrega_medic_antic')->with('exito', 'El medicamento ha sido agregado exitosamente');

            //return Redirect::to(route('view_reg_entrega_medic_antic'));

        }

    }

    public function obtenerInfoAdicionalEntrega(Request $request)
    {
        $medicamentos = tab_medic_entregado_anti::where('reg_entrega_medicamento', $request->id)->get();

        //Obtener nombres de medicamentos
        $nombres = array();
        foreach ($medicamentos as $med) {
            $registro_med = tab_medic_planific::where('id', $med->medicamento)->first();
            array_push($nombres, $registro_med->nombre);
        }
        return response()->json(['medicamentos' => $medicamentos, 'nombres' => $nombres]);
    }

    public function list()
    {
        return view('entrega de medicamentos antic.list');
    }

    public function getEntregaDeMedicamentosAnticAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "numero_de_identificacion", "usuario", "fecha");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 4)
            $bY = "numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "usuario";
        elseif ($sByColumn == 3)
            $bY = "fecha";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inventario = DB::select("SELECT id, numero_de_identificacion, usuario, fecha FROM tab_entrega_med_antis
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $inventario2 = DB::select("SELECT id, numero_de_identificacion, usuario, fecha FROM tab_entrega_med_antis 
                        " . $sWhere . "
                        " . $sOrder);

        $filteredInventario = count($inventario);
        $totalInventario = count($inventario2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInventario,
            "recordsFiltered" => $totalInventario,
            "data" => array(),
        );

        foreach ($inventario as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>' . $inv->id . '</p><button class="btn btn-warning" type="button"><i class="fa fa-address-card"></i></button>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion;
            $row[] = $inv->usuario;
            $row[] = $inv->fecha;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function getMedicamentosAnticAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "laboratorio", "existencia", "presentacion");

        $sWhere = ' WHERE existencia > 0 ';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1)
            $bY = "nombre";
        elseif ($sByColumn == 2 || $sByColumn == 5)
            $bY = "laboratorio";
        elseif ($sByColumn == 3)
            $bY = "existencia";
        elseif ($sByColumn == 4)
            $bY = "presentacion";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            $sWhere .= 'AND (';

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inventario = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medic_planifics 
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $inventario2 = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medic_planifics
                        " . $sWhere . "
                        " . $sOrder);

        $filteredInventario = count($inventario);
        $totalInventario = count($inventario2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInventario,
            "recordsFiltered" => $totalInventario,
            "data" => array(),
        );

        foreach ($inventario as $inv) {
            $options = '<p hidden>' . $inv->id . '</p><label hidden>' . $inv->nombre . '</label><button id="' . $inv->id . '" class="btn btn-sm btn-success" type="button"><i class="fa fa-check-square" aria-hidden="true"></i></button>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->laboratorio;
            $row[] = $inv->existencia;
            $row[] = $inv->presentacion;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }
}
