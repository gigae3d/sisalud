<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\tab_atencion_en_salud;
use App\Models\tab_insumo_atencion_salud;
use App\Models\tab_insumos_medico;
use App\Models\User;
use App\Models\tab_paciente;

use Redirect;
use Validate;
use DB;
use Auth;

class TabAtencionEnSalud extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        $error = false;

        return view('atencion en salud.atencion_en_salud', ['error_existencia' => $error]);
    }

    public function showFormWithId($id)
    {
        $numero_de_identificacion = $id;

        return view('atencion en salud.atencion_en_salud', ['numero_de_identificacion' => $numero_de_identificacion]);
    }

    public function new(Request $request)
    {

        $request->validate(
            [
                'numero_de_identificacion' => 'required',
                'equipo_medico_as' => 'required',
                'fecha' => 'required',
                'motivo_consulta' => 'required',
                'otros_insumos_as' => 'required',
            ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion.',
                'fecha.required' => 'Por favor ingrese una fecha.',
                'motivo_consulta.required' => 'Por favor ingrese un motivo de consulta',
                'equipo_medico_as.required' => 'Por favor ingrese un equipo medico.',
                'otros_insumos_as.required' => 'Por favor especifique si se usaron o no otros insumos.',
            ]
        );


        //Registrar atención en salud
        $usuario = Auth::id();
        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');

        //Verificar si la cantidad a descontar al insumo medico es menor o igual a la existencia actual en inventario
        $error = false; //Variable para indicar si hay un error al tratar de descontar de la existencia una cantidad a la existente

        //Registrar insumos usados para la atención en salud

        if (isset($request->insumos_usados)) {

            $N = count($request->insumos_usados);
            //Verificar si la cantidad ingresada del insumo medico usado en el inventario tiene un menor valor que el existente en el inventario
            if ($N > 0) {
                for ($i = 0; $i < $N; $i++) {
                    $insumo_medico = tab_insumos_medico::where('id', $request->insumos_usados[$i])->first();
                    if ($insumo_medico->existencia < $request->cantidad_im_as[$i]) {
                        $error = true;

                        return back()->with(['error_existencia' => $error])->withInput();
                    }
                }

                if (!$error) {

                    $resultado = tab_atencion_en_salud::create(
                        [
                            'numero_de_identificacion' => $request->numero_de_identificacion,
                            'usuario' => $usuario,
                            'equipo_medico' => $request->equipo_medico_as,
                            'fecha' => $date,
                            'motivo_de_consulta' => $request->motivo_consulta,
                            'otros_insumos_usados_as' => ($request->otros_insumos_as == null) ? '' : $request->otros_insumos_as,
                        ]
                    );

                    for ($i = 0; $i < $N; $i++) {
                        $resultado = tab_insumo_atencion_salud::create(
                            [
                                'atencion_en_salud' => $resultado->id,
                                'insumo_medico' => $request->insumos_usados[$i],
                                'cantidad' => $request->cantidad_im_as[$i],
                            ]
                        );

                        //Obtener valor actual existencia del insumo médico
                        $insumo_medico = tab_insumos_medico::where('id', $request->insumos_usados[$i])->first();
                        //Calcular el valor nuevo en la existencia
                        $existencia_actualizada = $insumo_medico->existencia - $request->cantidad_im_as[$i];
                        //Actualizar la existencia del insumo medico
                        tab_insumos_medico::where('id', $request->insumos_usados[$i])->update(
                            ['existencia' => $existencia_actualizada]
                        );
                    }

                }

            }
        }

        return redirect()->route('view_atenciones_en_salud')->with(
            'exito',
            'El registro ha sido agregado exitosamente'
        );
        //return Redirect::to(route('view_atenciones_en_salud'));

    }

    public function list()
    {
        return view('atencion en salud.list');
    }

    public function obtenerInfoAdicionalAtEnSalud(Request $request)
    {
        $dataConsulta = tab_atencion_en_salud::where('id', $request->id)->get();
        $insumos_medicos = tab_insumo_atencion_salud::where('id', $request->id)->get();

        //Obtener nombres de insumos médicos
        $nombres = array();
        foreach ($insumos_medicos as $ins) {
            $registro_ins = tab_insumos_medico::where('id', $ins->insumo_medico)->first();
            array_push($nombres, $registro_ins->nombre);
        }

        return response()->json(
            ['insumos_medicos' => $insumos_medicos, 'nombres' => $nombres, 'data' => $dataConsulta]
        );
    }

    public function getAtencionesEnSaludAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array(
            "tab_atencion_en_saluds.id",
            "tab_atencion_en_saluds.numero_de_identificacion",
            "tab_atencion_en_saluds.usuario",
            "tab_equipos_medicos.nombre",
            "tab_atencion_en_saluds.fecha",
        );

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0) {
            $bY = "tab_atencion_en_saluds.id";
        } elseif ($sByColumn == 1 || $sByColumn == 5) {
            $bY = "tab_atencion_en_saluds.numero_de_identificacion";
        } elseif ($sByColumn == 2) {
            $bY = "tab_atencion_en_saluds.usuario";
        } elseif ($sByColumn == 3) {
            $bY = "tab_equipos_medicos.nombre";
        } elseif ($sByColumn == 4) {
            $bY = "tab_atencion_en_saluds.fecha";
        }

        $sOrder = "ORDER BY ".$bY." ".$OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i].' LIKE "%'.$sSearch.'%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $atencion_salud = DB::select(
            "SELECT
                            tab_atencion_en_saluds.id,
                            tab_atencion_en_saluds.numero_de_identificacion,
                            tab_atencion_en_saluds.usuario,
                            tab_atencion_en_saluds.fecha,
                            tab_atencion_en_saluds.motivo_de_consulta,
                            tab_atencion_en_saluds.otros_insumos_usados_as,
                            tab_equipos_medicos.nombre AS equipo_medico
                            FROM
                            tab_atencion_en_saluds
                            INNER JOIN tab_equipos_medicos ON tab_equipos_medicos.id = tab_atencion_en_saluds.equipo_medico
                        ".$sWhere."
                        ".$sOrder."
                        LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart
        );


        $atencion_salud2 = DB::select(
            "SELECT
                            tab_atencion_en_saluds.id,
                            tab_atencion_en_saluds.numero_de_identificacion,
                            tab_atencion_en_saluds.usuario,
                            tab_atencion_en_saluds.fecha,
                            tab_atencion_en_saluds.motivo_de_consulta,
                            tab_atencion_en_saluds.otros_insumos_usados_as,
                            tab_equipos_medicos.nombre AS equipo_medico
                            FROM
                            tab_atencion_en_saluds
                            INNER JOIN tab_equipos_medicos ON tab_equipos_medicos.id = tab_atencion_en_saluds.equipo_medico
                        ".$sWhere."
                        ".$sOrder
        );

        $filteredAtencionSalud = count($atencion_salud);
        $totalAtencionSalud = count($atencion_salud2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredAtencionSalud,
            "recordsFiltered" => $totalAtencionSalud,
            "data" => array(),
        );

        foreach ($atencion_salud as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>'.$inv->id.'</p><button class="btn btn-warning" type="button">
                                <i class="fa fa-address-card"></i> 
                              </button></div>';

            $row = array();

            //Obtener nombres y apellidos del paciente
            $paciente = tab_paciente::where('numero_de_identificacion', $inv->numero_de_identificacion)->first();
            $usuario = User::where('id', Auth::id())->first();


            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion.': '.$paciente->nombres.' '.$paciente->apellidos;
            $row[] = $inv->usuario.': '.$usuario->name;
            $row[] = $inv->equipo_medico;
            $row[] = $inv->fecha;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function getEquiposMedicosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "modelo", "tipo");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];

        if ($sByColumn == 0) {
            $bY = "id";
        } elseif ($sByColumn == 1 || $sByColumn == 4) {
            $bY = "nombre";
        } elseif ($sByColumn == 2) {
            $bY = "modelo";
        } elseif ($sByColumn == 3) {
            $bY = "tipo";
        }

        $sOrder = "ORDER BY ".$bY." ".$OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i].' LIKE "%'.$sSearch.'%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $equipos_medicos = DB::select(
            "SELECT id, nombre, marca, modelo, tipo, existencia, numero_de_serie FROM tab_equipos_medicos
                        ".$sWhere."
                        ".$sOrder."
                        LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart
        );


        $equipos_medicos2 = DB::select(
            "SELECT id, nombre, marca, modelo, tipo, existencia, numero_de_serie FROM tab_equipos_medicos
                        ".$sWhere."
                        ".$sOrder
        );

        $filteredEquiposMedicos = count($equipos_medicos);
        $totalEquiposMedicos = count($equipos_medicos2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredEquiposMedicos,
            "recordsFiltered" => $totalEquiposMedicos,
            "data" => array(),
        );

        foreach ($equipos_medicos as $inv) {
            $options = '<p hidden>'.$inv->id.'</p><label hidden>'.$inv->nombre.'</label><button class="btn btn-sm btn-success" type="button"><i class="fa fa-check"></i></button>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->modelo;
            $row[] = $inv->tipo;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function getInsumosMedicosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "marca", "existencia", "fecha_de_vencimiento");

        $sWhere = ' WHERE existencia > 0 ';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0) {
            $bY = "id";
        } elseif ($sByColumn == 1 || $sByColumn == 5) {
            $bY = "nombre";
        } elseif ($sByColumn == 2) {
            $bY = "marca";
        } elseif ($sByColumn == 3) {
            $bY = "existencia";
        } elseif ($sByColumn == 4) {
            $bY = "fecha_de_vencimiento";
        }

        $sOrder = "ORDER BY ".$bY." ".$OrderD;


        if ($sSearch != null && $sSearch != "") {
            $sWhere .= 'AND (';

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i].' LIKE "%'.$sSearch.'%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $insumos_medicos = DB::select(
            "SELECT id, nombre, marca, existencia, lote, fecha_de_vencimiento, Registro_INVIMA FROM tab_insumos_medicos
                        ".$sWhere."
                        ".$sOrder."
                        LIMIT ".$iDisplayLength." OFFSET ".$iDisplayStart
        );


        $insumos_medicos2 = DB::select(
            "SELECT id, nombre, marca, existencia, lote, fecha_de_vencimiento, Registro_INVIMA FROM tab_insumos_medicos
                        ".$sWhere."
                        ".$sOrder
        );

        $filteredInsumosMedicos = count($insumos_medicos);
        $totalInsumosMedicos = count($insumos_medicos2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInsumosMedicos,
            "recordsFiltered" => $totalInsumosMedicos,
            "data" => array(),
        );

        foreach ($insumos_medicos as $inv) {
            $options = '<p hidden>'.$inv->id.'</p><label hidden>'.$inv->nombre.'</label><button id="'.$inv->id.'"  class="btn btn-sm btn-success" type="button"><i class="fa fa-check"></i></button>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->marca;
            $row[] = $inv->existencia;
            $row[] = $inv->fecha_de_vencimiento;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }
}
