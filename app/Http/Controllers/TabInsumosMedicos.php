<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;
use Validate;
use DB;

use App\Models\tab_insumos_medico;

class TabInsumosMedicos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('insumos medicos.register');
    }

    public function showEditForm(Request $request, $id)
    {
        $insumo_medico = tab_insumos_medico::where('id', $id)->first();
        return view('insumos medicos.edit', compact('insumo_medico'));
    }

    public function new(Request $request)
    {

        $request->validate([
            'nombre' => 'required',
            'marca' => 'required',
            'existencia' => 'required|numeric',
            'lote' => 'required',
            'fecha_vencimiento' => 'required',
            'registro_invima' => 'required',
        ],
            [
                'nombre.required' => 'Por favor ingrese un nombre.',
                'marca.required' => 'Por favor ingrese una marca.',
                'existencia.required' => 'Por favor ingrese una existencia',
                'lote.required' => 'Por favor ingrese un lote',
                'fecha_vencimiento.required' => 'Por favor ingrese una fecha de vencimiento',
                'registro_invima.required' => 'Por favor ingrese un registro INVIMA'
            ]
        );

        $date = date_create($request->fecha_vencimiento);
        $date = date_format($date, 'Y-m-d');


        $resultado = tab_insumos_medico::create([
            'nombre' => $request->nombre,
            'marca' => $request->marca,
            'existencia' => $request->existencia,
            'lote' => $request->lote,
            'fecha_de_vencimiento' => $date,
            'Registro_INVIMA' => $request->registro_invima
        ]);

        return redirect()->route('view_insumos_medicos')->with('exito', 'El insumo médico ha sido agregado exitosamente');
    }

    public function list()
    {
        return view('insumos medicos.list');
    }

    public function getInsumosMedicosAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "marca", "existencia", "lote", "fecha_de_vencimiento", "registro_invima");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];

        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 7)
            $bY = "nombre";
        elseif ($sByColumn == 2)
            $bY = "marca";
        elseif ($sByColumn == 3)
            $bY = "existencia";
        elseif ($sByColumn == 4)
            $bY = "lote";
        elseif ($sByColumn == 5)
            $bY = "fecha_de_vencimiento";
        elseif ($sByColumn == 6)
            $bY = "registro_invima";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $insumos_medicos = DB::select("SELECT id, nombre, marca, existencia, lote, fecha_de_vencimiento, Registro_INVIMA FROM tab_insumos_medicos " . $sWhere . " " . $sOrder . " LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $insumos_medicos2 = DB::select("SELECT id, nombre, marca, existencia, lote, fecha_de_vencimiento, Registro_INVIMA FROM tab_insumos_medicos " . $sWhere . " " . $sOrder);

        $filteredInsumosMedicos = count($insumos_medicos);
        $totalInsumosMedicos = count($insumos_medicos2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInsumosMedicos,
            "recordsFiltered" => $totalInsumosMedicos,
            "data" => array(),
        );

        foreach ($insumos_medicos as $inv) {
            $options = '<a class="btn btn-warning" href="' . route('edit_form_insumo_medico', ['id' => $inv->id]) . '"><i class="fa fa-edit"></i> Editar</a>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->marca;
            $row[] = $inv->existencia;
            $row[] = $inv->lote;
            $row[] = $inv->fecha_de_vencimiento;
            $row[] = $inv->Registro_INVIMA;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);
    }

    public function update(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'marca' => 'required',
            'existencia' => 'required',
            'lote' => 'required',
            'fecha_de_vencimiento' => 'required',
            'registro_invima' => 'required'
        ]);

        tab_insumos_medico::where('id', $request->id)
            ->update(['nombre' => $request->nombre, 'marca' => $request->marca, 'existencia' => $request->existencia, 'lote' => $request->lote, 'fecha_de_vencimiento' => $request->fecha_de_vencimiento, 'Registro_INVIMA' => $request->registro_invima]);

        return redirect()->route('view_insumos_medicos')->with('exito', 'Insumo Médico, ha sido actualizado satisfactoriamente!');
    }
}
