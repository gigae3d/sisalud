<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Redirect;
use Validate;
use DB;

use App\Models\tab_medic_planific;
use App\Models\tab_asesoria_sase;
use App\Models\tab_paciente;

class TabMedicamentosAnticonceptivos extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm()
    {
        return view('planificacion familiar.register');
    }

    public function showEditForm(Request $request, $id)
    {
        $medicamentos = tab_medic_planific::where('id', $id)->first();
        return view('planificacion familiar.edit', compact('medicamentos'));
    }

    public function showFormAsesoria()
    {
        return view('planificacion familiar.register_asesoria');
    }

    public function new(Request $request)
    {
        $request->validate([
            'nombre' => ['required', 'unique:tab_medicamentos,nombre'],
            'laboratorio' => 'required',
            'fecha_vencimiento' => 'required',
            'existencia' => 'required|numeric',
            'presentacion' => 'required',
            'lote' => 'required',
            'invima' => 'required',
        ],
            [
                'nombre.required' => 'Por favor ingrese un nombre',
                'laboratorio.required' => 'Por favor ingrese un laboratorio',
                'fecha_vencimiento.required' => 'Por favor ingrese una fecha de vencimiento',
                'existencia.required' => 'Por favor ingrese una existencia',
                'presentacion.required' => 'Por favor ingrese una presentacion',
                'lote.required' => 'Por favor ingrese un lote',
                'invima.required' => 'Por favor ingrese un registro invima',
                'nombre.unique' => 'Este Medicamento, ya esta registrado intenta con otro'
            ]
        );

        $date = date_create($request->fecha_vencimiento);
        $date = date_format($date, 'Y-m-d');

        tab_medic_planific::create([
            'nombre' => $request->nombre,
            'laboratorio' => $request->laboratorio,
            'fecha_vencimiento' => $date,
            'existencia' => $request->existencia,
            'presentacion' => $request->presentacion,
            'lote' => $request->lote,
            'invima' => $request->invima,
            'observaciones' => $request->observaciones,
        ]);

        return redirect()->route('view_medicamentos_antic')->with('exito', 'El medicamento ha sido agregado exitosamente');
    }

    public function update(Request $request)
    {
        $request->validate([
            'nombre' => 'required',
            'laboratorio' => 'required',
            'fecha_vencimiento' => 'required',
            'existencia' => 'required|numeric',
            'presentacion' => 'required',
            'lote' => 'required',
            'invima' => 'required',
        ]);

        $date = date_create($request->fecha_vencimiento);
        $date = date_format($date, 'Y-m-d');

        tab_medic_planific::where('id', $request->id)
            ->update(['nombre' => $request->nombre, 'laboratorio' => $request->laboratorio, 'fecha_vencimiento' => $date, 'existencia' => $request->existencia, 'presentacion' => $request->presentacion, 'lote' => $request->lote, 'invima' => $request->invima]);

        return redirect()->route('view_medicamentos_antic')->with('exito', 'Medicamento actualizado Exitosamente!');
    }

    public function newAsesoria(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required|exists:tab_pacientes,numero_de_identificacion',
            'fecha' => 'required',
            'asesoria' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese un numero de identificacion',
                'fecha.required' => 'Por favor ingrese una fecha',
                'asesoria.required' => 'Por favor describa la asesoría'
            ]
        );

        $date = date_create($request->fecha);
        $date = date_format($date, 'Y-m-d');

        tab_asesoria_sase::create([
            'numero_de_identificacion' => $request->numero_de_identificacion,
            'fecha' => $date,
            'asesoria' => $request->asesoria
        ]);

        return redirect()->route('view_asesorias_se')->with('exito', 'El registro ha sido agregado exitosamente');
    }

    public function list()
    {
        $medicamentos = tab_medic_planific::all();
        return view('planificacion familiar.list', compact('medicamentos'));
    }

    public function obtenerInfoAdicionalMed(Request $request)
    {
        $medicamento = tab_medic_planific::where('id', $request->id)->first();
        return response()->json(['observaciones' => $medicamento->observaciones]);
    }

    public function listAsesorias()
    {
        return view('planificacion familiar.list_asesorias');
    }

    public function getAsesoriasAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "numero_de_identificacion", "fecha", "asesoria");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1)
            $bY = "numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "fecha";
        elseif ($sByColumn == 3)
            $bY = "asesoria";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $asesorias = DB::select("SELECT id, numero_de_identificacion, fecha, asesoria FROM tab_asesoria_sases 
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);


        $asesorias2 = DB::select("SELECT id, numero_de_identificacion, fecha, asesoria FROM tab_asesoria_sases
                        " . $sWhere . "
                        " . $sOrder);

        $filteredAsesorias = count($asesorias);
        $totalAsesorias = count($asesorias2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredAsesorias,
            "recordsFiltered" => $totalAsesorias,
            "data" => array(),
        );

        foreach ($asesorias as $inv) {
            $row = array();

            //Obtener nombres y apellidos del paciente
            $paciente = tab_paciente::where('numero_de_identificacion', $inv->numero_de_identificacion)->first();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion . ': ' . $paciente->nombres . ' ' . $paciente->apellidos;
            $row[] = $inv->fecha;
            $row[] = $inv->asesoria;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function getMedicamentosAntiAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "nombre", "laboratorio", "fecha_vencimiento", "existencia", "presentacion", "lote", "invima");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 8)
            $bY = "nombre";
        elseif ($sByColumn == 2)
            $bY = "laboratorio";
        elseif ($sByColumn == 3)
            $bY = "fecha_vencimiento";
        elseif ($sByColumn == 4)
            $bY = "existencia";
        elseif ($sByColumn == 5)
            $bY = "presentacion";
        elseif ($sByColumn == 6)
            $bY = "lote";
        elseif ($sByColumn == 7)
            $bY = "invima";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $inventario = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medic_planifics 
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $inventario2 = DB::select("SELECT id, nombre, laboratorio, fecha_vencimiento, existencia, presentacion, lote, invima FROM tab_medic_planifics
                        " . $sWhere . "
                        " . $sOrder);

        $filteredInventario = count($inventario);
        $totalInventario = count($inventario2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredInventario,
            "recordsFiltered" => $totalInventario,
            "data" => array(),
        );

        foreach ($inventario as $inv) {
            $options = '<div class="btn-toolbar"><p hidden>' . $inv->id . '</p><b hidden>' . $inv->nombre . '</b>'
                .'<a class="btn btn-warning" href="' . route('edit_form_med_planif', ['id' => $inv->id]) . '">
                    <i class="fa fa-edit"></i></a>
                    <button class="btn btn-success" type="button"><i class="fa fa-address-card"></i></button></div>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->nombre;
            $row[] = $inv->laboratorio;
            $row[] = $inv->fecha_vencimiento;
            $row[] = $inv->existencia;
            $row[] = $inv->presentacion;
            $row[] = $inv->lote;
            $row[] = $inv->invima;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function obtenerEstadisticaMed(Request $request)
    {
        $nreg_entrega_med = DB::table("tab_entrega_med_antis")
            ->join("tab_medic_entregado_antis", "tab_entrega_med_antis.id", "=", "tab_medic_entregado_antis.reg_entrega_medicamento")
            ->where("tab_medic_entregado_antis.medicamento",  "=", $request->medicamento)
            ->whereBetween("tab_entrega_med_antis.fecha", array($request->fecha_inicio, $request->fecha_fin))
            ->select(DB::raw("Sum(tab_medic_entregado_antis.cantidad) AS cantidad"))
            ->get();

        foreach ($nreg_entrega_med as $value){
            $cantidad = $value->cantidad;break;
        }

        return response()->json(['nentregas_med' => count($nreg_entrega_med), 'cantidad' => $cantidad]);
    }
}
