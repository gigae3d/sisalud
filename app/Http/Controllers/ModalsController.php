<?php

namespace App\Http\Controllers;

use App\Models\tab_programas;
use Illuminate\Http\Request;

class ModalsController extends Controller
{
    public function getFiltrosReporteVacunas()
    {
        $programas = tab_programas::all();

        return view('modals.filtros.filtrosVacunas', compact('programas'));
    }
}
