<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\tab_h_cli_ocupacional;
use App\Models\tab_informacion_ocupacional;
use App\Models\tab_realiza_accion;
use App\Models\tab_realiza_actividad;
use App\Models\tab_exp_factores_riesgo;
use App\Models\tab_factor_de_riesgo;
use App\Models\tab_elementos_pr_per;
use App\Models\tab_elem_usados_prt;
use App\Models\tab_ant_acci_trabajo;
use App\Models\tab_enf_profesional;
use App\Models\tab_ant_familiar;
use App\Models\tab_ant_personal;
use App\Models\tab_ant_derma;
use App\Models\tab_ant_osteo;
use App\Models\tab_ant_qui;
use App\Models\tab_ant_trau;
use App\Models\tab_inmunizacion;
use App\Models\tab_ant_gineco;
use App\Models\tab_hab_toxico;
use App\Models\tab_frerg;
use App\Models\tab_mol_ua;
use App\Models\tab_exam_fi;
use App\Models\tab_det_exf;
use App\Models\tab_inspeccion;
use App\Models\tab_palpacion;
use App\Models\tab_movilidad;
use App\Models\tab_inuca;
use App\Models\tab_ihombro;
use App\Models\tab_icmm;
use App\Models\tab_evproceso;
use App\Models\tab_paraclinico;
use App\Models\tab_diagnostico;
use App\Models\tab_revision_psistema;
use App\Models\tab_ingreso_pveso;
use App\Models\tab_concepto_de_aptitud;

use App\Models\tab_trabajador;

use DB;
use Validate;
use PDF;
use Redirect;

class mypdf extends PDF {

    protected $last_page_flag = false;
  
    public function Close() {
      $this->last_page_flag = true;
      parent::Close();
    }
  
    public function Footer() {
        //$this->SetY(-15);
        //$this->SetFont('helvetica', 'I', 8);
        // Page number
        //$this->Cell(0, 10, 'Page '.$this->getAliasNumPage().'/'.$this->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');

      /*if ($this->last_page_flag) {

        // Position at 15 mm from bottom
        

        $txt = 'DECLARO QUE TODOS LOS DATOS SUMINISTRADOS Y CONSIDERADOS EN ESTE DOCUMENTO SON CIERTOS Y NO HE OCULTADO NINGUNA INFORMACIÓN SOBRE LOS ANTECEDENTES LABORALES, MÉDICOS O PATOLOGICOS EN LA ELABORACIÓN DE LA PRESENTE HISTORIA CLINICA OCUPACIONAL. IGUALMENTE AUTORIZO PARA QUE ESTA HISTORIA CLINICA QUEDE EN CUSTODIA DEL MEDICO DE LA EMPRESA Y PUEDA SER REVISADA POR EL DEPARTAMENTO DE RECURSOS HUMANOS CUANDO SE REQUIERA.';


    $this->MultiCell(0, 20, $txt . '\n', 1, 'C', 0, 1, '', '', true, 0, false, true, 30);

    $txt = '
______________________________________
Firma, sello y registro del médico';
    $this->MultiCell(95, 20, $txt, 1, 'C', 0, 0, '', '', true, 0, false, true, 30);

    $txt = '
______________________________________
Firma del trabajador';

    $this->MultiCell(95, 20, $txt, 1, 'C', 0, 1, '', '', true, 0, false, true, 30);

        
    
      } else {
          // Position at 15 mm from bottom
        $this->SetY(-30);

        $txt = 'DECLARO QUE TODOS LOS DATOS SUMINISTRADOS Y CONSIDERADOS EN ESTE DOCUMENTO SON CIERTOS Y NO HE OCULTADO NINGUNA INFORMACIÓN SOBRE LOS ANTECEDENTES LABORALES, MÉDICOS O PATOLOGICOS EN LA ELABORACIÓN DE LA PRESENTE HISTORIA CLINICA OCUPACIONAL. IGUALMENTE AUTORIZO PARA QUE ESTA HISTORIA CLINICA QUEDE EN CUSTODIA DEL MEDICO DE LA EMPRESA Y PUEDA SER REVISADA POR EL DEPARTAMENTO DE RECURSOS HUMANOS CUANDO SE REQUIERA.';


    $this->MultiCell(0, 20, $txt . '\n', 1, 'C', 0, 1, '', '', true, 0, false, true, 30);

    $txt = '
______________________________________
Firma, sello y registro del médico';
    $this->MultiCell(95, 20, $txt, 1, 'C', 0, 0, '', '', true, 0, false, true, 30);

    $txt = '
______________________________________
Firma del trabajador';

    $this->MultiCell(95, 20, $txt, 1, 'C', 0, 1, '', '', true, 0, false, true, 30);
        // ... footer for the normal page ...
      }*/
    }
}

class HistoriaClinica extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showFormTrabajador()
    {
        return view('historia clinica.register_trabajador');
    }

    public function newTrabajador(Request $request)
    {
        $request->validate([
            'numero_de_identificacion' => 'required|numeric|unique:tab_trabajadors,numero_de_identificacion',
            'nombres' => 'required',
            'apellidos' => 'required',
            'fecha_nacimiento' => 'required',
            'genero' => 'required',
            'edad' => 'required',
            'lugar_nacimiento' => 'required',
            'procedencia' => 'required',
            'residencia' => 'required',
            'escolaridad' => 'required',
            'prof_oficio' => 'required',
            'estado_civil' => 'required',
            'eps' => 'required',
            'arp_anterior' => 'required',
            'pensiones' => 'required'
        ],
            [
                'numero_de_identificacion.required' => 'Por favor ingrese una identificacion',
                'nombres.required' => 'Por favor ingrese un nombre',
                'apellidos.required' => 'Por favor ingrese un apellido',
                'fecha_nacimiento.required' => 'Por favor ingrese una fecha de nacimiento',
                'genero.required' => 'Por favor ingrese un genero',
                'edad.required' => 'Por favor ingrese una edad',
                'lugar_nacimiento.required' => 'Por favor ingrese un lugar de nacimiento',
                'procedencia.required' => 'Por favor ingrese una procedencia',
                'residencia.required' => 'Por favor ingrese una residencia',
                'escolaridad.required' => 'Por favor ingrese la escolaridad',
                'prof_oficio.required' => 'Por favor ingrese una profesion',
                'estado_civil.required' => 'Por favor ingrese un estado civil',
                'eps.required' => 'Por favor ingrese una eps',
                'arp_anterior.required' => 'Por favor ingrese una arp',
                'pensiones.required' => 'Por favor ingrese las pensiones'
            ]
        );

        $date = date_create($request->fecha_de_nacimiento);
        $date = date_format($date, 'Y-m-d');

        tab_trabajador::create([
            'numero_de_identificacion' => $request->numero_de_identificacion,
            'nombres' => $request->nombres,
            'apellidos' => $request->apellidos,
            'fecha_nacimiento' => $date,
            'genero' => $request->genero,
            'edad' => $request->edad,
            'lugar_nacimiento' => $request->lugar_nacimiento,
            'procedencia' => $request->procedencia,
            'residencia' => $request->residencia,
            'escolaridad' => $request->escolaridad,
            'prof_oficio' => $request->prof_oficio,
            'estado_civil' => $request->estado_civil,
            'eps' => $request->eps,
            'arp_anterior' => $request->arp_anterior,
            'pensiones' => $request->pensiones
        ]);

        return redirect()->route('view_trabajadores')->with('exito', 'El registro ha sido creado exitosamente');
    }

    public function listTrabajadores()
    {
        return view('historia clinica.list_trabajadores');
    }

    public function getTrabajadoresAjax(Request $request)
    {
        ini_set('max_execution_time', 0);

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $aColumns = array("id", "numero_de_identificacion", "nombres", "apellidos");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0)
            $bY = "id";
        elseif ($sByColumn == 1 || $sByColumn == 4)
            $bY = "numero_de_identificacion";
        elseif ($sByColumn == 2)
            $bY = "nombres";
        elseif ($sByColumn == 3)
            $bY = "apellidos";

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;

        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $trabajadores = DB::select("SELECT id, numero_de_identificacion, nombres, apellidos FROM tab_trabajadors
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart);

        $trabajadores2 = DB::select("SELECT id, numero_de_identificacion, nombres, apellidos FROM tab_trabajadors
                        " . $sWhere . "
                        " . $sOrder);

        $filteredtrabajadores = count($trabajadores);
        $totaltrabajadores = count($trabajadores2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredtrabajadores,
            "recordsFiltered" => $totaltrabajadores,
            "data" => array(),
        );

        foreach ($trabajadores as $inv) {
            $options = '<a class="btn btn-warning" href="' . route('view_h_clinicas_ocupacionales', ['id' => $inv->id]) . '">
                                <i class="fa fa-bars"></i>
                              </a>
                              <a class="btn btn-primary " href="' . route('edit_form_trabajador', ['id' => $inv->id]) . '">
                              <i class="fa fa-edit"></i>
                              </a>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->numero_de_identificacion;
            $row[] = $inv->nombres;
            $row[] = $inv->apellidos;
            $row[] = $options;

            $output['data'][] = $row;
        }

        return response()->json($output);
    }

    public function showEditFormTrabajador($id)
    {
        $trabajador = tab_trabajador::where('id', $id)->first();
        return view('historia clinica.edit_trabajador', compact('trabajador'));
    }

    public function updateTrabajador(Request $request)
    {
        $request->validate([
            'nombres' => 'required',
            'apellidos' => 'required',
            'fecha_nacimiento' => 'required',
            'genero' => 'required',
            'edad' => 'required',
            'lugar_nacimiento' => 'required',
            'procedencia' => 'required',
            'residencia' => 'required',
            'escolaridad' => 'required',
            'prof_oficio' => 'required',
            'estado_civil' => 'required',
            'eps' => 'required',
            'arp_anterior' => 'required',
            'pensiones' => 'required'
        ],
            [
                'nombres.required' => 'Por favor ingrese un nombre',
                'apellidos.required' => 'Por favor ingrese un apellido',
                'fecha_nacimiento.required' => 'Por favor ingrese una fecha de nacimiento',
                'genero.required' => 'Por favor ingrese un genero',
                'edad.required' => 'Por favor ingrese una edad',
                'lugar_nacimiento.required' => 'Por favor ingrese un lugar de nacimiento',
                'procedencia.required' => 'Por favor ingrese una procedencia',
                'residencia.required' => 'Por favor ingrese una residencia',
                'escolaridad.required' => 'Por favor ingrese la escolaridad',
                'prof_oficio.required' => 'Por favor ingrese una profesion',
                'estado_civil.required' => 'Por favor ingrese un estado civil',
                'eps.required' => 'Por favor ingrese una eps',
                'arp_anterior.required' => 'Por favor ingrese una arp',
                'pensiones.required' => 'Por favor ingrese las pensiones'
            ]
        );

        tab_trabajador::where('id', $request->id)->update(['nombres' => $request->nombres, 'apellidos' => $request->apellidos, 'fecha_nacimiento' => $request->fecha_nacimiento, 'genero' => $request->genero, 'edad' => $request->edad, 'lugar_nacimiento' => $request->lugar_nacimiento, 'procedencia' => $request->procedencia, 'residencia' => $request->residencia, 'escolaridad' => $request->escolaridad, 'prof_oficio' => $request->prof_oficio, 'estado_civil' => $request->estado_civil, 'eps' => $request->eps, 'arp_anterior' => $request->arp_anterior, 'pensiones' => $request->pensiones]);

        return redirect()->route('view_trabajadores')->with('exito', 'El registro ha sido modificado exitosamente');
    }

    public function viewHCTrabajador($id)
    {
        return view('historia clinica.list', ['trabajador' => $id]);
    }

    public function viewHCTrabajadorAjax(Request $request, $id)
    {
        ini_set('max_execution_time', 0);

        $registro = DB::select("SELECT id, tipo_historia, fecha, ciudad FROM tab_h_cli_ocupacionals WHERE trabajador = " . $id);

        $output = array();

        foreach ($registro as $inv)
        {
            $options = '<div class="btn-toolbar"><a class="btn btn-warning"'
                . 'href="' . route('view_h_clinica_ocupacional', ['id' => $inv->id]) . '">'
                . '<i class="fa fa-search"></i>'
                . '</a>'
                . '<a class="btn btn-primary"'
                . 'href="' . route('generarpdfHC', ['id' => $inv->id]) . '" target="_blank">'
                . '<i class="fa fa-file-text"></i>'
                . '</a>'
                . '<a class="btn btn-success"'
                . 'href="' . route('generarpdfCA', ['id' => $inv->id]) . '" target="_blank">'
                . '<i class="fa fa-address-card"></i>'
                . '</a>'
                . '</div>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->tipo_historia;
            $row[] = $inv->fecha;
            $row[] = $inv->ciudad;
            $row[] = $options;

            array_push($output, $row);
        }

        return response()->json($output);
    }

    public function showFormRegHCO($trabajador)
    {
        return view('historia clinica.consulta_medica', ['trabajador' => $trabajador]);
    }

    public function new(Request $request)
    {
        $request->validate([
            'tipo_historia'              => 'required',
            'fecha_historia'             => 'required',
            'ciudad_historia'            => 'required',
            //Antecedentes familiares
            'afv'                        => 'required',
            //Antecedentes personales
            'apv'                        => 'required',
            //Antecedentes dermatológicos
            'adv'                        => 'required',
            //Movilidad
            'mvp' => 'required',
            'mvv' => 'required'
        ]);

        //Crear registro de historia clinica
        $date_historia = date_create($request->fecha_historia);
        $date_historia = date_format($date_historia, 'Y-m-d');
        $historia_clinica_oc = tab_h_cli_ocupacional::create([
            'trabajador' => $request->trabajador,
            'tipo_historia' => $request->tipo_historia,
            'fecha' => $date_historia,
            'ciudad' => $request->ciudad_historia,
            'ha_sufrido_at' => $request->ha_sufrido_at,
            'ha_sufrido_ep' => $request->ha_sufrido_ep,
            'desc_ant_osteo' => $request->descripcion_ao,
            'secuelas' => $request->secuelas_ao,
            'mdv_act' => $request->medicamentos_ao,
            'alergico_algun_medicamento' => $request->alergias,
            'otras_inmunizaciones' => $request->otras_inmunizacion,
            'seg_evolucion_casos' => $request->seguimiento_casos,
            'desc_ampl_hallazgos_ef' => $request->deampl_EF,
            'desc_ampl_hallazgos_cv' => $request->deampl_CV,
            'resultados_audiometria' => $request->resultado_audiometria,
            'examen_visual' => $request->resultado_examen_visual,
            'usa_lentes' => $request->usa_lentes,
            'recomendaciones_medicas' => $request->recomendaciones_medicas
        ]);

        //Consultar el valor actual del AUTO_INCREMENT de la tabla historia clinica ocupacional
        /*$valor_id_historia = DB::select("SELECT AUTO_INCREMENT
                                 FROM  INFORMATION_SCHEMA.TABLES
                                 WHERE TABLE_SCHEMA =  'enfermeria'
                                 AND   TABLE_NAME   =  'tab_h_cli_ocupacionals'");
        $ultimo_id_insertado_historia = $valor_id_historia[0]->AUTO_INCREMENT;
        $ultimo_id_insertado_historia--;*/

        //Crear registro de la información ingresada en la seccion de información ocupacional
        $informacion_ocupacional = tab_informacion_ocupacional::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'antiguedad_en_empresa' => $request->antiguedad_en_empresa,
            'nombre_cargo_actual' => $request->nombre_cargo_actual,
            'antiguedad_en_cargo' => $request->antiguedad_en_cargo,
            'seccion' => $request->seccion,
            'turno' => $request->turno,
            'descripcion_del_cargo' => $request->descripcion_del_cargo,
            'equipos_herramientas_utilizadas' => $request->equipos_herramientas_utilizadas,
            'materias_primas_usadas' => $request->materias_primas_usadas
        ]);

        //Consulta para obtener el valor actual de AUTO_INCREMENT de una tabla
        /*$valor_id = DB::select("SELECT AUTO_INCREMENT
                                FROM  INFORMATION_SCHEMA.TABLES
                                WHERE TABLE_SCHEMA =  'enfermeria'
                                AND   TABLE_NAME   =  'tab_informacion_ocupacionals'");

        //foreach ($valor_id as $ultimoid) {
        //$ultimo_id_insertado = $ultimoid->AUTO_INCREMENT;
        //}
        $ultimo_id_insertado = $valor_id[0]->AUTO_INCREMENT;
        $ultimo_id_insertado--;*/
        //echo("valor id".$ultimo_id_insertado);

        /*if(strlen($request->valor_otra_accion) > 0){
            echo("Valor campo otra accion: ".$request->valor_otra_accion);
        }*/
        $acciones_que_realiza = $request->acciones_que_realiza;
        $N = count($acciones_que_realiza);

        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {

                $resultado2 = tab_realiza_accion::create([
                    'informacion_ocupacional' => $informacion_ocupacional->id,
                    'tipo' => $acciones_que_realiza[$i]
                ]);
            }
        }

        $actividades_que_realiza = $request->actividades_que_realiza;
        $N = count($actividades_que_realiza);

        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {

                $resultado3 = tab_realiza_actividad::create([
                    'informacion_ocupacional' => $informacion_ocupacional->id,
                    'tipo' => $actividades_que_realiza[$i]
                ]);

            }
        }

        //Crear registro seccion antecedentes laborales
        if(isset($request->empresas)){
        $empresas = $request->empresas;
        $N = count($empresas);

        if ($N > 0) {

            for ($i = 0; $i < $N; $i++) {

                $exp_factores_riesgo = tab_exp_factores_riesgo::create([
                    'historia_clinica' => $historia_clinica_oc->id,
                    'empresa' => $empresas[$i],
                    'estado_laboral' => $request->estado_laboral[$i],
                    'cargo' => $request->cargo[$i],
                    'tiempo' => $request->tiempo[$i],
                    'EPP' => $request->epp[$i]
                ]);

                /*$valor_id = DB::select("SELECT AUTO_INCREMENT
                                        FROM  INFORMATION_SCHEMA.TABLES
                                        WHERE TABLE_SCHEMA =  'enfermeria'
                                        AND   TABLE_NAME   =  'tab_exp_factores_riesgos'");
                $ultimo_id_insertado = $valor_id[0]->AUTO_INCREMENT;
                $ultimo_id_insertado--;*/

                $factores_de_riesgo_AL = $request->factores_de_riesgo_AL[$i];
                foreach ($factores_de_riesgo_AL as $factor) {
                    $resultado4 = tab_factor_de_riesgo::create([
                        'exp_factor_riesgo' => $exp_factores_riesgo->id,
                        'tipo' => $factor
                    ]);
                }

            }
        }
        }

        //Crear registro seccion Uso de elementos de proteccion personal
        $resultado5 = tab_elementos_pr_per::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'en_cargo_empresa_ant' => $request->cargo_empresa_anterior_ep,
            'examen_periodico' => $request->examen_periodico_ep
        ]);

        //Guardar elementos seleccionados de la seccion elementos proteccion personal
        /*$valor_id = DB::select("SELECT AUTO_INCREMENT
                                FROM  INFORMATION_SCHEMA.TABLES
                                WHERE TABLE_SCHEMA =  'enfermeria'
                                AND   TABLE_NAME   =  'tab_elementos_pr_pers'");

        $ultimo_id_insertado = $valor_id[0]->AUTO_INCREMENT;
        $ultimo_id_insertado--;*/

        if(isset($request->elementos_pr)){
        $elementos_pr = $request->elementos_pr;
        $N = count($elementos_pr);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado6 = tab_elem_usados_prt::create([
                    'elem_pro_per' => $resultado5->id,
                    'elemento' => $elementos_pr[$i]
                ]);
            }
        }
        }

        //Crear registro seccion antecedentes de accidentes de trabajo
        if($request->ha_sufrido_at == 'Si'){
        if(isset($request->fecha_aat)){
        $anteceden_acci_trabajo = $request->fecha_aat;
        $N = count($anteceden_acci_trabajo);
        if($N > 0){
         for($i=0; $i < $N; $i++){ 
        $resultado7 = tab_ant_acci_trabajo::create([
            'historia_clinica'     => $historia_clinica_oc->id,
            'fecha'                => $request->fecha_aat[$i],
            'empresa'              => $request->empresa_aat[$i],
            'causa'                => $request->causa_aat[$i],
            'tipo_de_lesion'       => $request->tlesion_aat[$i],
            'parte_del_cuerpo'     => $request->parte_aat[$i],
            'dias_de_incapacidad'  => $request->dincapacidad_aat[$i],
            'secuelas'             => $request->secuelas_aat[$i]
        ]);
        }
        }
        }
        }

        //Crear registro seccion enfermedad profesional
        if($request->ha_sufrido_at == 'Si'){
        if(isset($request->fecha_lep)){
        $enf_profesional = $request->fecha_lep;
        $N = count($enf_profesional);
        if($N > 0){
         for($i=0; $i < $N; $i++){
        $resultado8 = tab_enf_profesional::create([
            'historia_clinica'    => $historia_clinica_oc->id,
            'fecha'               => $request->fecha_lep[$i],
            'empresa'             => $request->empresa_lep[$i],
            'diagnostico'         => $request->diagnostico_lep[$i],
            'indemnizacion'       => $request->indemnizacion_lep[$i],
            'reubicacion'         => $request->reubicacion_lep[$i]
        ]);
        }
        }
        }
        }

        //Crear registro antecedentes familiares      
        for ($i = 0; $i < 12; $i++) {
            $resultado = tab_ant_familiar::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'enfermedad' => $request->afp[$i],
                'presente' => $request->afv[$i],
                'parentesco' => $request->afpr[$i]
                //(!isset($request->afpr[$i]))? NULL: $request->afpr[$i]
            ]);
        }

        //Crear registros antecedentes personales
        for ($i = 0; $i < 33; $i++) {
            $resultado = tab_ant_personal::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'patologia' => $request->app[$i],
                'presente' => $request->apv[$i],
                'hace_cuanto' => (!isset($request->aphc[$i])) ? NULL : $request->aphc[$i]
            ]);
        }

        //Registrar antecedentes dermatologicos
        for ($i = 0; $i < 3; $i++) {
            $resultado = tab_ant_derma::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'patologia' => $request->adp[$i],
                'presente' => $request->adv[$i],
                'hace_cuanto' => (!isset($request->adhc[$i])) ? NULL : $request->adhc[$i]
            ]);
        }

        //Registrar antecedentes osteomusculares
        for ($i = 0; $i < 4; $i++) {
            $resultado = tab_ant_osteo::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'patologia' => $request->aop[$i],
                'presente' => $request->aov[$i],
                'hace_cuanto' => (!isset($request->aohc[$i])) ? NULL : $request->aohc[$i]
            ]);
        }


        //Registrar antecedentes quirurgicos
        if(isset($request->antecedente_quirurgico)){
        $antecedentes_quirurgicos = $request->antecedente_quirurgico;
        $N = count($antecedentes_quirurgicos);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_ant_qui::create([
                    'historia_clinica' => $historia_clinica_oc->id,
                    'antecedente' => $request->antecedente_quirurgico[$i],
                    'fecha' => $request->fecha_aq[$i]
                ]);
            }
        }
        }

        //Registrar antecedentes traumáticos
        if(isset($request->antecedente_traumatico)){
        $antecedentes_traumaticos = $request->antecedente_traumatico;
        $N = count($antecedentes_traumaticos);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_ant_trau::create([
                    'historia_clinica' => $historia_clinica_oc->id,
                    'antecedente' => $request->antecedente_traumatico[$i],
                    'causa' => $request->causa_at[$i]
                ]);
            }
        }
        }

        //Registrar inmunizaciones
        $inmunizaciones = $request->inmunizaciones;
        $N = count($inmunizaciones);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_inmunizacion::create([
                    'historia_clinica' => $historia_clinica_oc->id,
                    'inmunizacion' => $inmunizaciones[$i],
                    'fecha' => $request->fecinmun[$i],
                    'dosis' => $request->dosinmun[$i]
                ]);
            }
        }


        //Registrar antecedentes gineco obstetricos
        if ($request->ant_gineco_obs == 'Si') {
            $ciclos = $request->dias_C1 . $request->dias_C2;

            $resultado = tab_ant_gineco::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'menarquia' => $request->menarquia,
                'ciclos' => $ciclos,
                'fecha_ultima_m' => $request->FUM,
                'dismenorrea' => $request->dismenorrea,
                'paridad_g' => $request->paridadg,
                'paridad_p' => $request->paridadp,
                'paridad_a' => $request->paridada,
                'paridad_c' => $request->paridadc,
                'paridad_e' => $request->paridade,
                'paridad_m' => $request->paridadm,
                'paridad_v' => $request->paridadv,
                'FPP' => $request->fpp,
                'planificacion' => $request->planificacion,
                'metodo' => $request->metodo_planificacion,
                'citologia_reciente' => $request->citologia_reciente,
                'fecha_citologia' => $request->fecha_citologia,
                'resultado_citologia' => $request->resultado_citologia
            ]);

        }

        //Registrar habitos toxicos
        $resultado = tab_hab_toxico::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'fumador' => $request->fumador,
            'exfumador' => $request->exfumador,
            'asuspension_h' => $request->asuspension_h,
            'adefumador' => $request->afumador,
            'cigpordia' => $request->cigarrillos_dia,
            'tomalicorh' => $request->tomalicorh,
            'frecuenciahab' => $request->frecuencia_habito,
            'tipolicor' => $request->tlicor,
            'problemas_con_bebida' => $request->pbebida,
            'exbebedor' => $request->exbebedor,
            'exasuspendido' => $request->asuspendido,
            'otros_toxicos' => $request->otoxicos,
            'med_regularmente' => $request->med_reg_ht,
            'cual_med_reg' => $request->cual_med_reg
        ]);

        //Registrar factores predisponentes para riesgo ergonómico
        $resultado = tab_frerg::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'sedentarismo' => $request->sedentarismo,
            'deportes_de_raqueta' => $request->deportes_raqueta,
            'sobrepeso' => $request->sobrepeso,
            'manualidades' => $request->manualidades,
            'deporte' => $request->deporte,
            'cual_deporte' => $request->cdeporte,
            'pasatiempos' => $request->pasatiempos
        ]);

        //Registrar revision de sistemas
        $resultado = tab_revision_psistema::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'sistema_nervioso' => $request->sistema_nervioso_rs,
            'insonmio' => $request->insomnio_rs,
            'd_para_concentrarse' => $request->dif_p_concentrarse_rs,
            'desp_realizar_act' => $request->des_real_act_rs,
            'tmuscular' => $request->tension_muscular_rs,
            'ojos' => $request->ojos_rs,
            'orl' => $request->ORL_rs,
            'respiratorio' => $request->respiratorio_rs,
            'cardiovascular' => $request->cardiovascular_rs,
            'digestivo' => $request->digestivo_rs,
            'piel_faneras' => $request->piel_faneras_rs,
            'musculoesqueletico' => $request->musculoesqueletico_rs,
            'genito_urinario' => $request->genito_urinario_rs
        ]);

        //Registrar molestias en el ultimo año
        for ($i = 0; $i < 4; $i++) {
            $resultado = tab_mol_ua::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'patologia' => $request->map[$i],
                'presente' => $request->mav[$i],
                'descripcion' => (!isset($request->macd[$i])) ? NULL : $request->macd[$i]
            ]);
        }

        //Registrar examen físico
        $resultado = tab_exam_fi::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'constitucion' => $request->constitucion,
            'dominancia' => $request->dominancia,
            'TA' => $request->t_a,
            'frec_cardiaca' => $request->frecuencia_cardiaca,
            'Peso' => $request->peso,
            'Talla' => $request->talla,
            'IMC' => $request->imc
        ]);

        //Registrar detalles examen fisico
        for ($i = 0; $i < 33; $i++) {
            $resultado = tab_det_exf::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'organo' => $request->efp[$i],
                'estado' => $request->efv[$i],
                'hallazgo' => (!isset($request->efhl[$i])) ? NULL : $request->efhl[$i]
            ]);
        }

        //Registrar inspecciones
        for ($i = 0; $i < 5; $i++) {
            $resultado = tab_inspeccion::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'inspeccion' => $request->inp[$i],
                'estado' => $request->inv[$i],
                'hallazgo' => (!isset($request->inhl[$i])) ? NULL : $request->inhl[$i]
            ]);
        }

        //Registrar palpaciones
        for ($i = 0; $i < 5; $i++) {
            $resultado = tab_palpacion::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'palpacion' => $request->pap[$i],
                'estado' => $request->pav[$i],
                'hallazgo' => (!isset($request->pahl[$i])) ? NULL : $request->pahl[$i]
            ]);
        }

        //Registrar movilidad
        for ($i = 0; $i < 6; $i++) {
            $resultado = tab_movilidad::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'movilidad' => $request->mvp[$i],
                'estado' => $request->mvv[$i],
                'hallazgo' => (!isset($request->mvhl[$i])) ? NULL : $request->mvhl[$i]
            ]);
        }

        //Registrar inspeccion nuca
        for ($i = 0; $i < 5; $i++) {
            $resultado = tab_inuca::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'descripcion' => $request->nsp[$i],
                'estado' => $request->nsv[$i],
                'hallazgo' => (!isset($request->nshl[$i])) ? NULL : $request->nshl[$i]
            ]);
        }

        //Registrar inspeccion hombros
        for ($i = 0; $i < 5; $i++) {
            $resultado = tab_ihombro::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'descripcion' => $request->hmp[$i],
                'estado' => $request->hmv[$i],
                'hallazgo' => (!isset($request->hmhl[$i])) ? NULL : $request->hmhl[$i]
            ]);
        }

        //Registrar inspeccion codos, mano y muñeca
        for ($i = 0; $i < 21; $i++) {
            $resultado = tab_icmm::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'descripcion' => $request->cmmp[$i],
                'estado' => $request->cmmv[$i],
                'hallazgo' => (!isset($request->cmhl[$i])) ? NULL : $request->cmhl[$i]
            ]);
        }

        //Registrar procesos evaluacion del estado mental
        for ($i = 0; $i < 6; $i++) {
            $resultado = tab_evproceso::create([
                'historia_clinica' => $historia_clinica_oc->id,
                'proceso' => $request->prp[$i],
                'estado' => $request->prv[$i],
                'hallazgo' => (!isset($request->prhl[$i])) ? NULL : $request->prhl[$i]
            ]);
        }

        //Registrar paraclinicos
        if(isset($request->nombre_para)){
        $paraclinicos = $request->nombre_para;
        $N = count($paraclinicos);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_paraclinico::create([
                    'historia_clinica' => $historia_clinica_oc->id,
                    'nombre_paraclinico' => $request->nombre_para[$i],
                    'fecha' => $request->fecha_para[$i],
                    'resultado' => $request->resultado_para[$i]
                ]);
            }
        }
        }

        //Registrar diagnosticos
        $diagnosticos = $request->diagnosticos;
        $N = count($diagnosticos);
        if ($N > 0) {
            for ($i = 0; $i < $N; $i++) {
                $resultado = tab_diagnostico::create([
                    'historia_clinica' => $historia_clinica_oc->id,
                    'diagnostico' => $request->diagnosticos[$i]
                ]);
            }
        }


        //Registrar concepto de aptitud
        $concepto_de_aptitud = tab_concepto_de_aptitud::create([
            'historia_clinica' => $historia_clinica_oc->id,
            'aptitud' => $request->concepto_aptitud,
            'examen_egreso_normal' => $request->examen_enormal,
            'examen_p_satisfactorio' => $request->epsatisfactorio
        ]);

        //Registrar ingreso PVESO
        /*$valor_id = DB::select("SELECT AUTO_INCREMENT
                                FROM  INFORMATION_SCHEMA.TABLES
                                WHERE TABLE_SCHEMA =  'enfermeria'
                                AND   TABLE_NAME   =  'tab_concepto_de_aptituds'");

        $ultimo_id_insertado = $valor_id[0]->AUTO_INCREMENT;
        $ultimo_id_insertado--;*/

        $ingreso_pveso = $request->ingreso_pveso;
        $N = count($ingreso_pveso);

        for ($i = 0; $i < $N; $i++) {
            $resultado = tab_ingreso_pveso::create([
                'concepto_de_aptitud' => $concepto_de_aptitud->id,
                'tipo' => $ingreso_pveso[$i]
            ]);
        }


        /*echo("You selected $N action(s): ");
        for($i=0; $i < $N; $i++)
        {
         echo($acciones_que_realiza[$i] . " ");
        }
        echo("  ");
        
        //echo("Valor id: ". $valor_id->AUTO_INCREMENT);
        //var_dump($valor_id);
        

        

        $factores_de_riesgo = $request->factores_de_riesgo_AL;
        $N = count($factores_de_riesgo);
        echo("You selected $N factor(s): ");
        for($i=0; $i < $N; $i++)
        {
         echo($factores_de_riesgo[0]["Fis"] . " ");
         echo($factores_de_riesgo[1]["Qui"] . " ");
        }*/

        //return Redirect::to(route('view_h_clinicas_ocupacionales', ['id'=>$request->trabajador]));

        return redirect()->route('view_h_clinicas_ocupacionales', ['id' => $request->trabajador])->with('exito', 'La historia clínica ha sido registrada exitosamente');

    }


    public function list()
    {
        return view('historia clinica.list');
    }

    public function getHCOcupacionalesAjax(Request $request)
    {

        ini_set('max_execution_time', 0);

        $f_i = $request->fecha_i;
        $f_f = $request->fecha_f;

        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $iSortCol_0 = $request->iSortCol_0;
        $iSortingCols = $request->iSortingCols;
        $aColumns = array("id", "tipo_historia", "fecha", "ciudad");

        $sWhere = '';

        //Searching
        $sSearch = $request->search['value'];
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if ($sByColumn == 0) {

            $bY = "id";

        } elseif ($sByColumn == 1) {

            $bY = "tipo_historia";

        } elseif ($sByColumn == 2) {

            $bY = "fecha";

        } elseif ($sByColumn == 3) {

            $bY = "ciudad";

        }

        $sOrder = "ORDER BY " . $bY . " " . $OrderD;


        if ($sSearch != null && $sSearch != "") {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $hclinicasoc = DB::select("SELECT id, tipo_historia, fecha, ciudad FROM tab_h_cli_ocupacionals
                        " . $sWhere . "
                        " . $sOrder . "
                        LIMIT " . $iDisplayLength . " OFFSET " . $iDisplayStart . "");


        $hclinicasoc2 = DB::select("SELECT id, tipo_historia, fecha, ciudad FROM tab_h_cli_ocupacionals
                        " . $sWhere . "
                        " . $sOrder);

        $filteredhclinicasoc = count($hclinicasoc);
        $totalhclinicasoc = count($hclinicasoc2);

        $output = array(
            "draw" => $sEcho,
            "recordsTotal" => $filteredhclinicasoc,
            "recordsFiltered" => $totalhclinicasoc,
            "data" => array(),
        );

        foreach ($hclinicasoc as $inv) {
            $options = '<a class="btn btn-success" href="' . route('view_h_clinica_ocupacional', ['id' => $inv->id]) . '">
                                <i class="fa fa-edit"></i> Ver Historia
                              </a>';

            $row = array();

            $row[] = $inv->id;
            $row[] = $inv->tipo_historia;
            $row[] = $inv->fecha;
            $row[] = $inv->ciudad;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);

    }

    public function showHistoriaClinicaOcupacional(Request $request, $id)
    {
        ini_set('max_execution_time', 0);

        $historia_ocupacional = tab_h_cli_ocupacional::where('id', $id)->first();
        $trabajador = tab_trabajador::where('id', $historia_ocupacional->trabajador)->first();
        $informacion_ocupacional = tab_informacion_ocupacional::where('historia_clinica', $id)->get();
        $realiza_accion = tab_informacion_ocupacional::find($informacion_ocupacional[0]->id)->acciones;
        $realiza_actividad = tab_informacion_ocupacional::find($informacion_ocupacional[0]->id)->actividades;

        $factores_de_riesgo_AL = tab_exp_factores_riesgo::where('historia_clinica', $id)->get();

        //Uso de elementos de protección personal
        $elementos_proteccion_p = tab_elementos_pr_per::where('historia_clinica', $id)->get();
        //obtener lista de elementos usados
        $elem_usados = tab_elementos_pr_per::find($elementos_proteccion_p[0]->id)->elementos_usados;

        //Obtener accidentes de trabajo
        $ant_acci_trabajo = tab_ant_acci_trabajo::where('historia_clinica', $id)->get();

        //Obtener enfermedades profesionales
        $enfer_profesionales = tab_enf_profesional::where('historia_clinica', $id)->get();

        //Obtener antecedentes familiares
        $ant_familiares = tab_ant_familiar::where('historia_clinica', $id)->get();

        //Obtener antecedentes personales
        $ant_personales = tab_ant_personal::where('historia_clinica', $id)->get();

        //Obtener antecedentes dermatologicos
        $ant_dermatologicos = tab_ant_derma::where('historia_clinica', $id)->get();

        //Obtener antecedentes osteomusculares
        $ant_osteomusculares = tab_ant_osteo::where('historia_clinica', $id)->get();

        //Obtener antecedentes quirurgicos
        $ant_quirurgicos = tab_ant_qui::where('historia_clinica', $id)->get();
        //Obtener antecedentes traumaticos
        $ant_traumaticos = tab_ant_trau::where('historia_clinica', $id)->get();
        //obtener inmunizaciones
        $inmunizaciones = tab_inmunizacion::where('historia_clinica', $id)->get();
        //Obtener antecedentes ginecoobstetricos
        $ant_gineco = tab_ant_gineco::where('historia_clinica', $id)->get();
        //obtener habitos toxicos
        $habitos_toxicos = tab_hab_toxico::where('historia_clinica', $id)->get();
        //obtener factores predisponentes para riesgo ergonomico
        $factores_predisponentes = tab_frerg::where('historia_clinica', $id)->get();
        //obtener revisión por sistemas
        $revision_sistemas = tab_revision_psistema::where('historia_clinica', $id)->get();

        //obtener molestias ultimo año
        $molestias_u_a = tab_mol_ua::where('historia_clinica', $id)->get();
        //obtener examen fisico
        $examen_fisico = tab_exam_fi::where('historia_clinica', $id)->get();
        //obtener detalles examen fisico
        $detalles_examen_fisico = tab_det_exf::where('historia_clinica', $id)->get();
        //obtener inspeccion
        $inspeccion = tab_inspeccion::where('historia_clinica', $id)->get();
        //obtener palpacion
        $palpacion = tab_palpacion::where('historia_clinica', $id)->get();
        //obtener movilidad
        $movilidad = tab_movilidad::where('historia_clinica', $id)->get();
        //obtener inspeccion nuca
        $nuca = tab_inuca::where('historia_clinica', $id)->get();
        //obtener inspeccion hombros
        $hombros = tab_ihombro::where('historia_clinica', $id)->get();
        //obtener inspeccion codos, mano, muñeca
        $cod_mano_mun = tab_icmm::where('historia_clinica', $id)->get();
        //obtener procesos evaluacion mental
        $pro_ev = tab_evproceso::where('historia_clinica', $id)->get();
        //obtener otros paraclinicos
        $otros_paraclinicos = tab_paraclinico::where('historia_clinica', $id)->get();
        //obtener diagnosticos
        $diagnosticos = tab_diagnostico::where('historia_clinica', $id)->get();
        //obtener concepto de aptitud
        $concepto_aptitud = tab_concepto_de_aptitud::where('historia_clinica', $id)->get();
        //obtener ingreso pveso
        //$ingreso_pveso = tab_ingreso_pveso::where('historia_clinica',$id)->get();
        $ingreso_pveso = tab_concepto_de_aptitud::find($concepto_aptitud[0]->id)->ingreso_pveso;

        $informacion = compact('historia_ocupacional', 'trabajador', 'informacion_ocupacional', 'realiza_accion', 'realiza_actividad', 'factores_de_riesgo_AL', 'elementos_proteccion_p', 'elem_usados', 'ant_acci_trabajo', 'enfer_profesionales', 'ant_familiares', 'ant_personales', 'ant_dermatologicos', 'ant_osteomusculares', 'ant_quirurgicos', 'ant_traumaticos', 'inmunizaciones', 'ant_gineco', 'habitos_toxicos', 'factores_predisponentes', 'revision_sistemas', 'molestias_u_a', 'examen_fisico', 'detalles_examen_fisico', 'inspeccion', 'palpacion', 'movilidad', 'nuca', 'hombros', 'cod_mano_mun', 'pro_ev', 'otros_paraclinicos', 'diagnosticos', 'concepto_aptitud', 'ingreso_pveso');

        return view('historia clinica.showInformacionHistoriaOc', $informacion);
    }

    public function generarPDFHistoriaClinicaOc(Request $request, $id)
    {
        ini_set('max_execution_time', 0);

        $historia_ocupacional = tab_h_cli_ocupacional::where('id', $id)->first();

        $trabajador = tab_trabajador::where('id', $historia_ocupacional->trabajador)->first();
        $informacion_ocupacional = tab_informacion_ocupacional::where('historia_clinica', $id)->get();
        $realiza_accion = tab_informacion_ocupacional::find($informacion_ocupacional[0]->id)->acciones;
        $realiza_actividad = tab_informacion_ocupacional::find($informacion_ocupacional[0]->id)->actividades;

        $factores_de_riesgo_AL = tab_exp_factores_riesgo::where('historia_clinica', $id)->get();


        //Uso de elementos de protección personal
        $elementos_proteccion_p = tab_elementos_pr_per::where('historia_clinica', $id)->get();
        //obtener lista de elementos usados
        $elem_usados = tab_elementos_pr_per::find($elementos_proteccion_p[0]->id)->elementos_usados;

        //Obtener accidentes de trabajo
        $ant_acci_trabajo = tab_ant_acci_trabajo::where('historia_clinica', $id)->get();

        //Obtener enfermedades profesionales
        $enfer_profesionales = tab_enf_profesional::where('historia_clinica', $id)->get();

        //Obtener antecedentes familiares
        $ant_familiares = tab_ant_familiar::where('historia_clinica', $id)->get();

        //Obtener antecedentes personales
        $ant_personales = tab_ant_personal::where('historia_clinica', $id)->get();

        //Obtener antecedentes dermatologicos
        $ant_dermatologicos = tab_ant_derma::where('historia_clinica', $id)->get();

        //Obtener antecedentes osteomusculares
        $ant_osteomusculares = tab_ant_osteo::where('historia_clinica', $id)->get();

        //Obtener antecedentes quirurgicos
        $ant_quirurgicos = tab_ant_qui::where('historia_clinica', $id)->get();
        //Obtener antecedentes traumaticos
        $ant_traumaticos = tab_ant_trau::where('historia_clinica', $id)->get();
        //obtener inmunizaciones
        $inmunizaciones = tab_inmunizacion::where('historia_clinica', $id)->get();
        //Obtener antecedentes ginecoobstetricos
        $ant_gineco = tab_ant_gineco::where('historia_clinica', $id)->get();
        //obtener habitos toxicos
        $habitos_toxicos = tab_hab_toxico::where('historia_clinica', $id)->get();
        //obtener factores predisponentes para riesgo ergonomico
        $factores_predisponentes = tab_frerg::where('historia_clinica', $id)->get();
        //obtener revisión por sistemas
        $revision_sistemas = tab_revision_psistema::where('historia_clinica', $id)->get();

        //obtener molestias ultimo año
        $molestias_u_a = tab_mol_ua::where('historia_clinica', $id)->get();
        //obtener examen fisico
        $examen_fisico = tab_exam_fi::where('historia_clinica', $id)->get();
        //obtener detalles examen fisico
        $detalles_examen_fisico = tab_det_exf::where('historia_clinica', $id)->get();
        //obtener inspeccion
        $inspeccion = tab_inspeccion::where('historia_clinica', $id)->get();
        //obtener palpacion
        $palpacion = tab_palpacion::where('historia_clinica', $id)->get();
        //obtener movilidad
        $movilidad = tab_movilidad::where('historia_clinica', $id)->get();
        //obtener inspeccion nuca
        $nuca = tab_inuca::where('historia_clinica', $id)->get();
        //obtener inspeccion hombros
        $hombros = tab_ihombro::where('historia_clinica', $id)->get();
        //obtener inspeccion codos, mano, muñeca
        $cod_mano_mun = tab_icmm::where('historia_clinica', $id)->get();
        //obtener procesos evaluacion mental
        $pro_ev = tab_evproceso::where('historia_clinica', $id)->get();
        //obtener otros paraclinicos
        $otros_paraclinicos = tab_paraclinico::where('historia_clinica', $id)->get();
        //obtener diagnosticos
        $diagnosticos = tab_diagnostico::where('historia_clinica', $id)->get();
        //obtener concepto de aptitud
        $concepto_aptitud = tab_concepto_de_aptitud::where('historia_clinica', $id)->get();
        //obtener ingreso pveso
        //$ingreso_pveso = tab_ingreso_pveso::where('historia_clinica',$id)->get();
        $ingreso_pveso = tab_concepto_de_aptitud::find($concepto_aptitud[0]->id)->ingreso_pveso;

        //Obtener ruta imagen logo uceva
        $path = public_path('imgs/siacu_logo.svg');

        PDF::SetTitle('Archivo Historia Clinica Ocupacional');
        PDF::setFooterCallback(function($pdf){
    
            // ... footer for the last page ...
          
        
        $pdf->SetY(-10);
        $pdf->SetFont('helvetica', 'I', 8);
          
          // Page number
          $pdf->Cell(0, 10, 'Página '.$pdf->getAliasNumPage().'/'.$pdf->getAliasNbPages(), 0, false, 'C', 0, '', 0, false, 'T', 'M');
        });

        PDF::AddPage();

        $html = '
  <div>
  <table style="width: 100%;" border="1">
  <tr> 
  <td rowspan="2"> <img src="' . $path . '" style="width:110px;height:50px;"></td>
  <td colspan="2"><h4 style="text-align: center;">BIENESTAR INSTITUCIONAL Y GESTIÓN HUMANA</h4></td>
  <td><h5>Código: 1025-39.1-135-F</h5></td>
  </tr>
    
  <tr>
  <td colspan="2"><h4 style="text-align: center;">HISTORIA CLÍNICA OCUPACIONAL</h4></td>
  <td><h5>Versión:00</h5></td>
  </tr>
  </table>
  </div>
  
  <div>
  <table style="width: 100%;" border="0">
  <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Numero de identificación:</label><br><label style="font-size:9px;">' . $trabajador->numero_de_identificacion . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Nombres:</label><br><label style="font-size:9px;">' . $trabajador->nombres . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Apellidos:</label><br><label style="font-size:9px;">' . $trabajador->apellidos . '</label></td>
  </tr>

  <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha de nacimiento:</label><br><label style="font-size:9px;">' . $trabajador->fecha_nacimiento . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Género:</label><br><label style="font-size:9px;">' . $trabajador->genero . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Edad:</label><br><label style="font-size:9px;">' . $trabajador->edad . '</label></td>
  </tr>

  <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Lugar de nacimiento:</label><br><label style="font-size:9px;">' . $trabajador->lugar_nacimiento . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Procedencia:</label><br><label style="font-size:9px;">' . $trabajador->procedencia . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Residencia:</label><br><label style="font-size:9px;">' . $trabajador->residencia . '</label></td>
  </tr>

  <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Escolaridad:</label><br><label style="font-size:9px;">' . $trabajador->escolaridad . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Profesión u oficio:</label><br><label style="font-size:9px;">' . $trabajador->prof_oficio . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado civil:</label><br><label style="font-size:9px;">' . $trabajador->estado_civil . '</label></td>
  </tr>

  <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">EPS:</label><br><label style="font-size:9px;">' . $trabajador->eps . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">ARP anterior:</label><br><label style="font-size:9px;">' . $trabajador->arp_anterior . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Pensiones:</label><br><label style="font-size:9px;">' . $trabajador->pensiones . '</label></td>
  </tr>
  </table>
  </div>
  
  <div>
  <table style="width: 100%;" border="0">
  <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Tipo:</label><br><label style="font-size:9px;">' . $historia_ocupacional->tipo_historia . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha del examen:</label><br><label style="font-size:9px;">' . $historia_ocupacional->fecha . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Ciudad:</label><br><label style="font-size:9px;">' . $historia_ocupacional->ciudad . '</label></td>
  </tr>
  </table>
  </div>

  <label style="font-size:12px; font-weight: bold;">Información ocupacional</label>

  <div>
  <table style="width: 100%;" border="0">
  <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Antiguedad en la empresa:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->antiguedad_en_empresa . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Nombre cargo actual o a desempeñar:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->nombre_cargo_actual . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Antiguedad en el cargo:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->antiguedad_en_cargo . '</label></td>
  </tr>

  <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Sección:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->seccion . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Turno:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->turno . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Descripción del cargo:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->descripcion_del_cargo . '</label></td>
  </tr>

  <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Herramientas utilizadas:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->equipos_herramientas_utilizadas . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Materias primas o insumos utilizados:</label><br><label style="font-size:9px;">' . $informacion_ocupacional[0]->materias_primas_usadas . '</label></td>
    <td></td>
  </tr>

  <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Acciones realizadas:</label><br>';
        foreach ($realiza_accion as $accion) {
            $html .= '<label style="font-size:10px;">' . $accion->tipo . '</label><br>';
        }
        $html .= '</td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Actividades realizadas:</label><br>';
        foreach ($realiza_actividad as $actividad) {
            $html .= '<label style="font-size:10px;">' . $actividad->tipo . '</label><br>';
        }
        $html .= '</td>
    <td></td>
  </tr>
  </table>
  </div>

  <label style="font-size:12px; font-weight: bold;">Exposición a factores de riesgo y antecedentes laborales</label>
  <div>
  <table style="width: 100%;" border="0">
    ';

        foreach ($factores_de_riesgo_AL as $factor_de_riesgo) {
            $html .= '
     <tr>
     <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Empresa:</label><br><label style="font-size:10px;">' . $factor_de_riesgo->empresa . '</label>
     </td>
     <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado laboral:</label><br><label style="font-size:10px;">' . $factor_de_riesgo->estado_laboral . '</label>
     </td>
     <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Cargo:</label><br><label style="font-size:10px;">' . $factor_de_riesgo->cargo . '</label>
     </td>
     </tr>

     <tr>
     <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Tiempo:</label><br><label style="font-size:10px;">' . $factor_de_riesgo->tiempo . '</label>
     </td>
     <td><label style="font-size:10px; font-style: italic; font-weight: bold;">EPP:</label><br><label style="font-size:10px;">' . $factor_de_riesgo->EPP . '</label>
     </td>
     </tr>
     ';
        }

        $html .= '
   </table>
   </div>
   <label style="font-size:12px; font-weight: bold;">Uso de elementos de protección personal</label>

  <div>
  <table style="width: 100%; height:10%;" border="0">
  <tr>
  <td><label style="font-size:10px; font-style: italic; font-weight: bold;">En el cargo o empresa anterior:</label><br><label style="font-size:10px;">' . $elementos_proteccion_p[0]->en_cargo_empresa_ant . '</label>
  </td>

  <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Examen periodico:</label><br><label style="font-size:10px;">' . $elementos_proteccion_p[0]->examen_periodico . '</label>
  </td>
  </tr>

  <tr>
  <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Elementos usados:</label><br>';

        foreach ($elem_usados as $elem_usado) {
            $html .= '<label style="font-size:10px;">' . $elem_usado->elemento . '</label><br>';
        }

        $html .= '
  </td>
  </tr>
  </table>
  </div>

  <label style="font-size:12px; font-weight: bold;">Antecedentes de accidentes de trabajo</label>

  <div>
  <table style="width: 100%;" border="0">';

        foreach ($ant_acci_trabajo as $ant_acci_t) {
            $html .= '
  <tr>
  <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha:</label>
  <label style="font-size:10px; font-style: italic;">' . $ant_acci_t->fecha . '</label>
  </td>

  <td>
  <label style="font-size:10px; font-style: italic; font-weight: bold;">Empresa:</label>
  <label style="font-size:10px; font-style: italic;">' . $ant_acci_t->empresa . '</label>
  </td>

  <td>
  <label style="font-size:10px; font-style: italic; font-weight: bold;">Causa:</label>
  <label style="font-size:10px; font-style: italic;">' . $ant_acci_t->causa . '</label>
  </td>

  <td>
  <label style="font-size:10px; font-style: italic; font-weight: bold;">Tipo de lesión:</label>
  <label style="font-size:10px; font-style: italic;">' . $ant_acci_t->tipo_de_lesion . '</label>
  </td>

  </tr>

  <tr>

  <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Parte del cuerpo afectado:</label>
  <label style="font-size:10px; font-style: italic; ">' . $ant_acci_t->parte_del_cuerpo . '</label>
  </td>

  <td>
  <label style="font-size:10px; font-style: italic; font-weight: bold;">Dias de incapacidad:</label>
  <label style="font-size:10px; font-style: italic;">' . $ant_acci_t->dias_de_incapacidad . '</label>
  </td>

  <td>
  <label style="font-size:10px; font-style: italic; font-weight: bold;">Secuelas:</label>
  <label style="font-size:10px; font-style: italic;">' . $ant_acci_t->secuelas . '</label>
  </td>

  </tr>';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Enfermedad profesional</label>

   <div>
   <table style="width: 100%;" border="0">';

        foreach ($enfer_profesionales as $enf_pro) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha:</label>
   <label style="font-size:10px; font-style: italic;">' . $enf_pro->fecha . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Empresa:</label>
   <label style="font-size:10px; font-style: italic;">' . $enf_pro->empresa . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Diagnóstico:</label>
   <label style="font-size:10px; font-style: italic;">' . $enf_pro->diagnostico . '</label>
   </td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">¿Indemnización?:</label>
   <label style="font-size:10px; font-style: italic;">' . $enf_pro->indemnizacion . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">¿Reubicación?:</label>
    <label style="font-size:10px; font-style: italic;">' . $enf_pro->reubicacion . '</label>
   </td>
   </tr>';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Antecedentes Familiares</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Enfermedad</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Presente</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Parentesco</label>
   </th>

   </tr>
   ';

        foreach ($ant_familiares as $ant_familiar) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_familiar->enfermedad . '</label>
   </td>
   
   <td><label style="font-size:10px; font-style: italic;">' . $ant_familiar->presente . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic;">' . $ant_familiar->parentesco . '</label>
   </td>

   </tr>
   ';

        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Antecedentes Personales</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Patología</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Presente</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Hace Cuanto</label>
   </th>

   </tr>
   ';

        foreach ($ant_personales as $ant_personal) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_personal->patologia . '</label>
   </td>
   
   <td><label style="font-size:10px; font-style: italic;">' . $ant_personal->presente . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic;">' . $ant_personal->hace_cuanto . '</label>
   </td>

   </tr>';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Antecedentes dermatológicos</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Patología</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Presente</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Hace Cuanto</label>
   </th>

   </tr>
   ';

        foreach ($ant_dermatologicos as $ant_dermatologico) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_dermatologico->patologia . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic;">' . $ant_dermatologico->presente . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic;">' . $ant_dermatologico->hace_cuanto . '</label>
   </td>

   </tr>
   ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Antecedentes osteomusculares</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Patología</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Presente</label>
   </th>

   <th><label style="font-size:10px; font-weight: bold;">Hace Cuanto</label>
   </th>

   </tr>  
   ';

        foreach ($ant_osteomusculares as $ant_osteomuscular) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_osteomuscular->patologia . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic;">' . $ant_osteomuscular->presente . '</label>
   </td>

   <td><label style="font-size:10px; font-style: italic;">' . $ant_osteomuscular->hace_cuanto . '</label>
   </td>

   </tr>
   ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Antecedentes quirúrgicos</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Antecedente</label>
   </th>
   <th><label style="font-size:10px; font-weight: bold;">Fecha</label>
   </th>

   </tr>
   ';

        foreach ($ant_quirurgicos as $ant_qui) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_qui->antecedente . '</label>
   </td>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_qui->fecha . '</label>
   </td>

   </tr>
   ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Antecedentes traumáticos</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Lesión</label></th>
   <th><label style="font-size:10px; font-weight: bold;">Causa</label></th>
   </tr>
   ';

        foreach ($ant_traumaticos as $ant_trau) {
            $html .= '
    <tr>
    <td><label style="font-size:10px; font-style: italic;">' . $ant_trau->antecedente . '</label>
    </td>
    <td><label style="font-size:10px; font-style: italic;">' . $ant_trau->causa . '</label>
    </td>
    </tr>';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:13px; font-weight: bold;">Secuelas</label>
   ' . $historia_ocupacional->secuelas

            . '<label style="font-size:13px; font-weight: bold;">Describa los medicamentos, drogas o vitaminas que toma en la actualidad</label>'
            . $historia_ocupacional->mdv_act

            . '<label style="font-size:12px; font-weight: bold;">Es usted alergico a algún medicamento, sustancia o comida?</label><p>'
            . $historia_ocupacional->alergico_algun_medicamento

            . '</p><label style="font-size:12px; font-weight: bold;">Inmunizaciones</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Inmunización</label></th>
   <th><label style="font-size:10px; font-weight: bold;">Fecha</label></th>
   <th><label style="font-size:10px; font-weight: bold;">Dosis</label></th>
   </tr>';

        foreach ($inmunizaciones as $inmunizacion) {
            $html .= '
    <tr>
    <td><label style="font-size:10px; font-style: italic;">' . $inmunizacion->inmunizacion . '</label> </td>
    <td><label style="font-size:10px; font-style: italic;">' . $inmunizacion->fecha . '</label> </td>
    <td><label style="font-size:10px; font-style: italic;">' . $inmunizacion->dosis . '</label> </td>
    </tr>
    ';

        }

        if (!$ant_gineco->isEmpty()) {
            $html .= '<label style="font-size:12px; font-weight: bold;">Antecedentes gineco-obstétricos</label>
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Menarquia:</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->menarquia . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Ciclos:</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->ciclos . '</label></td>

   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">FUM:</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->fecha_ultima_m . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Dismenorrea:</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->dismenorrea . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Paridad</label></td>
   <td></td>
   <td></td>
   <td></td>
   <td></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">G:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_g . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">P:</label>
    <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_p . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">A:</label>
    <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_a . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">C:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_c . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">E:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_e . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">M:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_m . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">V:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->paridad_v . '</label></td>
   <td></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">FPP:</label> <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->FPP . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Planificación:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->planificacion . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Método:</label><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->metodo . '</label></td>
   <td></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Citología reciente:</label>
   <label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->citologia_reciente . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha:</label><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->fecha_citologia . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Resultado:</label><label style="font-size:10px; font-style: italic;">' . $ant_gineco[0]->resultado_citologia . '</label></td>
   <td></td>
   </tr>
   </table>
   </div>';
        } else {
            $html .= '<label style="font-size:12px; font-weight: bold;">Antecedentes gineco-obstétricos</label><br>
       <label style="font-size:10px; font-style: italic;">No se registraron datos</label><br>';
        }

        $html .= '

   <label style="font-size:12px; font-weight: bold;">Habitos tóxicos</label>
 
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fumador:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->fumador . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Ex fumador:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->exfumador . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Años de suspensión del hábito:</label><label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->asuspension_h . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Años de fumador:</label><label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->adefumador . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Cigarrillos por día:</label><label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->cigpordia . '</label></td>
   <td></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Toma licor habitualmente?:</label>
    <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->tomalicorh . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Frecuencia del hábito:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->frecuenciahab . '</label></td>
   <td></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Tipo de licor:</label><label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->tipolicor . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">¿Ha tenido problemas con la bebida?:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->problemas_con_bebida . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">¿Exbebedor?:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->exbebedor . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Años suspendido:</label><label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->exasuspendido . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">¿Otros tóxicos?:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->otros_toxicos . '</label></td>
   <td></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Medicamentos regularmente:</label>
   <label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->med_regularmente . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Cual:</label><label style="font-size:10px; font-style: italic;">' . $habitos_toxicos[0]->cual_med_reg . '</label></td>
   <td></td>
   </tr>
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Factores predisponentes para riesgo ergonómico</label>
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Sedentarismo:</label>
   <label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->sedentarismo . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Deportes de raqueta(Tenis/Squash):</label>
   <label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->deportes_de_raqueta . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Sobrepeso:</label>
   <label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->sobrepeso . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Manualidades(Pintar, bordar, tejer, instrumento musical, bricolaje, etc):</label>
   <label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->manualidades . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Deporte:</label>
   <label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->deporte . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Cual:</label><label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->cual_deporte . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Pasatiempos(Jardinería, coser, tejer, pintar, instrumento de música, manualidades, oficios domésticos):</label>
    <label style="font-size:10px; font-style: italic;">' . $factores_predisponentes[0]->pasatiempos . '</label></td>
   </tr>
   </table>
   </div>

   <label style="font-size:12px; font-weight: bold;">Revisión por sistemas y enfermedad actual</label>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Sistema nervioso:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->sistema_nervioso . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Insomnio:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->insonmio . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Dificultad para concentrarse:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->d_para_concentrarse . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Desinteres para realizar actividades:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->desp_realizar_act . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Tensión muscular, cansancio, miedo:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->tmuscular . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Ojos, epifora, esfuerzo visual:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->ojos . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">ORL:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->orl . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Respiratorio(Disnea, hiperventilación, etc):</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->respiratorio . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Cardiovascular:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->cardiovascular . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Digestivo:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->digestivo . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Piel y Faneras:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->piel_faneras . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Musculoesquelético:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->musculoesqueletico . '</label>
   </div>

   <div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Genito urinario:</label><br>
   <label style="font-size:10px; font-style: italic;">' . $revision_sistemas[0]->genito_urinario . '</label>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">En el ultimo año ha tenido dolor o molestia a nivel de:</label><br>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-weight: bold;">Patología</label></th>
   <th><label style="font-size:10px; font-weight: bold;">Respuesta</label></th>
   <th><label style="font-size:10px; font-weight: bold;">Descripción</label></th>
   </tr>
   ';

        foreach ($molestias_u_a as $mol_u_a) {
            $html .= '<tr>
    <td><label style="font-size:10px; font-style: italic;">' . $mol_u_a->patologia . '</label></td>
    <td><label style="font-size:10px; font-style: italic;">' . $mol_u_a->presente . '</label></td>
    <td><label style="font-size:10px; font-style: italic;">' . $mol_u_a->descripcion . '</label></td>
    </tr>';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Seguimiento y evolución de casos</label>'
            . $historia_ocupacional->seg_evolucion_casos
            . '<label style="font-size:10px; font-style: italic; font-weight: bold;">Examen físico</label>
   
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Constitución:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->constitucion . '</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Dominancia:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->dominancia . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">T.A.:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->TA . '</label>
   <label style="font-size:10px; font-style: italic;">mmHg</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">F.C.:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->frec_cardiaca . '</label>
   <label style="font-size:10px; font-style: italic;">x MIN</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Peso:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->Peso . '</label>
   <label style="font-size:10px; font-style: italic;">Kg</label></td>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Talla:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->Talla . '</label>
   <label style="font-size:10px; font-style: italic;">mts</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">IMC:</label>
   <label style="font-size:10px; font-style: italic;">' . $examen_fisico[0]->IMC . '</label></td>
   <td></td>
   </tr>
   </table>
   </div>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Organo</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>';

        foreach ($detalles_examen_fisico as $det_examen) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $det_examen->organo . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $det_examen->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $det_examen->hallazgo . '</label></td>
   </tr>
   ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Descripción y ampliación de hallazgos</label>'
            . $historia_ocupacional->desc_ampl_hallazgos_ef
            . '
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Inspección</label><br>
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Organo</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($inspeccion as $insp) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $insp->inspeccion . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $insp->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $insp->hallazgo . '</label></td>
   </tr>
   ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Palpación</label>
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Palpación</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($palpacion as $palp) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $palp->palpacion . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $palp->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $palp->hallazgo . '</label></td>
   </tr>';
        }

        $html .= '
   </table>
   </div>

    <label style="font-size:10px; font-style: italic; font-weight: bold;">Movilidad</label>
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Movilidad</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($movilidad as $movil) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $movil->movilidad . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $movil->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $movil->hallazgo . '</label></td>
   </tr>
   ';
        }

        $html .= '
   </table> 
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Codos, mano, muñeca</label><br>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Nuca:</label>
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Descripción</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($nuca as $nu) {
            $html .= '
    <tr>
    <td><label style="font-size:10px; font-style: italic;">' . $nu->descripcion . '</label></td>
    <td><label style="font-size:10px; font-style: italic;">' . $nu->estado . '</label></td>
    <td><label style="font-size:10px; font-style: italic;">' . $nu->hallazgo . '</label></td>
    </tr>
    ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Hombros:</label>
   
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Descripción</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($hombros as $hom) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $hom->descripcion . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $hom->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $hom->hallazgo . '</label></td>
   </tr>
   ';
        }

        $html .= '
   </table>
   </div>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Codos, mano, muñeca:</label>
   
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Descripción</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($cod_mano_mun as $cmm) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $cmm->descripcion . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $cmm->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $cmm->hallazgo . '</label></td>
   </tr>
   ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Descripción y ampliación de hallazgos:</label>' . $historia_ocupacional->desc_ampl_hallazgos_cv . '
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Evaluación del estado mental:</label>
   
   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Proceso</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Estado</label></th>
   <th><label style="font-size:10px; font-style: italic; font-weight: bold;">Hallazgo</label></th>
   </tr>
   ';

        foreach ($pro_ev as $pro) {
            $html .= '
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $pro->proceso . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $pro->estado . '</label></td>
   <td><label style="font-size:10px; font-style: italic;">' . $pro->hallazgo . '</label></td>
   </tr>
   ';
        }

        $html .= '
    </table> 
    </div>

    <label style="font-size:10px; font-style: italic; font-weight: bold;">Audiometría:</label>' . $historia_ocupacional->resultados_audiometria . '

    <label style="font-size:10px; font-style: italic; font-weight: bold;">Examen visual:</label>' . $historia_ocupacional->examen_visual . '

    <label style="font-size:10px; font-style: italic; font-weight: bold;">¿Usa lentes?:</label>' . $historia_ocupacional->usa_lentes . '
   <br>
   <label style="font-size:10px; font-style: italic; font-weight: bold;">Otros paraclinicos:</label>

   <div>
   <table style="width: 100%;" border="0">
   ';


        foreach ($otros_paraclinicos as $otro_par) {
            $html .= '
    <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Nombre del paraclínico:</label>
    <label style="font-size:10px; font-style: italic;">' . $otro_par->nombre_paraclinico . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Fecha:</label>
    <label style="font-size:10px; font-style: italic;">' . $otro_par->fecha . '</label></td>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Resultado:</label>
    <label style="font-size:10px; font-style: italic;">' . $otro_par->resultado . '</label></td>
    </tr>
    ';
        }


        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Diagnósticos:</label>
   
   <div>
   <table style="width: 100%;" border="0">
   ';

        foreach ($diagnosticos as $diagnostico) {
            $html .= '
    <tr>
    <td><label style="font-size:10px; font-style: italic; font-weight: bold;">' . $diagnostico->diagnostico . '</label></td>
    </tr>
    ';
        }

        $html .= '
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Concepto de aptitud:</label>

   <div>
   <table style="width: 100%;" border="0">
   <tr>
   <td><label style="font-size:10px; font-style: italic;">' . $concepto_aptitud[0]->aptitud . '</label></td>
   </tr>
   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Examen de egreso normal:</label>
   <label style="font-size:10px; font-style: italic;">' . $concepto_aptitud[0]->examen_egreso_normal . '</label></td>
   </tr>

   <tr>
   <td><label style="font-size:10px; font-style: italic; font-weight: bold;">Examen periódico satisfactorio:</label>
   <label style="font-size:10px; font-style: italic;">' . $concepto_aptitud[0]->examen_p_satisfactorio . '</label></td>
   </tr>
   </table>
   </div>

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Recomendaciones médicas:</label>' . $historia_ocupacional->recomendaciones_medicas . '

   <label style="font-size:10px; font-style: italic; font-weight: bold;">Ingreso PVESO:</label>

   <div>
   <table style="width: 100%;" border="0">
   ';

        foreach ($ingreso_pveso as $ingr_pv) {
            $html .= '
    <tr>
    <td><label style="font-size:10px;">' . $ingr_pv->tipo . '</label></td>
    </tr>
    ';
        }

        $html .= '
   </table>
   </div>
   ';

        PDF::WriteHTML($html);

        $y = PDF::GetY() + 45;
        if( $y >= 252){
            PDF::AddPage();
        }

        $txt = 'DECLARO QUE TODOS LOS DATOS SUMINISTRADOS Y CONSIDERADOS EN ESTE DOCUMENTO SON CIERTOS Y NO HE OCULTADO NINGUNA INFORMACIÓN SOBRE LOS ANTECEDENTES LABORALES, MÉDICOS O PATOLOGICOS EN LA ELABORACIÓN DE LA PRESENTE HISTORIA CLINICA OCUPACIONAL. IGUALMENTE AUTORIZO PARA QUE ESTA HISTORIA CLINICA QUEDE EN CUSTODIA DEL MEDICO DE LA EMPRESA Y PUEDA SER REVISADA POR EL DEPARTAMENTO DE RECURSOS HUMANOS CUANDO SE REQUIERA.';


        PDF::MultiCell(0, 20, $txt, 1, 'C', 0, 1, '', '', true, 0, false, true, 39);

        $txt = '
   ______________________________________
   Firma, sello y registro del médico';
        PDF::MultiCell(95, 10, $txt, 1, 'C', 0, 0, '', '', true, 0, false, true, 20);

        $txt = '
   ______________________________________
   Firma del trabajador';

        PDF::MultiCell(95, 10, $txt, 1, 'C', 0, 1, '', '', true, 0, false, true, 20);


        PDF::Output('Historia Clinica Ocupacional.pdf', 'I');


    }

    public function generarPDFConceptoAptitud(Request $request, $id)
    {
        ini_set('max_execution_time', 0);

        $historia_ocupacional = tab_h_cli_ocupacional::where('id', $id)->first();
        $concepto_aptitud = tab_concepto_de_aptitud::where('historia_clinica', $id)->get();

        $trabajador = tab_trabajador::where('id', $historia_ocupacional->trabajador)->first();

        //Obtener ruta imagen logo uceva
        $path = public_path('imgs/siacu_logo.svg');

        PDF::SetTitle('Certificado ocupacional');
        PDF::AddPage();

        $html = '
  <div>
  <table style="width: 100%;" border="1">
  <tr> 
  <td rowspan="2"> <img src="' . $path . '" style="width:110px;height:50px;"></td>
  <td colspan="2"><h4 style="text-align: center;">BIENESTAR INSTITUCIONAL Y GESTIÓN HUMANA</h4></td>
  <td><h5>Código: 1025-39.1-108-F</h5></td>
  </tr>
    
  <tr>
  <td colspan="2"><h4 style="text-align: center;">CERTIFICADO OCUPACIONAL</h4></td>
  <td><h5>Versión:01</h5></td>
  </tr>
  </table>
  </div>

  <div>
    <label style="font-size:12px; font-weight: bold;">Fecha: </label><label style="font-size:12px;">' . date('d/n/Y') . '</label><br><br>
    <label style="font-size:12px; font-weight: bold;">Nombre trabajador: </label><label style="font-size:12px;">' . $trabajador->nombres . ' ' . $trabajador->apellidos . '</label><br><br>
    <label style="font-size:12px; font-weight: bold;">C.C.: </label><label style="font-size:12px;">' . $trabajador->numero_de_identificacion . '</label><br><br>
    <label style="font-size:12px; font-weight: bold;">Empresa: __________________________________ </label><label style="font-weight: bold; font-size:12px;">Cargo: ______________________________</label>
  </div>
  <div>
  <div style="text-align: center; font-size:15px; font-style: italic; font-weight: bold;">CONCEPTO DE APTITUD</div>
    </div>
  <div>
  <table style="width: 100%;" border="0">
  <tr>
  <td><label style="font-size:12px; font-weight: bold;">Apto: [ ] </label> </td>
  <td><label style="font-size:12px; font-weight: bold;">Apto con Restricciones: [ ]</label></td>
  </tr>

  <tr>
  <td><label style="font-size:12px; font-weight: bold;">NO Apto: [ ] </label></td> 
  <td><label style="font-size:12px; font-weight: bold;">Aplazado: [ ]</label></td>
  </tr>
  </table>
  </div>
  <div>
  <div style="text-align: center; font-size:15px; font-style: italic; font-weight: bold;">APTO PARA MANIPULAR ALIMENTOS</div>
    </div>
<div>
  <label style="font-size:12px; font-weight: bold;">El Examen de egrso no representa patologia profesional o PCL: [ ] </label><br><br>

  <label style="font-size:12px; font-weight: bold;">Observaciones: _________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________________</label>

  <br><br><br><br><br>

  <label style="font-size:12px; font-weight: bold;">Firma del Trabajador: _______________________________________________
  </label>

  <br><br><br><br><br>

  <label style="font-size:12px; font-weight: bold;">Firma y sello medico: ______________________________________________
  </label>

  <br><br>

  </div>

  



  ';


        PDF::WriteHTML($html);


        $txt = 'DANDO CUMPLIMIENTO A LA RESOLUCIÓN 2346/07 Y RESOLUCIÓN 1918/09 SE EXPIDE ESTE CERTIFICADO YA QUE LA GUARDA Y CUSTODIA DE LAS HISTORIAS MÉDICAS OCUPACIONALES DEBEN ESTAR A CARGO DEL MÉDICO OCUPACIONAL DE LA EMPRESA O EN SU DEFECTO EN LA EPS A LA CUAL ESTE AFILIADO EL TRABAJADOR.';


        PDF::MultiCell(0, 20, $txt, 0, 'C', 0, 1, '', '', true, 0, false, true, 30);

        PDF::Output('concepto_de_aptitud.pdf', 'I');

    }


}
