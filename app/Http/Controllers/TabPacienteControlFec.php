<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Redirect;
use Validate;
use DB;

use App\Models\tab_paciente_control_fec;

class TabPacienteControlFec extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function showForm(){
        return view('historia seg control fecundidad.register_datos_persona');
    }

    public function new(Request $request){

    	$request->validate([
    		'numero_de_identificacion' =>  'required',
            'nombresyapellidos'        =>  'required',
            'sexo'                     =>  'required',
            'direccion'                =>  'required',
            'telefono'                 =>  'required',
            'fecha_de_nacimiento'      =>  'required',
            'estado_civil'             =>  'required',
            'uso_ant_previo'           =>  'required',
            'numero_hijos_vivos'       =>  'required'
        ]);

        $date   =   date_create($request->fecha_de_nacimiento);
        $date   =   date_format($date,'Y-m-d');

        $resultado      =   tab_paciente_control_fec::create([
        	'numero_de_identificacion' =>  $request->numero_de_identificacion,
            'nombres_apellidos'        =>  $request->nombresyapellidos,
            'sexo'                     =>  $request->sexo,
            'direccion'                =>  $request->direccion,
            'telefono'                 =>  $request->telefono,
            'fecha_de_nacimiento'      =>  $date,
            'estado_civil'             =>  $request->estado_civil,
            'uso_antic_previo'         =>  $request->uso_ant_previo,
            'cual_antic_previo'        =>  $request->cual_anticonceptivo,
            'numero_hijos_vivos'       =>  $request->numero_hijos_vivos
        ]);
      //echo("Ejecuto");
      //return view('historia seg control fecuncidad.register_datos_persona');
      return Redirect::to(route('view_pacientes_controlfec'));

    }

    public function list(){
      return view('historia seg control fecundidad.list_pac_control');
    }

    public function getPacientesControlAjax(Request $request){
    	ini_set('max_execution_time',0);

        $f_i = $request->fecha_i;
        $f_f = $request->fecha_f;
    
        $sEcho = $request->draw;
        $iDisplayStart = $request->start;
        $iDisplayLength = $request->length;

        //Ordering
        $iSortCol_0 = $request->iSortCol_0;
        $iSortingCols = $request->iSortingCols;
        $aColumns = array("numero_de_identificacion","nombres_apellidos","telefono","fecha_de_nacimiento","estado_civil","uso_antic_previo");
        
        $sWhere = '';
        
        //Searching
        $sSearch = $request->search['value'];        
        $OrderD = $request->order['0']['dir'];

        //Ordering
        $sByColumn = $request->order['0']['column'];
        if($sByColumn == 0){

            $bY="numero_de_identificacion";

        }elseif($sByColumn == 1){

            $bY="nombres_apellidos";

        }elseif($sByColumn == 2){

            $bY="telefono";

        }elseif($sByColumn == 3){

            $bY="fecha_de_nacimiento";

        }elseif($sByColumn == 4){

            $bY="estado_civil";

        }elseif($sByColumn == 5){

            $bY="uso_antic_previo";

        }

        $sOrder = "ORDER BY ".$bY." ".$OrderD;
        
        
        if ($sSearch != null && $sSearch != "")
        {
            if ($sWhere == '') {
                $sWhere .= 'WHERE (';
            } else {
                $sWhere .= 'AND (';
            }

            for ($i = 0; $i < count($aColumns); $i++) {
                $sWhere .= $aColumns[$i] . ' LIKE "%' . $sSearch . '%" OR ';
            }

            $sWhere = substr_replace($sWhere, '', -3);
            $sWhere .= ')';
        }

        $pacientes_control = DB::select("SELECT numero_de_identificacion,nombres_apellidos,telefono,fecha_de_nacimiento,estado_civil,uso_antic_previo FROM tab_paciente_control_fecs
                        ". $sWhere . "
                        ". $sOrder . "
                        LIMIT ". $iDisplayLength . " OFFSET " . $iDisplayStart . "");


        $pacientes_control2 = DB::select("SELECT numero_de_identificacion,nombres_apellidos,telefono,fecha_de_nacimiento,estado_civil,uso_antic_previo FROM tab_paciente_control_fecs
                        ". $sWhere . "
                        ". $sOrder);

        $filteredPacientes_control = count($pacientes_control);
        $totalPacientes_control    = count($pacientes_control2);

        $output = array(
            "draw"            => $sEcho,
            "recordsTotal"    => $filteredPacientes_control,
            "recordsFiltered" => $totalPacientes_control,
            "data"            => array(),
        );

        foreach ($pacientes_control as $inv)
        {
            $options    =   '<div class="btn-toolbar"><a class="btn btn-success" href="#">'
                                .'<i class="fa fa-edit"></i>'
                              .'</a>'
                              .'<a class="btn btn-success"'
                              .'href="'.route('new_historia_clinica_seg', ['numero_de_identificacion'=>$inv->numero_de_identificacion]).'">'
                                .'<i class="fa fa-medkit"></i>'
                              .'</a></div></div>';

            $row = array();          

            $row[] = $inv->numero_de_identificacion;
            $row[] = $inv->nombres_apellidos;
            $row[] = $inv->telefono;
            $row[] = $inv->fecha_de_nacimiento;
            $row[] = $inv->estado_civil;
            $row[] = $inv->uso_antic_previo;
            $row[] = $options;

            $output['data'][] = $row;

        }

        return response()->json($output);

    }


}
