<?php

namespace App\Http\Controllers;

use App\Models\tab_atencion_en_salud;
use App\Models\tab_entrega_medicamento;
use App\Models\tab_glucometria;
use App\Models\tab_inyectologia;
use App\Models\tab_tpresion_arterial;
use Illuminate\Http\Request;
use DB;
use App\Models\tab_medicamento;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $medicamentos_existencia = tab_medicamento::orderBy('existencia', 'asc')->take(8)->get();
        $glucometrias = tab_glucometria::all()->count();
        $atencion_salud = tab_atencion_en_salud::all()->count();
        $presion_arterial = tab_tpresion_arterial::all()->count();
        $inyectologias = tab_inyectologia::all()->count();
        $medicamento = tab_entrega_medicamento::all()->count();

        return view('home', compact(
            'glucometrias',
            'medicamentos_existencia',
            'atencion_salud',
            'presion_arterial',
            'inyectologias',
            'medicamento'
        ));
    }
}
