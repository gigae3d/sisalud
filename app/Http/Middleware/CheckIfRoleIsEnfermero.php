<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class CheckIfRoleIsEnfermero
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(Auth::user()->role != 'Enfermero' && Auth::user()->role != 'Admin'){
            return redirect('/error_de_permisos');
        }

        return $next($request);
    }
}
