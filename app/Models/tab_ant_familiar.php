<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ant_familiar extends Model
{
    public $fillable = [
    	'historia_clinica',
    	'enfermedad',
        'presente',
        'parentesco'
    ];
}
