<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_esquema_vacuna extends Model
{
    public $fillable = [
    'est_control_v',
    'vacuna',
    'n_dosis_aplicadas',
    'fecha_ultima_dosis'
    ];

}
