<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ant_trau extends Model
{
    public $fillable = [
    	 'historia_clinica',
         'antecedente',
         'causa'
    ];

}
