<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_inyectologia extends Model
{
	public $fillable = [
	'numero_de_identificacion',	
    'medicamento',
    'fecha',
    'orden_medica'
    ];
}
