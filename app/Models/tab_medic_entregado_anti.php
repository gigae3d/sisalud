<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_medic_entregado_anti extends Model
{
    public $fillable = [
    'reg_entrega_medicamento',
    'medicamento',
    'cantidad'
    ];
}
