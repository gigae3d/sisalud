<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_movilidad extends Model
{
	public $fillable = [
		    'historia_clinica',
            'movilidad',
            'estado',     
            'hallazgo'
    ];
}
