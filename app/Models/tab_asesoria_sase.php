<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_asesoria_sase extends Model
{
    public $fillable =[
    'numero_de_identificacion',
    'fecha',
    'asesoria'
    ];
}
