<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_estudiante extends Model
{
    public $fillable = [
    	'numero_de_identificacion',
        'programa',
        'id_programa',
        'semestre'
    ];
}
