<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_informacion_ocupacional extends Model
{
    public $fillable = [
        'historia_clinica',
        'antiguedad_en_empresa',
        'nombre_cargo_actual',
        'antiguedad_en_cargo',
        'seccion',
        'turno',
        'descripcion_del_cargo',
        'equipos_herramientas_utilizadas',
        'materias_primas_usadas'
    ];

    public function acciones(){
        return $this->hasMany('App\Models\tab_realiza_accion', 'informacion_ocupacional');
    }

    public function actividades(){
        return $this->hasMany('App\Models\tab_realiza_actividad', 'informacion_ocupacional');
    }

}
