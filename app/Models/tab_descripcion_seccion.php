<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_descripcion_seccion extends Model
{
    public $fillable = [
            'descripcion_secuela',
            'descripcion_mdv',
            'alergia',
            'seguimiento_ev_caso',
            'descripcion_amh_exf',
            'descripcion_amh_cv',
            'audiometria_res',
            'examen_visual_res',
            'usa_lentes',
            'recomendacion_medica'
    ];
}
