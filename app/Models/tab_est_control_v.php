<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_est_control_v extends Model
{
    public $fillable = [
    'usuario',
    'nombres',
    'apellidos', 
    'pregrado', 
    'semestre',
    'fecha_diligenciamiento',
    'numero_de_identificacion', 
    'tipo', 
    'd_expedida_en',
    'fecha_de_nacimiento',
    'lugar_de_nacimiento',
    'genero',
    'grupo_sanguineo',
    'RH',
    'direccion',
    'barrio',
    'ciudad',
    'telefono_fijo',
    'celular', 
    'correo_electronico',
    'enfermedades_previas',
    'antecedentes_alergicos',
    'medicamentos_tomados',
    'pr_ppd_resultado', 
    'pr_ppd_fecha',
    'pr_ppd_laboratorio',
    'EPS',
    'conoce_regl_pr', 
    'eme_nomyape', 
    'eme_direccion',
    'eme_telefono'
    ];

    public function esquema_vacunas(){
        return $this->hasMany('App\Models\tab_esquema_vacuna', 'est_control_v');
    }

    public function titulacion_anticuerpos(){
    return $this->hasMany('App\Models\tab_titulacion_antic', 'est_control_v');
    }
}
