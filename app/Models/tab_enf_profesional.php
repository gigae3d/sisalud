<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_enf_profesional extends Model
{
    public $fillable = [
    	'historia_clinica',
        'fecha',
        'empresa',
        'diagnostico',
        'indemnizacion',
        'reubicacion'
    ];
}
