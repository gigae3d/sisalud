<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ant_acci_trabajo extends Model
{
    public $fillable = [
    	'historia_clinica',
        'fecha',
        'empresa',
        'causa',
        'tipo_de_lesion',
        'parte_del_cuerpo',
        'dias_de_incapacidad',
        'secuelas'
    ];
}
