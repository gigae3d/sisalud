<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_glucometria extends Model
{
    public $fillable = [
    'numero_de_identificacion',
    'equipo_medico',
    'usuario',
    'fecha',
    'patologia_base',
    'resultado',
    'tirillas',
    'lancetas',
    'guantes'
    ];
}
