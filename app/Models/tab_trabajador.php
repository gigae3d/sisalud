<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_trabajador extends Model
{
    public $fillable = [
    'numero_de_identificacion',
            'nombres', 
            'apellidos',                  
            'fecha_nacimiento',             
            'genero',                       
            'edad',                   
            'lugar_nacimiento',             
            'procedencia',                   
            'residencia',                    
            'escolaridad',                   
            'prof_oficio',                   
            'estado_civil',                 
            'eps',                           
            'arp_anterior',                
            'pensiones'
    ];
}
