<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_hab_toxico extends Model
{
     public $fillable = [
            'historia_clinica',
            'fumador',              
            'exfumador',            
            'asuspension_h',        
            'adefumador',
            'cigpordia',
            'tomalicorh',
            'frecuenciahab',
            'tipolicor',
            'problemas_con_bebida',
            'exbebedor',
            'exasuspendido',
            'otros_toxicos',
            'med_regularmente',
            'cual_med_reg'
    ];
}
