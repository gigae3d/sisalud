<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_docente extends Model
{  
    public $fillable = [
        'numero_de_identificacion',
        'facultad'
    ];

}
