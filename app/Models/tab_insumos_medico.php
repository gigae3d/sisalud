<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_insumos_medico extends Model
{
    public $fillable = [
        'nombre',
        'marca',
        'existencia',
        'lote',
        'fecha_de_vencimiento',
        'Registro_INVIMA'
    ];
}
