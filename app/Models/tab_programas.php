<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_programas extends Model
{
    public $fillable = [
        'name',
        'status',
        ];
}
