<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_exam_fi extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'constitucion',   
            'dominancia',    
            'TA',              
            'frec_cardiaca',
            'Peso',
            'Talla',
            'IMC'
    ];
}
