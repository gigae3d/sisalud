<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_inmunizacion extends Model
{
    public $fillable = [
    	'historia_clinica',
        'inmunizacion',
        'fecha',
        'dosis'
    ];
}
