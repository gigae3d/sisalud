<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_paraclinico extends Model
{
    public $fillable = [
        'historia_clinica',
        'nombre_paraclinico',
        'fecha',
        'resultado'          
    ];
}
