<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_exp_factores_riesgo extends Model
{
    public $fillable = [
    	'historia_clinica',
        'empresa',
        'estado_laboral',
        'cargo',
        'tiempo',
        'EPP'
    ];
}
