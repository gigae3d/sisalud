<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ingreso_pveso extends Model
{
	public $fillable = [
    'concepto_de_aptitud',
    'tipo'
    ];
}
