<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_insumo_atencion_salud extends Model
{
    public $fillable = [
            'atencion_en_salud',
            'insumo_medico',
            'cantidad'
    ];

}
