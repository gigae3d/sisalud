<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_inspeccion extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'inspeccion', 
            'estado',    
            'hallazgo'
    ];
}
