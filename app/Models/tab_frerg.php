<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_frerg extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'sedentarismo', 
            'deportes_de_raqueta',  
            'sobrepeso',             
            'manualidades',       
            'deporte', 
            'cual_deporte', 
            'pasatiempos'
    ];
}
