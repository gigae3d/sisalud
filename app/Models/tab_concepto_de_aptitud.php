<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_concepto_de_aptitud extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'aptitud',
            'examen_egreso_normal',       
            'examen_p_satisfactorio'
    ];

    public function ingreso_pveso(){
      return $this->hasMany('App\Models\tab_ingreso_pveso','concepto_de_aptitud');
    }
}
