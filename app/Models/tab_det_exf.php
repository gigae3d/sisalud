<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_det_exf extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'organo',      
            'estado',       
            'hallazgo' 
    ];
}
