<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_titulacion_antic extends Model
{
    public $fillable = [
    'est_control_v',
    'anticuerpos',
    'resultado', 
    'laboratorio',
    'fecha'
    ];
}
