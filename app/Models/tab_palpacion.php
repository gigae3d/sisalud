<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_palpacion extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'palpacion',
            'estado',     
            'hallazgo'
    ];
}
