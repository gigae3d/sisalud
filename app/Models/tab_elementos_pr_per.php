<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_elementos_pr_per extends Model
{
    public $fillable = [
    	'historia_clinica',
        'en_cargo_empresa_ant',
        'examen_periodico'
    ];

    public function elementos_usados(){
        return $this->hasMany('App\Models\tab_elem_usados_prt', 'elem_pro_per');
    }
}
