<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_mensajes_cv extends Model
{
    
    public $fillable = [
            'destinatario',
            'remitente',
            'mensaje'    
    ];

}
