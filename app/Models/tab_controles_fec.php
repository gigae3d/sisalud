<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_controles_fec extends Model
{
    public $fillable = [
    'historia_control_fec',
    'fecha',
    'peso', 
    'TA',
    'sangrado_intermenstrual', 
    'efectos_secundarios',
    'falla_metodo',
    'signo_infeccion_pelvica', 
    'cambio_anticonceptivo',
    'anexos', 
    'observaciones'
    ];
}
