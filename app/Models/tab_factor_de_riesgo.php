<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_factor_de_riesgo extends Model
{
    public $fillable = [
        'exp_factor_riesgo',
        'tipo'
    ];
}
