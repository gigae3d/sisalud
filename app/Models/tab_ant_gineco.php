<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ant_gineco extends Model
{
    public $fillable = [
         'historia_clinica',
         'menarquia',     
         'ciclos',           
         'fecha_ultima_m',      
         'dismenorrea',      
         'paridad_g',           
         'paridad_p',           
         'paridad_a',          
         'paridad_c',           
         'paridad_e',           
         'paridad_m',           
         'paridad_v',           
         'FPP',               
         'planificacion',       
         'metodo',              
         'citologia_reciente', 
         'fecha_citologia',   
         'resultado_citologia'
    ];
}
