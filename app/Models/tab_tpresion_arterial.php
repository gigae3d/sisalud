<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_tpresion_arterial extends Model
{
    public $fillable = [
    'numero_de_identificacion',
    'equipo_medico',
    'usuario',
    'fecha',
    'tipo',
    'posicion',
    'resultado'
    ];

    public function afinamientos(){
        return $this->hasMany('App\Models\tab_afinamiento_toma_presion', 'toma_presion_arterial');
    }
}
