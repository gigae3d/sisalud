<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_medic_planific extends Model
{
    public $fillable = [
        'nombre',
        'laboratorio',
        'fecha_vencimiento',
        'existencia',
        'presentacion',
        'lote',
        'invima',
        'observaciones',
    ];
}
