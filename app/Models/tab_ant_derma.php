<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ant_derma extends Model
{
    public $fillable = [
    	'historia_clinica',
        'patologia',
        'presente',
        'hace_cuanto'
    ];
}
