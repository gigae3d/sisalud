<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//Modelo tabla citas para medica Ana Carolina Garcia

class tab_cita extends Model
{
    public $fillable = [
            'fecha',
            'hora', 
            'numero_de_identificacion',
            'motivo_consulta',
            'medico'
    ];
}
