<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_realiza_accion extends Model
{
    public $fillable = [
    	'informacion_ocupacional',
        'tipo'
    ];
}
