<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_historia_control_fec extends Model
{
    public $fillable = [
    'paciente_control_fec',
    'menarquia',
    'ciclos_menstruales',
    'FUM',
    'paridad_g',
    'paridad_p',
    'paridad_a',
    'edad_inicio_rel_sex',
    'FUP',
    'numero_comp_sexuales',
    'cirugia_ginecologica',
    'fecha_citologia',
    'resultado_citologia', 
    'metodo_deseado', 
    'metodo_adoptado'
    ];
}
