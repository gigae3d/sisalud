<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_h_cli_ocupacional extends Model
{
    public $fillable = [
            'trabajador',
    	    'tipo_historia',
            'fecha',
            'ciudad',
            'ha_sufrido_at',
            'ha_sufrido_ep',
            'desc_ant_osteo',
            'secuelas',
            'mdv_act',
            'alergico_algun_medicamento',
            'otras_inmunizaciones',
            'seg_evolucion_casos',
            'desc_ampl_hallazgos_ef',
            'desc_ampl_hallazgos_cv',
            'resultados_audiometria',
            'examen_visual',
            'usa_lentes',
            'recomendaciones_medicas'
    ];
}
