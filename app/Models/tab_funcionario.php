<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_funcionario extends Model
{
    public $fillable = [
        'numero_de_identificacion',
        'departamento'
    ];
}
