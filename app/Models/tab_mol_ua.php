<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_mol_ua extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'patologia',   
            'presente',       
            'descripcion'
    ];
}
