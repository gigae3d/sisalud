<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_ant_qui extends Model
{
    public $fillable = [
      'historia_clinica',
      'antecedente',
      'fecha'
    ];
}
