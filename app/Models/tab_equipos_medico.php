<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_equipos_medico extends Model
{
    public $fillable = [
    	'nombre',
        'marca',
        'modelo',
        'tipo',
        'existencia',
        'numero_de_serie'
    ];
}
