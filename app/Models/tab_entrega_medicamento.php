<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_entrega_medicamento extends Model
{
	public $fillable = [
    'numero_de_identificacion',
    'usuario',
    'medicamento',
    'fecha',
    'cantidad'
    ];
}
