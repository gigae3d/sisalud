<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_medicamento_entregado extends Model
{
    public $fillable = [
    'reg_entrega_medicamento',
    'medicamento',
    'cantidad'
    ];
}
