<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_atencion_en_salud extends Model
{
    public $fillable = [
          'numero_de_identificacion',
          'usuario',
          'equipo_medico',
          'fecha',
          'motivo_de_consulta',
          'otros_insumos_usados_as'
    ];
}
