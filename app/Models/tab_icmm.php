<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_icmm extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'descripcion',
            'estado',       
            'hallazgo'
    ];
}
