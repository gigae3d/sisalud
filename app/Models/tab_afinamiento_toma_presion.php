<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_afinamiento_toma_presion extends Model
{
    public $fillable = [
          'toma_presion_arterial',
          'postura',
          'MSD',
          'MSI'
    ];
}
