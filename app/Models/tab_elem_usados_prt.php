<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_elem_usados_prt extends Model
{
    public $fillable = [
        'elem_pro_per',
        'elemento'
    ];
}
