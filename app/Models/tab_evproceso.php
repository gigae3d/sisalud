<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_evproceso extends Model
{
    public $fillable = [
    	    'historia_clinica',
            'proceso',
            'estado',      
            'hallazgo'
    ];
}
