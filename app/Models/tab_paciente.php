<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_paciente extends Model
{
    public $fillable = [
    	'numero_de_identificacion',
        'nombres',
        'apellidos',
        'telefono',
        'tipo_de_usuario',
    ];
}
