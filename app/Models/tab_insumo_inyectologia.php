<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_insumo_inyectologia extends Model
{
    public $fillable = [
         'inyectologia',
         'insumo_medico',
         'cantidad'
    ];
}
