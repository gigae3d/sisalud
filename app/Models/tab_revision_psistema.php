<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_revision_psistema extends Model
{
    public $fillable = [
            'historia_clinica',
    	    'sistema_nervioso',
            'insonmio',
            'd_para_concentrarse', 
            'desp_realizar_act',
            'tmuscular',
            'ojos',
            'orl',
            'respiratorio',
            'cardiovascular',
            'digestivo',
            'piel_faneras',
            'musculoesqueletico',
            'genito_urinario'
    ];
}
