<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class tab_paciente_control_fec extends Model
{
    public $fillable = [
    'numero_de_identificacion',
    'nombres_apellidos',
    'sexo',
    'direccion',
    'telefono',
    'fecha_de_nacimiento',
    'estado_civil',
    'uso_antic_previo', 
    'cual_antic_previo',
    'numero_hijos_vivos'
    ];
}
