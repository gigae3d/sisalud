<?php

use Illuminate\Database\Seeder;

class SeederAdmin extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(['name'=>'Admin','email'=>'admin123@gmail.com','password'=>bcrypt('admin123'), 'status'=>'Offline','estado_usuario'=>'Habilitado','role'=>'Admin','num_records'=>0]);

        DB::table('users')->insert(['name'=>'Giovanna Gil','email'=>'ggil@uceva.edu.co','password'=>bcrypt('uceva2018'), 'status'=>'Offline','estado_usuario'=>'Habilitado','role'=>'Enfermero','num_records'=>0]);

        DB::table('users')->insert(['name'=>'Ana Carolina Garcia','email'=>'agarcia@uceva.edu.co','password'=>bcrypt('uceva2018'), 'status'=>'Offline','estado_usuario'=>'Habilitado','role'=>'Medico','num_records'=>0]);

        DB::table('users')->insert(['name'=>'Adriana Suarez','email'=>'asuarez@uceva.edu.co','password'=>bcrypt('uceva2018'), 'status'=>'Online','estado_usuario'=>'Habilitado','role'=>'UControlVacunas','num_records'=>0]);

        DB::table('users')->insert(['name'=>'Estudiante Prueba','email'=>'estudiante@uceva.edu.co','password'=>bcrypt('uceva2018'), 'status'=>'Online','estado_usuario'=>'Habilitado','role'=>'Estudiante','num_records'=>0]);

        //Añadir medicamentos
        DB::table('tab_medicamentos')->insert(['nombre'=>'Acetaminofen','laboratorio'=>'MK', 'fecha_vencimiento'=>'2018-08-22', 'existencia'=>50, 'presentacion'=>'Tabletas', 'lote'=>'621D-21', 'invima'=>'OI-4FGN', 'observaciones'=>'<p>Medicamento para el malestar general</p>']);

        DB::table('tab_medicamentos')->insert(['nombre'=>'Ibuprofeno','laboratorio'=>'MK', 'fecha_vencimiento'=>'2018-06-03', 'existencia'=>80, 'presentacion'=>'Pastas', 'lote'=>'AN31D-21', 'invima'=>'DKW-A3231','observaciones'=>'<p>Medicamento para el dolor abdominal</p>']);

        DB::table('tab_medicamentos')->insert(['nombre'=>'Buscapina','laboratorio'=>'Boehringer Ingelheim', 'fecha_vencimiento'=>'2018-06-27', 'existencia'=>100, 'presentacion'=>'Tabletas', 'lote'=>'JFHR-27', 'invima'=>'JDELMAX2','observaciones'=>'<p>Medicamento para los colicos, dolores estomacales y abdominales</p>']);

        DB::table('tab_medicamentos')->insert(['nombre'=>'Naproxeno','laboratorio'=>'Bayer', 'fecha_vencimiento'=>'2018-06-27', 'existencia'=>100, 'presentacion'=>'Tabletas', 'lote'=>'KZXWQ-82', 'invima'=>'JDELMAX2','observaciones'=>'<p>Antiinflamatorio no esteroideo</p>']);

        DB::table('tab_medicamentos')->insert(['nombre'=>'Nytax','laboratorio'=>'Genfar', 'fecha_vencimiento'=>'2018-06-27', 'existencia'=>100, 'presentacion'=>'Tabletas', 'lote'=>'OPTR-53', 'invima'=>'JDELMAX2','observaciones'=>'<p>Medicamento purgante</p>']);

        DB::table('tab_medicamentos')->insert(['nombre'=>'Cetirizina','laboratorio'=>'Boehringer Ingelheim', 'fecha_vencimiento'=>'2019-03-20', 'existencia'=>80, 'presentacion'=>'Pastillas', 'lote'=>'ERIQP-45', 'invima'=>'NU5NAS','observaciones'=>'<p>Medicamento para las alergias nasales</p>']);

        //Agregar registro prueba de historia clinica ocupacional

        /*DB::table('tab_h_cli_ocupacionals')->insert(['tipo_historia'=>'Ingreso', 'fecha'=>'2018-05-16', 'ciudad'=>'Tuluá', 'ha_sufrido_at'=>'No','ha_sufrido_ep'=>'Si', 'desc_ant_osteo'=>'<p>Se presentan varios sintomas relacionados con ...</p>','secuelas'=>'<p>Ninguna hasta el momento presentada</p>','mdv_act'=>'<p>Acetaminofen, Evely</p>','alergico_algun_medicamento'=>'Alergico al ácido acetil salisílico','otras_inmunizaciones'=>'Difteria, Sarampión','seg_evolucion_casos'=>'<p>Por el momento se est&aacute; evaluando al paciente para poder emitir un diagn&oacute;stico</p>','desc_ampl_hallazgos_ef'=>'<p>El paciente presenta al parecer conjuntivitis por lo que es sensible a la luz</p>','desc_ampl_hallazgos_cv'=>'<p>Los hallazgos fueron los siguientes ...</p>','resultados_audiometria'=>'<p>Los resultados de la audiometr&iacute;a fueron positivos</p>','examen_visual'=>'<p>Resultados positivos</p>','usa_lentes'=>'Si','recomendaciones_medicas'=>'<p>Por el momento, solo que el paciente espere a recuperarse completamente de la conjuntivitis para poder iniciar labores</p>']);

         DB::table('tab_informacion_ocupacionals')->insert(['historia_clinica'=>'1', 'antiguedad_en_empresa'=>'7','nombre_cargo_actual'=>'Desarrollador web','antiguedad_en_cargo'=>'7','seccion'=>'Tecnologías de la información','turno'=>'Diurno','descripcion_del_cargo'=>'Desarrollar aplicaciones web a la medida','equipos_herramientas_utilizadas'=>'Computador,Portatil','materias_primas_usadas'=>'Papel, lapicero']);

         DB::table('tab_realiza_accions')->insert(['informacion_ocupacional'=>'1','tipo'=>'Halar']);
         DB::table('tab_realiza_accions')->insert(['informacion_ocupacional'=>'1','tipo'=>'Teclear']);

         DB::table('tab_exp_factores_riesgos')->insert([''=>''])*/



    }
}
