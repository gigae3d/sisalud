<?php

use Illuminate\Database\Seeder;

class SeederPrograms extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0;');
        DB::table('tab_programas')->insert(['name' => 'Ingeniería de Sistemas','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Ingeniería Industrial','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Ingeniería Ambiental','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Medicina','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Enfermería','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Ingeniería Electrónica','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Administración de Empresas','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Contaduría Pública','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Comercio Internacional','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Derecho','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Licenciatura en Educación Física, Recreación y Deportes','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Licenciatura en Lenguas Extranjeras con énfasis en Inglés','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Licenciatura en Ciencias Sociales','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Ingeniería Agropecuaria','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Tecnología en Logística Empresarial','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Tecnología en Agropecuaria Ambiental','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Especialización en Derecho Constitucional','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Especialización en Gestión de Calidad y Normalización Técnica','status' => '1']);
        DB::table('tab_programas')->insert(['name' => 'Especialización en Seguridad y Salud en el Trabajo','status' => '1']);

        DB::statement('SET FOREIGN_KEY_CHECKS = 1;');
    }
}
