<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabInsumoInyectologiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_insumo_inyectologias', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('inyectologia')->unsigned();
            $table->foreign('inyectologia')->references('id')->on('tab_inyectologias');
            $table->integer('insumo_medico')->unsigned();
            $table->foreign('insumo_medico')->references('id')->on('tab_insumos_medicos');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_insumo_inyectologias');
    }
}
