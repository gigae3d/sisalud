<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAtencionEnSaludsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_atencion_en_saluds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_de_identificacion',30);
            $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
            $table->integer('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('users');
            $table->integer('equipo_medico')->unsigned();
            $table->foreign('equipo_medico')->references('id')->on('tab_equipos_medicos');
            $table->date('fecha');
            $table->longText('motivo_de_consulta');
            $table->string('otros_insumos_usados_as', 200);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_atencion_en_saluds');
    }
}
