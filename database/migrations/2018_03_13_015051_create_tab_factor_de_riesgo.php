<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabFactorDeRiesgo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_factor_de_riesgos', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('exp_factor_riesgo')->unsigned();
             $table->foreign('exp_factor_riesgo')->references('id')->on('tab_exp_factores_riesgos');
             $table->string('tipo', 80);
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_factor_de_riesgos');
    }
}
