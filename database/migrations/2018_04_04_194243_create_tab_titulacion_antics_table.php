<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabTitulacionAnticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_titulacion_antics', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('est_control_v')->unsigned();
            $table->foreign('est_control_v')->references('id')->on('tab_est_control_vs');
            $table->string('anticuerpos', 100);
            $table->string('resultado', 100)->nullable();
            $table->string('laboratorio', 100)->nullable();
            $table->date('fecha')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_titulacion_antics');
    }
}
