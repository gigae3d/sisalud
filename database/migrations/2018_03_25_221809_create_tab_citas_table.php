<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabCitasTable extends Migration
{
    //Migracion para crear la tabla de citas para la medica Ana Carolina Garcia
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_citas', function (Blueprint $table) {
            $table->increments('id');
            $table->date('fecha');
            $table->string('hora', 30);
            $table->string('numero_de_identificacion',30);
            $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
            $table->string('motivo_consulta', 200);
            $table->string('medico', 100);
            $table->timestamps();
            $table->softDeletes();
        });
        DB::statement("CREATE OR REPLACE VIEW Citas_Ana_Garcia AS
                      SELECT id, fecha, hora, numero_de_identificacion, motivo_consulta
                      FROM tab_citas
                      WHERE medico = 'Ana Carolina Garcia'");
        DB::statement("CREATE OR REPLACE VIEW Citas_Italo_Zuniga AS
                      SELECT id, fecha, hora, numero_de_identificacion, motivo_consulta
                      FROM tab_citas
                      WHERE medico = 'Italo Zuñiga'");
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //DB::statement("DROP VIEW Citas_Ana_Garcia");
        //DB::statement("DROP VIEW Citas_Italo_Zuniga");
        Schema::dropIfExists('tab_citas');
    }
}
