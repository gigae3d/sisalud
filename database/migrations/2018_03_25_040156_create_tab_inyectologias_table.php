<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabInyectologiasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_inyectologias', function (Blueprint $table) {
          $table->increments('id');
          $table->string('numero_de_identificacion',30);
          $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
          $table->integer('medicamento')->unsigned();
          $table->foreign('medicamento')->references('id')->on('tab_medicamentos');
          $table->date('fecha');
          $table->string('orden_medica', 100);
          $table->timestamps();
          $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_inyectologias');
    }
}
