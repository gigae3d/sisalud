<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabFrerg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_frergs', function (Blueprint $table) { 
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');   
            $table->string('sedentarismo', 100); 
            $table->string('deportes_de_raqueta', 100);  
            $table->string('sobrepeso', 100);             
            $table->string('manualidades', 100);       
            $table->string('deporte', 100); 
            $table->string('cual_deporte', 100)->nullable(); 
            $table->string('pasatiempos', 100); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_frergs');
    }
}
