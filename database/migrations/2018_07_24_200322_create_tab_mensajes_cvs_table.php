<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabMensajesCvsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_mensajes_cvs', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('destinatario');
            $table->foreign('destinatario')->references('id')->on('users');
            $table->unsignedInteger('remitente');
            $table->foreign('remitente')->references('id')->on('users');
            $table->text('mensaje');
            //$table->date('fecha');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_mensajes_cvs');
    }
}
