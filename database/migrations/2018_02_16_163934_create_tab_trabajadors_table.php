<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabTrabajadorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_trabajadors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_de_identificacion');
            $table->string('nombres', 100); 
            $table->string('apellidos', 100);                   
            $table->date('fecha_nacimiento');             
            $table->string('genero', 100);                       
            $table->integer('edad');                    
            $table->string('lugar_nacimiento', 100);             
            $table->string('procedencia', 100);                  
            $table->string('residencia',100);                    
            $table->string('escolaridad',100);                   
            $table->string('prof_oficio',100);                   
            $table->string('estado_civil',100);                 
            $table->string('eps',100);                           
            $table->string('arp_anterior',100);                
            $table->string('pensiones',100);                   
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_trabajadors');
    }
}
