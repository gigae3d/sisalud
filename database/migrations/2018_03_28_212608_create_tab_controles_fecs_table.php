<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabControlesFecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_controles_fecs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_control_fec')->unsigned();
            $table->foreign('historia_control_fec')->references('id')->on('tab_historia_control_fecs');
            $table->date('fecha');
            $table->string('peso', 30);
            $table->string('TA', 30);
            $table->string('sangrado_intermenstrual', 10);
            $table->string('efectos_secundarios', 10);
            $table->string('falla_metodo', 10);
            $table->string('signo_infeccion_pelvica', 10);
            $table->string('cambio_anticonceptivo', 200);
            $table->string('anexos', 20);
            $table->longText('observaciones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_controles_fecs');
    }
}
