<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabRealizaGlucometria extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_glucometrias', function (Blueprint $table) {
           $table->increments('id');
           $table->string('numero_de_identificacion',30);
           $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
           $table->integer('equipo_medico')->unsigned();
           $table->foreign('equipo_medico')->references('id')->on('tab_equipos_medicos');
           $table->integer('usuario')->unsigned();
           $table->foreign('usuario')->references('id')->on('users');
           $table->date('fecha');
           $table->string('patologia_base', 80);
           $table->string('resultado', 80);
           $table->integer('tirillas');
           $table->integer('lancetas');
           $table->integer('guantes');
           $table->timestamps();
           $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_glucometrias');
    }
}
