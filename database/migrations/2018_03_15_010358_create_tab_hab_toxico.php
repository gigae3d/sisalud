<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabHabToxico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_hab_toxicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
            $table->string('fumador', 100);              
            $table->string('exfumador', 100);            
            $table->string('asuspension_h', 100)->nullable();        
            $table->string('adefumador', 100);
            $table->string('cigpordia', 100);
            $table->string('tomalicorh', 100);
            $table->string('frecuenciahab', 100);
            $table->string('tipolicor', 100)->nullable();
            $table->string('problemas_con_bebida', 100);
            $table->string('exbebedor', 100);
            $table->string('exasuspendido', 100);
            $table->string('otros_toxicos', 100);
            $table->string('med_regularmente', 100);
            $table->string('cual_med_reg', 100)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_hab_toxicos');
    }
}
