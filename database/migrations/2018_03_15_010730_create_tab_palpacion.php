<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabPalpacion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_palpacions', function (Blueprint $table) { 
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals'); 
            $table->string('palpacion',100);
            $table->string('estado',100);     
            $table->string('hallazgo',100)->nullable();   
            $table->timestamps();    
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_palpacions');
    }
}
