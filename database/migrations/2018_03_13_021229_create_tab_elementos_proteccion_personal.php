<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabElementosProteccionPersonal extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_elementos_pr_pers', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('historia_clinica')->unsigned();
             $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
             $table->string('en_cargo_empresa_ant', 80);
             $table->string('examen_periodico', 80);
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_elementos_pr_pers');
    }
}
