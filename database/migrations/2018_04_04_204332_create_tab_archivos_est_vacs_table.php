<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabArchivosEstVacsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_archivos_est_vacs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('est_control_v')->unsigned();
            $table->foreign('est_control_v')->references('id')->on('tab_est_control_vs');
            $table->string('archivo');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_archivos_est_vacs');
    }
}
