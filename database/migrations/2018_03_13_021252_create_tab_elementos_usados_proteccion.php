<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabElementosUsadosProteccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_elem_usados_prts', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('elem_pro_per')->unsigned();
             $table->foreign('elem_pro_per')->references('id')->on('tab_elementos_pr_pers');
             $table->string('elemento', 100);
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_elem_usados_prts');
    }
}
