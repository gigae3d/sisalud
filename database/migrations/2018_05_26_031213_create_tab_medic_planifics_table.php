<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabMedicPlanificsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_medic_planifics', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre');
            $table->string('laboratorio');
            $table->date('fecha_vencimiento'); 
            $table->integer('existencia');
            $table->string('presentacion');
            $table->string('lote');
            $table->string('invima');
            $table->longText('observaciones');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_medic_planifics');
    }
}
