<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabEstudiante extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_estudiantes', function (Blueprint $table) { 
           $table->string('numero_de_identificacion', 30);
           $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
           $table->string('programa', 150);
           //$table->foreign('programa')->references('id')->on('tab_programas');
           $table->integer('semestre')->unsigned();
           $table->timestamps();
           $table->softDeletes();
        });

        Schema::table('tab_estudiantes', function (Blueprint $table) {
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_estudiantes');
    }
}
