<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabEquipoMedico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_equipos_medicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',80);
            $table->string('marca',80);
            $table->string('modelo',80);
            $table->string('tipo',80);
            $table->integer('existencia');
            $table->string('numero_de_serie',80);
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_equipos_medicos');
    }
}
