<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabRevisionPsistemasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_revision_psistemas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
            $table->longText('sistema_nervioso');
            $table->longText('insonmio');
            $table->longText('d_para_concentrarse'); 
            $table->longText('desp_realizar_act');
            $table->longText('tmuscular');
            $table->longText('ojos');
            $table->longText('orl');
            $table->longText('respiratorio');
            $table->longText('cardiovascular');
            $table->longText('digestivo');
            $table->longText('piel_faneras');
            $table->longText('musculoesqueletico');
            $table->longText('genito_urinario');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_revision_psistemas');
    }
}
