<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabIngresoPvesosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_ingreso_pvesos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('concepto_de_aptitud')->unsigned();
            $table->foreign('concepto_de_aptitud')->references('id')->on('tab_concepto_de_aptituds');
            $table->string('tipo',100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_ingreso_pvesos');
    }
}
