<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabHCliOcupacionalsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_h_cli_ocupacionals', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('trabajador')->unsigned();
            $table->foreign('trabajador')->references('id')->on('tab_trabajadors');
            $table->string('tipo_historia', 150);
            $table->date('fecha');
            $table->string('ciudad', 150);
            $table->string('ha_sufrido_at', 30);
            $table->string('ha_sufrido_ep', 30);
            $table->longText('desc_ant_osteo');
            $table->longText('secuelas');
            $table->longText('mdv_act');
            $table->string('alergico_algun_medicamento', 200);
            $table->string('otras_inmunizaciones', 200);
            $table->longText('seg_evolucion_casos');
            $table->longText('desc_ampl_hallazgos_ef');
            $table->longText('desc_ampl_hallazgos_cv');
            $table->longText('resultados_audiometria');
            $table->longText('examen_visual');
            $table->string('usa_lentes', 30);
            $table->longText('recomendaciones_medicas');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_h_cli_ocupacionals');
    }
}
