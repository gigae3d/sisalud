<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAntGineco extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_ant_ginecos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
            $table->integer('menarquia');     
            $table->string('ciclos', 100);           
            $table->date('fecha_ultima_m');      
            $table->string('dismenorrea', 100);      
            $table->integer('paridad_g');           
            $table->integer('paridad_p');           
            $table->integer('paridad_a');          
            $table->integer('paridad_c');           
            $table->integer('paridad_e');           
            $table->integer('paridad_m');           
            $table->integer('paridad_v');           
            $table->date('FPP')->nullable();                 
            $table->string('planificacion', 100);       
            $table->string('metodo', 100)->nullable();             
            $table->string('citologia_reciente', 100);  
            $table->date('fecha_citologia')->nullable();     
            $table->string('resultado_citologia', 100)->nullable(); 
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_ant_ginecos');
    }
}
