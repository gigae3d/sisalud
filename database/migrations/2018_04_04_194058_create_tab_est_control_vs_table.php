<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabEstControlVsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_est_control_vs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('usuario')->unsigned();
            $table->foreign('usuario')->references('id')->on('users');
            $table->string('nombres', 150);
            $table->string('apellidos', 150);
            $table->string('pregrado', 150);
            $table->integer('semestre');
            $table->date('fecha_diligenciamiento');
            $table->string('numero_de_identificacion', 30);
            $table->string('tipo', 10);
            $table->string('d_expedida_en', 100);
            $table->date('fecha_de_nacimiento');
            $table->string('lugar_de_nacimiento', 150);
            $table->string('genero', 30);
            $table->string('grupo_sanguineo', 30);
            $table->string('RH', 10);
            $table->string('direccion', 100);
            $table->string('barrio', 100);
            $table->string('ciudad', 100);
            $table->string('telefono_fijo', 100);
            $table->string('celular', 100);
            $table->string('correo_electronico', 100);
            $table->longText('enfermedades_previas');
            $table->longText('antecedentes_alergicos');
            $table->longText('medicamentos_tomados');
            $table->string('pr_ppd_resultado', 150);
            $table->date('pr_ppd_fecha');
            $table->string('pr_ppd_laboratorio', 150);
            $table->string('EPS');
            $table->string('conoce_regl_pr', 30);
            $table->string('eme_nomyape', 150);
            $table->string('eme_direccion', 150);
            $table->string('eme_telefono', 150);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_est_control_vs');
    }
}
