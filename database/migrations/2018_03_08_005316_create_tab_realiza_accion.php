<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabRealizaAccion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_realiza_accions', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('informacion_ocupacional')->unsigned();
             $table->foreign('informacion_ocupacional')->references('id')->on('tab_informacion_ocupacionals');
             $table->string('tipo', 100);
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_realiza_accions');
    }
}
