<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabInformacionOcupacional extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_informacion_ocupacionals', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('historia_clinica')->unsigned();
             $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
             $table->integer('antiguedad_en_empresa');
             $table->string('nombre_cargo_actual',100);
             $table->integer('antiguedad_en_cargo');
             $table->string('seccion',100);
             $table->string('turno',80);
             $table->longText('descripcion_del_cargo');
             $table->longText('equipos_herramientas_utilizadas');
             $table->longText('materias_primas_usadas');
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_informacion_ocupacionals');
    }
}
