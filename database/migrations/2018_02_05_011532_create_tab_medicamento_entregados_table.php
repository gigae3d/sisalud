<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabMedicamentoEntregadosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_medicamento_entregados', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reg_entrega_medicamento')->unsigned();
            $table->foreign('reg_entrega_medicamento')->references('id')->on('tab_entrega_medicamentos');
            $table->integer('medicamento')->unsigned();
            $table->foreign('medicamento')->references('id')->on('tab_medicamentos');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_medicamento_entregados');
    }
}
