<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAfinamientoTomaPresionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_afinamiento_toma_presions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('toma_presion_arterial')->unsigned();
            $table->foreign('toma_presion_arterial')->references('id')->on('tab_tpresion_arterials');
            $table->string('postura', 100);
            $table->string('MSD', 100);
            $table->string('MSI', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_afinamiento_toma_presions');
    }
}
