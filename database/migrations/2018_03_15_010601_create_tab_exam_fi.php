<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabExamFi extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_exam_fis', function (Blueprint $table) { 
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals'); 
            $table->string('constitucion',100);   
            $table->string('dominancia',100);    
            $table->string('TA',100);              
            $table->string('frec_cardiaca',100);
            $table->string('Peso',100);
            $table->string('Talla',100);
            $table->string('IMC',100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_exam_fis');
    }
}
