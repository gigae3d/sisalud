<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RelEstudiantesProgramas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tab_estudiantes', function(Blueprint $table) {
            $table->unsignedInteger('id_programa')->after('programa');
            $table->foreign('id_programa')->references('id')->on('tab_programas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('tab_estudiantes', function(Blueprint $table) {
            $table->dropForeign('tab_estudiantes_id_programa_foreign');
        });
    }
}
