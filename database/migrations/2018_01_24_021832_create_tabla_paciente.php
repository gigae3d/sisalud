<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablaPaciente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_pacientes', function (Blueprint $table) {
            $table->string('numero_de_identificacion', 30);
            $table->string('nombres', 80);
            $table->string('apellidos', 80); 
            $table->string('telefono', 30);
            $table->string('tipo_de_usuario', 30);
            $table->timestamps();
            $table->softDeletes();
            $table->primary('numero_de_identificacion');
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_entrega_medicamentos');
        Schema::dropIfExists('tab_pacientes');
    }
}
