<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAsesoriaSasesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_asesoria_sases', function (Blueprint $table) {
            $table->increments('id');
            $table->string('numero_de_identificacion',30);
            $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
            $table->date('fecha');
            $table->string('asesoria', 200);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_asesoria_sases');
    }
}
