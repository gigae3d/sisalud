<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabInsumoMedico extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_insumos_medicos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre',80);
            $table->string('marca',80);
            $table->integer('existencia');
            $table->string('lote',80);
            $table->string('fecha_de_vencimiento',80);
            $table->string('Registro_INVIMA',80);
            $table->timestamps();
            $table->softDeletes();

        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_insumos_medicos');
    }
}
