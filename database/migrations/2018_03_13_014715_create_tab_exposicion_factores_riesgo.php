<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabExposicionFactoresRiesgo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_exp_factores_riesgos', function (Blueprint $table) {
             $table->increments('id');
             $table->integer('historia_clinica')->unsigned();
             $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
             $table->string('empresa', 100);
             $table->string('estado_laboral', 80);
             $table->string('cargo', 100);
             $table->string('tiempo', 100);
             $table->string('EPP', 100);
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_exp_factores_riesgos');
    }
}
