<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabEsquemaVacunasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_esquema_vacunas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('est_control_v')->unsigned();
            $table->foreign('est_control_v')->references('id')->on('tab_est_control_vs');
            $table->string('vacuna', 150);
            $table->integer('n_dosis_aplicadas')->nullable();
            $table->date('fecha_ultima_dosis')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_esquema_vacunas');
    }
}
