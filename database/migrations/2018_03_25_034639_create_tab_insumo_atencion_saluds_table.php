<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabInsumoAtencionSaludsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_insumo_atencion_saluds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('atencion_en_salud')->unsigned();
            $table->foreign('atencion_en_salud')->references('id')->on('tab_atencion_en_saluds');
            $table->integer('insumo_medico')->unsigned();
            $table->foreign('insumo_medico')->references('id')->on('tab_insumos_medicos');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_insumo_atencion_saluds');
    }
}
