<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabMedicEntregadoAntisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_medic_entregado_antis', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('reg_entrega_medicamento')->unsigned();
            $table->foreign('reg_entrega_medicamento')->references('id')->on('tab_entrega_med_antis');
            $table->integer('medicamento')->unsigned();
            $table->foreign('medicamento')->references('id')->on('tab_medic_planifics');
            $table->integer('cantidad');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_medic_entregado_antis');
    }
}
