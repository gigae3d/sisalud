<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabPacienteControlFecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_paciente_control_fecs', function (Blueprint $table) {
            $table->string('numero_de_identificacion', 30);
            $table->string('nombres_apellidos',200);
            $table->string('sexo', 20);
            $table->string('direccion', 100);
            $table->string('telefono', 100);
            $table->date('fecha_de_nacimiento');
            $table->string('estado_civil', 50);
            $table->string('uso_antic_previo', 30);
            $table->string('cual_antic_previo', 50)->nullable();
            $table->integer('numero_hijos_vivos');
            $table->timestamps();
            $table->softDeletes();
            $table->primary('numero_de_identificacion');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_paciente_control_fecs');
    }
}
