<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabDocente extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_docentes', function(Blueprint $table) {
           $table->string('numero_de_identificacion', 30);
           $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
           $table->string('facultad', 80);
           $table->timestamps();
           $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_docentes');
    }
}
