<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabDescripcionSeccionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_descripcion_seccions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
            $table->longText('descripcion_secuela');
            $table->longText('descripcion_mdv');
            $table->longText('alergia'); 
            $table->longText('seguimiento_ev_caso');
            $table->longText('descripcion_amh_exf');
            $table->longText('descripcion_amh_cv');
            $table->longText('audiometria_res');
            $table->longText('examen_visual_res');
            $table->string('usa_lentes', 50);
            $table->longText('recomendacion_medica');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_descripcion_seccions');
    }
}
