<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabRealizaTomaDePresionArterial extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_tpresion_arterials', function (Blueprint $table) {
             $table->increments('id');
             $table->string('numero_de_identificacion',30);
             $table->foreign('numero_de_identificacion')->references('numero_de_identificacion')->on('tab_pacientes');
             $table->integer('equipo_medico')->unsigned();
             $table->foreign('equipo_medico')->references('id')->on('tab_equipos_medicos');
             $table->integer('usuario')->unsigned();
             $table->foreign('usuario')->references('id')->on('users');
             $table->date('fecha');
             $table->string('tipo', 80);
             $table->string('posicion', 80)->nullable();
             $table->string('resultado', 80);
             $table->timestamps();
             $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_tpresion_arterials');
    }
}
