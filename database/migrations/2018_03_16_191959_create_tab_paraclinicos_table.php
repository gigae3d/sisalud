<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabParaclinicosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_paraclinicos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
            $table->string('nombre_paraclinico', 100);
            $table->date('fecha');
            $table->string('resultado', 150);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_paraclinicos');
    }
}
