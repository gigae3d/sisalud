<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabAntAcciTrabajo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_ant_acci_trabajos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('historia_clinica')->unsigned();
            $table->foreign('historia_clinica')->references('id')->on('tab_h_cli_ocupacionals');
            $table->date('fecha');
            $table->string('empresa', 100);
            $table->string('causa', 100);
            $table->string('tipo_de_lesion', 100);
            $table->string('parte_del_cuerpo', 100);
            $table->integer('dias_de_incapacidad');
            $table->string('secuelas', 100);
            $table->timestamps();
            $table->softDeletes();
        });  
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_ant_acci_trabajos');
    }
}
