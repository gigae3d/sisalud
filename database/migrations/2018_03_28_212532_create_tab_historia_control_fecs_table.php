<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTabHistoriaControlFecsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tab_historia_control_fecs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('paciente_control_fec', 30);
            $table->foreign('paciente_control_fec')->references('numero_de_identificacion')->on('tab_paciente_control_fecs');
            $table->integer('menarquia');
            $table->string('ciclos_menstruales', 30);
            $table->date('FUM');
            $table->integer('paridad_g');
            $table->integer('paridad_p');
            $table->integer('paridad_a');
            $table->integer('edad_inicio_rel_sex');
            $table->date('FUP');
            $table->integer('numero_comp_sexuales');
            $table->string('cirugia_ginecologica', 10);
            $table->date('fecha_citologia')->nullable();
            $table->string('resultado_citologia', 50)->nullable();
            $table->string('metodo_deseado', 100);
            $table->string('metodo_adoptado', 100);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tab_historia_control_fecs');
    }
}
