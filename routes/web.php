<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function () {
    return Redirect::to(route('Home'));
});

Route::get('/pruebavue2', function () {
    return view('medicamentos.list_prueba2');
});

//Pagina Principal
Route::get('/home', ['as'=>'Home', 'uses'=>'HomeController@index']);

//Página de error de permisos
Route::view('/error_de_permisos', 'auth.PermisoDenegado');

// Logueo Usuario
Route::get('/login', ['as'=>'login', 'uses'=>'SistemaController@showLoginForm']);
Route::post('/login', ['as'=>'Ingreso', 'uses'=>'SistemaController@login']);

//Deslogueo
Route::get('/logout', ['as'=>'Logout', 'uses'=>'SistemaController@logout']);

//Registro Nuevo Usuario
Route::get('/register', ['as'=>'register', 'uses'=>'SistemaController@showRegistrationForm']);
Route::post('/register', ['as'=>'Registrar', 'uses'=>'SistemaController@register']);

//MODULO ADMIN
//Mostrar formulario de registro de usuario
Route::get('/register_user', ['as'=>'register_user','uses'=>'SistemaController@showCreateUserForm'])->middleware('role_admin');
//Registrar nuevo usuario de sistema
Route::post('/register_user', ['as'=>'create_user','uses'=>'SistemaController@newUser'])->middleware('role_medico');

//Listar Usuarios
Route::get('/list_usuarios', ['as'=>'view_usuarios', 'uses'=>'SistemaController@listUsers'])->middleware('role_admin');
Route::get('/list_usuarios_ajax', ['as'=>'list_usuarios_ajax', 'uses'=>'SistemaController@listUsersAjax'])->middleware('role_admin');

//Eliminar usuario
Route::get('/borrarUsuario/{id}',['as'=>'borrar_usuario','uses'=>'SistemaController@borrarUsuario'])->middleware('role_admin');

//MODULO MEDICAMENTOS
Route::middleware(['role_enfermero'])->group(function (){ //Funcion para asignar este middleware a varias rutas
//Inventario de Medicamentos
Route::get('/form', ['as'=>'new_Medicamento', 'uses'=>'TabMedicamentos@showForm']);
Route::post('/create_medicamento', ['as'=>'create_medicamento', 'uses'=>'TabMedicamentos@new']);
//Ruta nueva para crear nuevo medicamento
Route::post('/regMedNew', ['as'=> 'guardar_medicamento', 'uses'=>'TabMedicamentos@registrarMedicamento']);

//Listar Medicamentos realizando una consulta
Route::get('/list_medicamentos', ['as'=>'view_medicamentos', 'uses'=>'TabMedicamentos@list']);
Route::get('/obtenerMedicamentos', ['as'=>'obtener_medicamentos', 'uses'=>'TabMedicamentos@obtenerMedicamentos']);
//Route::get('/list_medicamentos_ajax', ['as'=>'list_medicamentos_ajax', 'uses'=>'TabMedicamentos@getMedicamentosAjax']);

//Editar información de los medicamentos
Route::post('/ediMed', ['as'=> 'editar_medicamento', 'uses'=>'TabMedicamentos@obtenerMedicamento']);
Route::post('/actMed', ['as'=> 'actualizar_medicamento', 'uses'=>'TabMedicamentos@editarMedicamento']);
//Route::get('/edit_medicamento/{id}', ['as'=>'edit_form_medicamento', 'uses'=>'TabMedicamentos@showEditForm']);
//Route::post('/edit_medicamento', ['as'=>'edit_medicamento', 'uses'=>'TabMedicamentos@update']);

//Obtener informacion adicional medicamento
Route::get('/get_observaciones_med', ['as'=>'get_observaciones_med','uses'=>'TabMedicamentos@obtenerInfoAdicionalMed']);

//Obtener estadística medicamento
Route::get('/est_medicamento',['as'=>'est_medicamento','uses'=>'TabMedicamentos@obtenerEstadisticaMed']);


//MODULO MEDICAMENTOS DE PLANIFICACIÓN FAMILIAR
//Mostrar formulario nuevo medicamento de planificación familiar
Route::get('/new_medicamento_antic', ['as'=>'new_medicamento_antic','uses'=>'TabMedicamentosAnticonceptivos@showForm']);
//Registrar nuevo medicamento de planificación familiar
Route::post('/create_medicamento_antic',['as'=>'create_medicamento_antic','uses'=>'TabMedicamentosAnticonceptivos@new']);
//Listar Medicamentos anticonceptivos permitiento hacer una consulta
Route::get('/list_medicamentos_antic',['as'=>'view_medicamentos_antic','uses'=>'TabMedicamentosAnticonceptivos@list']);
Route::get('/list_medicamentos_antic_ajax',['as'=>'list_medicamentos_antic_ajax','uses'=>'TabMedicamentosAnticonceptivos@getMedicamentosAntiAjax']);

//Mostrar formulario de registro de asesoria
Route::get('/new_asesoria_se',['as'=>'new_asesoria_se','uses'=>'TabMedicamentosAnticonceptivos@showFormAsesoria']);
//Registrar asesoria en salud sexual y reproductiva
Route::post('/create_asesoria_se',['as'=>'create_asesoria_se','uses'=>'TabMedicamentosAnticonceptivos@newAsesoria']);
//Listar asesorias
Route::get('/list_asesorias_se',['as'=>'view_asesorias_se','uses'=>'TabMedicamentosAnticonceptivos@listAsesorias']);
Route::get('/list_asesorias_se_ajax',['as'=>'list_asesorias_se_ajax','uses'=>'TabMedicamentosAnticonceptivos@getAsesoriasAjax']);

//Obtener información adicional medicamento de planificación
Route::get('/get_observ_med_planif', ['as'=>'get_observ_med_planif','uses'=>'TabMedicamentosAnticonceptivos@obtenerInfoAdicionalMed']);
//Mostrar formulario para editar medicamento de planificación
Route::get('/edit_medic_planif/{id}',['as'=>'edit_form_med_planif','uses'=>'TabMedicamentosAnticonceptivos@showEditForm']);
Route::post('/edit_medic_planif',['as'=>'edit_medicamento_planif','uses'=>'TabMedicamentosAnticonceptivos@update']);
//Obtener estadistica de medicamento anticonceptivo
Route::get('/est_medicamento_antic',['as'=>'est_medicamento_antic','uses'=>'tabMedicamentosAnticonceptivos@obtenerEstadisticaMed']);

//MODULO ENTREGA DE MEDICAMENTOS ANTICONCEPTIVOS O PLANIFICACIÓN
//Mostrar formulario de entrega de medicamento anticonceptivo
Route::get('/new_entrega_med_anti',['as'=>'new_entrega_med_anti','uses'=>'TabEntregaMedAntic@showForm']);
Route::post('/create_entrega_med_anti',['as'=>'create_entrega_med_anti','uses'=>'TabEntregaMedAntic@new']);

//Listar medicamentos anticonceptivos para registrar una entrega de medicamentos anticonceptivos
Route::get('/list_med_antic_ent_ajax',['as'=>'list_med_antic_ent_ajax','uses'=>'TabEntregaMedAntic@getMedicamentosAnticAjax']);
//Listar entregas de medicamentos anticonceptivos
Route::get('/list_reg_entrega_medic_antic',['as'=>'view_reg_entrega_medic_antic','uses'=>'TabEntregaMedAntic@list']);
Route::get('/list_reg_entrega_medic_antic_ajax',['as'=>'list_reg_entrega_medic_antic_ajax','uses'=>'TabEntregaMedAntic@getEntregaDeMedicamentosAnticAjax']);

//Obtener información adicional entrega de medicamentos anticonceptivos
Route::get('/get_med_entr_anti', ['as'=>'get_med_entr_anti','uses'=>'TabEntregaMedAntic@obtenerInfoAdicionalEntrega']);

//MÓDULO PACIENTES
//Validar Paciente
Route::get('/validate', ['as'=>'validate','uses'=>'TabPacientes@showValidate']);
Route::post('/val_cedula', ['as'=>'val_cedula','uses'=>'TabPacientes@valCedula']);
Route::get('/val_cedula', ['as'=>'val_cedula','uses'=>'TabPacientes@valCedula']);


//Registro Nuevo Paciente
Route::get('/new_paciente/{nidentificacion?}', ['as'=>'new_paciente','uses'=>'TabPacientes@showForm']);
Route::post('/create_paciente', ['as'=>'create_paciente', 'uses'=>'TabPacientes@new']);

//Listar Pacientes realizando una consulta
Route::get('/list_pacientes', ['as'=>'view_pacientes', 'uses'=>'TabPacientes@list']);
Route::get('/list_pacientes_ajax', ['as'=>'list_pacientes_ajax', 'uses'=>'TabPacientes@getPacientesAjax']);

//Editar información de los pacientes
Route::get('/edit_paciente/{id}', ['as'=>'edit_form_paciente', 'uses'=>'TabPacientes@showEditForm']);
Route::post('/edit_paciente', ['as'=>'edit_paciente', 'uses'=>'TabPacientes@update']);

//Obtener informacion adicional pacientes y estadísticas
Route::get('/inf_adicional_paciente',['as'=>'inf_adicional_paciente','uses'=>'TabPacientes@obtenerEstadisticasPaciente']);
//Obtener estadísticas por programas
Route::get('/est_programa',['as'=>'est_programa','uses'=>'TabPacientes@obtenerEstadisticasPorPrograma']);

//MODULO ENTREGA DE MEDICAMENTOS
//Mostrar formulario de registro de una nueva entrega de medicamento
Route::get('/new_entrega_med', ['as'=>'new_entrega_med_w', 'uses'=>'TabEntregaMedicamentos@showForm']);
Route::get('/new_entrega_med/{numero_de_identificacion}',['as'=>'new_entrega_med','uses'=>'TabEntregaMedicamentos@showFormWithId']);

//Obtener nombres y apellidos de un paciente desde el formulario de registro de entrega
Route::get('/get_nomape_paciente', ['as'=>'get_nomape_paciente', 'uses'=>'TabEntregaMedicamentos@obtenerInfoPaciente']);
//Registrar nueva entrega de medicamento
Route::post('/create_entrega_medicamento',['as'=>'create_entrega_med','uses'=>'TabEntregaMedicamentos@new']);

//Listar medicamentos para registrar entrega de medicamento
Route::get('/list_entrega_medicamentos_ajax', ['as'=>'list_entrega_medicamentos_ajax', 'uses'=>'TabEntregaMedicamentos@getMedicamentosAjax']);
Route::get('/obtenerEntregasMedicamentos', ['as'=>'obtener_entregasMedicamentos', 'uses'=>'TabEntregaMedicamentos@obtenerEntregasMedicamentos']);
//Listar registros de entregas de medicamentos
Route::get('/list_reg_entrega_medicamentos', ['as'=>'view_entrega_de_medicamentos', 'uses'=>'TabEntregaMedicamentos@list']);
Route::get('/list_reg_entrega_medicamentos_ajax',['as'=>'list_reg_entrega_medicamentos_ajax','uses'=>'TabEntregaMedicamentos@getEntregaDeMedicamentosAjax']);
//Obtener información de una entrega de medicamentos
Route::get('/get_medicamentos_entr',['as'=>'get_medicamentos_entr','uses'=>'TabEntregaMedicamentos@obtenerInfoAdicionalEntrega']);


//MODULO INSUMOS MEDICOS
//Mostrar formulario de nuevo insumo médico
Route::get('/new_insumo_medico',['as'=>'new_insumo_medico','uses'=>'TabInsumosMedicos@showForm']);
//Registrar nuevo insumo médico
Route::post('/create_insumo_medico', ['as'=>'create_insumo_medico','uses'=>'TabInsumosMedicos@new']);

//Listar insumos medicos
Route::get('/list_insumos_medicos', ['as'=>'view_insumos_medicos','uses'=>'TabInsumosMedicos@list']);
Route::get('/list_insumos_medicos_ajax', ['as'=>'list_insumos_medicos_ajax','uses'=>'TabInsumosMedicos@getInsumosMedicosAjax']);

//Editar información de los insumos médicos
Route::get('/edit_insumo_medico/{id}', ['as'=>'edit_form_insumo_medico', 'uses'=>'TabInsumosMedicos@showEditForm']);
Route::post('/edit_insumo_medico', ['as'=>'edit_insumo_medico', 'uses'=>'TabInsumosMedicos@update']);


//MODULO EQUIPOS MEDICOS
//Mostrar formulario de registro de nuevo equipo médico
Route::get('/new_equipo_medico',['as'=>'new_equipo_medico','uses'=>'TabEquiposMedicos@showForm']);
//Registrar nuevo equipo médico
Route::post('/create_equipo_medico', ['as'=>'create_equipo_medico','uses'=>'TabEquiposMedicos@new']);
//Nueva ruta para crear nuevo equipo medico
Route::post('/regEquipoMedico', ['as'=>'guardar_equipo_medico', 'uses'=>'TabEquiposMedicos@registrarEquipoMedico']);

//Listar equipos medicos
Route::get('/list_equipos_medicos', ['as'=>'view_equipos_medicos','uses'=>'TabEquiposMedicos@list']);
Route::get('/list_equipos_medicos_ajax', ['as'=>'list_equipos_medicos_ajax','uses'=>'TabEquiposMedicos@getEquiposMedicosAjax']);
Route::get('/obtenerEqMedicos', ['as'=>'obtener_eqmedicos', 'uses'=>'TabEquiposMedicos@obtenerEquiposMedicos']);

//Editar información de los equipos médicos
Route::get('/edit_equipo_medico/{id}',['as'=>'edit_form_equipo_medico','uses'=>'TabEquiposMedicos@showEditForm']);
Route::post('/edit_equipo_medico',['as'=>'edit_equipo_medico','uses'=>'TabEquiposMedicos@update']);

//MODULO TOMA DE PRESION ARTERIAL
//Mostrar formulario de registro de nueva toma de presión arterial
Route::get('/new_tpresion_arterial',['as'=>'new_tpresion_arterial_w','uses'=>'TabTpresionArterial@showForm']);
Route::get('/new_tpresion_arterial/{numero_de_identificacion}',['as'=>'new_tpresion_arterial','uses'=>'TabTpresionArterial@showFormWithId']);
//Registrar toma de presión arterial
Route::post('/create_tpresion_arterial', ['as'=>'create_tpresion_arterial','uses'=>'TabTpresionArterial@new']);

//Listar registros de toma de presión arterial
Route::get('/list_tpresion_arteriales', ['as'=>'view_tpresion_arteriales','uses'=>'TabTpresionArterial@list']);
Route::get('/list_tpresion_arteriales_ajax', ['as'=>'list_tpresion_arteriales_ajax','uses'=>'TabTpresionArterial@getTomaDePresionArterialAjax']);
Route::get('/obtenerTPresionArterial', ['as'=>'obtener_tpresion_arterial','uses'=>'TabTpresionArterial@obtenerTPresionArterial']);

//Listar afinamientos
Route::get('/list_afinamientos/{id}', ['as'=>'list_afinamientos', 'uses'=>'TabTpresionArterial@listarAfinamientosPaciente']);

//Obtener afinamientos de un registro en particular
Route::get('/get_afinamientos_reg',['as'=>'get_afinamientos_reg','uses'=>'TabTpresionArterial@obtenerInfoAdicionalAfinamiento']);

//MODULO GLUCOMETRIA
//Mostrar formulario de registro de glucometria
Route::get('/new_glucometria', ['as'=>'new_glucometria_w','uses'=>'TabGlucometria@showForm']);
Route::get('/new_glucometria/{numero_de_identificacion}',['as'=>'new_glucometria','uses'=>'TabGlucometria@showFormWithId']);
//Registrar glucometría
Route::post('/create_glucometria', ['as'=>'create_glucometria','uses'=>'TabGlucometria@new']);

//Listar registros de glucometría
Route::get('/list_glucometrias',['as'=>'view_glucometrias','uses'=>'TabGlucometria@list']);
Route::get('/obtenerGlucometrias', ['as'=>'obtener_glucometrias', 'uses'=> 'TabGlucometria@obtenerGlucometrias']);
Route::get('/list_glucometrias_ajax',['as'=>'list_glucometrias_ajax','uses'=>'TabGlucometria@getGlucometriasAjax']);
//Listar equipos médicos para seleccionar uno
Route::get('/list_equipos_medicos_ajax_gl',['as'=>'list_equipos_medicos_ajax_gl','uses'=>'TabGlucometria@getEquiposMedicosAjax']);

//MODULO INYECTOLOGÍA
//Mostrar formulario de registro de inyectologia
Route::get('/new_inyectologia', ['as'=>'new_inyectologia_w','uses'=>'TabInyectologia@showForm']);
Route::get('/new_inyectologia/{numero_de_identificacion}', ['as'=>'new_inyectologia','uses'=>'TabInyectologia@showFormWithId']);
//Registrar Inyectologia
Route::post('/create_inyectologia', ['as'=>'create_inyectologia','uses'=>'TabInyectologia@new']);
//Listar inyectologias
Route::get('/list_inyectologias', ['as'=>'view_inyectologias','uses'=>'TabInyectologia@list']);
Route::get('/list_inyectologias_ajax',['as'=>'list_inyectologias_ajax','uses'=>'TabInyectologia@getInyectologiasAjax']);

//Obtener información adicional para una inyectologia registrada
Route::get('/get_insumos_iny',['as'=>'get_insumos_iny','uses'=>'TabInyectologia@obtenerInfoAdicionalIny']);

//Listar insumos médicos en formulario de inyectologia
Route::get('/list_insumos_medicos_ajax_iny',['as'=>'list_insumos_medicos_ajax_iny','uses'=>'TabAtencionEnSalud@getInsumosMedicosAjax']);

//MODULO ATENCION EN SALUD
//Mostrar formulario de registro de atencion en salud
Route::get('/new_atencion_en_salud',['as'=>'new_atencion_en_salud_w','uses'=>'TabAtencionEnSalud@showForm']);
Route::get('/new_atencion_en_salud/{numero_de_identificacion}',['as'=>'new_atencion_en_salud','uses'=>'TabAtencionEnSalud@showFormWithId']);
//Registrar Atencion en salud
Route::post('/create_atencion_en_salud', ['as'=>'create_atencion_en_salud','uses'=>'TabAtencionEnSalud@new']);

//Listar atenciones de salud registradas 
Route::get('/list_atenciones_en_salud', ['as'=>'view_atenciones_en_salud','uses'=>'TabAtencionEnSalud@list']);
Route::get('/list_atenciones_en_salud_ajax',['as'=>'list_atenciones_en_salud_ajax','uses'=>'TabAtencionEnSalud@getAtencionesEnSaludAjax']);


//Obtener información adicional para una atencion en salud registrada
Route::get('/get_insumos_atsalud',['as'=>'get_insumos_atsalud','uses'=>'TabAtencionEnSalud@obtenerInfoAdicionalAtEnSalud']);

//Listar equipos médicos para atencion en salud
Route::get('/list_equipos_medicos_ajax_as', ['as'=>'list_equipos_medicos_ajax_as','uses'=>'TabAtencionEnSalud@getEquiposMedicosAjax']);

//Listar insumos médicos para atencion en salud
Route::get('/list_insumos_medicos_ajax_as', ['as'=>'list_insumos_medicos_ajax_as', 'uses'=>'TabAtencionEnSalud@getInsumosMedicosAjax']);

}); //Cierre grupo de rutas asignadas con el middleware role_enfermeria

//MODULO ATENCION EN ENFERMERIA
//Mostrar formulario de registro de atención en enfermeria
Route::get('/new_atencion_enfermeria', ['as'=>'new_atencion_enfermeria','uses'=>'AtencionEnEnfermeria@form_atencion_en_enfermeria']);

Route::get('/menu_atencion_enfermeria/{id}', ['as'=>'menu_atencion_enfermeria','uses'=>'AtencionEnEnfermeria@showMenu']);

//MODULO HISTORIAS CLINICAS
//Registrar una historia clínica para un trabajador
Route::get('/reg_hist_clinica_oc/{trabajador}', ['as'=>'reg_hist_clinica_oc','uses'=>'HistoriaClinica@showFormRegHCO'])->middleware('role_medico');

Route::post('/create_consulta_medica', ['as'=>'create_consulta_medica','uses'=>'HistoriaClinica@new']);

//Mostrar formulario de registro de nuevo trabajador
Route::get('/new_trabajador_hco', ['as'=>'new_trabajador_hco','uses'=>'HistoriaClinica@showFormTrabajador'])->middleware('role_medico');
//Registrar trabajador
Route::post('/create_trabajador_hco', ['as'=>'create_trabajador_hco','uses'=>'HistoriaClinica@newTrabajador']);
//Mostrar formulario para editar trabajador
Route::get('/edit_trabajador/{id}',['as'=>'edit_form_trabajador','uses'=>'HistoriaClinica@showEditFormTrabajador'])->middleware('role_medico');
//Editar trabajador
Route::post('/edit_trabajador',['as'=>'edit_trabajador','uses'=>'HistoriaClinica@updateTrabajador']);

//Listar trabajadores
Route::get('/list_trabajadores',['as'=>'view_trabajadores','uses'=>'HistoriaClinica@listTrabajadores'])->middleware('role_medico');
Route::get('/list_trabajadores_ajax',['as'=>'list_trabajadores_ajax','uses'=>'HistoriaClinica@getTrabajadoresAjax']);

//Listar historias clinicas ocupacionales
Route::get('/list_h_clinicas_ocupacionales/{id}', ['as'=>'view_h_clinicas_ocupacionales','uses'=>'HistoriaClinica@viewHCTrabajador'])->middleware('role_medico');
Route::get('/list_h_clinicas_ocupacionales_ajax/{id}', ['as'=>'list_h_clinicas_ocupacionales_ajax','uses'=>'HistoriaClinica@viewHCTrabajadorAjax']);

Route::get('/view_h_clinica_ocupacional/{id}', ['as'=>'view_h_clinica_ocupacional','uses'=>'HistoriaClinica@showHistoriaClinicaOcupacional'])->middleware('role_medico');

//Generar PDF historia clinica ocupacional
Route::get('/generarpdfHC/{id}', ['as'=>'generarpdfHC','uses'=>'HistoriaClinica@generarPDFHistoriaClinicaOc'])->middleware('role_medico');

//Generar PDF concepto de aptitud
Route::get('/generarpdfCA/{id}', ['as'=>'generarpdfCA', 'uses'=>'HistoriaClinica@generarPDFConceptoAptitud'])->middleware('role_medico');


//MODULO CITAS
//Mostrar formulario de programacion de citas con Medica Ana Carolina Garcia
Route::get('/new_cita_medico_1',['as'=>'new_cita_medico_1','uses'=>'TabCita@showForm1'])->middleware('role_enfermero');
//Mostrar formulario de programación de citas con Medico Italo Zuñiga
Route::get('/new_cita_medico_2',['as'=>'new_cita_medico_2','uses'=>'TabCita@showForm2'])->middleware('role_enfermero');

//Listar citas programadas medica Ana Carolina Garcia
Route::get('/list_citas_cgarcia_ajax', ['as'=>'list_citas_cgarcia_ajax','uses'=>'TabCita@getCitasMedicoAnaAjax']);

//Registrar nueva cita con alguno de los médicos
Route::post('/create_cita', ['as'=>'create_cita','uses'=>'TabCita@new']);

//Listar citas programadas medico Italo Zuñiga
Route::get('/list_citas_izuniga_ajax',['as'=>'list_citas_izuniga_ajax','uses'=>'TabCita@getCitasMedicoItaloAjax']);

//Eliminar cita
Route::get('/delete_cita/{id}', ['as'=>'delete_cita','uses'=>'TabCita@eliminarCita'])->middleware('role_enfermero');

//MODULO HISTORIAS DE SEGUIMIENTO DEL METODO PARA EL CONTROL DE LA FECUNDIDAD
//Mostrar formulario para registrar datos de una persona
Route::get('/new_historia_seg_con_fec', ['as'=>'new_historia_seg_con_fec','uses'=>'TabPacienteControlFec@showForm']);
//Registrar datos paciente control fecundidad
Route::post('/create_hist_confec', ['as'=>'create_hist_confec','uses'=>'TabPacienteControlFec@new']);
//Listar pacientes control fecundidad
Route::get('/list_pacientes_controlfec',['as'=>'view_pacientes_controlfec','uses'=>'TabPacienteControlFec@list']);
Route::get('/list_pacientes_controlfec_ajax',['as'=>'list_pacientes_controlfec_ajax','uses'=>'TabPacienteControlFec@getPacientesControlAjax']);

//Mostrar formulario para registrar una nueva historia clinica de seguimiento del metodo para el control de fecundidad para una persona
Route::get('/new_historia_clinica_seg',['as'=>'new_historia_clinica_seg_w','uses'=>'TabHistoriaClinicaFec@showForm']);
Route::get('/new_historia_clinica_seg/{numero_de_identificacion}',['as'=>'new_historia_clinica_seg','uses'=>'TabHistoriaClinicaFec@showFormWithId']);
//Registrar nueva historia clinica de seguimiento
Route::post('/create_historia_seg_con',['as'=>'create_historia_seg_con','uses'=>'TabHistoriaClinicaFec@new']);
//Listar historias control fecundidad
Route::get('/list_h_control_fec',['as'=>'view_historia_con_fec','uses'=>'TabHistoriaClinicaFec@list']);
Route::get('/list_h_control_fec_ajax',['as'=>'list_h_control_fec_ajax','uses'=>'TabHistoriaClinicaFec@getHistoriasControlFecAjax']);

//Mostrar formulario para registrar un nuevo control a una historia clinica 
Route::get('/new_control_historia',['as'=>'new_control_historia_w','uses'=>'TabControlHistFec@showForm']);
Route::get('/new_control_historia/{id}',['as'=>'new_control_historia','uses'=>'TabControlHistFec@showFormWithId']);
//Registrar nuevo control a una historia clinica
Route::post('/create_control_historia',['as'=>'create_control_historia','uses'=>'TabControlHistFec@new']);
//Listar controles asociados a una historia clinica
Route::get('/list_controles_hist/{historia}',['as'=>'list_controles_hist','uses'=>'TabControlHistFec@list']);
Route::get('/list_controles_hist_ajax/{historia}',['as'=>'list_controles_hist_ajax','uses'=>'TabControlHistFec@getControlesHistoriaAjax']);


//MODULO CONTROL DE VACUNAS
//Mostrar formulario de registro de información de un estudiante
Route::get('/new_reg_control_vacuna',['as'=>'new_reg_control_vacuna','uses'=>'TabControlVacunas@showForm'])->middleware('role_estudiante');
Route::post('/create_reg_control_vacuna',['as'=>'create_reg_control_vacuna','uses'=>'TabControlVacunas@new']);
//Listar estudiantes registrados
Route::get('/list_estudiantes_reg',['as'=>'list_estudiantes_reg','uses'=>'TabControlVacunas@list'])->middleware('role_ucontrolv');
Route::get('/list_estudiantes_reg_ajax',['as'=>'list_estudiantes_reg_ajax','uses'=>'TabControlVacunas@getRegistrosControlVacunas']);

//Filtrar estudiantes
Route::post('/filtrar_estudiantes',['as'=>'filtrar_estudiantes','uses'=>'TabControlVacunas@filtrarEstudiantes'])->middleware('role_ucontrolv');

//Mostrar archivo anexo para un estudiante
Route::get('/show_file/{id}',['as'=>'show_file','uses'=>'TabControlVacunas@mostrarArchivoAnexo'])->middleware('role_estud_o_ucontrolv');

//Mostrar información de un registro de control de vacunas 
Route::get('/view_reg_control_vac/{id}',['as'=>'view_reg_control_vac','uses'=>'TabControlVacunas@showRegistroControlVacunas'])->middleware('role_estud_o_ucontrolv');
//Generar PDF de una hoja de vida registrada por un estudiante
Route::get('/generarpdf/{id}',['as'=>'generarpdf','uses'=>'TabControlVacunas@generarPDFHojaDeVida'])->middleware('role_estud_o_ucontrolv');

//Borrar un registro completo de un estudiante, junto con su archivo PDF guardado
Route::get('/borrarregistro/{id}',['as'=>'borrarR','uses'=>'TabControlVacunas@borrarRegistroEst'])->middleware('role_estud_o_ucontrolv');

//Mostrar solo el registro de control de vacunas para el estudiante
Route::get('/list_registro_est', ['as'=>'view_registro_est','uses'=>'TabControlVacunas@viewRegistro'])->middleware('role_estud_o_ucontrolv');
Route::get('/list_registro_est_ajax/{id}',['as'=>'list_registro_est_ajax','uses'=>'TabControlVacunas@viewRegistroAjax'])->middleware('role_estud_o_ucontrolv');

//Reportes Excel
Route::get('/reports', ['as'=>'reports','uses'=>'TabReportsExcel@viewReports']);
Route::get('/rep_cuadro_vacunas', ['as'=>'rep_cuadro_vacunas','uses'=>'TabReportsExcel@reporteCuadroVacunas']);
Route::get('/rep_medicamentos', ['as'=>'rep_medicamentos','uses'=>'TabReportsExcel@reporteMedicamentos']);
Route::get('/rep_anticonceptivos', ['as'=>'rep_anticonceptivos','uses'=>'TabReportsExcel@reporteAnticonceptivos']);
Route::get('/rep_insumos_medicos', ['as'=>'rep_insumos_medicos','uses'=>'TabReportsExcel@reportInsumosMedicos']);
Route::get('/rep_equipos_medicos', ['as'=>'rep_equipos_medicos','uses'=>'TabReportsExcel@reportEquiposMedicos']);

//Mostrar formulario de envio de mensaje a estudiante
Route::get('/new_mensaje/{usuario}', ['as'=>'new_mensaje','uses'=>'TabControlVacunas@showFormEnviarMensaje'])->middleware('role_ucontrolv');

//Mostrar formulario de envio de mensaje a funcionario
Route::get('/new_mensaje_funcionario', ['as'=>'new_mensaje_funcionario','uses'=>'TabControlVacunas@showFormEnviarMensajeFuncionario'])->middleware('role_estudiante');

//Enviar mensaje a estudiante
Route::post('/create_mensaje', ['as'=>'create_mensaje','uses'=>'TabControlVacunas@newMensaje'])->middleware('role_ucontrolv');

//Enviar mensaje a funcionario
Route::post('/create_mensaje_funcionario', ['as'=>'create_mensaje_funcionario','uses'=>'TabControlVacunas@newMensajeFuncionario'])->middleware('role_estudiante');

//Listar mensajes del estudiante o el funcionario
Route::get('/list_mensajes', ['as'=>'list_mensajes', 'uses'=>'TabControlVacunas@listMensajes'])->middleware('role_estud_o_ucontrolv');
Route::get('/list_mensajes_ajax/{id}', ['as'=>'list_mensajes_ajax','uses'=>'TabControlVacunas@listMensajesAjax'])->middleware('role_estud_o_ucontrolv');

//Marcar una notificación como leída
Route::get('/marcarNotificacion/{id}', ['as'=>'marcarNotificacion','uses'=>'TabControlVacunas@marcarNotificacion']);

//Filtros Reportes
Route::post('/filtro_reporte_vacunas', ['as'=>'filtro_reporte_vacunas','uses'=>'ModalsController@getFiltrosReporteVacunas'])->middleware('role_ucontrolv');

//Prueba PDF
Route::get('/pruebaFooter', 'HistoriaClinica@PDFPrueba');