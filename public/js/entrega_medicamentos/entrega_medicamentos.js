
var app = new Vue({
    el: "#contenedor_vue",
    data :{
        busquedaEntregaMedicamento: "",
        entregaMedicamentos: [],
        nueva_entrega_medicamento: {
            
        },
        pagina: 1,
		numeroDePaginas: 0,
		porPagina: 5
    },

    methods: {
        obtener_entregas_medicamento: function(paciente) {
            axios.get('/obtenerEntregaMedicamentos',{params:{palabra:this.busquedaEntregaMedicamento, persona: paciente, page: this.pagina}}).then(function(response){
                app.entregaMedicamentos =  response.data.entrega_medicamentos.data;
                app.numeroDePaginas = Math.ceil(response.data.nentrega_medicamentos/app.porPagina);
            });
        }
    }

});