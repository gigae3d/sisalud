Vue.component('agregar-glucometria',{
   template: `<div class="modal fade" id="modAgregarGlucometria" tabindex="-1" role="dialog"> 
   <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Agregar Glucometría</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                  <label>Patologia base:</label>
                  <input type="text" class="form-control" v-model="glucometria.patologia" placeholder="Patología"></input>         
                  </div>
                  </div>

                  <div class="col-md-6">
                  <div class="form-group">
                  <label>Resultado:</label>
                  <input type="text" class="form-control" v-model="glucometria.resultado" placeholder="Resultado"></input>         
                  </div>
                  </div>
              </div>

              <div class="row">
                 <div class="col-md-4">
                 <div class="form-group">
                  <label>Tirillas:</label>
                  <input type="number" class="form-control" v-model="glucometria.tirillas" placeholder="Tirillas"></input>         
                  </div>
                 </div>

                 <div class="col-md-4">
                 <div class="form-group">
                  <label>Lancetas:</label>
                  <input type="number" class="form-control" v-model="glucometria.lancetas" placeholder="Lancetas"></input>         
                  </div>
                 </div>

                 <div class="col-md-4">
                 <div class="form-group">
                  <label>Guantes:</label>
                  <input type="number" class="form-control" v-model="glucometria.guantes" placeholder="Guantes"></input>         
                  </div>
                 </div>
              </div>
    </div>

    <div class="modal-footer">
       <label style="color:red;">*Debe llenar todos los campos.</label>
       <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       <button type="button" :disabled="form_glucometria_vacio" class="btn btn-primary">Guardar</button>
    </div>
  </div>
  </div>
  </div>`,
  data: function() {
      return {
          glucometria: {
            patologia: "", resultado: "", tirillas: "", lancetas: "", guantes: ""
          }
      }
  },

  computed: {
      form_glucometria_vacio: function() {
        return Object.values(this.glucometria).some(campo => campo.length==0);
      }
  }
});

Vue.component('agregar-toma-presion1',{
    template: `<div class="modal fade" id="modAgregarTPresionArt1" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Agregar Toma de Presión Arterial</h4>
     </div>
     <div class="modal-body">
     <div class="row">
        <div class="col-md-12">
        <div style="display: flex; justify-content:center;">
        <label>Seleccione un equipo médico</label>
        </div>
        </div>     
     </div>  

     <div class="row">
        <div class="col-md-12">
        <table class="table table-hover">
          <thead>
          <th>Sel</th>
          <th>Nombre</th>
          <th>Modelo</th>
          <th>Tipo</th>
          </thead>
          <tbody>
          <tr v-if="equipos_medicos.length == 0">
            <td colspan="4">No hay datos...</td>
          </tr>
           <tr v-else v-for="equipo_medico in equipos_medicos">
           <td><input type="radio" name="id_equipo_medico" v-model="equipoMedicoSeleccionado"></td>
           <td>{{equipo_medico.nombre}}</td>
           <td>{{equipo_medico.modelo}}</td>
           <td>{{equipo_medico.tipo}}</td>
           </tr>
          </tbody>
        </table>
        </div> 
     </div>

     </div>
 
     <div class="modal-footer">
        <label style="color:red;">*Debe seleccionar un equipo médico.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click="$emit('agregar_toma_p_arterial2')">Siguiente</button>
     </div>
   </div>
   </div>
   </div>`,
   data: function(){
    return {
        equipos_medicos: [],
        numeroDePaginas: 1,
        porPagina: 5,
        paginaEqMedico: 1,
        busquedaEqMedico: "",
        equipoMedicoSeleccionado: ""
    }
   },

   methods: {
    obtener_equipos_medicos: function() {
        let self = this;
        console.log("se ejecutó");
        axios.get('/obtenerEqMedicos',{params:{palabra:this.busquedaEqMedico, page: this.paginaEqMedico}}).then(function(response){
            console.log("Equipos medicos:", response.data.equipos_medicos);
            console.log("numero Equipos medicos:", response.data.nequipos_medicos);
            self.equipos_medicos =  response.data.equipos_medicos.data;
            self.numeroDePaginas= Math.ceil(response.data.nequipos_medicos/self.porPagina);
        });
    }

   }

 });

 Vue.component('agregar-toma-presion2',{
    template: `<div class="modal fade" id="modAgregarTPresionArt2" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Agregar Toma de Presión Arterial</h4>
     </div>
     <div class="modal-body"> 

     <div class="row">
          <div class="col-md-6">
          <div class="form-group">
              <label>Resultado:</label>
              <input type="text" class="form-control" v-model="toma_presion.resultado"
                     placeholder="Resultados">
          </div>
          </div>

          <div class="col-md-6">
           <div class="form-group">
              <label>Tipo:</label>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <label class="radio-inline">
                  <input type="radio" value="Normal" v-model="seleccion_tipo">
                  Normal
              </label>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <label class="radio-inline">
                  <input type="radio" value="Afinamiento" v-model="seleccion_tipo">
                  Afinamiento
              </label>
           </div>
          </div>
          
          
          <div class="col-md-12" v-show="mostrar_normal">
            <div class="form-group">
                <label>Posición:</label>
                <input type="text" class="form-control" v-model="toma_presion.posicion"
                       placeholder="Posicion">
            </div>
          </div>
          
          <div class="col-md-12" v-show="mostrar_afinamiento">
             <div class="row">
                 <div class="col-md-2">
                     <div class="form-group">
                         <label class="checkbox-inline">
                             <input type="checkbox" value="de_pie"  v-model="seleccion_afinamiento">
                             De pie
                         </label>
                     </div>
                 </div>
                 <div class="col-md-5" v-show="mostrar_de_pie">
                     <div class="input-group">
                         <span class="input-group-addon">MSD</span>
                         <input type="text" class="form-control" name="" v-model="toma_presion.de_pie.MSD">
                         <span class="input-group-addon">(mmHg)</span>
                     </div>
                 </div>
                            <div class="col-md-5" v-show="mostrar_de_pie">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" v-model="toma_presion.de_pie.MSI">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
             </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="sentado_tp" value="sentado" v-model="seleccion_afinamiento">
                                        Sentado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_sentado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" v-model="toma_presion.sentado.MSD">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_sentado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" v-model="toma_presion.sentado.MSI">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="acostado_tp" value="acostado" v-model="seleccion_afinamiento">
                                        Acostado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_acostado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" v-model="toma_presion.acostado.MSD">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_acostado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" v-model="toma_presion.acostado.MSI">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>
                    </div>

     </div>

     </div>
 
     <div class="modal-footer">
        <label style="color:red;">*Debe seleccionar un equipo médico.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" :disabled="!form_toma_presion_vacio">Siguiente</button>
     </div>
   </div>
   </div>
   </div>`,
   data: function(){
       return {
           toma_presion: {
            resultado: "", 
            posicion: "",
            de_pie: {
                MSD: "", MSI:""
            },
            sentado: {
                MSD:"", MSI:""
            },
            acostado: {
                MSD:"", MSI: ""
            }
           },
           validaciones_afinamiento:[],
           mostrar_normal: false,
           mostrar_afinamiento: false,
           seleccion_tipo: "",
           mostrar_de_pie: false,
           mostrar_sentado: false,
           mostrar_acostado: false,
           seleccion_afinamiento: []
       }
   },

   watch: {
       seleccion_tipo: function(){
           if(this.seleccion_tipo == "Normal"){
            this.mostrar_afinamiento = false;  
             this.mostrar_normal = true;
           } else if(this.seleccion_tipo == "Afinamiento"){
            this.mostrar_normal = false;
            this.mostrar_afinamiento = true;
           }
       },

       seleccion_afinamiento: function() {
           if(this.seleccion_afinamiento.includes("de_pie")){
               this.mostrar_de_pie = true;
           } else {
               this.mostrar_de_pie = false;
           }

           if(this.seleccion_afinamiento.includes("sentado")){
               this.mostrar_sentado = true;
           } else {
               this.mostrar_sentado = false;
           }

           if(this.seleccion_afinamiento.includes("acostado")){
               this.mostrar_acostado = true;
           } else{
               this.mostrar_acostado = false;
           }
       }
   },

   computed: {
    form_toma_presion_vacio: function() {

      let validacion_general = true;

      if(this.seleccion_afinamiento.length != 0){
      for(let i = 0; i < this.seleccion_afinamiento.length; i++){
          if(Object.values(this.toma_presion[this.seleccion_afinamiento[i]]).some(campo => campo.length == 0)){
              validacion_general = false;
              break;
          }
      }
        return validacion_general;
      } else{
        validacion_general = false;
        return validacion_general;
      }

      
    }
   }
 });




var app = new Vue({
    el: "#contenedor_vue",
    data: {
        identificacion_paciente: "",
        glucometrias:[],
        tomas_pre_arterial: [],
        entregas_medicamento: [],
        equipos_medicos:[],
        //Datos para tabla medicamentos
        medicamentos:[],
        medicamentos_seleccionados: [],
        busquedaMedicamento: "",
        paginaMedicamento: 1,
        numPaginasMedic: 0,
        porPaginaMed: 5
    },

    methods: {
        agregar_glucometria: function () {
            $('#modAgregarGlucometria').modal('show');
        },
        agregar_toma_p_arterial: function () {
            $('#modAgregarTPresionArt1').modal('show');
            this.$refs.sel_equipo_medico.obtener_equipos_medicos();
        },
        agregar_toma_p_arterial2: function() {
            $('#modAgregarTPresionArt1').modal('hide');
            $('#modAgregarTPresionArt2').modal('show');
        },

        agregar_entrega_medicamentos: function (){
            this.obtener_medicamentos();
            $('#modAgregarMedicamentos').modal('show');
        },

        obtener_medicamentos: function() {
            console.log("Se ejecuto obtener_medicamentos");
            let self = this;
            axios.get('/obtenerMedicamentos',{params:{palabra:this.busquedaMedicamento, page: this.paginaMedicamento}}).then(function(response){
                console.log("Medicamentos:", response.data.medicamentos.data);
                console.log("Numero Medicamentos:", response.data.nmedicamentos);
                self.medicamentos =  response.data.medicamentos.data;
                self.numPaginasMedic = Math.ceil(response.data.nmedicamentos/self.porPaginaMed);
            });
        },

        agregar_medicamento: function(medicamento) {
            console.log("intentanto agregar medicamento");
            if(this.medicamentos_seleccionados.filter(dato => dato.id == medicamento[0]).length == 0 && this.medicamentos_seleccionados.length < 5){
                this.medicamentos_seleccionados.push({id: medicamento[0], nombre:medicamento[1], cantidad: ""});
            } else if(this.medicamentos_seleccionados.length == 0){
                this.medicamentos_seleccionados.push({id: medicamento[0], nombre:medicamento[1], cantidad: ""});
            }
        },

        quitar_medicamento: function(medicamento) {
            this.medicamentos_seleccionados = this.medicamentos_seleccionados.filter(dato => dato.id !== medicamento[0]);
        },

        //Metodo para registrar nueva entrega de medicamento
        registrar_entrega_medicamento: function(){
            let fecha_hoy = new Date();
            fecha_hoy = fecha_hoy.getFullYear();
            axios.post('/create_entrega_medicamento', {numero_de_identificacion: this.identificacion_paciente, fecha_de_entrega: fecha_hoy, medicamentos: this.medicamentos_seleccionados})
            .then(function(response){
              console.log(response);
              $('#modAgregarMedicamentos').modal('hide');
              Swal.fire("¡Operación exitosa!",
	      				"Se ha registrado exitosamente la entrega de medicamento",
	      				"success");
            })
            .catch(function(error){
              Swal.fire("¡Error!",
	      				"Hubo un error al intentar realizar la operación.",
	      				"success");
              console.log(error);
            });
        }
    },

    watch:{
		busquedaMedicamento: function(){
			this.paginaMedicamento = 1;
			this.obtener_medicamentos();
		},
		paginaMedicamento: function() {
			this.obtener_medicamentos();
		}

	},

});