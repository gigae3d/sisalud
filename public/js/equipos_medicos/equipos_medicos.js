

Vue.component('editar-equipo-medico', {
    props: ['equipo_medico'],
    template: ``

});


var app = new Vue({
    el: "#contenedor_vue",
    data: {
    equipos_medicos: [],
    nuevo_equipomedico: {
        nombre: "",
        marca: "",
        modelo: "",
        tipo:"",
        existencia:"",
        numero_serie:""
    },
    busquedaEquipoMedico: "",
    pagina: 1,
	numeroDePaginas: 0,
	porPagina: 5
    },

    methods: {
        obtener_equipos_medicos: function () {
            axios.get('/obtenerEqMedicos',{params:{palabra:this.busquedaEquipoMedico, page: this.pagina}}).then(function(response){
                app.equipos_medicos =  response.data.equipos_medicos.data;
                app.numeroDePaginas = Math.ceil(response.data.nequipos_medicos/app.porPagina);
            });
        },

        modal_reg_equipo_medico: function () {
            $('#modAgregarEquipoMedico').modal('show');
        },

        registrar_equipo_medico: function () {
            axios.post('/regEquipoMedico', { equipo_medico: this.nuevo_equipomedico})
					.then(function(response){
					    console.log(response);
							$('#modAgregarEquipoMedico').modal('hide');
							Swal.fire("¡Registro exitoso!",
												"Se ha registrado exitosamente el medicamento",
												"success");
					})
					.catch(function(error) {
						Swal.fire("¡Error!",
												"Hubo un error al registrar ",
												"success");
						console.log(error);
					});
		    this.obtener_equipos_medicos();
            console.log("Metodo registrar equipo medico");
        }

    },

    created: function() {
		this.obtener_equipos_medicos();
    },
    
    computed: {
		campos_nuevo_equipom_vacio: function(){
				return Object.values(this.nuevo_equipomedico).some(equipo => equipo.length==0);		
		}
	}

});