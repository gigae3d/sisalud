Vue.component('editar-medicamento', {
  props: ['medicamento'],
  template: `<div class="modal fade" id="modEditarMedicamento" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Editar medicamento</h4>
        </div>
        <div class="modal-body">
          
                  <div class="row">
                      <div class="col-md-6">
                      <div class="form-group">
                <label>Nombre</label>
                <input type="text" class="form-control" id="nombre" v-model="ed_medicamento.nombre" placeholder="Nombre">{{medicamento.nombre}}</input>
                              
            </div>
  
                      <div class="form-group">
              <label>Fecha de Vencimiento:</label>
              <input type="text" class="form-control" id="fecha_vencimiento" v-model="ed_medicamento.fecha_vencimiento"
                         >{{medicamento.fecha_vencimiento}}</input>
            </div>
  
                      <div class="form-group">
                <label>Presentación:</label>
                <input type="text" class="form-control" v-model="ed_medicamento.presentacion" placeholder="Presentación"
                 >{{medicamento.presentacion}}</input>
            </div>
  
                      <div class="form-group">
                 <label>Registro INVIMA</label>
                <input type="text" class="form-control" id="invima" v-model="ed_medicamento.invima"
                  placeholder="Registro INVIMA" >{{ medicamento.invima }}</input>
             </div>
                      </div>						
                      
                      <div class="col-md-6">
                      <div class="form-group">
            <label>Laboratorio</label>
            <input type="text" class="form-control" id="laboratorio" v-model="ed_medicamento.laboratorio"
                   placeholder="Laboratorio" >{{ medicamento.laboratorio }}</input>
            </div>
  
                      <div class="form-group">
                <label>Existencia</label>
                <input type="number" class="form-control" id="existencia" v-model="ed_medicamento.existencia"
                       placeholder="Cantidad" min="1">{{ medicamento.existencia }}</input>
            </div>
  
                      <div class="form-group">
              <label>Lote</label>
              <input type="text" class="form-control" id="lote" v-model="ed_medicamento.lote" placeholder="Lote"
                     ></input>
              </div>
                      </div>
  
                      <div class="col-md-12">
                      <div class="form-group">
                      <label>Observaciones:</label>
                      <textarea class="form-control" rows="5" v-model="ed_medicamento.observaciones" >{{ medicamento.observaciones }}</textarea>
                      </div>
                    </div>
  
        </div>
        <div class="modal-footer">
                <label v-show="edi_med_vacio" style="color:red;">*Debe llenar todos los campos.</label>
          <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
          <button type="button" class="btn btn-primary" v-on:click="actualizar_medicamento" :disabled="edi_med_vacio">Guardar</button>
        </div>
      </div>
    </div>
  </div>
  </div>`,

  data: function () {
    return {
      ed_medicamento: {
        id: "", nombre: "", laboratorio: "", fecha_vencimiento: "", existencia: "", presentacion: "",
        lote: "", invima: "", observaciones: ""
      }
    }
  },

  methods: {
    actualizar_medicamento: function(){
      axios.post('/actMed', {medicamento: this.ed_medicamento})
      .then(function(response){
        console.log(response);
        $('#modEditarMedicamento').modal('hide');
        Swal.fire("¡Operación exitosa!",
												"Se ha actualizado exitosamente el medicamento",
												"success");
      })
      .catch(function(error){
        Swal.fire("¡Error!",
												"Hubo un error al intentar realizar la operación.",
												"success");
        console.log(error);
      });
      console.log(this.ed_medicamento);
      this.$emit('obtener_medicamentos');
    },

    modal_editar: function(medicamento) {
      console.log("se ejecutó");
      var self2 = this;
      axios.post('/ediMed', { id: medicamento})
      .then(function(response) {
        self2.ed_medicamento = response.data.medicamento;
        console.log(response.data);
      })
      .catch(function(error) {
        console.log(error);
      });

      $('#modEditarMedicamento').modal('show');
    }
  },

  computed: {
    edi_med_vacio: function(){ 
        return Object.values(this.ed_medicamento).some(med => med.length==0);		
    }
  }

});