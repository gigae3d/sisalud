<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Bienestar</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('plugins/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('plugins/dist/css/skins/_all-skins.min.css') }}">
    <!-- Morris chart -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/morris.js/morris.css') }}">
    <!-- jvectormap -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/jvectormap/jquery-jvectormap.css') }}">
    <!-- Date Picker -->
    <link rel="stylesheet"
          href="{{ asset('plugins/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css') }}">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.css') }}">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="{{ asset('plugins/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}">
    <!-- Google Font -->
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <!-- ICheck -->
    <link rel="stylesheet" href="{{ asset('plugins/plugins/iCheck/all.css') }}">
    <!-- DataTables -->
    <link rel="stylesheet"
          href="{{ asset('plugins/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <!--custom css-->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">
    @yield('css')
</head>
<body class="hold-transition skin-blue-light sidebar-mini">
  
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="{{ route('Home') }}" class="logo" style="background-color: white;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>S</b>iacu</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{ asset('plugins/dist/img/LOGO_SISALUD.jpg') }}" width="200" height="45" alt="Logo"></span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">

          @if (Auth::user()->role == 'UControlVacunas' || Auth::user()->role == 'Estudiante')
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
             <i class="fa fa-bell"></i>
             @if ( auth()->user()->unreadNotifications->count() )
             <span class="badge badge-light">{{ auth()->user()->unreadNotifications->count() }}</span>
             @endif
            </a>
          <ul class="dropdown-menu">
            @if ( auth()->user()->unreadNotifications->count() != 0)
            @foreach (auth()->user()->unreadNotifications as $notification)
               <li style="background-color: yellow;"><a href="{{ route('marcarNotificacion', ['id'=>$notification->id]) }}">{{ $notification->data['data'] }}</a></li>
            @endforeach

            @else
            <label >No hay notificaciones</label>
            @endif
          </ul>
          </li>
          @endif
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ asset('plugins/dist/img/icons8-user-24.png') }}" class="user-image" alt="User Image">
                <span class="hidden-xs">{{ Auth::user()->name }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                  <img src="{{ asset('plugins/dist/img/icons8-user-24.png') }}" class="img-circle" alt="User Image">

                  <p>
                    {{ Auth::user()->name }}
                    <small>Miembro desde {{ Auth::user()->created_at }}</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="#" class="btn btn-default btn-flat">Mi Cuenta</a>
                  </div>
                  <div class="pull-right">
                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat">Salir</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </nav>
    </header>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            <!-- Sidebar user panel -->
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{ asset('plugins/dist/img/user_icon.png') }}" class="img-circle bg-primary" alt="User Image">
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <a href="javascript:void(0);"><i class="fa fa-circle text-success"></i> Conectado</a>
                </div>
            </div>
            <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Opciones de Men&uacute;</li>

                @if (Auth::user()->role == 'Admin')
                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-user"></i>
                            <span>Usuarios</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>

                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('register_user') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Crear Usuario</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_usuarios') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Lista Usuarios
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (Auth::user()->role == 'Admin' || Auth::user()->role == 'Enfermero')
                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-user"></i>
                            <span>Pacientes</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>

                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('validate') }}">
                                    <i class="fa fa-circle-o"></i>
                                    <span>Validar Paciente</span>
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_pacientes') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Listar Pacientes
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-medkit"></i>
                            <span>Inventario</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('view_medicamentos') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Medicamentos
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_insumos_medicos') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Insumos Médicos
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_equipos_medicos') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Equipos Médicos
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="">
                        <a href="{{ route('view_entrega_de_medicamentos') }}">
                            <i class="fa fa-plus"></i> <span>Entregas de Medicamentos</span>
                        </a>
                    </li>

                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-plus-square"></i> <span>Atención de Enfermería</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('view_atenciones_en_salud') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Atención en salud
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_inyectologias') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Inyectologia
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_glucometrias') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Glucometria
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_tpresion_arteriales') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Toma de presión
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="treeview">
                        <a href="javascript:void(0);">
                            <i class="fa fa-check-square"></i> <span>Citas</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('new_cita_medico_1') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Ana Carolina Garcia
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('new_cita_medico_2') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Italo Zuñiga
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                @if (Auth::user()->role == 'Admin' || Auth::user()->role == 'Medico')
                    <li class="">
                        <a href="{{ route('view_trabajadores') }}">
                            <i class="fa fa-address-card"></i> <span>Historia Clinica Ocupacional</span>
                        </a>
                    </li>
                @endif

                @if (Auth::user()->role == 'Admin' || Auth::user()->role == 'Enfermero')
                    <li class="treeview ">
                        <a href="javascript:void(0);">
                            <i class="fa fa-check-square"></i> <span>Planificación familiar</span>
                            <span class="pull-right-container">
                                <i class="fa fa-angle-left pull-right"></i>
                            </span>
                        </a>
                        <ul class="treeview-menu">
                            <li>
                                <a href="{{ route('view_medicamentos_antic') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Medicamentos Anticonceptivos
                                </a>
                            </li>
                            <li>
                                <a href="{{ route('view_asesorias_se') }}">
                                    <i class="fa fa-circle-o"></i>
                                    Asesorías en salud sexual
                                </a>
                            </li>
                        </ul>
                    </li>

                    <li class="">
                        <a href="{{ route('view_reg_entrega_medic_antic') }}">
                            <i class="fa fa-plus"></i> <span>Entregas de Anticonceptivos</span>
                        </a>
                    </li>
                @endif

          @if (Auth::user()->role == 'Admin' || Auth::user()->role == 'UControlVacunas' || Auth::user()->role == 'Enfermero')
          
          <li class="">
            <a href="{{ route('reports') }}">
              <i class="fa fa-tasks"></i> <span>Reportes</span>
            </a>
          </li>
          @endif

          @if (Auth::user()->role == 'Admin' || Auth::user()->role == 'UControlVacunas')
          <li class="">
            <a href="{{ route('list_estudiantes_reg') }}">
              <i class="fa fa-tasks"></i> <span>Control de vacunas</span>
            </a>
          </li>
          @endif

          @if (Auth::user()->role == 'Admin' || Auth::user()->role == 'Estudiante')
          <li class="">
            <a href="{{ route('view_registro_est') }}">
              <i class="fa fa-tasks"></i> <span>Estudiante</span>
            </a>
          </li>

          <li class="">
            <a href="{{ route('new_mensaje_funcionario') }}">
              <i class="fa fa-comment-o"></i> <span>Nuevo Mensaje</span>
            </a>
          </li>
          @endif


            </ul>
        </section>
        <!-- /.sidebar -->
    </aside>

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            @yield('migas')
        </section>

        <!-- Main content -->
        <section class="content">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                @yield('contenido')
            </div>
            <!-- /.row -->
        </section>
        <!-- /.content -->
    </div>
    <div class="layer-opacity-loading" id="id-layer-opacity-loading">
        <div style=" width: 100%;height: 100%;">
            <div style="padding-top: 10%; text-align: center;">
                <img src="{{ asset('imgs/loading.gif') }}">
                <br>Procesando ...
            </div>
        </div>
    </div>
    <footer class="main-footer">
        <div class="pull-right">
            <b>Version</b> 1.0
        </div>
        &nbsp;
    </footer>
</div>
<!-- jQuery 3 -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ asset('plugins/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
    $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<!-- Morris.js charts -->
<script src="{{ asset('plugins/bower_components/raphael/raphael.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/morris.js/morris.min.js') }}"></script>
<!-- Sparkline -->
<script src="{{ asset('plugins/bower_components/jquery-sparkline/dist/jquery.sparkline.min.js') }}"></script>
<!-- jvectormap -->
<script src="{{ asset('plugins/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<!-- jQuery Knob Chart -->
<script src="{{ asset('plugins/bower_components/jquery-knob/dist/jquery.knob.min.js') }}"></script>
<!-- daterangepicker -->
<script src="{{ asset('plugins/bower_components/moment/min/moment.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
<!-- datepicker -->
<script src="{{ asset('plugins/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js') }}"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="{{ asset('plugins/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}"></script>
<!-- Slimscroll -->
<script src="{{ asset('plugins/bower_components/jquery-slimscroll/jquery.slimscroll.min.js') }}"></script>
<!-- FastClick -->
<script src="{{ asset('plugins/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('plugins/dist/js/adminlte.min.js') }}"></script>

<!-- AdminLTE for demo purposes -->
<script src="{{ asset('plugins/dist/js/demo.js') }}"></script>

<script type="text/javascript" src="{{ asset('plugins/plugins/jQueryValidate/dist/jquery.validate.js') }}"></script>

<script src="{{ asset('plugins/plugins/iCheck/icheck.min.js') }}"></script>

<!-- DataTables -->
<script src="{{ asset('plugins/bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>

<script src="{{ asset('plugins/bower_components/ckeditor/ckeditor.js') }}"></script>

<script src="{{ asset('js/app.js') }}"></script>
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>

@yield('js')

</body>
</html>
