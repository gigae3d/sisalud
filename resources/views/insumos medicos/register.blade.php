@extends('base.app')

@section('migas')
    <h1>Insumos Médicos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Insumos Médicos</li>
        <li class="active">Crear Insumo Médico</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Insumo Médico</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_insumo_medico') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                    errores:
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input type="text" class="form-control" id="nombre" name="nombre"
                                       placeholder="Nombre del insumo" value="{{ old('nombre') }}">
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label>Marca:</label>
                                <input type="text" class="form-control" id="marca"
                                       name="marca" placeholder="Marca del insumo" value="{{ old('marca') }}">
                            </div>
                            <div class="form-group">
                                <label>Existencia:</label>
                                <input type="number" class="form-control" id="existencia" name="existencia"
                                       placeholder="Existencia en el inventario" min="1"
                                       value="{{ old('existencia') }}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lote:</label>
                                <input type="text" class="form-control" id="lote" name="lote"
                                       placeholder="Especifique el lote" value="{{ old('lote') }}">
                            </div>
                            <!-- /.form group -->
                            <div class="form-group">
                                <label>Fecha de vencimiento:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="fecha_vencimiento"
                                           name="fecha_vencimiento"
                                           placeholder="01/01/2000" value="{{ old('fecha_vencimiento') }}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label>Registro INVIMA:</label>
                                <input type="text" class="form-control"
                                       id="registro_invima" name="registro_invima" placeholder="Registro INVIMA"
                                       value="{{ old('registro_invima') }}">
                            </div>
                        </div>

                        <div class="col-md-12 box-footer">
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                            <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_insumos_medicos') }}"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        //Date picker
        $('#fecha_vencimiento').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    </script>
@endsection

