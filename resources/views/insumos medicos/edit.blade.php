@extends('base.app')

@section('migas')
    <h1>Insumos Médicos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Insumos Médicos</li>
        <li class="active">Editar Insumo Médico</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Editar Insumo Médico</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('edit_insumo_medico') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $insumo_medico->id }}">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                    errores:
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="box-body">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nombre:</label>
                                <input type="text" class="form-control" id="nombres"
                                       name="nombre" value="{{ $insumo_medico->nombre}}">
                            </div>
                            <div class="form-group">
                                <label>Marca:</label>
                                <input type="text" class="form-control" id="marca" name="marca"
                                       value="{{ $insumo_medico->marca}}">
                            </div>
                            <!-- /.form group -->
                            <div class="form-group">
                                <label>Existencia:</label>
                                <input type="number" class="form-control" id="existencia" name="existencia"
                                       value="{{ $insumo_medico->existencia}}">
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Lote:</label>
                                <input type="text" class="form-control" id="lote" name="lote"
                                       value="{{ $insumo_medico->lote}}">
                            </div>
                            <div class="form-group">
                                <label>Fecha de vencimiento:</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control" id="fecha_de_vencimiento"
                                           name="fecha_de_vencimiento" placeholder="01/01/2000"
                                           value="{{ $insumo_medico->fecha_de_vencimiento}}">
                                </div>
                                <!-- /.input group -->
                            </div>
                            <div class="form-group">
                                <label>Registro INVIMA:</label>
                                <input type="text" class="form-control"
                                       id="registro_invima" name="registro_invima"
                                       value="{{ $insumo_medico->Registro_INVIMA }}">
                            </div>
                        </div>
                        <!-- /.box -->
                        <!-- /.box-body -->
                        <div class="col-md-12 box-footer">
                            <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                            <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_insumos_medicos') }}"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                        </div>
                        <!-- /.box-footer -->
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        //Date picker
        $('#fecha_de_vencimiento').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    </script>
@endsection