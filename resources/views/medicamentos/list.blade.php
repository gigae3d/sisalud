@extends('base.app')

@section('migas')
    <h1>Medicamentos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Medicamentos</li>
        <li class="active">Lista medicamentos</li>
    </ol>
@endsection

@section('contenido')
<div id="contenedor_vue">

  	<div class="col-md-12">
	   <div class="box box-success">

		 <div class="box-header with-border">
		 <div class="form-inline" >
		  <label>Buscar:</label>
          <input class="form-control" type="text" id="buscar" v-model="busquedaMedicamento">
         </div>
                <div class="box-tools pull-right">
                    <!-- <a href="{{ route('new_Medicamento')}}" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Medicamento</a>-->
					<a v-on:click="modal_reg_medicamento" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Medicamento</a> 
                </div>
         </div>
			

		<div class="box-body">
    	<table class="table table-hover">
        	<thead>
          	<th>Nombre</th>
          	<th>Laboratorio</th>
            <th>Fecha de vencimiento</th>
          	<th>Existencia</th>
            <th>Presentacion</th>
            <th>Lote</th>
            <th>Invima</th>
            <th>Acciones</th>
        	</thead>

        <tbody v-if="medicamentos.length == 0">
				<tr v-if="medicamentos.length == 0">
				<td colspan="8">No se encontraron resultados...</td>
				<tr>
				</tbody>
				<tbody v-else>
        <tr v-for="medicamento in medicamentos">
        <td>@{{medicamento.nombre}}</td>
        <td>@{{medicamento.laboratorio}}</td>
        <td>@{{medicamento.fecha_vencimiento}}</td>
				<td>@{{medicamento.existencia}}</td>
				<td>@{{medicamento.presentacion}}</td>
				<td>@{{medicamento.lote}}</td>
				<td>@{{medicamento.invima}}</td>
				<td>
				<div style="display: flex;">
				<button type="button" class="btn btn-link "><span class="glyphicon glyphicon-pencil" style="color:#219F08;" aria-hidden="true" @click="modal_edi_medicamento(medicamento.id)"></span></button>
				<button type="button" class="btn btn-link "><span class="glyphicon glyphicon-menu-hamburger" style="color:blue;" aria-hidden="true" @click="modal_observaciones(medicamento.observaciones)"></span></button>
				</div>
				</td>
        </tr>
        </tbody>
    	</table>

    <div class="pull-right">
		<label>Página @{{pagina}} de @{{numeroDePaginas}}</label>
		<button v-if="pagina == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		<button v-else type="button" class="btn btn-link " @click="pagina--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		<button v-if="pagina == numeroDePaginas || numeroDePaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
    <button v-else type="button" class="btn btn-link " @click="pagina++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		</div>
    </div>

		</div>
  	</div>

<div class="modal fade" id="modAgregarMedicamento" tabindex="-1" role="dialog"> <!-- inicio modal agregar medicamento -->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Registro de medicamento</h4>
      </div>
      <div class="modal-body">
        
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" id="nombre" v-model="nuevo_medicamento.nombre" placeholder="Nombre"
              >
							
          </div>

					<div class="form-group">
            <label>Fecha de Vencimiento:</label>
            <input type="text" class="form-control" id="fecha_vencimiento" v-model="nuevo_medicamento.fecha_vencimiento"
                       value="{{ date('m/d/Y')}}">
          </div>

					<div class="form-group">
              <label>Presentación:</label>
              <input type="text" class="form-control" id="presentacion" v-model="nuevo_medicamento.presentacion" placeholder="Presentación"
              >
          </div>

					<div class="form-group">
               <label>Registro INVIMA</label>
              <input type="text" class="form-control" id="invima" v-model="nuevo_medicamento.invima"
                placeholder="Registro INVIMA">
           </div>
					</div>						
					
					<div class="col-md-6">
					<div class="form-group">
          <label>Laboratorio</label>
          <input type="text" class="form-control" id="laboratorio" v-model="nuevo_medicamento.laboratorio"
                 placeholder="Laboratorio">
          </div>

					<div class="form-group">
              <label>Existencia</label>
              <input type="number" class="form-control" id="existencia" v-model="nuevo_medicamento.existencia"
                     placeholder="Cantidad" min="1" >
          </div>

					<div class="form-group">
            <label>Lote</label>
            <input type="text" class="form-control" id="lote" v-model="nuevo_medicamento.lote" placeholder="Lote"
                   >
            </div>
					</div>

					<div class="col-md-12">
					<div class="form-group">
					<label>Observaciones:</label>
					<textarea class="form-control" rows="5" v-model="nuevo_medicamento.observaciones"></textarea>
					</div>
				  </div>

      </div>
      <div class="modal-footer">
			  <label v-show="campos_nuevo_med_vacio" style="color:red;">*Debe llenar todos los campos.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click="registrar_medicamento" :disabled="campos_nuevo_med_vacio">Guardar</button>
      </div>
    </div>
  </div>
</div>
</div> <!--Fin modal agregar medicamento-->  

<editar-medicamento ref="ediMed" v-bind:medicamento="editar_medicamento" v-on:obtener_medicamentos="obtener_medicamentos"></editar-medicamento> <!-- Modal para editar medicamento -->


<div id="modObsMedicamento" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Observaciones</h4>
      </div>
      <div class="modal-body">
        <p v-html="ver_observaciones"></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
      </div>
    </div>
  </div>
</div>

</div><!--Cierre contenedor Vue-->

@endsection

@section('js')

<script src="{{ asset('js/medicamentos/medicamentos.js') }}"></script>
<script src="{{ asset('js/sweetalert2.js') }}"></script>

<script>

var app = new Vue({
    el: "#contenedor_vue",
    data: {
    medicamentos: [],
		nuevo_medicamento: {
		  nombre: "", laboratorio: "", fecha_vencimiento: "", existencia: "", presentacion: "",
		  lote: "", invima: "", observaciones: ""
		},
		editar_medicamento: {},
		busquedaMedicamento: "",
		pagina: 1,
		numeroDePaginas: 0,
		porPagina: 5,
		ver_observaciones: ""
    },


    methods: {

				limpiar_formulario_medicamento: function(){
				this.nuevo_medicamento = {
			    nombre: "", laboratorio: "", fecha_vencimiento: "", existencia: "", presentacion: "",
			    lote: "", invima: "", observaciones: ""
		    }
				},

        obtener_medicamentos: function () {
			  axios.get('/obtenerMedicamentos',{params:{palabra:this.busquedaMedicamento, page: this.pagina}}).then(function(response){
			  	app.medicamentos =  response.data.medicamentos.data;
				  app.numeroDePaginas = Math.ceil(response.data.nmedicamentos/app.porPagina);
				});
			
        },

				modal_reg_medicamento: function () {
					this.limpiar_formulario_medicamento();
					$('#modAgregarMedicamento').modal('show');
				},

				registrar_medicamento: function() {
					axios.post('/regMedNew', { medicamento: this.nuevo_medicamento})
					.then(function(response){
					    console.log(response);
							$('#modAgregarMedicamento').modal('hide');
							Swal.fire("¡Registro exitoso!",
												"Se ha registrado exitosamente el medicamento",
												"success");
					})
					.catch(function(error) {
						Swal.fire("¡Error!",
												"Hubo un error al registrar ",
												"success");
						console.log(error);
					});
					this.obtener_medicamentos();
				},

			  modal_edi_medicamento: function(medicamento) {
					this.$refs.ediMed.modal_editar(medicamento);
				},

				modal_observaciones: function(observaciones) {
					this.ver_observaciones = observaciones;
					$('#modObsMedicamento').modal('show');
				}
				
    },

	created: function() {
		this.obtener_medicamentos();
	},

	watch:{
		busquedaMedicamento: function(){
			this.pagina = 1;
			this.obtener_medicamentos();
		},
		pagina: function() {
			this.obtener_medicamentos();
		}

	},

	computed: {
		campos_nuevo_med_vacio: function(){
				return Object.values(this.nuevo_medicamento).some(med => med.length==0);		
		}
	}

});



</script>

@endsection