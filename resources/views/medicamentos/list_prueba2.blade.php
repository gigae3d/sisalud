<!DOCTYPE html>

<head>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:regular,bold,italic,thin,light,bolditalic,black,medium&amp;lang=en">
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<style>

.fila-opciones{
  display: flex;
  justify-content: space-between;

}

.caja-opcion {
    height: 120px;
    width: 80%;
    min-width: 160px;
    display: flex;
    border-radius: 7px;
    overflow: hidden;
    margin: 7px;
    box-shadow: 0 0 5px gray;
}

.caja-opcion-acciones{
	width: 30%;
	height:120px;
	background-color: #219F08;
}

.caja-opcion-nombre{
    width: 70%;
    height:120px;
    background-color: #55CB3D;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
}

.botones-menu{
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;

}

@font-face{
  font-family: Roboto-Regular;
  src: url("RobotoTTF/Roboto-Regular.ttf");
 
}

p{
  font-family:Roboto-Regular;
}



.margenes{
	padding: 8px;
	border-color: #DAD7D7;
	border-style: solid;
	border-radius: 7px;
	border-width: thin;
}

.fondo-grisaceo {
	background-color: #e6e6e6;
	
}

</style>

</head>

<body>

<div id="contenedor_vue">

<nav class="navbar navbar-default navbar-static-top">
    	<div class="container">
    	<div class="navbar-header ">
      	<a class="navbar-brand" href="#">Inventario</a>
    	</div>
    	</div>
</nav>

<div class="container-fluid">

<div class="row">

	<div class="col-md-12"><!--  -->

 	<div class="row fila-opciones" style="height: 190px;">
  	<div class="col-md-2">
  	<div class="caja-opcion">
   		 <div class="caja-opcion-nombre">
           <i class="material-icons" style="color:white; font-size: 65px;" aria-hidden="true">create</i>
   		   <p style="font-size: 15px; color: white;">Medicamentos</p>
   		 </div>
   		 <div class="caja-opcion-acciones">
   			<div class="botones-menu">
         <button type="button" class="btn btn-link" v-on:click="modal_reg_medicamento"><i class="material-icons" style="color:white; font-size: 35px;" aria-hidden="true">add_circle</i></button>
   			 <button type="button" class="btn btn-link"><i class="material-icons" style="color:white; font-size: 35px;" aria-hidden="true">search</i></button>
   			</div>
   		 </div>
  	</div>
  	</div>

  	<div class="col-md-2">
  	<div class="caja-opcion">
   		 <div class="caja-opcion-nombre">
           <i class="material-icons" style="color:white; font-size: 65px;" aria-hidden="true">favorite</i>
   		   <p style="font-size: 15px; color: white;">Insumos Médicos</p>
   		 </div>
   		 <div class="caja-opcion-acciones">
   			<div class="botones-menu">
   			 <button type="button" class="btn btn-link"><span class="glyphicon glyphicon-plus-sign" style="color:white; font-size: 30px;" aria-hidden="true"></span></button>
   			 <button type="button" class="btn btn-link"><span class="glyphicon glyphicon-search" style="color:white; font-size: 30px;" aria-hidden="true"></span></button>
   			</div>
   		 </div>
  	</div>
  	</div>

  	<div class="col-md-2">
  	<div class="caja-opcion">
   		 <div class="caja-opcion-nombre">
		   <i class="material-icons" style="color:white; font-size: 65px;" aria-hidden="true">store</i>
   		   <p style="font-size: 15px; color: white;">Equipos Médicos</p>
   		 </div>
   		 <div class="caja-opcion-acciones">
   			<div class="botones-menu">
   			 <button type="button" class="btn btn-link"><span class="glyphicon glyphicon-plus-sign" style="color:white; font-size: 30px;" aria-hidden="true"></span></button>
   			 <button type="button" class="btn btn-link"><span class="glyphicon glyphicon-search" style="color:white; font-size: 30px;" aria-hidden="true"></span></button>
   			</div>
   		 </div>
  	</div>
  	</div>

  	<div class="col-md-2">
  	<div class="caja-opcion">
      	<div class="caja-opcion-nombre">
    	<p style="font-size: 15px; color: white;">Anticonceptivos</p>
      	</div>
      	<div class="caja-opcion-acciones">
         	<div class="botones-menu">
    	<button type="button" class="btn btn-link"><span class="glyphicon glyphicon-plus-sign" style="color:white; font-size: 30px;" aria-hidden="true"></span></button>
    	<button type="button" class="btn btn-link"><span class="glyphicon glyphicon-search" style="color:white; font-size: 30px;" aria-hidden="true"></span></button>
     	</div>
      	</div>
  	</div>
  	</div>
 	</div>


 	<div class="row">
  	<div class="col-md-12">
		  <div class="margenes">

			<div class="row">
			<div class="col-md-4">
			
        <div class="form-inline" >
				  <label>Buscar:</label>
          <input class="form-control" type="text" id="buscar" v-model="busquedaAsesorAyer">
        </div>
      </div>

	    <div class="col-md-offset-6 col-md-2">
      <label>Listado de Medicamentos</label>
			</div>
			</div>

    	<table class="table table-hover">
        	<thead>
          	<th>Nombre</th>
          	<th>Laboratorio</th>
            <th>Fecha de vencimiento</th>
          	<th>Existencia</th>
            <th>Presentacion</th>
            <th>Lote</th>
            <th>Invima</th>
            <th>Acciones</th>
        	</thead>

        	<tbody>
          	<tr v-for="medicamento in medicamentos">
            	<td>@{{medicamento.nombre}}</td>
            	<td>@{{medicamento.laboratorio}}</td>
            	<td>@{{medicamento.fecha_vencimiento}}</td>
				      <td>@{{medicamento.existencia}}</td>
				      <td>@{{medicamento.presentacion}}</td>
				      <td>@{{medicamento.lote}}</td>
				      <td>@{{medicamento.invima}}</td>
				<td>
				<div style="display: flex;">
				<button type="button" class="btn btn-link "><span class="glyphicon glyphicon-pencil" style="color:#219F08;" aria-hidden="true" @click="modal_edi_medicamento(medicamento.id)"></span></button>
				<button type="button" class="btn btn-link "><span class="glyphicon glyphicon-remove" style="color:red;" aria-hidden="true"></span></button>
				</div>
				</td>
          	</tr>
        	</tbody>
    	</table>


			<table v-show="mostrar_insumos" class="table table-hover">
				<thead>
					<th>Nombre</th>
					<th>Marca</th>
					<th>Existencia</th>
					<th>Lote</th>
					<th>Fecha de vencimiento</th>
					<th>Registro Invima</th>
					<th>Acciones</th>
				</thead>
				<tbody>
					<tr><td>Prueba</td></tr>
				</tbody>
			</table>

			</div>
  	</div>

  	<!--<div class="col-md-4 ">
    	<div class="margenes"><h4>DETALLES REGISTRO</h4>
    	<p>TEADASD ADSADA SSADASODJOOC EOJOE DOAJEODEAN CEOJOE</p>
    	</div>
  	</div>-->

 	</div>
    
	</div>
   	 
</div>

</div>

<div class="modal fade" id="modAgregarMedicamento" tabindex="-1" role="dialog"> <!-- inicio modal agregar medicamento -->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Registro de medicamento</h4>
      </div>
      <div class="modal-body">
        
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" id="nombre" v-model="nuevo_medicamento.nombre" placeholder="Nombre"
              value="{{ old('nombre') }}">
          </div>

					<div class="form-group">
            <label>Fecha de Vencimiento:</label>
            <input type="text" class="form-control" id="fecha_vencimiento" v-model="nuevo_medicamento.fecha_vencimiento"
                       alue="{{ date('m/d/Y')}}">
          </div>

					<div class="form-group">
              <label>Presentación:</label>
              <input type="text" class="form-control" id="presentacion" v-model="nuevo_medicamento.presentacion" placeholder="Presentación"
              >
          </div>

					<div class="form-group">
               <label>Registro INVIMA</label>
              <input type="text" class="form-control" id="invima" v-model="nuevo_medicamento.invima"
                placeholder="Registro INVIMA">
           </div>
					</div>						
					
					<div class="col-md-6">
					<div class="form-group">
          <label>Laboratorio</label>
          <input type="text" class="form-control" id="laboratorio" v-model="nuevo_medicamento.laboratorio"
                 placeholder="Laboratorio">
          </div>

					<div class="form-group">
              <label>Existencia</label>
              <input type="number" class="form-control" id="existencia" v-model="nuevo_medicamento.existencia"
                     placeholder="Cantidad" min="1" >
          </div>

					<div class="form-group">
            <label>Lote</label>
            <input type="text" class="form-control" id="lote" v-model="nuevo_medicamento.lote" placeholder="Lote"
                   >
            </div>
					</div>

					<div class="col-md-12">
					<div class="form-group">
					<label>Observaciones:</label>
					<textarea class="form-control" rows="5" v-model="nuevo_medicamento.observaciones"></textarea>
					</div>
					
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click="registrar_medicamento">Guardar</button>
      </div>
    </div>
  </div>
</div>
</div> <!--Fin modal agregar medicamento-->


<div class="modal fade" id="modEditarMedicamento" tabindex="-1" role="dialog"><!-- inicio modal editar medicamento -->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalLabelEditarMedicamento">Editar medicamento</h4>
      </div>
      <div class="modal-body">
        
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" placeholder="Nombre" >
          </div>

					<div class="form-group">
            <label>Fecha de Vencimiento:</label>
            <input type="text" class="form-control" >
          </div>

					<div class="form-group">
              <label>Presentación:</label>
              <input type="text" class="form-control" placeholder="Presentación"
              >
          </div>

					<div class="form-group">
               <label>Registro INVIMA</label>
              <input type="text" class="form-control"  placeholder="Registro INVIMA" >
           </div>
					</div>						
					
					<div class="col-md-6">
					<div class="form-group">
          <label>Laboratorio</label>
          <input type="text" class="form-control"  
                 placeholder="Laboratorio" >
          </div>

					<div class="form-group">
              <label>Existencia</label>
              <input type="number" class="form-control"  placeholder="Cantidad" min="1" >
          </div>

					<div class="form-group">
            <label>Lote</label>
            <input type="text" class="form-control" placeholder="Lote">
            </div>
					</div>

					<div class="col-md-12">
					<div class="form-group">
					<label>Observaciones:</label>
					<textarea class="form-control" rows="5"></textarea>
					</div>
					
				</div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click="registrar_medicamento">Guardar</button>
      </div>
    </div>
  </div>
</div>
</div> <!-- fin modal editar medicamento -->



</div>
    
<script src="{{ asset('js/vue.js') }}"></script>
<script src="{{ asset('js/axios.min.js') }}"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script>


var app = new Vue({
    el: "#contenedor_vue",
    data: {
        medicamentos: [],
		    nuevo_medicamento: {
			  nombre: "", laboratorio: "", fecha_vencimiento: "", existencia: "", presentacion: "",
			  lote: "", invima: "", observaciones: ""
		    }
    },


    methods: {

				limpiar_formulario_medicamento: function(){
				this.nuevo_medicamento = {
			  nombre: "", laboratorio: "", fecha_vencimiento: "", existencia: "", presentacion: "",
			  lote: "", invima: "", observaciones: ""
		    }
				},

        obtener_medicamentos: function () {
			  axios.get('/obtenerMedicamentos').then(function(response){
			  	app.medicamentos =  response.data.medicamentos;
			  });
        },

				modal_reg_medicamento: function () {
					this.limpiar_formulario_medicamento();
					$('#modAgregarMedicamento').modal('show');
				},

				registrar_medicamento: function() {
					axios.post('/regMedNew', { medicamento: this.nuevo_medicamento})
					.then(function(response){
					    console.log(response);
					    $('#modAgregarMedicamento').modal('hide');
					})
					.catch(function(error) {
						console.log(error);
					});
				},

				modal_edi_medicamento: function(medicamento) {
					var self = this;
					axios.post('/ediMed', { id: medicamento})
					.then(function(response) {
						self.nuevo_medicamento = response.data.medicamento;
						console.log(response);
					})
					.catch(function(error) {
						console.log(error);
					})

					$('#modAgregarMedicamento').modal('show');
				},

				mostrar_medicamentos: function(){
					this.mostrar_insumos = false;
					this.mostrar_medicamentos = true;
				},

				mostrar_insumos: function(){

				}
					
				
    },

	created: function() {
		this.obtener_medicamentos();
	}

});

</script>

</body>

</html>
