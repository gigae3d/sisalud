@extends('base.app')

@section('migas')
    <h1>Medicamentos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Medicamentos</li>
        <li class="active">Crear medicamento</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Medicamento</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_medicamento') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre"
                                   value="{{ old('nombre') }}">
                        </div>
                        <div class="form-group">
                            <label>Laboratorio</label>
                            <input type="text" class="form-control" id="laboratorio" name="laboratorio"
                                   placeholder="Laboratorio" value="{{ old('laboratorio') }}">
                        </div>
                        <!-- Date -->
                        <div class="form-group">
                            <label>Fecha de Vencimiento:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="fecha_vencimiento" name="fecha_vencimiento"
                                       alue="{{ date('m/d/Y')}}">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        <div class="form-group">
                            <label>Existencia</label>
                            <input type="number" class="form-control" id="existencia" name="existencia"
                                   placeholder="Cantidad" min="1" value="{{ old('existencia')}}">
                        </div>
                        <div class="form-group">
                            <label>Presentacion</label>
                            <input type="text" class="form-control" id="presentacion" name="presentacion"
                                   placeholder="Jarabe, Pastas" value="{{ old('presentacion') }}">
                        </div>
                        <div class="form-group">
                            <label>Lote</label>
                            <input type="text" class="form-control" id="lote" name="lote" placeholder="Lote"
                                   value="{{ old('lote') }}">
                        </div>
                        <div class="form-group">
                            <label>Registro INVIMA</label>
                            <input type="text" class="form-control" id="invima" name="invima"
                                   placeholder="Registro INVIMA" value="{{ old('invima') }}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Observaciones</label>
                            <textarea id="observaciones" name="observaciones" rows="10" cols="80"
                                      value="{{ old('observaciones') }}">
                                Descripcion
                            </textarea>
                        </div>
                    </div>
                    <!-- /.box -->
                    <!-- /.box-body -->
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"
                                                                                    aria-hidden="true"></i> Guardar
                        </button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_medicamentos') }}"><i
                                    class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            CKEDITOR.replace('observaciones')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
        //Date picker
        $('#fecha_vencimiento').datepicker({
            startDate: '-10d',
            autoclose: true
        });
    </script>
@endsection
