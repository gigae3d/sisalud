@extends('base.app')

@section('migas')
    <h1>Medicamentos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Medicamentos</li>
        <li class="active">Editar medicamento</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Editar Medicamento</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('edit_medicamento') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $medicamentos->id }}">
                <div class="box-body">
                    @if ($errors->any())
                        <div class="box-body">
                            <div class="col-md-12">
                                <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                                aria-hidden="true">&times;</span></button>
                                    <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                    errores:
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    @endif

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" id="nombre"
                                   name="nombre" placeholder="Nombre" value="{{ $medicamentos->nombre}}">
                        </div>
                        <div class="form-group">
                            <label>Laboratorio:</label>
                            <input type="text" class="form-control" id="laboratorio" name="laboratorio"
                                   placeholder="Laboratorio" value="{{ $medicamentos->laboratorio}}">
                        </div>
                        <!-- Date -->
                        <div class="form-group">
                            <label>Fecha de vencimiento:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="fecha_vencimiento"
                                       name="fecha_vencimiento"
                                       placeholder="01/01/2000" value="{{ $medicamentos->fecha_vencimiento}}">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <!-- /.form group -->
                        <div class="form-group">
                            <label>Existencia:</label>
                            <input type="number" class="form-control" id="existencia" name="existencia"
                                   placeholder="Cantidad" min="0" value="{{ $medicamentos->existencia}}">
                        </div>
                        <div class="form-group">
                            <label>Presentacion:</label>
                            <input type="text" class="form-control" id="presentacion" name="presentacion"
                                   placeholder="Jarabe, Pastas" value="{{ $medicamentos->presentacion}}">
                        </div>
                        <div class="form-group">
                            <label>Lote:</label>
                            <input type="text" class="form-control" id="lote" name="lote" placeholder=""
                                   value="{{ $medicamentos->lote}}">
                        </div>
                        <div class="form-group">
                            <label>Registro INVIMA:</label>
                            <input type="text" class="form-control" id="invima" name="invima" placeholder=""
                                   value="{{ $medicamentos->invima}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Observaciones</label>
                            <textarea id="observaciones" name="observaciones" rows="10" cols="80">
	                    	        {{ $medicamentos->observaciones }}
                                </textarea>
                        </div>
                    </div>
                    <!-- /.box -->
                    <!-- /.box-body -->
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"
                                                                                    aria-hidden="true"></i> Guardar
                        </button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_medicamentos') }}"><i
                                    class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(function () {
            // Replace the <textarea id="observaciones"> with a CKEditor
            // instance, using default configuration.
            CKEDITOR.replace('observaciones')
            //bootstrap WYSIHTML5 - text editor
            $('.textarea').wysihtml5()
        })
        //Date picker
        $('#fecha_vencimiento').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    </script>
@endsection