@extends('base.app')

@section('migas')
    <h1>Medicamentos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Medicamentos</li>
        <li class="active">Lista medicamentos</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Medicamentos</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('new_Medicamento')}}" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Medicamento</a>
                </div>
            </div>
            <div class="box-body">
                @if(session()->has('exito'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok"></span> {{ session('exito') }}
                    </div>
                @endif
                <table id="lista_medicamentos" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nombre</th>
                        <th>Laboratorio</th>
                        <th>Fecha de Vencimiento</th>
                        <th>Existencia</th>
                        <th>Presentacion</th>
                        <th>Lote</th>
                        <th>INVIMA</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbspConsultar estadísticas para un
                    medicamento</h3>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Medicamento</label>
                        <select class="form-control" name="medicamento" id="medicamento">
                            <option value="">- Seleccione -</option>
                            @foreach($medicamentos as $medicamento)
                                <option value="{{ $medicamento->id }}">{{ $medicamento->nombre }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Desde:</label>
                        <div class="form-inline input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="fecha_inicio" name="fecha_inicio" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Hasta:</label>
                        <div class="form-inline input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="fecha_fin" name="fecha_fin" required="">
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-inline">
                            <button id="EstMedicamento" type="button" class="btn btn-primary pull-right"><i
                                        class="fa fa-search" aria-hidden="true"></i> Consultar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMedicamento" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Información adicional del Medicamento</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <b><p>Observaciones:</p></b>
                            <p id="observaciones"></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEstadisticaMed" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Estadística por medicamento</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Medicamento: <span id="nameMedicamento"></span></h3>
                            <h5>Rango de Fechas: <span id="rangoFechas"></span></h5>
                            <h5>Numero de entregas: <span id="num_veces_entregado"></span></h5>
                            <h5>Cantidad total Entregada: <span id="cantidad"></span> Dosis</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lista_medicamentos').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar de manera Ascendente",
                        "sSortDescending": ": Ordenar de manera Descendente"
                    }
                },
                "ajax": '{{ route("list_medicamentos_ajax") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            //Mostrar informacion adicional de un medicamento
            $('#lista_medicamentos ').on('click', 'tbody tr td div button', function () {
                var codigo = $(this).parent().children('p').text();//Obtener el id del medicamento

                $.ajax({
                    method: "GET",
                    url: "{{ route('get_observaciones_med') }}",
                    data: {id: codigo},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        $('#observaciones').text($(json.observaciones).text());
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });

                $('#modalMedicamento').modal("show");
            });

            //Mostrar estadistica para un medicamento
            $('#EstMedicamento').on('click', function () {
                var fecha_inicioA = new Date($('#fecha_inicio').val());
                var fecha_finA = new Date($('#fecha_fin').val());
                $("#nameMedicamento").text($('select[name="medicamento"] option:selected').text());

                function pad(number) {
                    if (number < 10) {
                        return '0' + number;
                    }
                    return number;
                }

                var fechaInicio = fecha_inicioA.getFullYear() + "-" + pad(fecha_inicioA.getMonth() + 1) + "-" + pad(fecha_inicioA.getDate());
                var fechaFin = fecha_finA.getFullYear() + "-" + pad(fecha_finA.getMonth() + 1) + "-" + pad(fecha_finA.getDate());

                $("#rangoFechas").text('Del ' + fechaInicio + ' al ' + fechaFin);

                $.ajax({
                    method: "GET",
                    url: "{{ route('est_medicamento') }}",
                    data: {
                        medicamento: $('#medicamento').val(),
                        fecha_inicio: fechaInicio,
                        fecha_fin: fechaFin
                    },
                    dataType: "JSON"
                })
                    .done(function (json) {
                        if(json.cantidad > 0) {
                            $('#num_veces_entregado').text(json.nentregas_med);
                            $('#cantidad').text(json.cantidad);
                        }else{
                            $('#num_veces_entregado').text(0);
                            $('#cantidad').text(0);
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
                $('#modalEstadisticaMed').modal("show");
            });

            $('#fecha_inicio').datepicker({
                autoclose: true
            });

            $('#fecha_fin').datepicker({
                autoclose: true
            });
        });
    </script>
@endsection