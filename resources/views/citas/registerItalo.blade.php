@extends('base.app')

@section('migas')
    <h1>
        Programación de Citas Italo Zuñiga
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Citas</li>
        <li>Italo Zuñiga</li>
        <li class="active">Registra Nueva Cita</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-plus-circle" aria-hidden="true"></i> Registra Nueva Cita</h3>
            </div>
            <form role="form" action="{{ route('create_cita') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if(session()->has('exito'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok"></span> {{ session('exito') }}
                    </div>
                @endif

                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                @if ($validar_registro)
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> en la base de datos, ya existe una cita con la fecha y
                                la hora ingresadas.
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body" id="nofountId" style="display: none">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>Importante!</strong> El Número de identificación, No esta registrado en la Base de
                            Datos, por favor, primero registra el paciente en
                            <a href="{{ route('new_paciente')}}" class="btn btn-link">Aqui</a>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha</label>
                            <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="buscar_en_fecha" name="fecha">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Hora</label>
                            <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-clock-o"></i></div>
                                <select class="form-control" name="hora">
                                    <option value="">- Seleccione -</option>
                                    <option value="14:00 PM">14:00 PM</option>
                                    <option value="14:30 PM">14:30 PM</option>
                                    <option value="15:00 PM">15:00 PM</option>
                                    <option value="15:30 PM">15:30 PM</option>
                                    <option value="16:00 PM">16:00 PM</option>
                                    <option value="16:30 PM">16:30 PM</option>
                                    <option value="17:00 PM">17:00 PM</option>
                                    <option value="17:30 PM">17:30 PM</option>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número de identificación</label>
                            <input type="text" class="form-control" id="numero_de_identificacion"
                                   name="numero_de_identificacion">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombres y apellidos</label>
                            <input type="text" class="form-control" id="nombres_y_apellidos" name="nombres_y_apellidos">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Motivo de consulta</label>
                            <input type="text" class="form-control" name="motivo_consulta">
                            <input type="hidden" name="medico" value="Italo Zuniga">
                        </div>
                    </div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-info">
            <div class="col-md-12 box-header with-border">
                <h3 class="box-title">Programación de Citas</h3>
            </div>
            <div class="box-body">
                <table id="lista_citas" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Fecha</th>
                        <th>Hora</th>
                        <th>Numero de identificacion</th>
                        <th>Motivo de consulta</th>
                        <th>Medico</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lista_citas').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": {"url": '{{ route("list_citas_cgarcia_ajax") }}'},
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            //Date picker
            $('#buscar_en_fecha').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            var table = $('#lista_citas').DataTable();

            $('#buscar_en_fecha').on('change', function () {
                var v = $(this).val();  // getting search input value
                var date = new Date(v);

                function pad(number) {
                    if (number < 10) {
                        return '0' + number;
                    }
                    return number;
                }
                var fecha = date.getFullYear() + "-" + pad(date.getMonth() + 1) + "-" + pad(date.getDate());
                table.search(fecha).draw();
            });

            //Evento para mostrar el nombre y apellido del paciente al escribir su CC
            $('#numero_de_identificacion').blur(function () {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        if (json.nombres == '-' && json.apellidos == '-') {
                            $("#nofountId").show();
                            $('#numero_de_identificacion').val('');
                            $('#nombres_y_apellidos').val('');
                        } else {
                            $('#nombres_y_apellidos').val(json.nombres + ' ' + json.apellidos);
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            });

            $('#numero_de_identificacion').focus(function () {
                $("#nofountId").hide();
            });
        });
    </script>
@endsection