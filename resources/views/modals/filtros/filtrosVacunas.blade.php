<form role="form" action="{{ route('rep_cuadro_vacunas') }}" method="GET">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label style="font-size:16px; font-style: italic;">Programa:</label>
                <select class="form-control" name="programa">
                    @if ($programas)
                        <option value="all">Todos</option>
                        @foreach($programas as $programa)
                            <option value="{{ $programa->id }}">{{ $programa->name }}</option>
                        @endforeach
                    @endif
                </select>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label style="font-size:16px; font-style: italic;">Semestre:</label>
                <select class="form-control" name="semestre">
                    <option value="0">Todos</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                </select>
            </div>
        </div>
        <!-- /.box -->
        <!-- /.box-body -->
        <div class="col-md-12 box-footer">
            <button type="submit" class="btn btn-info pull-right">Generar</button>
        </div>
        <!-- /.box-footer -->
    </div>
</form>

<script type="text/javascript">
    $(document).ready(function () {
        var $modal = $('#cerrarModal');
        $modal.click(function () {
            var $closeModal = $('#modalFiltrosVacunas');
            closeModal($closeModal);
        })
    });
</script>
