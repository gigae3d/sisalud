@extends('base.app')

@section('migas')
<h1>
Historias Control Fecundidad
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Historia Control Fecuncidad</li>
</ol>
@endsection

@section('contenido')

<div class="col-xs-12 col-md-12">
	<div class="box box-success">
        <div class="box-header with-border">
          	<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Historias de Control</h3>
			<div class="box-tools pull-right">
		        <a href="{{ route('new_historia_clinica_seg_w')}}" class="btn btn-success"><i class="fa fa-list"></i>&nbsp;Crear nueva historia de control</a>
	      	</div>
		</div>
        <div class="box-body">
			<table id="lista_historias_control_fec" class="table table-bordered table-striped" width="100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Paciente</th>
						<th>Menarquia</th>
						<th>Ciclos menstruales</th>
						<th>F.U.M</th>
						<th>Cirugía ginecológica</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('#lista_historias_control_fec').DataTable( {
			"language": {
			    "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			},
	        "ajax": '{{ route("list_h_control_fec_ajax") }}',
	        "deferRender":    false,
	        "processing": 	  true,
	        "scrollCollapse": true,
	        "scrollX":       true,
	        "scroller":       true,
	        "stateSave":      true,
          	"serverSide": 	true
	    } );
	});
</script>

@endsection