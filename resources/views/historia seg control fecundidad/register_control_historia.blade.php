@extends('base.app')

@section('migas')
<h1>
Historias seguimiento control fecundidad
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Historias seguimiento control fecundidad</li>
</ol>
@endsection

@section('contenido')

<div class="col-md-12">
	<div class="box box-success">

	   <div class="col-md-12 box-header with-border">
        <h3 class="box-title" style="text-align: center;">Registrar nuevo control a historia clínica</h3>
       </div>

    <form role="form" action="{{ route('create_control_historia') }}" method="POST">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <div class="box-body">

        <div class="row">
          <div class="col-md-4">
           <div class="form-group">
            <label style="font-size:16px; font-style: italic;">Número de identificación:</label>
           @if (isset($id))
              <input type="text" class="form-control" id="numero_de_identificacion" value="{{ $id }}" disabled>
              <input name="historia" type="hidden" value="{{ $id }}">  
           @else 
            <input type="text" class="form-control" id="numero_de_identificacion" name="numero_de_identificacion" placeholder="Cédula, Tarjeta de identidad">
           @endif
            </div>
      </div>

        </div>
        
        <div class="row">
        <div class="col-md-4">
            <div class="form-group">
	          <label>Fecha</label>
	          <input type="text" class="form-control" id="fecha" name="fecha">
	        </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
	          <label>Peso</label>
	          <input type="text" class="form-control" name="peso">
	        </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
	          <label>T.A.</label>
	          <input type="text" class="form-control" name="tension_arterial">
	        </div>
        </div>

        </div>

        <div class="row">
        <div class="col-md-4">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Sangrado intermenstrual:</label>

               <label class="radio-inline"> 
               <input type="radio" name="sangrado_intermenstrual" value="Si">
               Si
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="sangrado_intermenstrual" value="No">
               No
               </label>
	        </div>
        </div>

        <div class="col-md-4">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Efectos secundarios:</label>

               <label class="radio-inline"> 
               <input type="radio" name="efectos_secundarios" value="Si">
               Si
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="efectos_secundarios" value="No">
               No
               </label>
	        </div>
        </div>

        <div class="col-md-4">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Falla del método:</label>

               <label class="radio-inline"> 
               <input type="radio" name="falla_del_metodo" value="Si">
               Si
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="falla_del_metodo" value="No">
               No
               </label>
	        </div>
        </div>


        </div>

        <div class="row">
        <div class="col-md-4">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Signos de infección pélvica:</label>

               <label class="radio-inline"> 
               <input type="radio" name="signos_infeccion_pel" value="Si">
               Si
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="signos_infeccion_pel" value="No">
               No
               </label>
	        </div>
        </div>

        <div class="col-md-4">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Anexos:</label>

               <label class="radio-inline"> 
               <input type="radio" name="anexos" value="Normal">
               Normal
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="anexos" value="Anormal">
               Anormal
               </label>
	        </div>
        </div>
        </div>

        <div class="row">
           <div class="col-md-12">
            <div class="form-group">
	          <label>Cambio anticonceptivo y motivo</label>
	          <input type="text" class="form-control" name="cambio_anticonceptivo" placeholder="Escriba aqui si hubo cambio de anticonceptivo o no, y si lo hubo explique el motivo">
	        </div>
        </div>
        </div>

        <div class="row">
        <div class="col-md-12">
              <h3 class="box-title">Observaciones:
              </h3> 
              <div class="box-body pad">
                <textarea id="observaciones" name="observaciones" rows="10" cols="80">
                      Descripcion
               </textarea>
              </div>
        </div>
        </div>

        <div class="col-md-12 box-footer">
            <button type="submit" class="btn btn-default">Cancelar</button>
            <button type="submit" class="btn btn-info pull-right">Registrar Control</button>
        </div>

       </div> 
     </form>
 
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript">

 $('#fecha').datepicker({
      autoclose: true
    });

 $(function () {
      // Replace the <textarea id="observaciones"> with a CKEditor
      // instance, using default configuration.
      CKEDITOR.replace('observaciones')
      //bootstrap WYSIHTML5 - text editor
      $('.textarea').wysihtml5()
    });

</script>

@endsection