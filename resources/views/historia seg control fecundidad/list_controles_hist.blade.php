@extends('base.app')

@section('migas')
<h1>
Historias Control Fecundidad
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Historia Control Fecuncidad</li>
</ol>
@endsection

@section('contenido')

<div class="col-xs-12 col-md-12">
	<div class="box box-success">
        <div class="box-header with-border">
          	<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Controles para la historia seleccionada</h3>
		</div>
		<input type="hidden" id="historia" value="{{ $historia }}">
        <div class="box-body">
			<table id="lista_controles_historia_fec" class="table table-bordered table-striped" width="100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Fecha</th>
						<th>Peso</th>
						<th>TA</th>
						<th>Sangrado Int.</th>
						<th>Ef. Secundarios</th>
						<th>Falla metodo</th>
						<th>Signo infeccion pélvica</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('#lista_controles_historia_fec').DataTable( {
			"language": {
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			},
	        "ajax": {"url":'{{ route("list_controles_hist_ajax",["historia"=>$historia]) }}',dataSrc:''},
	        "deferRender":    false,
	        "processing": 	  false,
	        "scrollCollapse": true,
	        "scrollX":       true,
	        "scroller":       true,
	        "stateSave":      true
	    } );
	});
</script>

@endsection