@extends('base.app')

@section('migas')
<h1>
Historias seguimiento control fecundidad
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Historias seguimiento control fecundidad</li>
</ol>
@endsection

@section('contenido')

<div class="col-md-12">
	<div class="box box-success">
       
       <div class="col-md-12 box-header with-border">
        <h3 class="box-title" style="text-align: center;">Registrar nueva historia clínica</h3>
       </div>

    <form role="form" action="{{ route('create_historia_seg_con') }}" method="POST">

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

       <div class="box-body">
        <div class="row">
        <div class="col-md-4">
           <div class="form-group">
            <label style="font-size:16px; font-style: italic;">Número de identificación:</label>
           @if (isset($numero_de_identificacion))
              <input type="text" class="form-control" id="numero_de_identificacion" value="{{ $numero_de_identificacion }}" disabled>  
           @else 
            <input type="text" class="form-control" id="numero_de_identificacion" name="numero_de_identificacion" placeholder="Cédula, Tarjeta de identidad">
           @endif
            </div>
      </div>
      </div>
      <input name="numero_de_identificacion" type="hidden" value="{{ $numero_de_identificacion }}">
       	<div class="row">
            <div class="col-md-4">
             <div class="form-group">
	          <label>Menarquia</label>
	          <input type="text" class="form-control" name="menarquia" placeholder="Menarquia">
	         </div>
            </div>

            <div class="col-md-4">
             <div class="form-group">
	          <label>Ciclos menstruales</label>
	          <input type="text" class="form-control" name="ciclos_menstruales" placeholder="Ciclos menstruales">
	         </div>
            </div>

            <div class="col-md-4">
             <div class="form-group">
	          <label>F.U.M.</label>
	          <input type="text" class="form-control" id="fum" name="fum" placeholder="Fecha última menstruación">
	         </div>
            </div>
		 </div>

		 <div class="row">

         <div class="col-md-4">
		 <div class="form-group form-inline">
            <label style="font-size:16px; font-style: italic;">G:</label>
            <input type="number" class="form-control" name="G">
         </div>
         </div>

         <div class="col-md-4">
         <div class="form-group form-inline">
            <label style="font-size:16px; font-style: italic;">P:</label>
            <input type="number" class="form-control" name="P">
         </div>
         </div>

         <div class="col-md-4">
         <div class="form-group form-inline">
            <label style="font-size:16px; font-style: italic;">A:</label>
            <input type="number" class="form-control" name="A">
         </div>
         </div>

         </div>

         <div class="row">
         <div class="col-md-3">
             <div class="form-group">
	          <label>Edad inicio relaciones sexuales</label>
	          <input type="number" class="form-control" name="edad_in_rel_se" placeholder="Edad de inicio de relaciones sexuales">
	         </div>
         </div>

         <div class="col-md-3">
             <div class="form-group">
	          <label>F.U.P.</label>
	          <input type="text" class="form-control" id="fecha_ultimo_parto" name="fecha_ultimo_parto" placeholder="Fecha de último parto">
	         </div>
         </div>

         <div class="col-md-3">
             <div class="form-group">
	          <label>Numero de compañeros sexuales</label>
	          <input type="number" class="form-control" name="num_com_se">
	         </div>
         </div>

          <div class="col-md-3">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Cirugía ginecológica:</label>

               <label class="radio-inline"> 
               <input type="radio" name="cirugia_ginecologica" value="Si">
               Si
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="cirugia_ginecologica" value="No">
               No
               </label>
	        </div>
          </div>

		 </div>

         <div class="row">
         	<div class="col-md-3">
            <div class="form-group">
	          <label>Citología:</label>
	        </div>
            </div>

            <div class="col-md-4">
		    <div class="form-group form-inline">
            <label style="font-size:16px; font-style: italic;">Fecha:</label>
            <input type="text" class="form-control" id="fecha_citologia" name="fecha_citologia">
            </div>
            </div>

            <div class="col-md-4">
		    <div class="form-group form-inline">
            <label style="font-size:16px; font-style: italic;">Resultado:</label>
            <input type="text" class="form-control" name="resultado_citologia">
            </div>
            </div>

         </div>

         <div class="row">
         	<div class="col-md-6">
            <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Método deseado:</label>

                        
                        <label class="radio-inline">
                        <input type="radio" name="metodo_deseado" value="Anovulatorios">
                        Anovulatorios
                        </label>
                        
                        
                        
                        <label class="radio-inline">
                        <input type="radio" name="metodo_deseado" value="DIU">
                        DIU
                        </label>
                        

                        
                        <label class="radio-inline">
                        <input type="radio" name="metodo_deseado" value="Preservativo">
                        Preservativo
                        </label>
                        
            </div>
            </div>

            <div class="col-md-6">
             <div class="form-group">
	          <label>Método adoptado</label>
	          <input type="text" class="form-control" name="metodo_adoptado">
	         </div>
            </div>
         </div>

          <div class="row">
            <div class="col-md-12 box-footer">
            <button type="submit" class="btn btn-default">Cancelar</button>
            <button type="submit" class="btn btn-info pull-right">Registrar historia clinica</button>
            </div>
           </div>

       </div>
    </form>

    </div>
</div>



@endsection

@section('js')

<script type="text/javascript">

  //Date picker
    $('#fecha_citologia').datepicker({
      autoclose: true
    });

    $('#fecha_ultimo_parto').datepicker({
      autoclose: true
    });

    $('#fum').datepicker({
      autoclose: true
    });

</script>

@endsection