@extends('base.app')

@section('migas')
<h1>
Historias seguimiento control fecundidad
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Historias seguimiento control fecundidad</li>
</ol>
@endsection

@section('contenido')

<div class="col-md-12">
	<div class="box box-success" style="background-color: #ccffcc;">
		
        <div class="col-md-12 box-header with-border">
        <h3 class="box-title" style="text-align: center;">Registrar datos de la persona</h3>
        </div>
        
    <form role="form" action="{{ route('create_hist_confec') }}" method="POST" >

    <input type="hidden" name="_token" value="{{ csrf_token() }}">

		<div class="box-body">
      @if ($errors->any())     
                <div class="alert alert-danger">         
                  <ul>             
                    @foreach ($errors->all() as $error)                 
                    <li>{{ $error }}</li>             
                    @endforeach         
                  </ul>     
                </div> 
      @endif
			<div class="row">
            <div class="col-md-4">
             <div class="form-group">
	          <label>Número de identificacion</label>
	          <input type="text" class="form-control" name="numero_de_identificacion" placeholder="Cedula, numero de identificacion, tarjeta de identidad">
	         </div>
            </div>

            <div class="col-md-4">
             <div class="form-group">
	          <label>Nombres y apellidos</label>
	          <input type="text" class="form-control" name="nombresyapellidos" placeholder="Nombres y apellidos">
	         </div>
            </div>

            <div class="col-md-4">
             <div class="form-group">
	          <label>Direccion</label>
	          <input type="text" class="form-control" name="direccion" placeholder="Direccion">
	         </div>
            </div>
			</div>

			<div class="row">
           <div class="col-md-4">
           <div class="form-group">
	         <label style="font-size:16px; font-style: italic;">Sexo:</label>

               <label class="radio-inline"> 
               <input type="radio" name="sexo" value="Masculino">
               Masculino
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="sexo" value="Femenino">
               Femenino
               </label>
	        </div>
          </div>

            <div class="col-md-4">
             <div class="form-group">
	          <label>Teléfono</label>
	          <input type="text" class="form-control" name="telefono" placeholder="teléfono">
	         </div>
            </div>

            <div class="col-md-4">
            <div class="form-group">
	          <label>Fecha de nacimiento</label>
	          <input type="text" class="form-control" id="fecha_de_nacimiento" name="fecha_de_nacimiento">
	          </div>
            </div>
			</div>

			<div class="row">
            <div class="col-md-4">
             <div class="form-group">
	          <label>Estado civil</label>
	          <input type="text" class="form-control" name="estado_civil" >
	         </div>
            </div>

            <div class="col-md-4">
             <div class="form-group">
	          <label>No. de hijos actualmente vivos</label>
	          <input type="number" class="form-control" name="numero_hijos_vivos">
	         </div>
            </div>
			</div>

      <div class="row">
			<div class="col-md-4">
             <div class="form-group">
	          <label style="font-size:16px; font-style: italic;">Uso de anticonceptivos previo:</label>

               <label class="radio-inline"> 
               <input type="radio" name="uso_ant_previo" value="Si">
               Si
               </label>

               <label class="radio-inline"> 
               <input type="radio" name="uso_ant_previo" value="No">
               No
               </label>
	         </div>
            </div>

              <div class="col-md-4 form-inline">
              <label style="font-size:16px; font-style: italic;">Cual:</label>
                <input type="text" class="form-control" name="cual_anticonceptivo">
              </div>

      </div>

      <div class="row">
            <div class="col-md-12 box-footer">
            <button type="submit" class="btn btn-default">Cancelar</button>
            <button type="submit" class="btn btn-info pull-right">Registrar datos de persona</button>
            </div>
      </div>
		</div>
  </form>
    
    </div>
</div>

@endsection

@section('js')

<script type="text/javascript">

	  $(document).ready(function() {

      //Date picker
      $('#fecha_de_nacimiento').datepicker({
        dateFormat: 'yy-mm-dd'
      });


      });

</script>


@endsection