@extends('base.menupaciente')

@section('migas')
<h1>
Atención de Enfermeria
<small>Registrar una atención de enfermeria</small>
</h1>
<ol class="breadcrumb">
<li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Atención de enfermeria</li>
</ol>
@endsection

@section('contenido')
<div id="contenedor_vue">
<div class="col-md-12">

	  <div class="box box-success">
       <div class="col-md-12 box-header with-border">
          <h3 class="box-title" style="text-align: center;">Formulario de registro de atención de enfermería</h3>
        </div>

    <div class="box-body">
      <div class="row">
       <div class="col-md-6">
        
         <label><b>Paciente: </b>Martin Eduardo Santos</label>
       </div>

      <div class="col-md-6">
         <label><b>Identificación:</b>1114830989</label>
      </div>
      
      <div class="col-md-12">
         <label>Motivo de consulta:</label>
         <textarea class="form-control" rows="5"></textarea>
       </div>
       </div>
    </div>
    </div>
</div>

<div class="col-md-4">
<div class="box box-success">
       <div class="col-md-12 box-header with-border">
          <h3 class="box-title" style="text-align: center;">Glucometria</h3>
        </div>

    <div class="box-body">
      <div class="col-md-12">
       <div style="display: flex; justify-content:center; align-items:center; flex-direction:column;">
         <label style="color:orange;">Sin completar</label>
         <button type="button" class="btn btn-default" v-on:click="agregar_glucometria">
           <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
         </button>
       </div>
      </div>
    </div>
    </div>
</div>


<div class="col-md-4">
<div class="box box-success">
       <div class="col-md-12 box-header with-border">
          <h3 class="box-title" style="text-align: center;">Toma de presión arterial</h3>
        </div>

    <div class="box-body">
    <div class="col-md-12">
       <div style="display: flex; justify-content:center; align-items:center; flex-direction:column;">
         <label style="color:orange;">Sin completar</label>
         <button type="button" class="btn btn-default" v-on:click="agregar_toma_p_arterial">
           <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
         </button>
       </div>
      </div>
    </div>
    </div>
</div>
<div class="col-md-4">
<div class="box box-success">
       <div class="col-md-12 box-header with-border">
          <h3 class="box-title" style="text-align: center;">Entrega de medicamentos</h3>
        </div>

    <div class="box-body">
    <div class="col-md-12">
       <div style="display: flex; justify-content:center; align-items:center; flex-direction:column;">
         <label style="color:orange;">Sin completar</label>
         <button type="button" class="btn btn-default" v-on:click="agregar_entrega_medicamentos">
           <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
         </button>
       </div>
       </div>
    </div>
    </div>
</div> 

<div class="col-md-12">
  <div style="display: flex; justify-content: flex-end;">
    <button type="button" class="btn btn-primary" >Registrar</button>
  </div>
</div>

<agregar-glucometria></agregar-glucometria> <!-- Modal para agregar glucometria -->
<agregar-toma-presion1 ref="sel_equipo_medico" v-on:agregar_toma_p_arterial2="agregar_toma_p_arterial2"></agregar-toma-presion1><!-- Modal para agregar toma de presion arterial 1-->
<agregar-toma-presion2></agregar-toma-presion2><!-- Modal para agregar toma de presion arterial 2-->


<div class="modal fade" id="modAgregarMedicamentos" tabindex="-1" role="dialog"> 
   <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Entrega de Medicamentos</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-7">
                  
                  <table class="table table-hover">
                   <thead>
                   <th>Nombre</th>
                   <th>Laboratorio</th>
                   <th>F.Vencimiento</th>
                   <th>Existencia</th>
                   <th>Lote</th>
                   <th></th>
                   </thead>
                   <tbody>
                    <tr v-if="medicamentos.length == 0">
                     <td colspan="4">No hay datos...</td>
                    </tr>
                    <tr v-else v-for="medicamento in medicamentos">
                    <td>@{{medicamento.nombre}}</td>
                    <td>@{{medicamento.laboratorio}}</td>
                    <td>@{{medicamento.fecha_vencimiento}}</td>
                    <td>@{{medicamento.existencia}}</td>
                    <td>@{{medicamento.lote}}</td>
                    <td><button class="btn btn-sm btn-success" type="button" @click="agregar_medicamento([medicamento.id, medicamento.nombre])">
                    <i class="fa fa-check-square"></i></button></td>
                    </tr>
                   </tbody>
                  </table>
                  <div class="pull-right">
		              <label>Página @{{paginaMedicamento}} de @{{numPaginasMedic}}</label>
		              <button v-if="paginaMedicamento == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-else type="button" class="btn btn-link " @click="paginaMedicamento--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-if="paginaMedicamento == numPaginasMedic || numPaginasMedic == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
                  <button v-else type="button" class="btn btn-link " @click="paginaMedicamento++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		              </div>
                  </div>

                  <div class="col-md-5">
                  <label>Listado de seleccionados</label>
                  <table class="table table-hover">
                  <thead>
                  <th>Medicamento</th>
                  <th>Cantidad</th>
                  <th></th>
                  </thead>
                  <tbody>
                    <tr v-for="medicamento in medicamentos_seleccionados">
                    <td>@{{medicamento.nombre}}</td>
                    <td><input type="text" class="form-control"></td>
                    <td><button class="btn btn-sm btn-success" type="button" @click="quitar_medicamento([medicamento.id, medicamento.nombre])">
                    <i class="fa fa-times-circle"></i></button></td>
                    </tr>
                  </tbody>
                  </table>
                  </div>
              </div>

    </div>

    <div class="modal-footer">
       <label style="color:red;">*Debe llenar todos los campos.</label>
       <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       <button type="button" class="btn btn-primary" >Guardar</button>
    </div>
  </div>
  </div>
  </div>

</div>

@endsection

@section('js')

<script src="{{ asset('js/atencion_de_enfermeria.js') }}"></script>
<script src="{{ asset('js/sweetalert2.js') }}"></script>

@endsection

