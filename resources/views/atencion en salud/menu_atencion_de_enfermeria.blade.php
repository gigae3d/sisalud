@extends('base.app')

@section('migas')
<h1>
Atencion de enfermeria
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Atención de enfermeria</li>
</ol>
@endsection

@section('contenido')

<div class="col-md-12">
	<div class="box box-success">

		<div class="col-md-12 box-header with-border">
			<center>
              <h3 class="box-title" style="text-align: center;">Seleccion de opción para el usuario: {{ $numero_de_identificacion }}</h3>
            </center>
        </div>

        <div class="box-body">

        	<div class="row">
            <div class="col-md-12">
            <center>
            <div class="btn-group-vertical" role="group">

            <a class="btn btn-default btn-lg" href="{{ route('new_atencion_en_salud',['id'=>$numero_de_identificacion]) }}" role="button">Atención en salud</a>
            <a class="btn btn-default btn-lg" href="{{ route('new_tpresion_arterial',['id'=>$numero_de_identificacion]) }}" role="button">Toma de presión arterial</a>
            <a class="btn btn-default btn-lg" href="{{ route('new_glucometria',['id'=>$numero_de_identificacion]) }}" role="button">Glucometría</a>
            <a class="btn btn-default btn-lg" href="{{ route('new_inyectologia',['id'=>$numero_de_identificacion]) }}" role="button">Inyectologia</a>
            <a class="btn btn-default btn-lg" href="{{ route('new_entrega_med',['id'=>$numero_de_identificacion]) }}" role="button">Entrega de medicamento</a>

            </div>
            </center>
            </div>
			</div>



        </div>

 
    </div>
</div>


@endsection