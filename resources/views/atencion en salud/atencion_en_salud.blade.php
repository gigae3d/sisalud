@extends('base.app')

@section('migas')
    <h1>Atención en Salud</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Atención de Enfermería</li>
        <li>Atención en Salud</li>
        <li class="active">Crear Atención en Salud</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Crear registro de Atención en Salud</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form id="formulario" role="form" action="{{ route('create_atencion_en_salud') }}" method="POST"
                  data-entrada="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if (session()->has('error_existencia') and  session('error_existencia'))
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> Error al ingresar, verifique que no se está tratando de
                                descontar para un medicamento un valor mayor al de su existencia.
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body" id="nofountId" style="display: none">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>Importante!</strong> El Número de identificación, No esta registrado en la Base de
                            Datos, por favor, primero registra el paciente en
                            <a href="{{ route('new_paciente')}}" class="btn btn-link">Aqui</a>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha</label>
                            <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="fecha" name="fecha"
                                       value="{{ date('m/d/Y')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación</label>
                            @if (isset($numero_de_identificacion))
                                <input type="text" class="form-control" id="numero_de_identificacion"
                                       name="numero_de_identificacion" value="{{ $numero_de_identificacion }}"
                                       readonly>
                                <input type="hidden" name="input_cedula" value="{{ $numero_de_identificacion }}">
                            @else
                                <input type="text" class="form-control" id="numero_de_identificacion"
                                       name="numero_de_identificacion" placeholder="Cédula, Tarjeta de identidad"
                                       value="{{ old('numero_de_identificacion') }}" required>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombres</label>
                            <input type="text" class="form-control" id="nombres" name="nombre" placeholder="Nombres"
                                   value="{{ old('nombre') }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellido"
                                   placeholder="Apellidos" value="{{ old('apellido') }}">
                        </div>
                    </div>

                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="padding-top: 10px">Motivo de la Consulta:</h3>
                    </div>

                    <div class="col-md-12">
                        <div class="box-body pad">
                            <textarea id="motivo_consulta" name="motivo_consulta" rows="10"></textarea>
                        </div>
                    </div>

                    <div class="col-md-12 box-header with-border">
                        <div class="row">
                            <div class="col-xs-6">
                                <h3 class="box-title" style="padding-top: 10px">Insumos utilizados:</h3>
                            </div>
                            <div class="col-xs-6">
                                <button id="add_insumo_as" type="button" class="btn btn-link  pull-right"><i
                                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                                </button>
                                <button id="remove_insumo_as" type="button"
                                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                                        aria-hidden="true"></i> Quitar
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box-header with-border insumos_usados_as"></div>

                    <div class="col-md-12 box-header with-border">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Otros insumos usados:</label>
                                    <input type="text" class="form-control" name="otros_insumos_as"
                                           placeholder="Mencione aqui otros insumos usados"
                                           value="{{ old('otros_insumos_as') }}">
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label>Equipo médico:</label>
                                    <div class="form-inline">
                                        <div class="row">
                                            <div class="col-xs-9 col-md-9">
                                                <input type="hidden" id="equipo_medico" name="equipo_medico_as">
                                                <input type="text" class="form-control" style="width: 100%"
                                                       id="equipo_medico_name"
                                                       name="equipo_medico_name" placeholder="Equipo medico usado">
                                            </div>
                                            <div class="col-xs-3 col-md-3">
                                                <button id="seleccionarEqu" type="button"
                                                        class="btn btn-warning pull-right"><i
                                                            class="fa fa-hand-pointer-o" aria-hidden="true"></i>
                                                    Seleccionar
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        
                        <!--NO BORRAR, SECCION USADA PARA PRUEBAS -->
                        <!--<div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="padding-top: 10px">Registro de Toma de presión arterial</h3>
                        <label class="radio-inline">
                                <input type="radio" id="si_tp" name="toma_presion" value="Normal">
                                Si
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="no_tp" name="toma_presion"  value="Afinamiento">
                                No
                            </label>
                       </div>

                    <div class="col-md-12" id="seccion_tipo_tp">
                        <div class="form-group">
                            <label>Tipo:</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="normal_tp" name="tipo_toma_presion" value="Normal">
                                Normal
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="afinamiento_tp" name="tipo_toma_presion" value="Afinamiento">
                                Afinamiento
                            </label>
                        </div>
                    </div>


                    <div class="col-md-6" id="seccion_posicion_tp">
                        <div class="form-group">
                            <label>Posición:</label>
                            <input type="text" class="form-control" id="posicion" name="posicion_tp"
                                   placeholder="Posicion">
                        </div>
                    </div>

                    <div class="col-md-12" id="seccion_afinamiento_tp">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="de_pie_tp" value="De pie" name="afinamientos[]">
                                        De pie
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" id="msd_dp" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" id="msi_dp" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="sentado_tp" value="Sentado" name="afinamientos[]">
                                        Sentado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" id="msd_se" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" id="msi_se" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="acostado_tp" value="Acostado" name="afinamientos[]">
                                        Acostado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" id="msd_ac" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" id="msi_ac" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="padding-top: 10px">Registro de Glucometría</h3>
                        <label class="radio-inline">
                                <input type="radio" id="si_gl" name="glucometria" value="Normal">
                                Si
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="no_gl" name="glucometria"  value="Afinamiento">
                                No
                            </label>
                    </div>


                    <div class="col-md-12" id="seccion_glucometria">
                    <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Equipo médico:</label>
                            <div class="form-inline">
                                <div class="row">
                                    <div class="col-xs-9 col-md-9">
                                        <input type="hidden" id="equipo_medico" name="equipo_medico">
                                        <input type="text" class="form-control" style="width: 100%"
                                               id="equipo_medico_name"
                                               name="equipo_medico_name" placeholder="Equipo medico usado">
                                    </div>
                                    <div class="col-xs-3 col-md-3">
                                        <button id="seleccionarEqu" type="button"
                                                class="btn btn-warning pull-right"><i
                                                    class="fa fa-hand-pointer-o" aria-hidden="true"></i>
                                            Seleccionar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Patología base:</label>
                            <input type="text" class="form-control" id="patologia_base" name="patologia_base"
                                   placeholder="Patologia" value="{{ old('patologia_base') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Resultado:</label>
                            <input type="text" class="form-control" id="resultado" name="resultado"
                                   placeholder="Resultado" value="{{ old('resultado') }}">
                        </div>
                    </div>

                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title">Insumos utilizados para glucometría</h3>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label>Tirillas</label>
                                    <input type="number" class="form-control" name="tirillas_gl"
                                           placeholder="Numero de tirillas">
                                </div>
                            </div>
                            <div class="col-md-4">
                                <label>Lancetas</label>
                                <input type="number" class="form-control" name="lancetas_gl"
                                       placeholder="Numero de lancetas">
                            </div>
                            <div class="col-md-4">
                                <label>Guantes</label>
                                <input type="number" class="form-control" name="guantes_gl"
                                       placeholder="Numero de guantes">
                            </div>
                        </div>
                    </div>
                    </div>
                    </div>

                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="padding-top: 10px">Registro de Entrega de medicamentos</h3>
                        <label class="radio-inline">
                                <input type="radio" id="si_em" name="entrega_medicamento" value="Normal">
                                Si
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="no_em" name="entrega_medicamento"  value="Afinamiento">
                                No
                            </label>
                    </div>
                    
                    <div class="col-md-12 box-header with-border">
                    <div class="row">
                            <div class="col-xs-6">
                                <h3 class="box-title" style="padding-top: 10px">Medicamentos:</h3>
                            </div>
                            <div class="col-xs-6">
                                <button id="add_medicamento" type="button" class="btn btn-link  pull-right"><i
                                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                                </button>
                                <button id="remove_medicamento" type="button"
                                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                                        aria-hidden="true"></i> Quitar
                                </button>
                            </div>
                    </div>
                    </div>--><!--NO BORRAR, SECCION PARA PRUEBAS-->

                        </div>
                    </div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        @if (isset($numero_de_identificacion))
                            <button class="btn btn-primary pull-right margin-r-5" type="submit" formaction="{{ route('val_cedula') }}">
                                <i class="fa fa-ban" aria-hidden="true"></i> Cancelar
                            </button>
                        @else
                            <a class="btn btn-primary pull-right margin-r-5"
                               href="{{ route('view_atenciones_en_salud') }}">
                                <i class="fa fa-ban" aria-hidden="true"></i>
                                Cancelar
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade modal-responsive" id="modalInsumoMedico" tabindex="-1" role="dialog"
         aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Listado de Insumo Médico</h4>
                </div>
                <div class="modal-body">
                    <table id="lista_insumos_medicos" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Existencia</th>
                            <th>Fecha de vencimiento</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade modal-responsive" id="modalEquipoMedico" tabindex="-1" role="dialog"
         aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Listado de Equipo Médico</h4>
                </div>
                <div class="modal-body">
                    <table id="lista_equipos_medicos" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Modelo</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        var added_insumos_as = new Array();

        $(document).ready(function () {

            //Date picker
            $('#fecha').datepicker({
                startDate: '-285d',
                endDate: '0d',
                autoclose: true
            });

            //Evento para mostrar el nombre y apellido del paciente al escribir su CC
            $('#numero_de_identificacion').blur(function () {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        if (json.nombres == '-' && json.apellidos == '-') {
                            $("#nofountId").show();
                            $('#numero_de_identificacion').val('');
                            $('#nombres').val('');
                            $('#apellidos').val('');
                        } else {
                            $('#nombres').val(json.nombres);
                            $('#apellidos').val(json.apellidos);
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });

            });

            $('#numero_de_identificacion').focus(function () {
                $("#nofountId").hide();
            });

            //Evento para mostrar el nombre y apellido del paciente con su CC ya establecido
            if ($('#numero_de_identificacion').val() > 0) {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        console.log("consulta hecha " + json);
                        $('#nombres').val(json.nombres);
                        $('#apellidos').val(json.apellidos);
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            }

            //Funcion para añadir insumo medico en formulario atencion en salud

            var numero_actual_campos_insumos_as = 0;
            $('#add_insumo_as').on('click', function () {

                if (numero_actual_campos_insumos_as < 6) {
                    $('.insumos_usados_as').append('<div class="row">'
                        + '<div class="col-md-6 margin-bottom">'
                        + '<div class="input-group" data-numero="' + numero_actual_campos_insumos_as + '">'
                        + '<span class="input-group-addon">Descripción</span>'
                        + '<input type="hidden" class="form-control" id="insumo_usado_as' + numero_actual_campos_insumos_as + '" name="insumos_usados[]" required>'
                        + '<input type="text" class="form-control" id="nombre_insumo_med' + numero_actual_campos_insumos_as + '" readonly>'
                        + '<span class="input-group-btn">'
                        + '<button id="seleccionarInsumoMedico' + numero_actual_campos_insumos_as + '" type="button" class="btn btn-warning"><i class="fa fa-search-plus" aria-hidden="true"></i></button>'
                        + '</span>'
                        + '</div>'
                        + '</div>'

                        + '<div class="col-md-6 margin-bottom">'
                        + '<div class="input-group">'
                        + '<span class="input-group-addon">Cantidad</span>'
                        + '<input type="number" class="form-control" name="cantidad_im_as[]" min="1" required>'
                        + '</div>'
                        + '</div>'
                        + '</div>');

                    //Funcion de evento para mostrar ventana para seleccionar un insumo médico
                    $('#seleccionarInsumoMedico' + numero_actual_campos_insumos_as).on('click', function () {

                        $('#formulario').attr("data-entrada", $(this).parents("div").attr("data-numero"));
                        if (added_insumos_as.length != 0) { //deshabilitar botones
                            for (var i = 0; i < added_insumos_as.length; i++) {
                                $('#' + added_insumos_as[i]).prop('disabled', true);
                            }
                        }
                        $('#modalInsumoMedico').modal("show"); //Muestra el cuadro para seleccionar
                        $('#lista_insumos_medicos').on('click', 'tbody tr td button', function () {

                            if (added_insumos_as.length != 0) { //Habilitar botones
                                for (var i = 0; i < added_insumos_as.length; i++) {
                                    if (added_insumos_as[i] == $('#insumo_usado_as' + $('#formulario').attr("data-entrada")).val()) {
                                        $('#' + added_insumos_as[i]).prop('disabled', false);
                                        added_insumos_as.splice(i, 1);
                                    }
                                }
                            }

                            $('#insumo_usado_as' + $('#formulario').attr("data-entrada")).val($(this).parent().children('p').text()); //Cambia el valor
                            $('#nombre_insumo_med' + $('#formulario').attr("data-entrada")).val($(this).parent().children('label').text()); //Pone el nombre
                            added_insumos_as.push($(this).parent().children('p').text());
                            $('#modalInsumoMedico').modal("hide");//Ocultar el cuadro para seleccionar
                        });
                    });

                    numero_actual_campos_insumos_as++;
                }
            });

            //Evento que hace que se muestre la ventana para seleccionar un equipo medico
            $('#seleccionarEqu').on('click', function () {
                $('#modalEquipoMedico').modal("show"); //Muestra el cuadro para seleccionar
                $('#lista_equipos_medicos').on('click', 'tbody tr td button', function () {
                    $('#equipo_medico').val($(this).parent().children('p').text()); //Cambia el valor
                    $('#equipo_medico_name').val($(this).parent().children('label').text()); //Cambia el nombre
                    $('#modalEquipoMedico').modal("hide");//Ocultar el cuadro para seleccionar
                });
            });

            $('#lista_insumos_medicos').DataTable({
              "drawCallback": function( settings ) { // Dibujar la tabla despues de haber seleccionado un medicamento con los medicamentos seleccionados deshabilitados
              if(added_insumos_as.length != 0){
              for(var i = 0; i < added_insumos_as.length; i++){
              $('#'+added_insumos_as[i]).prop('disabled', true);
              }
              }
              },
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_insumos_medicos_ajax_as") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });


            //Funcion para quitar un registro de insumo medico en atencion en salud
            $('#remove_insumo_as').on('click', function () {
                $('div.insumos_usados_as > div').last().remove();
                numero_actual_campos_insumos_as--;
            });

            $('#lista_equipos_medicos').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "M",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_equipos_medicos_ajax_as") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            $('#seccion_tipo_tp').hide(); //Ocultar la opción para escoger tipo presión arterial

            $('#si_tp').change(function () { //Mostrar opción para escoger tipo de presión arterial
                $('#seccion_tipo_tp').show();
            });

            $('#no_tp').change(function () { //Ocultar opción para escoger tipo de presión arterial
                $('#seccion_tipo_tp').hide();
                $('#seccion_posicion_tp').hide();
                $('#seccion_afinamiento_tp').hide();
                $('#normal_tp').prop('checked', false); 
                $('#afinamiento_tp').prop('checked', false); 
            });

            //Funcion para mostrar campo posicion cuando se elige toma de presion arterial tipo normal
            $('#seccion_posicion_tp').hide();
            $('#seccion_afinamiento_tp').hide();

            $('#normal_tp').change(function () {
                $('#seccion_posicion_tp').show();
                $('#seccion_afinamiento_tp').hide();
            });

            //Funcion para mostrar campos afinamiento cuando se elige toma de presion arterial tipo afinamiento
            $('#afinamiento_tp').change(function () {
                $('#seccion_afinamiento_tp').show();
                $('#seccion_posicion_tp').hide();
            });

            //Funcion para mostrar u ocultar campos enseguida de campo de pie
            $('#msd_dp').hide();
            $('#msi_dp').hide();
            $('#de_pie_tp').change(function () {
                if ($(this).prop('checked')) {
                    $('#msd_dp').show();
                    $('#msi_dp').show();
                    $('#msd_dp').attr("name", "msd_tp[]");
                    $('#msi_dp').attr("name", "msi_tp[]");
                } else {
                    $('#msd_dp').hide();
                    $('#msi_dp').hide();
                    $('#msd_dp').attr("name", "");
                    $('#msi_dp').attr("name", "");
                }
            });

            //Funcion para mostrar u ocultar campos enseguida de campo sentado
            $('#msd_se').hide();
            $('#msi_se').hide();
            $('#sentado_tp').change(function () {
                if ($(this).prop('checked')) {
                    $('#msd_se').show();
                    $('#msi_se').show();
                    $('#msd_se').attr("name", "msd_tp[]");
                    $('#msi_se').attr("name", "msi_tp[]");
                } else {
                    $('#msd_se').hide();
                    $('#msi_se').hide();
                    $('#msd_se').attr("name", "");
                    $('#msi_se').attr("name", "");
                }
            });

            //Funcion para mostrar u ocultar campos enseguida de campo acostado
            $('#msd_ac').hide();
            $('#msi_ac').hide();
            $('#acostado_tp').change(function () {
                if ($(this).prop('checked')) {
                    $('#msd_ac').show();
                    $('#msi_ac').show();
                    $('#msd_ac').attr("name", "msd_tp[]");
                    $('#msi_ac').attr("name", "msi_tp[]");
                } else {
                    $('#msd_ac').hide();
                    $('#msi_ac').hide();
                    $('#msd_ac').attr("name", "");
                    $('#msi_ac').attr("name", "");
                }
            });


            //Sección glucometría
            $('#seccion_glucometria').hide(); //Ocultar sección glucometría
            $('#si_gl').change(function () { //Mostrar seccion registro de glucometria
                $('#seccion_glucometria').show();
            });

            $('#no_gl').change(function () { //Ocultar seccion glucometria
                $('#seccion_glucometria').hide();
            });

            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('motivo_consulta')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });
        });
    </script>
@endsection
