<div id="glucometria" class="box-body tab-pane fade in">
    
    <div class="row">
    <div class="col-md-6">        

	                <div class="form-group">
	                  	<label style="font-size:16px; font-style: italic;">Equipo médico:</label>
	                  	<div class="form-inline">
	                  	<input type="text" class="form-control" style="width:80%;" id="equipo_medico_gl" name="equipo_medico_gl" placeholder="Equipo medico usado">
                        <button id="seleccionarEquipoMed" type="button" class="btn btn-info pull-right">Seleccionar</button>
                        </div>
	                </div>

	                <div class="form-group">
	                	<label style="font-size:16px; font-style: italic;">Fecha de realización:</label>
	                	<div class="input-group date">
	                  		<div class="input-group-addon">
	                    		<i class="fa fa-calendar"></i>
	                  		</div>
	                  		<input type="text" class="form-control" id="fecha" name="fecha" placeholder="01/01/2000">
	                	</div>
	                <!-- /.input group -->
	              	</div>

	              	<div class="form-group">
		              	<label style="font-size:16px; font-style: italic;">Patología base:</label>
		              	<input type="text" class="form-control" id="patologia_base_gl" name="patologia_base_gl" placeholder="Patologia">
		            </div>

		            </div>

		            <div class="col-md-6">
		              <div class="form-group">
		              	<label style="font-size:16px; font-style: italic;">Resultado:</label>
	                    <input type="text" class="form-control" id="resultado" name="resultado_gl" placeholder="Resultado">
	                    </div>
	                </div>
	   </div>

	   <div class="row">
          <div class="col-md-12 box-header with-border">
          <h3 class="box-title" style="text-align: center;">Insumos utilizados</h3>
          </div>
        </div>

	   <div class="row">
	   	
	   	<div class="form-inline col-md-4">
         <label style="font-size:16px; font-style: italic;">Tirillas</label>
	     <input type="number" class="form-control" name="cantidad_tirillas_gl" placeholder="Numero de tirillas">
	     </div>

	     <div class="col-md-4 form-inline">
          <label style="font-size:16px; font-style: italic;">Lancetas</label>
	      <input type="number" class="form-control" name="cantidad_lancetas_gl" placeholder="Numero de lancetas">
	     </div>

	     <div class="col-md-4 form-inline">
          <label style="font-size:16px; font-style: italic;">Guantes</label>
	      <input type="number" class="form-control" name="cantidad_guantes_gl" placeholder="Numero de guantes">
	     </div>

	   </div>




</div>