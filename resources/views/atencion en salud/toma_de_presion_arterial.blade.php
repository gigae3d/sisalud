<div id="toma_presion_arterial" class="box-body tab-pane fade in">
     
    
	
            	<div class="col-md-6">
	                

	                <div class="form-group">
	                  	<label style="font-size:16px; font-style: italic;">Equipo médico:</label>
	                  	<div class="form-inline">
	                  	<input type="text" class="form-control" style="width:80%;" id="equipo_medico_tp" name="equipo_medico_tp" placeholder="Equipo medico usado">
                        <button id="seleccionarMed" type="button" class="btn btn-info pull-right">Seleccionar</button>
                        </div>
	                </div>

		        </div>

		            <div class="col-md-6">

	                   <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Resultado:</label>
	                  	<input type="text" class="form-control" id="resultado" name="resultado_tp" placeholder="Resultados" >
	                  </div>
	                </div>


	           <div class="row">
                   <div class="col-md-12">
                    <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Tipo:</label>

                        <label class="radio-inline"> 
                        <input type="radio" id="normal_tp" name="tipo_toma_presion" value="Normal">
                        Normal
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" id="afinamiento_tp" name="tipo_toma_presion" value="Afinamiento">
                        Afinamiento
                        </label>

                    </div>

	               </div>
	           </div>

              <div id="seccion_posicion_tp">
	           <div class="row">
	           	<div class="col-md-6">
                  <div class="form-group">
		              	<label style="font-size:16px; font-style: italic;">Posición:</label>
		              	<input type="text" class="form-control" id="posicion" name="posicion_tp" placeholder="Posicion">
		          </div>
		        </div>
	           </div>
	          </div>

              <div id="seccion_afinamiento_tp">
	           <div class="row">
                <div class="col-md-2">
                       <div class="form-group" style="padding: 3px 0;">
                       <label style="font-size:16px; font-style: italic;" class="checkbox-inline"> 
                        <input type="checkbox" id="de_pie_tp" value="De pie" name="afinamientos[]">
                        De pie
                        </label>
                       </div>
	                </div>

	                <div class="col-md-4 form-inline">
                       <label style="font-size:16px; font-style: italic;">MSD</label>
	                   <input type="text" class="form-control" id="msd_dp" name="msd_tp[]">
	                </div>

	                <div class="col-md-4 form-inline">
                       <label style="font-size:16px; font-style: italic;">MSI</label>
	                   <input type="text" class="form-control" id="msi_dp" name="msi_tp">
	                </div>
	           </div>

	           <div class="row">
                <div class="col-md-2">
                       <div class="form-group" style="padding: 3px 0;">
                       <label style="font-size:16px; font-style: italic;" class="checkbox-inline"> 
                        <input type="checkbox" id="sentado_tp" value="Sentado" name="afinamientos[]">
                        Sentado
                        </label>
                       </div>
	                </div>

	                <div class="col-md-4 form-inline">
                       <label style="font-size:16px; font-style: italic;">MSD</label>
	                   <input type="text" class="form-control" id="msd_se" name="msd_tp[]">
	                </div>

	                <div class="col-md-4 form-inline">
                       <label style="font-size:16px; font-style: italic;">MSI</label>
	                   <input type="text" class="form-control" id="msi_se" name="msi_tp">
	                </div>

	           </div>  

	           <div class="row">
                <div class="col-md-2">
                       <div class="form-group" style="padding: 3px 0;">
                       <label style="font-size:16px; font-style: italic;" class="checkbox-inline"> 
                        <input type="checkbox" id="acostado_tp" value="Acostado" name="afinamientos[]">
                        Acostado
                        </label>
                       </div>
	                </div>

	                <div class="col-md-4 form-inline">
                       <label style="font-size:16px; font-style: italic;">MSD</label>
	                   <input type="text" class="form-control" id="msd_ac" name="msd_tp[]">
	                </div>

	                <div class="col-md-4 form-inline">
                       <label style="font-size:16px; font-style: italic;">MSI</label>
	                   <input type="text" class="form-control" id="msi_ac" name="msi_tp">
	                </div>
	           </div>   
	           </div>           
    
</div>
