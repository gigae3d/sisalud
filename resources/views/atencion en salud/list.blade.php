@extends('base.app')

@section('migas')
    <h1>Atención en Salud</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Atención de Enfermería</li>
        <li>Atención en Salud</li>
        <li class="active">Lista de Atención en Salud</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Atenciones en Salud</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('new_atencion_en_salud_w')}}" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Crear Registro</a>
                </div>
            </div>
            <div class="box-body">
                @if(session()->has('exito'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok"></span> {{ session('exito') }}
                    </div>
                @endif
                <table id="lista_atenciones_en_salud" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Paciente</th>
                        <th>Usuario que registra</th>
                        <th>Equipo medico</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAtencionSalud" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Información Adicional</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Motivo Consulta:</h4>
                            <div class="motivoConsulta"></div>
                        </div>
                        <div class="col-md-12">
                            <h4>Insumos médicos usados:</h4>
                            <table id="lista_entrega_medicamentos" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                </tr>
                                </thead>
                                <tbody class="insumos_medicos_at">
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <h4>Otros Insumos:</h4>
                            <div class="otrosInsumos"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lista_atenciones_en_salud').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_atenciones_en_salud_ajax") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            //Mostrar informacion adicional de una atencion en salud
            $('#lista_atenciones_en_salud').on('click', 'tbody tr td div button', function ()
            {
                $(".motivoConsulta").html('');
                $(".insumos_medicos_at").html('');
                $(".otrosInsumos").html('');
                var codigo = $(this).parent().children('p').text();//Obtener el id del registro de atencion en salud

                $.ajax({
                    method: "GET",
                    url: "{{ route('get_insumos_atsalud') }}",
                    data: {id: codigo},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        var i;
                        $(".motivoConsulta").empty('');
                        $(".insumos_medicos_at").empty('');
                        $(".otrosInsumos").empty('');
                        $('.motivoConsulta').html(json.data[0].motivo_de_consulta);
                        for (i = 0; i < json.insumos_medicos.length; i++) {
                            $('.insumos_medicos_at').append('<tr role="row" class="odd">'
                                + '<td>' + json.insumos_medicos[i].insumo_medico + '</td>'
                                + '<td>' + json.nombres[i] + '</td>'
                                + '<td>' + json.insumos_medicos[i].cantidad + '</td>'
                                + '</tr>'
                            );
                        }
                        $('.otrosInsumos').html(json.data[0].otros_insumos_usados_as);
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });

                $('#modalAtencionSalud').modal("show");
            });
        });
    </script>
@endsection