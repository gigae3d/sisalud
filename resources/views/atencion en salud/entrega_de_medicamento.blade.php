<div id="entrega_de_medicamento" class="box-body tab-pane fade in">
    
    <div class="col-md-6">           

	                <div class="form-group">
	                  	<label style="font-size:16px; font-style: italic;">Medicamento:</label>
	                  	<div class="form-inline">
	                  	<input type="text" class="form-control" style="width:80%;" id="medicamento_ent" name="medicamento_ent" placeholder="Medicamento que se entrega">
                        <button id="seleccionarMed" type="button" class="btn btn-info pull-right">Seleccionar</button>
                        </div>
	                </div>

	                <div class="form-group">
	                	<label style="font-size:16px; font-style: italic;">Fecha de entrega:</label>
	                	<div class="input-group date">
	                  		<div class="input-group-addon">
	                    		<i class="fa fa-calendar"></i>
	                  		</div>
	                  		<input type="text" class="form-control" id="fecha_de_entrega" name="fecha_de_entrega" placeholder="01/01/2000">
	                	</div>
	                <!-- /.input group -->
	              	</div>

	              	<div class="form-group">
		              	<label style="font-size:16px; font-style: italic;">Cantidad que se entrega</label>
		              	<input type="number" class="form-control" id="cantidad" name="cantidad_ent" placeholder="Ingrese la cantidad" min="1">
		            </div>

		            </div>


</div>