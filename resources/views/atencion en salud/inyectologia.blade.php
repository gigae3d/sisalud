<div id="inyectologia" class="box-body tab-pane fade in">

   <div class="row">
      <div class="col-md-6">
        <div class="form-group">
	      <label style="font-size:16px; font-style: italic;">Medicamento:</label>
	      <div class="form-inline">
	      <input type="text" class="form-control" style="width:80%;" id="medicamento_inyectologia" name="medicamento_inyectologia" placeholder="Medicamento administrado">
           <button id="seleccionarMedIny" type="button" class="btn btn-info pull-right">Seleccionar</button>
          </div>
	     </div>
      </div>

      <div class="col-md-6">
      <div class="form-group">
	        <label style="font-size:16px; font-style: italic;">Orden médica</label>
	          <input type="text" class="form-control" id="orden_medica" name="orden_medica_iny" placeholder="Orden medica">  
      </div>
      </div>
  </div>

     <div class="row">
     <div class="col-md-12">
      <div class="insumos_usados_iny">
        <div class="row">
          <div class="col-md-12 box-header with-border">
          <h3 class="box-title" style="text-align: center;">Insumos utilizados</h3>
          </div>
        </div>
      </div>
     </div>
     </div>
                  
      <div class="row">
      <div class="col-md-12" style="text-align: center;">
      <button id="add_insumo_iny" type="button" class="btn btn-primary">Añadir</button>
      <button id="remove_insumo_iny" type="button" class="btn btn-danger">Quitar</button>
      </div>

      </div>



</div>