@extends('base.app')

@section('contenido')

<div class="col-xs-12 col-md-12">
        <div class="jumbotron">

         <h1>Error. Usted no posee permisos para acceder a este sitio.</h1>
         <p>Estimado(a) usuario, usted no posee los privilegios suficientes para acceder al sitio.</p>
        </div>
</div>

@endsection