<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>SISALUD | Ingreso</title>

    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">

    <!-- Bootstrap 3.3.7 -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/font-awesome/css/font-awesome.min.css') }}">
    <!-- Ionicons -->
    <link rel="stylesheet" href="{{ asset('plugins/bower_components/Ionicons/css/ionicons.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('plugins/dist/css/AdminLTE.min.css') }}">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="{{ asset('plugins/dist/css/skins/_all-skins.min.css') }}">
    <!-- iCheck -->
    <link rel="stylesheet" href="{{ asset('plugins/plugins/iCheck/all.css') }}">
    <!-- app.css -->
    <link rel="stylesheet" href="{{ asset('css/app.css') }}">

</head>
<body class="hold-transition login-page-back">
<div class="container-fluid">
    <div class="login-box">
        <div class="login-logo login-logo-color" >
            <a class="login-box-msg-uceva" href="#"><img src="{{ asset('plugins/dist/img/LOGO_SISALUD.jpg') }}" width="340" height="120" alt="Logo" style="border-style: solid;
    border-width: 3px; border-color: cyan; border-radius: 10px;"></a>
        </div>
        <!-- /.login-logo -->
        <div class="login-box-body-uceva" style="border-style: solid;
    border-width: 5px; border-color: cyan; border-radius: 10px;">
            <form method="post" id="ingreso" action="{{ route('Ingreso') }}">
                {!! csrf_field() !!}

                <div class="form-group has-feedback {{ $errors->has('email') ? ' has-error' : '' }}">
                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email">
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                    @if ($errors->has('email'))
                        <span class="help-block">
                    <strong>{{ $errors->first('email') }}</strong>
                </span>
                    @endif
                </div>

                <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
                    <input type="password" class="form-control" placeholder="Contraseña" name="password">
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                    @if ($errors->has('password'))
                        <span class="help-block">
                    <strong>{{ $errors->first('password') }}</strong>
                </span>
                    @endif
                </div>
                <center>
                    <div class="row">
                        <div class="col-lg-6">
                            <button type="submit" class="btn button-login"><i class="fa fa-btn fa-sign-in"></i> Loguearse
                            </button>
                        </div>
                        <div class="col-lg-6">
                            <a href="{{ route('register') }}" class="text-center btn button-login">Registrarse&nbsp;<i class="fa fa-edit"></i></a>
                        </div>
                    </div>
                </center>
            </form>
        </div>
    </div>
    @if (session('error_log'))
        <div class="alert alert-danger pull-right" id="error_log">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            <strong>{{ session('error_log') }} </strong>
        </div>
    @endif
</div>

</div>


<!-- jQuery 3 -->
<script src="{{ asset('plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
<!-- Bootstrap 3.3.7 -->
<script src="{{ asset('plugins/bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/bower_components/fastclick/lib/fastclick.js') }}"></script>
<!-- AdminLTE App -->
<script src="{{ asset('plugins/dist/js/adminlte.min.js') }}"></script>
<!-- iChecked -->
<script src="{{ asset('plugins/plugins/iCheck/icheck.min.js') }}"></script>
<!-- validate -->
<script type="text/javascript" src="{{ asset('plugins/plugins/jQueryValidate/dist/jquery.validate.js') }}"></script>

<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
    });

    $("#ingreso").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                minlength: 6
            },
        },
        messages: {
            email: {
                required: "Por favor ingresa tu email",
                email: "Ingresa un email válido"
            },
            password: {
                required: "Por favor ingresa tu Contraseña",
                minlength: jQuery.validator.format("Al menos {0} Caracteres requeridos!")
            },
        }
    });
</script>
@if (session('error_log'))
    <script type="text/javascript">
        $(document).ready(function () {
            setTimeout(function () {
                $("#error_log").fadeOut(1000);
            },500);
        });
    </script>
@endif
</body>
</html>
