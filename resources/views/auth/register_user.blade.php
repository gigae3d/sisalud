@extends('base.app')

@section('migas')
    <h1>Usuarios</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Usuarios</li>
        <li class="active">Crear usuario</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Usuario</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_user') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombre Completo</label>
                            <input type="text" class="form-control" id="name" name="name" placeholder="Nombre Completo" value="{{ old('name') }}">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" value="{{ old('email') }}">
                        </div>
                        <div class="form-group">
                            <label>Rol</label>
                            <select class="form-control" id="rol" name="rol">
                                <option value="">- Seleccione -</option>
                                <option value="Estudiante">Estudiante</option>
                                <option value="Enfermero">Enfermero</option>
                                <option value="Medico">Medico</option>
                                <option value="UControlVacunas">Funcionario Ciencias de Salud</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contraseña</label>
                            <input type="password" class="form-control" id="password" name="password"
                                   placeholder="Contraseña" value="{{ old('password') }}">
                        </div>
                        <div class="form-group">
                            <label>Confirmar contraseña</label>
                            <input type="password" class="form-control" id="password" name="password_confirmation"
                                   placeholder="Confimrar contraseña">
                        </div>
                    </div>
                    <!-- /.box -->
                    <!-- /.box-body -->
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_usuarios') }}"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>
        </div>
    </div>
@endsection