@extends('base.app')

@section('migas')
    <h1>Pacientes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Pacientes</li>
        <li class="active">Crear Paciente</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Paciente</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_paciente') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número de identificación</label>
                            @if (isset($nidentificacion))
                            
                            <input type="number" class="form-control" id="numero_de_identificacion"
                                   name="numero_de_identificacion" placeholder="Cédula, Tarjeta de Identidad"
                                   value="{{ $nidentificacion }}">
                            
                            @else
                            <input type="number" class="form-control" id="numero_de_identificacion"
                                   name="numero_de_identificacion" placeholder="Cédula, Tarjeta de Identidad"
                                   value="{{ old('numero_de_identificacion') }}">
                            @endif
                        </div>
                        <div class="form-group">
                            <label>Nombres</label>
                            <input type="text" class="form-control" id="nombres"
                                   name="nombres" placeholder="Nombres" value="{{ old('nombres') }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"
                                   placeholder="Apellidos" value="{{ old('apellidos') }}">
                        </div>
                        <div class="form-group">
                            <label>Teléfono:</label>
                            <input type="number" class="form-control" id="telefono" name="telefono"
                                   placeholder="Teléfono móvil, fijo" value="{{ old('telefono') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de usuario</label>
                            <select class="form-control" id="tipo_de_usuario" name="tipo_de_usuario">
                                <option value="">- Seleccione -</option>
                                <option value="Estudiante">Estudiante</option>
                                <option value="Docente">Docente</option>
                                <option value="Funcionario">Funcionario</option>
                                <option value="Otros">Otros</option>
                            </select>
                        </div>

                        <div id="Estudiante" class="opciones">
                            <div class="form-group">
                                <label>Programa:</label>
                                <select class="form-control" name="programa">
                                    <option value="">- Seleccione -</option>
                                    @foreach($programas as $programa)
                                        <option value="{{ $programa->id }}">{{ $programa->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Semestre:</label>
                                <select class="form-control" name="semestre">
                                    <option value="">- Seleccione -</option>
                                    @for($i=1; $i<=12; $i++)
                                        <option value="{{ $i }}">{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        </div>

                        <div id="Docente" class="opciones">
                            <div class="form-group">
                                <label>Facultad:</label>
                                <select class="form-control" name="facultad">
                                    <option value="">- Seleccione -</option>
                                    <option value="Ciencias de la Salud">Ciencias de la Salud</option>
                                    <option value="Ingenierias">Ingenierías</option>
                                    <option value="Ciencias de la Educacion">Ciencias de la Educación</option>
                                    <option value="Ciencias Administrativas">Ciencias Administrativas</option>
                                    <option value="Ciencias Juridicas">Ciencias Juridicas</option>
                                    <option value="Oficina de educación virtual y a distancia">Oficina de educación virtual y a distancia</option>
                                </select>
                            </div>
                        </div>

                        <div id="Funcionario" class="opciones">
                            <div class="form-group">
                                <label>Departamento:</label>
                                <select class="form-control" name="departamento">
                                    <option value="Oficina Asesora Juridica">Oficina Asesora Juridica</option>
                                    <option value="Oficina Asesora de Planeacion">Oficina Asesora de Planeacion</option>
                                    <option value="Oficina de Bienestar Institucional y Gestion Humana">Oficina de Bienestar Institucional y Gestion Humana</option>
                                    <option value="Oficina de Informatica y Telematica">Oficina de Informatica y Telemática</option>
                                    <option value="Presupuesto y Contabilidad">Presupuesto y Contabilidad</option>
                                    <option value="Servicios Generales">Servicios Generales</option>
                                    <option value="Tesoreria">Tesoreria</option>
                                    <option value="Almacen">Almacen</option>
                                    <option value="Admisiones y Registro Academico">Tecnologías de la información</option>
                                    <option value="Biblioteca y documentacion">Biblioteca y documentación</option>
                                    <option value="Laboratorios">Laboratorios</option>
                                    <option value="Departamentos">Departamentos</option>
                                    <option value="Secretaria General">Secretaria General</option>
                                    <option value="Oficina de Control Interno">Oficina de Control Interno</option>
                                    <option value="Oficina Asesora de Comunicaciones">Oficina Asesora de Comunicaciones</option>
                                    <option value="Facultad de Ciencias de la Salud">Facultad de Ciencias de la Salud</option>
                                    <option value="Facultad de Ciencias Administrativas Economicas">Facultad de Ciencias Administrativas Economicas</option>
                                    <option value="Facultad de Ciencias de la Educacion">Facultad de Ciencias de la Educacion</option>
                                    <option value="Facultad de Ingenieria">Facultad de Ingenieria</option>
                                    <option value="Facultad de Ciencias Juridicas">Facultad de Ciencias Juridicas</option>
                                    <option value="Oficina de Educacion Virtual y a Distancia">Oficina de Educación Virtual y a Distancia</option>
                                    <option value="Vicerrectoria Academica">Vicerrectoria Academica</option>
                                    <option value="Vicerrectoria Administrativa y Financiera">Vicerrectoria Administrativa y Financiera</option>
                                    <option value="Vicerrectoria de Investigacion y Proyeccion a la comunidad">Vicerrectoria de Investigacion y Proyeccion a la comunidad</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <!-- /.box -->
                    <!-- /.box-body -->
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_pacientes') }}"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.opciones').hide();

            $('#tipo_de_usuario').change(function () {
                $('.opciones').hide();
                $('#' + $(this).val()).show();
            });
        });
    </script>
@endsection
