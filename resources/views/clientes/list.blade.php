@extends('base.app')

@section('migas')
    <h1>Pacientes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Pacientes</li>
        <li class="active">Listar Pacientes</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Pacientes</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('new_paciente')}}" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Crear
                        Paciente</a>
                </div>
            </div>
            <div class="box-body">
                @if(session()->has('exito'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok"></span> {{ session('exito') }}
                    </div>
                @endif
                <table id="lista_pacientes" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Numero de identificacion</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Teléfono</th>
                        <th>Tipo de usuario</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbspEstadísticas por Programa</h3>
            </div>
            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        <label>Programa</label>
                        <select class="form-control" id="programa" name="programa">
                            <option value="">- Seleccione -</option>
                            @foreach($programas as $programa)
                                <option value="{{ $programa->name }}">{{ $programa->name }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Desde</label>
                        <div class="form-inline input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="fecha_inicio" name="fecha_inicio" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>Hasta</label>
                        <div class="form-inline input-group date">
                            <div class="input-group-addon">
                                <i class="fa fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="fecha_fin" name="fecha_fin" required>
                        </div>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-group">
                        <label>&nbsp;</label>
                        <div class="form-inline">
                            <button id="EstPrograma" type="button" class="btn btn-primary pull-right"><i
                                        class="fa fa-search" aria-hidden="true"></i> Consultar
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalInfoPaciente" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Información Adicional del Paciente</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Estadísticas del Paciente:</h4>
                            <b><p id="informacion"></p></b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Tomas de presión arterial (Tpa): <span class="label label-primary"
                                                                                     id="tomas_presion_arterial"></span></div>
                        <div class="col-md-6">Entregas de medicamento (Med): <span class="label label-primary"
                                                                             id="entregas_de_medicamento"></span></div>
                        <div class="col-md-6">Glucometrias (Gluc): <span class="label label-primary" id="glucometrias"></span>
                        </div>
                        <div class="col-md-6">Atenciones en salud (Atn Sld): <span class="label label-primary"
                                                                         id="atenciones_en_salud"></span></div>
                        <div class="col-md-6">Inyectologías (Iny): <span class="label label-primary"
                                                                   id="inyectologias"></span></div>
                        <div class="col-md-12">
                            <div id="bar-chart"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEstadisticasPrograma" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Información Adicional por Programa</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Estadísticas para el programa seleccionado:</h4>
                            <b><p id="informacionPrograma"></p></b>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">Tomas de presión arterial (Tpa): <span class="label label-primary"
                                                                                     id="tomas_presion_arterialP"></span></div>
                        <div class="col-md-6">Entregas de medicamento (Med): <span class="label label-primary"
                                                                                   id="entregas_de_medicamentoP"></span></div>
                        <div class="col-md-6">Glucometrias (Gluc): <span class="label label-primary" id="glucometriasP"></span>
                        </div>
                        <div class="col-md-6">Atenciones en salud (Atn Sld): <span class="label label-primary"
                                                                                   id="atenciones_en_saludP"></span></div>
                        <div class="col-md-6">Inyectologías (Iny): <span class="label label-primary"
                                                                         id="inyectologiasP"></span></div>
                        <div class="col-md-12">
                            <div id="bar-chart-pro"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lista_pacientes').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_pacientes_ajax") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            //Grafico para estadisticas
            var grafico_est = Morris.Bar({
                element: 'bar-chart',
                data: [
                    {y: 'E.I.', a: 0},
                    {y: 'GLUC', a: 0},
                    {y: 'AT. SAL', a: 0},
                    {y: 'TPA', a: 0},
                    {y: 'INY', a: 0}
                ],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Veces'],
                fillOpacity: 0.6,
                hideHover: 'auto',
                behaveLikeLine: true,
                resize: true,
                pointFillColors: ['#ffffff'],
                pointStrokeColors: ['black'],
                lineColors: ['gray', 'red']
            });

            var grafico_prog = Morris.Bar({
                element: 'bar-chart-pro',
                data: [
                    {y: 'E.I.', a: 0},
                    {y: 'GLUC', a: 0},
                    {y: 'AT. SAL', a: 0},
                    {y: 'TPA', a: 0},
                    {y: 'INY', a: 0}
                ],
                xkey: 'y',
                ykeys: ['a'],
                labels: ['Veces'],
                fillOpacity: 0.6,
                hideHover: 'auto',
                behaveLikeLine: true,
                resize: true,
                pointFillColors: ['#ffffff'],
                pointStrokeColors: ['black'],
                lineColors: ['gray', 'red']
            });

            //Mostrar informacion estadísticas paciente
            $('#lista_pacientes').on('click', 'tbody tr td div button', function () {
                var nidentificacion = $(this).parent().children('p').text();//Obtener el numero de identificación del paciente
                var user = $(this).parent().children('b').text();//Obtener el nombre del paciente
                $('#informacion').text(user);

                $.ajax({
                    method: "GET",
                    url: "{{ route('inf_adicional_paciente') }}",
                    data: {numero_de_identificacion: nidentificacion},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        $('#entregas_de_medicamento').text(json.entregas_de_medicamento);
                        $('#glucometrias').text(json.glucometrias);
                        $('#atenciones_en_salud').text(json.atenciones_en_salud);
                        $('#tomas_presion_arterial').text(json.toma_presiones_arteriales);
                        $('#inyectologias').text(json.inyectologias);

                        grafico_est.setData([
                            {y: 'Med', a: json.entregas_de_medicamento},
                            {y: 'Gluc', a: json.glucometrias},
                            {y: 'Atn Sld.', a: json.atenciones_en_salud},
                            {y: 'Tpa', a: json.toma_presiones_arteriales},
                            {y: 'Iny', a: json.inyectologias}
                        ]);
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });

                $('#modalInfoPaciente').modal("show");
                $('#bar-chart').height('150px');
            });

            //Consultar estadísticas por programa
            $('#EstPrograma').on('click', function () {
                $('#informacionPrograma').text($('#programa').val());
                var fecha_inicioA = new Date($('#fecha_inicio').val());
                var fecha_finA = new Date($('#fecha_fin').val());

                function pad(number) {
                    if (number < 10) {
                        return '0' + number;
                    }
                    return number;
                }

                var fechaInicio = fecha_inicioA.getFullYear() + "-" + pad(fecha_inicioA.getMonth() + 1) + "-" + pad(fecha_inicioA.getDate());
                var fechaFin = fecha_finA.getFullYear() + "-" + pad(fecha_finA.getMonth() + 1) + "-" + pad(fecha_finA.getDate());

                $.ajax({
                    method: "GET",
                    url: "{{ route('est_programa') }}",
                    data: {
                        programa: $('#programa').val(),
                        fecha_inicio: fechaInicio,
                        fecha_fin: fechaFin
                    },
                    dataType: "JSON"
                })
                    .done(function (json) {
                        $('#entregas_de_medicamentoP').text(json.np_entregas_de_medicamento);
                        $('#glucometriasP').text(json.np_glucometrias);
                        $('#atenciones_en_saludP').text(json.np_atenciones_en_salud);
                        $('#tomas_presion_arterialP').text(json.np_toma_presion_arterial);
                        $('#inyectologiasP').text(json.np_inyectologias);

                        grafico_prog.setData([
                            {y: 'Med', a: json.np_entregas_de_medicamento},
                            {y: 'Gluc', a: json.np_glucometrias},
                            {y: 'Atn Sld.', a: json.np_atenciones_en_salud},
                            {y: 'Tpa', a: json.np_toma_presiones_arteriales},
                            {y: 'Iny', a: json.np_inyectologias}
                        ]);
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
                $('#modalEstadisticasPrograma').modal("show");
                $('#bar-chart-pro').height('150px');
            });

            $('#fecha_inicio').datepicker({
                autoclose: true
            });

            $('#fecha_fin').datepicker({
                autoclose: true
            });
        });
    </script>
@endsection
