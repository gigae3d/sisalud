@extends('base.app')

@section('migas')
    <h1>Pacientes</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Pacientes</li>
        <li class="active">Editar Paciente</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Editar Paciente</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('edit_paciente') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número de identificación</label>
                            <input type="number" class="form-control" id="numero_de_identificacion"
                                   name="numero_de_identificacion" placeholder="Cédula, Tarjeta de Identidad"
                                   value="{{ $pacientes->numero_de_identificacion }}" readonly>
                        </div>
                        <div class="form-group">
                            <label>Nombres</label>
                            <input type="text" class="form-control" id="nombres"
                                   name="nombres" value="{{ $pacientes->nombres}}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"
                                   value="{{ $pacientes->apellidos}}">
                        </div>
                        <div class="form-group">
                            <label>Teléfono</label>
                            <input type="text" class="form-control" id="telefono" name="telefono"
                                   value="{{ $pacientes->telefono}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Tipo de usuario</label>
                            <select class="form-control" id="tipo_de_usuario" name="tipo_de_usuario">
                                <option value="{{ $pacientes->tipo_de_usuario}}">{{ $pacientes->tipo_de_usuario}}</option>
                            </select>
                        </div>

                        @if ($pacientes->tipo_de_usuario == 'Estudiante')
                            <div class="form-group">
                                <label>Programa:</label>
                                <select class="form-control" name="programa">
                                    @foreach($programas as $programa)
                                        <option
                                                value="{{ $programa->id }}"
                                                @if($estudiantes->id_programa == $programa->id)
                                                    selected
                                                @endif
                                        >{{ $programa->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Semestre:</label>
                                <select class="form-control" name="semestre">
                                    @for($i=1; $i<=10; $i++)
                                        <option
                                                value="{{ $i }}"
                                                @if($estudiantes->semestre == $i)
                                                    selected
                                                @endif
                                        >{{ $i }}</option>
                                    @endfor
                                </select>
                            </div>
                        @endif

                        @if ($pacientes->tipo_de_usuario == 'Docente')
                            <div class="form-group">
                                <label>Facultad:</label>
                                <select class="form-control" name="facultad">
                                    <option value="Ciencias de la Salud" @if($docente->facultad == 'Ciencias de la Salud') selected @endif>Ciencias de la Salud</option>
                                    <option value="Ingenierias" @if($docente->facultad == 'Ingenierias') selected @endif>Ingenierías</option>
                                    <option value="Ciencias Sociales" @if($docente->facultad == 'Ciencias Sociales') selected @endif>Ciencias Sociales</option>
                                    <option value="Ciencias Administrativas" @if($docente->facultad == 'Ciencias Administrativas') selected @endif>Ciencias Administrativas</option>
                                </select>
                            </div>
                        @endif

                        @if ($pacientes->tipo_de_usuario == 'Funcionario')
                            <div class="form-group">
                                <label>Departamento:</label>
                                <select class="form-control" name="departamento">
                                    <option value="Administración" @if($funcionario->departamento == 'Administración') selected @endif>Administración</option>
                                    <option value="Talento Humano" @if($funcionario->departamento == 'Talento Humano') selected @endif>Talento Humano</option>
                                    <option value="Financiero" @if($funcionario->departamento == 'Financiero') selected @endif>Financiero</option>
                                    <option value="Tecnologías de la información" @if($funcionario->departamento == 'Tecnologías de la información') selected @endif>Tecnologías de la información</option>
                                </select>
                            </div>
                        @endif
                    </div>
                    <!-- /.box -->
                    <!-- /.box-body -->
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_pacientes') }}"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                    <!-- /.box-footer -->
                </div>

            </form>
        </div>
    </div>


@endsection