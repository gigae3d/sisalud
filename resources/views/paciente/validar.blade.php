@extends('base.app')

@section('migas')
    <h1>Pacientes</h1>
    <ol class="breadcrumb">
        <li><a href="/home"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Pacientes</li>
        <li class="active">Pacientes</li>
    </ol>
@endsection

@section('contenido')
<div id="contenedor_vue">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <div class="box box-success">
                    <form role="form" id="validate" method="POST" action="{{ route('val_cedula') }}">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="form-group ">
                            <div class="box-header with-border">
                                <h3 class="box-title">Validar Paciente</h3>
                            </div>
                            <div class="box-body" style="height: 95px;">
                                <div class="col-xs-7 margin-bottom">
                                    <input type="number" class="form-control" id="input_cedula" name="input_cedula"
                                           @if ($resultado['entro']) value="{{ $resultado['cedula'] }}"
                                           @else placeholder="Numero de Identificación" @endif required>
                                </div>
                                <div class="col-xs-5">
                                    <button type="submit" id="btn_validate" class="btn btn-success"><i
                                                class="fa fa-search"></i> Consultar
                                    </button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="box box-danger @if ($resultado['entro'] == false or !empty($resultado['datos'])) hidden @endif"
                     id="confirm">
                    <div class="box-body">
                        <div class="col-md-12">
                            El Numero de Identificación @if ($resultado['entro']) {{ $resultado['cedula'] }} @endif no
                            se encuetra registrado.<br>
                            Desea crear un nuevo registro?
                        </div>
                        <div class="col-md-12 box-footer">
                            <div class="box-tools pull-right">
                                @if(isset($resultado['cedula']))
                                <a href="{{ route('new_paciente',['nidentificacion'=>$resultado['cedula']]) }}" class="btn btn-primary"><i
                                            class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Paciente</a>
                                @else
                                <a href="{{ route('new_paciente') }}" class="btn btn-primary"><i
                                            class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Paciente</a>
                                @endif            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="box box-success @if (empty($resultado['datos']))hidden @endif" id="table">
                    <div class="form-group ">
                        <div class="box-header with-border">
                            <h3 class="box-title">Información Personal</h3>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <tbody>
                                <tr>
                                    <th>Cedula</th>
                                    <th>Nombres</th>
                                    <th>Apellidos</th>
                                    <th>Tipo</th>
                                </tr>
                                @if(!empty($resultado['datos']))
                                    @foreach($resultado['datos'] as $data)
                                        <tr>
                                            <td>{{ $data['numero_de_identificacion'] }}</td>
                                            <td>{{ $data['nombres'] }}</td>
                                            <td>{{ $data['apellidos'] }}</td>
                                            <td>{{ $data['tipo_de_usuario'] }}</td>
                                        </tr>
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <div class="box box-info @if (empty($resultado['datos']))hidden @endif">
                    <div class="form-group ">
                        <div class="box-header with-border">
                            <h3 class="box-title">Acciones</h3>
                        </div>
                        @foreach($resultado['datos'] as $data)
                            <div class="box-body">
                                <div class="col-md-3 col-xs-6 margin-bottom">
                                    <a class="btn btn-block btn-accion"
                                       href="{{ route('edit_form_paciente', ['id'=>$data['numero_de_identificacion']]) }}">
                                        <span class="glyphicon glyphicon-pencil"></span><br> Actualizar Datos
                                    </a>
                                </div>
                                <!--<div class="col-md-3 col-xs-6 margin-bottom">
                                    <a class="btn btn-block btn-accion"
                                       href="{{ route('new_atencion_en_salud', ['numero_de_identificacion'=>$data['numero_de_identificacion']]) }}">
                                        <span class="badge bg-yellow">{{ $atenciones_en_salud }}</span>
                                        <i class="fa fa-heartbeat"></i><br> Atención En Salud
                                    </a>
                                </div>-->
                                <div class="col-md-3 col-xs-6 margin-bottom">
                                    <div class="btn btn-accion" style="display: flex; ">
                                    <div style="width: 70%; display: flex; justify-content:center; align-items:center;">
                                    Entrega de medicamentos
                                    </div>
                                    <div style="width: 30%;">
                                    <a class=" btn-block btn-accion" v-on:click="agregar_entrega_medicamentos">
                                       <!--href="{{ route('new_entrega_med', ['numero_de_identificacion'=>$data['numero_de_identificacion']]) }}">-->
                                        <!--<span class="badge bg-info">{{ $entregas_de_medicamento }}</span>
                                        <span class="badge bg-info">{{ $entregas_de_medicamento }}</span>-->
                                        
                                        <i class="glyphicon glyphicon-plus-sign"></i>
                                    </a>
                                    <a class=" btn-block btn-accion" v-on:click="listar_entrega_medicamentos">
                                    <i class="glyphicon glyphicon-search"></i><br>
                                    </a>
                                    </div>
                                    </div>
                                    
                                </div>
                                <!--<div class="col-md-3 col-xs-6 margin-bottom">
                                    <a class="btn btn-block btn-accion"
                                       href="{{ route('new_inyectologia', ['numero_de_identificacion'=>$data['numero_de_identificacion']]) }}">
                                        <span class="badge bg-yellow">{{ $inyectologias }}</span>
                                        <span class="glyphicon glyphicon-folder-open"></span><br> Inyectologia
                                    </a>
                                </div>-->
                                <div class="col-md-3 col-xs-6 margin-bottom">
                                    <div class="btn btn-accion" style="display: flex; ">
                                    <div style="width: 70%; display: flex; justify-content:center; align-items:center;">
                                    Glucometría
                                    </div>
                                    <div style="width: 30%;">
                                    <a class=" btn-block btn-accion" v-on:click="agregar_glucometria1">
                                    <!--<a class="btn btn-block btn-accion"
                                       href="{{ route('new_glucometria', ['numero_de_identificacion'=>$data['numero_de_identificacion']]) }}">
                                        <span class="badge bg-info">{{ $glucometrias }}</span>
                                        <span class="glyphicon glyphicon-modal-window"></span>
                                    </a>-->
                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                    </a>
                                    <a class=" btn-block btn-accion" v-on:click="listar_glucometrias">
                                    <i class="glyphicon glyphicon-search"></i><br>
                                    </a>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6 margin-bottom">
                                <div class="btn btn-accion" style="display: flex; ">
                                    <div style="width: 70%; display: flex; justify-content:center; align-items:center;">
                                    Toma presión arterial
                                    </div>
                                    <div style="width: 30%;">
                                    <a class=" btn-block btn-accion" v-on:click="agregar_toma_p_arterial">
                                    <!--<a class="btn btn-block  btn-accion"
                                       href="{{ route('new_tpresion_arterial', ['numero_de_identificacion'=>$data['numero_de_identificacion']]) }}">
                                        <span class="badge bg-yellow">{{ $toma_presiones_arteriales }}</span>
                                        <span class="glyphicon glyphicon-heart"></span><br> Toma de presión arterial
                                    </a>-->
                                    <i class="glyphicon glyphicon-plus-sign"></i>
                                    </a>
                                    <a class=" btn-block btn-accion" v-on:click="listar_tpresion_arterial">
                                    <i class="glyphicon glyphicon-search"></i><br>
                                    </a>
                                    </div>
                                    </div>
                                </div>
                                <div class="col-md-3 col-xs-6 margin-bottom">
                                    <a class="btn btn-block btn-accion"
                                       href="{{ route('list_afinamientos', ['numero_de_identificacion'=>$data['numero_de_identificacion']]) }}">
                                        <span class="glyphicon glyphicon-heart"></span><br> Lista de afinamientos
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

<agregar-glucometria1 ref="glucometria1" v-on:agregar_glucometria2="agregar_glucometria2"></agregar-glucometria1> <!-- Modal para agregar glucometria 1-->
<agregar-glucometria2 ref="glucometria2" v-on:registrar_glucometria="registrar_glucometria"></agregar-glucometria2> <!-- Modal para agregar glucometria 2-->
<agregar-toma-presion1 ref="tpresion1" v-on:agregar_toma_p_arterial2="agregar_toma_p_arterial2"></agregar-toma-presion1><!-- Modal para agregar toma de presion arterial 1-->
<agregar-toma-presion2 ref="tpresion2" v-on:registrar_toma_presion_arterial="registrar_toma_presion_arterial"></agregar-toma-presion2><!-- Modal para agregar toma de presion arterial 2-->
<listado-entrega-medicamentos ref="listaEntregaMed"></listado-entrega-medicamentos>
<listado-glucometrias ref="listaGlucometrias"></listado-glucometrias>
<listado-tpresionarterial ref="listaTPresionArt"></listado-tpresionarterial>

<div class="modal fade" id="modAgregarMedicamentos" tabindex="-1" role="dialog"> 
   <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Entrega de Medicamentos</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-7">
                  <div class="form-inline" >
		           <label>Buscar:</label>
                   <input class="form-control" type="text" id="busquedaMedicamento" v-model="busquedaMedicamento">
                  </div>

                  <table class="table table-hover">
                   <thead>
                   <th>Nombre</th>
                   <th>Laboratorio</th>
                   <th>F.Vencimiento</th>
                   <th>Existencia</th>
                   <th>Lote</th>
                   <th></th>
                   </thead>
                   <tbody>
                    <tr v-if="medicamentos.length == 0">
                     <td colspan="4">No hay datos...</td>
                    </tr>
                    <tr v-else v-for="medicamento in medicamentos">
                    <td>@{{medicamento.nombre}}</td>
                    <td>@{{medicamento.laboratorio}}</td>
                    <td>@{{medicamento.fecha_vencimiento}}</td>
                    <td>@{{medicamento.existencia}}</td>
                    <td>@{{medicamento.lote}}</td>
                    <td><button class="btn btn-sm btn-success" type="button" @click="agregar_medicamento([medicamento.id, medicamento.nombre])">
                    <i class="fa fa-check-square"></i></button></td>
                    </tr>
                   </tbody>
                  </table>
                  <div class="pull-right">
		              <label>Página @{{paginaMedicamento}} de @{{numPaginasMedic}}</label>
		              <button v-if="paginaMedicamento == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-else type="button" class="btn btn-link " @click="paginaMedicamento--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-if="paginaMedicamento == numPaginasMedic || numPaginasMedic == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
                  <button v-else type="button" class="btn btn-link " @click="paginaMedicamento++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		              </div>
                  </div>

                  <div class="col-md-5">
                  <label>Listado de seleccionados</label>
                  <table class="table table-hover">
                  <thead>
                  <th>Medicamento</th>
                  <th>Cantidad</th>
                  <th></th>
                  </thead>
                  <tbody>
                    <tr v-for="medicamento in medicamentos_seleccionados">
                    <td>@{{medicamento.nombre}}</td>
                    <td><input type="text" class="form-control" v-model="medicamento.cantidad"></td>
                    <td><button class="btn btn-sm btn-success" type="button" @click="quitar_medicamento([medicamento.id, medicamento.nombre])">
                    <i class="fa fa-times-circle"></i></button></td>
                    </tr>
                  </tbody>
                  </table>
                  </div>
              </div>

    </div>

    <div class="modal-footer">
       <label style="color:red;">*Debe llenar todos los campos.</label>
       <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       <button type="button" class="btn btn-primary" v-on:click="registrar_entrega_medicamento">Guardar</button>
    </div>
  </div>
  </div>
  </div>

</div>

</div><!-- div cierre contenedor vue-->
@endsection

@section('js')

<script src="{{ asset('js/sweetalert2.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $('#validate').validate({
                errorClass: "my-error-class",
                rules: {
                    input_cedula: {
                        required: true,
                        minlength: 6
                    }
                },
                messages: {
                    input_cedula: {
                        required: "Por favor ingresa un numero valido",
                        minlength: jQuery.validator.format("Al menos {0} Caracteres son requeridos!"),
                    }
                }
            });

            $('.opciones').hide();

            $('#crear_paciente').click( function(){
              $('#numero_de_identificacion').val( $('#input_cedula').val());
            });

            $('#tipo_de_usuario').change(function () {
                $('.opciones').hide();
                $('#' + $(this).val()).show();
            });

        });
    </script>

<script type="text/javascript">

Vue.component('agregar-glucometria1', {
    template: `<div class="modal fade" id="modAgregarGlucometria1" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Agregar Glucometria</h4>
     </div>
     <div class="modal-body">
     <div class="row">
        <div class="col-md-12">
        <div style="display: flex; justify-content:center;">
        <label>Seleccione un equipo médico</label>
        </div>
        </div>     
     </div>  

     <div class="row">
        <div class="col-md-12">
        <table class="table table-hover">
          <thead>
          <th></th>
          <th>Nombre</th>
          <th>Modelo</th>
          <th>Tipo</th>
          </thead>
          <tbody>
          <tr v-if="equipos_medicos.length == 0">
            <td colspan="4">No hay datos...</td>
          </tr>
           <tr v-else v-for="equipo_medico in equipos_medicos">
           <td><input type="radio" name="id_equipo_medico" @click="seleccion_equipo_medico(equipo_medico.id)"></td>
           <td>@{{equipo_medico.nombre}}</td>
           <td>@{{equipo_medico.modelo}}</td>
           <td>@{{equipo_medico.tipo}}</td>
           </tr>
          </tbody>
        </table>
        </div> 
     </div>

     </div>
 
     <div class="modal-footer">
        <label style="color:red;">*Debe seleccionar un equipo médico.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" :disabled="seleccion_equipo_m_vacio" v-on:click="$emit('agregar_glucometria2')">Siguiente</button>
     </div>
   </div>
   </div>
   </div>`,
   data: function(){
    return {
        equipos_medicos: [],
        numeroDePaginas: 1,
        porPagina: 5,
        paginaEqMedico: 1,
        busquedaEqMedico: "",
        equipoMedicoSeleccionado: ""
    }
   },

   methods: {
    obtener_equipos_medicos: function() {
        let self = this;
        console.log("se ejecutó");
        axios.get('/obtenerEqMedicos',{params:{palabra:this.busquedaEqMedico, page: this.paginaEqMedico}}).then(function(response){
            console.log("Equipos medicos:", response.data.equipos_medicos);
            console.log("numero Equipos medicos:", response.data.nequipos_medicos);
            self.equipos_medicos =  response.data.equipos_medicos.data;
            self.numeroDePaginas= Math.ceil(response.data.nequipos_medicos/self.porPagina);
        });
    },

    seleccion_equipo_medico: function(equipo_medico) {
        this.equipoMedicoSeleccionado = equipo_medico;
    }

   },

   computed: {
      seleccion_equipo_m_vacio: function() {
        return (this.equipoMedicoSeleccionado.length == 0) ? true: false;
      }
   }
});


Vue.component('agregar-glucometria2',{
   template: `<div class="modal fade" id="modAgregarGlucometria2" tabindex="-1" role="dialog"> 
   <div class="modal-dialog" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Agregar Glucometría</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-6">
                  <div class="form-group">
                  <label>Patologia base:</label>
                  <input type="text" class="form-control" v-model="glucometria.patologia" placeholder="Patología"></input>         
                  </div>
                  </div>

                  <div class="col-md-6">
                  <div class="form-group">
                  <label>Resultado:</label>
                  <input type="text" class="form-control" v-model="glucometria.resultado" placeholder="Resultado"></input>         
                  </div>
                  </div>
              </div>

              <div class="row">
                 <div class="col-md-4">
                 <div class="form-group">
                  <label>Tirillas:</label>
                  <input type="number" class="form-control" v-model="glucometria.tirillas" placeholder="Tirillas"></input>         
                  </div>
                 </div>

                 <div class="col-md-4">
                 <div class="form-group">
                  <label>Lancetas:</label>
                  <input type="number" class="form-control" v-model="glucometria.lancetas" placeholder="Lancetas"></input>         
                  </div>
                 </div>

                 <div class="col-md-4">
                 <div class="form-group">
                  <label>Guantes:</label>
                  <input type="number" class="form-control" v-model="glucometria.guantes" placeholder="Guantes"></input>         
                  </div>
                 </div>
              </div>
    </div>

    <div class="modal-footer">
       <label style="color:red;">*Debe llenar todos los campos.</label>
       <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
       <button type="button" :disabled="form_glucometria_vacio" class="btn btn-primary" v-on:click="$emit('registrar_glucometria')">Guardar</button>
    </div>
  </div>
  </div>
  </div>`,
  data: function() {
      return {
          glucometria: {
            patologia: "", resultado: "", tirillas: "", lancetas: "", guantes: ""
          }
      }
  },

  computed: {
      form_glucometria_vacio: function() {
        return Object.values(this.glucometria).some(campo => campo.length==0);
      }
  }
});

Vue.component('agregar-toma-presion1',{
    template: `<div class="modal fade" id="modAgregarTPresionArt1" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Agregar Toma de Presión Arterial</h4>
     </div>
     <div class="modal-body">
     <div class="row">
        <div class="col-md-12">
        <div style="display: flex; justify-content:center;">
        <label>Seleccione un equipo médico</label>
        </div>
        </div>     
     </div>  

     <div class="row">
        <div class="col-md-12">
        <table class="table table-hover">
          <thead>
          <th></th>
          <th>Nombre</th>
          <th>Modelo</th>
          <th>Tipo</th>
          </thead>
          <tbody>
          <tr v-if="equipos_medicos.length == 0">
            <td colspan="4">No hay datos...</td>
          </tr>
           <tr v-else v-for="equipo_medico in equipos_medicos">
           <td><input type="radio" name="id_equipo_medico" @click="seleccion_equipo_medico(equipo_medico.id)"></td>
           <td>@{{equipo_medico.nombre}}</td>
           <td>@{{equipo_medico.modelo}}</td>
           <td>@{{equipo_medico.tipo}}</td>
           </tr>
          </tbody>
        </table>
        <div class="pull-right">
		  <label>Página @{{paginaEqMedico}} de @{{numPaginas}}</label>
		  <button v-if="paginaEqMedico == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		  <button v-else type="button" class="btn btn-link " @click="paginaEqMedico--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		  <button v-if="paginaEqMedico == numPaginas || numPaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
          <button v-else type="button" class="btn btn-link " @click="paginaEqMedico++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		</div>
        </div> 
     </div>

     </div>
 
     <div class="modal-footer">
        <label style="color:red;">*Debe seleccionar un equipo médico.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click="$emit('agregar_toma_p_arterial2')">Siguiente</button>
     </div>
   </div>
   </div>
   </div>`,
   data: function(){
    return {
        equipos_medicos: [],
        numPaginas: 1,
        porPagina: 5,
        paginaEqMedico: 1,
        busquedaEqMedico: "",
        equipoMedicoSeleccionado: ""
    }
   },

   methods: {
    obtener_equipos_medicos: function() {
        let self = this;
        console.log("se ejecutó");
        axios.get('/obtenerEqMedicos',{params:{palabra:this.busquedaEqMedico, page: this.paginaEqMedico}}).then(function(response){
            console.log("Equipos medicos:", response.data.equipos_medicos);
            console.log("numero Equipos medicos:", response.data.nequipos_medicos);
            self.equipos_medicos =  response.data.equipos_medicos.data;
            self.numeroDePaginas= Math.ceil(response.data.nequipos_medicos/self.porPagina);
        });
    },

    seleccion_equipo_medico: function(equipo_medico) {
        this.equipoMedicoSeleccionado = equipo_medico;
        console.log("Equipo medico sel:", this.equipoMedicoSeleccionado);
    }

   }

 });

 Vue.component('agregar-toma-presion2',{
    template: `<div class="modal fade" id="modAgregarTPresionArt2" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Agregar Toma de Presión Arterial</h4>
     </div>
     <div class="modal-body"> 

     <div class="row">
          <div class="col-md-6">
          <div class="form-group">
              <label>Resultado:</label>
              <input type="text" class="form-control" v-model="toma_presion.resultado"
                     placeholder="Resultados">
          </div>
          </div>

          <div class="col-md-6">
           <div class="form-group">
              <label>Tipo:</label>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <label class="radio-inline">
                  <input type="radio" value="Normal" v-model="seleccion_tipo">
                  Normal
              </label>
              &nbsp;&nbsp;&nbsp;&nbsp;
              <label class="radio-inline">
                  <input type="radio" value="Afinamiento" v-model="seleccion_tipo">
                  Afinamiento
              </label>
           </div>
          </div>
          
          
          <div class="col-md-12" v-show="mostrar_normal">
            <div class="form-group">
                <label>Posición:</label>
                <input type="text" class="form-control" v-model="toma_presion.posicion"
                       placeholder="Posicion">
            </div>
          </div>
          
          <div class="col-md-12" v-show="mostrar_afinamiento">
             <div class="row">
                 <div class="col-md-2">
                     <div class="form-group">
                         <label class="checkbox-inline">
                             <input type="checkbox" value="de_pie"  v-model="seleccion_afinamiento">
                             De pie
                         </label>
                     </div>
                 </div>
                 <div class="col-md-5" v-show="mostrar_de_pie">
                     <div class="input-group">
                         <span class="input-group-addon">MSD</span>
                         <input type="text" class="form-control" name="" v-model="toma_presion.de_pie.MSD">
                         <span class="input-group-addon">(mmHg)</span>
                     </div>
                 </div>
                            <div class="col-md-5" v-show="mostrar_de_pie">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" v-model="toma_presion.de_pie.MSI">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
             </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="sentado_tp" value="sentado" v-model="seleccion_afinamiento">
                                        Sentado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_sentado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" v-model="toma_presion.sentado.MSD">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_sentado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" v-model="toma_presion.sentado.MSI">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="acostado_tp" value="acostado" v-model="seleccion_afinamiento">
                                        Acostado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_acostado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" v-model="toma_presion.acostado.MSD">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5" v-show="mostrar_acostado">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" v-model="toma_presion.acostado.MSI">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>
                    </div>

     </div>

     </div>
 
     <div class="modal-footer">
        <label style="color:red;">*Debe seleccionar un equipo médico.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" :disabled="!form_toma_presion_vacio" v-on:click="$emit('registrar_toma_presion_arterial')">Siguiente</button>
     </div>
   </div>
   </div>
   </div>`,
   data: function(){
       return {
           toma_presion: {
            resultado: "", 
            posicion: "",
            de_pie: {
                MSD: "", MSI:""
            },
            sentado: {
                MSD:"", MSI:""
            },
            acostado: {
                MSD:"", MSI: ""
            }
           },
           validaciones_afinamiento:[],
           mostrar_normal: false,
           mostrar_afinamiento: false,
           seleccion_tipo: "",
           mostrar_de_pie: false,
           mostrar_sentado: false,
           mostrar_acostado: false,
           seleccion_afinamiento: []
       }
   },

   watch: {
       seleccion_tipo: function(){
           if(this.seleccion_tipo == "Normal"){
            this.mostrar_afinamiento = false;  
             this.mostrar_normal = true;
           } else if(this.seleccion_tipo == "Afinamiento"){
            this.mostrar_normal = false;
            this.mostrar_afinamiento = true;
           }
       },

       seleccion_afinamiento: function() {
           if(this.seleccion_afinamiento.includes("de_pie")){
               this.mostrar_de_pie = true;
           } else {
               this.mostrar_de_pie = false;
           }

           if(this.seleccion_afinamiento.includes("sentado")){
               this.mostrar_sentado = true;
           } else {
               this.mostrar_sentado = false;
           }

           if(this.seleccion_afinamiento.includes("acostado")){
               this.mostrar_acostado = true;
           } else{
               this.mostrar_acostado = false;
           }
       }
   },

   computed: {
    form_toma_presion_vacio: function() {

      let validacion_general = true;

      if(this.seleccion_tipo == "Afinamiento"){
        if(this.seleccion_afinamiento.length != 0){
        for(let i = 0; i < this.seleccion_afinamiento.length; i++){
          if(Object.values(this.toma_presion[this.seleccion_afinamiento[i]]).some(campo => campo.length == 0)){
              validacion_general = false;
              break;
          }
        } 
        
        }else if(this.seleccion_afinamiento.length == 0){
            validacion_general = false;
        }

      } else if(this.seleccion_tipo == "Normal") {
        if(this.toma_presion.posicion.length == 0){
        validacion_general = false;
        } 
      } 
    
      if(this.toma_presion.resultado.length == 0 || (this.toma_presion.resultado.length != 0 && this.seleccion_tipo == "")){
        validacion_general = false;
        
      }
      
      return validacion_general;
      
    }
   }
 });


Vue.component('listadoEntregaMedicamentos',{
    template: `<div class="modal fade" id="modListaEntregaMedicamentos" tabindex="-1" role="dialog"> 
   <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Entrega de Medicamentos</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-8">
                  <!--<div class="form-inline" >
		           <label>Buscar:</label>
                   <input class="form-control" type="text" v-model="busquedaEntMed">
                  </div>-->

                  <table class="table table-hover">
                   <thead>
                   <th>Fecha</th>
                   <th>Usuario que registra</th>
                   <th>Acciones</th>
                   </thead>
                   <tbody>
                    <tr v-if="entregas_medicamentos.length == 0">
                     <td colspan="4">No hay datos...</td>
                    </tr>
                    <tr v-else v-for="entrega in entregas_medicamentos">
                    <td>@{{entrega.fecha}}</td>
                    <td>@{{entrega.usuario}}</td>
                    <td>
                    <button type="button" class="btn btn-link "><span class="glyphicon glyphicon-menu-hamburger" style="color:blue;" aria-hidden="true" @click="detalles_entrega(entrega.id)"></span></button>
                    </td>
                    </tr>
                   </tbody>
                  </table>
                  <div class="pull-right">
		              <label>Página @{{pagina}} de @{{numPaginas}}</label>
		              <button v-if="pagina == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-else type="button" class="btn btn-link " @click="pagina--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-if="pagina == numPaginas || numPaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
                      <button v-else type="button" class="btn btn-link " @click="pagina++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		          </div>
                  </div>
                
                  <div class="col-md-4">
                  <label>Detalles Entrega</label>
                  <table class="table table-hover">
                   <thead>
                   <th>Medicamento</th>
                   <th>Cantidad</th>
                   </thead>
                   <tbody>
                    <tr v-for="detalle in detalle_entrega">
                    <td>@{{detalle.nombre}}</td>
                    <td>@{{detalle.cantidad}}</td>
                    </tr>
                   </tbody>
                  </table>
                  
                  </div>
                  
              </div>

    </div>

    <div class="modal-footer">
       <label style="color:red;">*Debe llenar todos los campos.</label>
       <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
  </div>`,
  data: function(){
    return {
        entregas_medicamentos: [],
        numPaginas: 1,
        porPagina: 5,
        pagina: 1,
        busquedaEntMed : "",
        detalle_entrega: []
    }
   },

   methods: {
       obtener_entregas_medicamento: function(){
        let self = this;
        axios.get('/obtenerEntregasMedicamentos',{params:{paciente:document.getElementById('input_cedula').value,palabra:this.busquedaEntMed, page: this.pagina}}).then(function(response){
            console.log("Entregas de medicamentos:", response.data.entregas_medicamentos.data);
            self.entregas_medicamentos = response.data.entregas_medicamentos.data;
            self.numPaginas =  Math.ceil(response.data.nentregas_medicamentos/self.porPagina);
        });
       },

       detalles_entrega: function (id_i) {
       let self = this;
       self.detalle_entrega.length = 0;
       axios.get('/get_medicamentos_entr', {params:{id: id_i}}).then(function(respuesta){
           console.log("respuesta detalles entrega:", respuesta.data.medicamentos);
           for(let i = 0; i < respuesta.data.medicamentos.length; i++){
           self.detalle_entrega.push({nombre:respuesta.data.nombres[i], cantidad: respuesta.data.medicamentos[i].cantidad});
           }
           console.log("respuesta detalles nombres:", respuesta.data.nombres);
       });
       }
   }
});

Vue.component('listadoGlucometrias',{
    template: `<div class="modal fade" id="modListaGlucometrias" tabindex="-1" role="dialog"> 
   <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Glucometrias paciente</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-12">
                  <!--<div class="form-inline" >
		           <label>Buscar:</label>
                   <input class="form-control" type="text" v-model="busquedaGlucometria">
                  </div>-->

                  <table class="table table-hover">
                   <thead>
                   <th>Fecha</th>
                   <th>Usuario que registra</th>
                   <th>Equipo Medico</th>
                   <th>Patología base</th>
                   <th>Resultado</th>
                   </thead>
                   <tbody>
                    <tr v-if="glucometrias.length == 0">
                     <td colspan="4">No hay datos...</td>
                    </tr>
                    <tr v-else v-for="glucometria in glucometrias">
                    <td>@{{glucometria.fecha}}</td>
                    <td>@{{glucometria.usuario}}</td>
                    <td>@{{glucometria.equipo_medico}}</td>
                    <td>@{{glucometria.patologia_base}}</td>
                    <td>@{{glucometria.resultado}}</td>
                    </tr>
                   </tbody>
                  </table>
                  <div class="pull-right">
		              <label>Página @{{pagina}} de @{{numPaginas}}</label>
		              <button v-if="pagina == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-else type="button" class="btn btn-link " @click="pagina--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-if="pagina == numPaginas || numPaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
                      <button v-else type="button" class="btn btn-link " @click="pagina++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		          </div>
                  </div>    
                  
              </div>

    </div>

    <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
  </div>`,
    data: function() {
        return {
            busquedaGlucometria: "",
            glucometrias: [],
            numPaginas: 1,
            porPagina: 5,
            pagina: 1,
        }
    },

    methods: {
        obtener_glucometrias: function() {
            let self = this;
            axios.get('/obtenerGlucometrias',{params:{paciente:document.getElementById('input_cedula').value, page: this.pagina}}).then(respuesta => {
                self.glucometrias = respuesta.data.glucometrias.data;
                self.numPaginas =  Math.ceil(respuesta.data.nglucometrias/self.porPagina);
                
            });
        }
    }
});


Vue.component('listadoTpresionarterial',{
    template: `<div class="modal fade" id="modListaTPresionArt" tabindex="-1" role="dialog"> 
   <div class="modal-dialog modal-lg" role="document">
   <div class="modal-content">
    <div class="modal-header">
      <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <h4 class="modal-title">Tomas de presion arterial de paciente registradas</h4>
    </div>
    <div class="modal-body">
      
              <div class="row">
                  <div class="col-md-8">
                  <!--<div class="form-inline" >
		           <label>Buscar:</label>
                   <input class="form-control" type="text" v-model="busquedaTpresionart">
                  </div>-->

                  <table class="table table-hover">
                   <thead>
                   <th>Equipo Medico</th>
                   <th>Registra</th>
                   <th>Fecha</th>
                   <th>Patología base</th>
                   <th>Posicion</th>
                   <th>Resultado</th>
                   <th>Acciones</th>
                   </thead>
                   <tbody>
                    <tr v-if="tomasPresionT.length == 0">
                     <td colspan="4">No hay datos...</td>
                    </tr>
                    <tr v-else v-for="tomapresion in tomasPresionT">
                    <td>@{{tomapresion.equipo_medico}}</td>
                    <td>@{{tomapresion.usuario}}</td>
                    <td>@{{tomapresion.fecha}}</td>
                    <td>@{{tomapresion.tipo}}</td>
                    <td>@{{tomapresion.posicion}}</td>
                    <td>@{{tomapresion.resultado}}</td>
                    <td><button type="button" class="btn btn-link "><span class="glyphicon glyphicon-menu-hamburger" style="color:blue;" aria-hidden="true" @click="detalles_afinamiento(tomapresion.id)"></span></button></td>
                    </tr>
                   </tbody>
                  </table>
                  <div class="pull-right">
		              <label>Página @{{pagina}} de @{{numPaginas}}</label>
		              <button v-if="pagina == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-else type="button" class="btn btn-link " @click="pagina--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		              <button v-if="pagina == numPaginas || numPaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
                      <button v-else type="button" class="btn btn-link " @click="pagina++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		          </div>
                  </div>
                  
                  <div class="col-md-4">
                  <label>Detalles</label>
                  <table class="table table-hover">
                   <thead>
                   <th>Postura</th>
                   <th>MSI</th>
                   <th>MSD</th>
                   </thead>
                   <tbody>
                   <tr v-for="detalle in detalles">
                    <td>@{{detalle.postura}}</td>
                    <td>@{{detalle.MSD}}</td>
                    <td>@{{detalle.MSI}}</td>
                    </tr>
                   </tbody>
                  </table>
                  
                  </div>
                  
              </div>

    </div>

    <div class="modal-footer">
       <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
    </div>
  </div>
  </div>
  </div>`,
    data: function() {
        return {
            busquedaTpresionart: "",
            tomasPresionT: [],
            numPaginas: 1,
            porPagina: 5,
            pagina: 1,
            detalles: []
        }
    },

    methods: {
        obtener_tomas_presion: function() {
            let self = this;
            axios.get('/obtenerTPresionArterial',{params:{paciente:document.getElementById('input_cedula').value, page: this.pagina}}).then(respuesta => {
                self.tomasPresionT = respuesta.data.tpresion_arteriales.data;
                self.numPaginas =  Math.ceil(respuesta.data.ntpresion_arteriales/self.porPagina);
                
            });
        },

        detalles_afinamiento: function(id) {
            let self = this;
            console.log("id:", id);
            axios.get("{{ route('get_afinamientos_reg') }}", {params:{id: id}}).then(respuesta => {
                self.detalles = respuesta.data.afinamientos;

                console.log(respuesta.data.afinamientos);
            });
        }
    }
});

/*Vue.component('actualizar-datos', {
    template: `<div class="modal fade" id="modActualizarDatos" tabindex="-1" role="dialog"> 
    <div class="modal-dialog" role="document">
    <div class="modal-content">
     <div class="modal-header">
       <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
       <h4 class="modal-title">Actualizacion de datos</h4>
     </div>
     <div class="modal-body">
     <div class="row">
        <div class="col-md-12">
        <div style="display: flex; justify-content:center;">
        <label>Seleccione un equipo médico</label>
        </div>
        </div>     
     </div>  

     <div class="row">
        <div class="col-md-12">
        <table class="table table-hover">
          <thead>
          <th></th>
          <th>Nombre</th>
          <th>Modelo</th>
          <th>Tipo</th>
          </thead>
          <tbody>
          <tr v-if="equipos_medicos.length == 0">
            <td colspan="4">No hay datos...</td>
          </tr>
           <tr v-else v-for="equipo_medico in equipos_medicos">
           <td><input type="radio" name="id_equipo_medico" @click="seleccion_equipo_medico(equipo_medico.id)"></td>
           <td>@{{equipo_medico.nombre}}</td>
           <td>@{{equipo_medico.modelo}}</td>
           <td>@{{equipo_medico.tipo}}</td>
           </tr>
          </tbody>
        </table>
        </div> 
     </div>

     </div>
 
     <div class="modal-footer">
        <label style="color:red;">*Debe seleccionar un equipo médico.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" :disabled="seleccion_equipo_m_vacio" v-on:click="$emit('agregar_glucometria2')">Siguiente</button>
     </div>
   </div>
   </div>
   </div>`,
   data: function(){
    return {
        equipos_medicos: [],
        numeroDePaginas: 1,
        porPagina: 5,
        paginaEqMedico: 1,
        busquedaEqMedico: "",
        equipoMedicoSeleccionado: ""
    }
   },

   methods: {
    obtener_equipos_medicos: function() {
        let self = this;
        console.log("se ejecutó");
        axios.get('/obtenerEqMedicos',{params:{palabra:this.busquedaEqMedico, page: this.paginaEqMedico}}).then(function(response){
            console.log("Equipos medicos:", response.data.equipos_medicos);
            console.log("numero Equipos medicos:", response.data.nequipos_medicos);
            self.equipos_medicos =  response.data.equipos_medicos.data;
            self.numeroDePaginas= Math.ceil(response.data.nequipos_medicos/self.porPagina);
        });
    },

    seleccion_equipo_medico: function(equipo_medico) {
        this.equipoMedicoSeleccionado = equipo_medico;
    }

   },

   computed: {
      seleccion_equipo_m_vacio: function() {
        return (this.equipoMedicoSeleccionado.length == 0) ? true: false;
      }
   }
});*/

var app = new Vue({
    el: "#contenedor_vue",
    data: {
        identificacion_paciente: document.getElementById('input_cedula').value,
        glucometrias:[],
        tomas_pre_arterial: [],
        entregas_medicamento: [],
        equipos_medicos:[],
        //Datos para tabla medicamentos
        medicamentos:[],
        medicamentos_seleccionados: [],
        busquedaMedicamento: "",
        paginaMedicamento: 1,
        numPaginasMedic: 0,
        porPaginaMed: 5
    },

    methods: {
        agregar_glucometria1: function () {
            $('#modAgregarGlucometria1').modal('show');
            this.$refs.glucometria1.obtener_equipos_medicos();
        },

        agregar_glucometria2: function (){
            $('#modAgregarGlucometria1').modal('hide');
            $('#modAgregarGlucometria2').modal('show');
        },

        agregar_toma_p_arterial: function () {
            $('#modAgregarTPresionArt1').modal('show');
            this.$refs.tpresion1.obtener_equipos_medicos();
        },
        agregar_toma_p_arterial2: function() {
            $('#modAgregarTPresionArt1').modal('hide');
            $('#modAgregarTPresionArt2').modal('show');
        },

        agregar_entrega_medicamentos: function (){
            this.obtener_medicamentos();
            $('#modAgregarMedicamentos').modal('show');
        },

        listar_entrega_medicamentos: function () {
            $('#modListaEntregaMedicamentos').modal('show');
            this.$refs.listaEntregaMed.obtener_entregas_medicamento();

        },

        listar_glucometrias: function() {
            this.$refs.listaGlucometrias.obtener_glucometrias();
            $('#modListaGlucometrias').modal('show');
        },

        listar_tpresion_arterial: function(){
            this.$refs.listaTPresionArt.obtener_tomas_presion();
            $('#modListaTPresionArt').modal('show');
        },

        obtener_medicamentos: function() {
            console.log("Se ejecuto obtener_medicamentos");
            let self = this;
            axios.get('/obtenerMedicamentos',{params:{palabra:this.busquedaMedicamento, page: this.paginaMedicamento}}).then(function(response){
                console.log("Medicamentos:", response.data.medicamentos.data);
                console.log("Numero Medicamentos:", response.data.nmedicamentos);
                self.medicamentos =  response.data.medicamentos.data;
                self.numPaginasMedic = Math.ceil(response.data.nmedicamentos/self.porPaginaMed);
            });
        },

        agregar_medicamento: function(medicamento) {
            console.log("intentanto agregar medicamento");
            if(this.medicamentos_seleccionados.filter(dato => dato.id == medicamento[0]).length == 0 && this.medicamentos_seleccionados.length < 5){
                this.medicamentos_seleccionados.push({id: medicamento[0], nombre:medicamento[1], cantidad: ""});
            } else if(this.medicamentos_seleccionados.length == 0){
                this.medicamentos_seleccionados.push({id: medicamento[0], nombre:medicamento[1], cantidad: ""});
            }
        },

        //Metodo para registrar nueva entrega de medicamento
        registrar_entrega_medicamento: function(){
            let fecha_hoy = new Date();
            fecha_hoy = fecha_hoy.getFullYear()+'/'+(fecha_hoy.getMonth()+1)+'/'+fecha_hoy.getDate();
            axios.post('/create_entrega_medicamento', {numero_de_identificacion: this.identificacion_paciente, fecha_de_entrega: fecha_hoy, medicamentos: this.medicamentos_seleccionados})
            .then(function(response){
              console.log(response);
              if(response.data.respuesta == "error_existencia"){
                Swal.fire("¡Error!",
	      				"Por favor validar la cantidad a descontar de los medicamentos",
	      				"error");
              }else{
              $('#modAgregarMedicamentos').modal('hide');
              Swal.fire("¡Operación exitosa!",
	      				"Se ha registrado exitosamente la entrega de medicamento",
                          "success");
              }
            })
            .catch(function(error){
              Swal.fire("¡Error!",
	      				"Hubo un error al intentar realizar la operación.",
	      				"error");
              console.log(error);
            });
        },
        
        //Metodo para registrar glucometria
        registrar_glucometria: function() {
            let equipo_medico_seleccionado = this.$refs.glucometria1.equipoMedicoSeleccionado;
            let glucometria_detalles = this.$refs.glucometria2.glucometria;
            let fecha_hoy = new Date();
            fecha_hoy = fecha_hoy.getFullYear()+'/'+(fecha_hoy.getMonth()+1)+'/'+fecha_hoy.getDate();
            axios.post('/create_glucometria', {numero_de_identificacion: this.identificacion_paciente,equipo_medico: equipo_medico_seleccionado, fecha: fecha_hoy, glucometria: glucometria_detalles}).then(function(respuesta) {
                console.log(respuesta);
                $('#modAgregarGlucometria2').modal('hide');
                Swal.fire("¡Operación exitosa!",
	      				"Se ha registrado exitosamente la glucometria",
	      				"success");
            })
            .catch(function(error){
                $('#modAgregarGlucometria2').modal('hide');
                Swal.fire("¡Error!",
	      				"Hubo un error al intentar realizar la operación.",
	      				"success");
                console.log(error);
            });
        },

        //Método para registrar una toma de presión arterial
        registrar_toma_presion_arterial: function() {
            let equipo_medico_seleccionado = this.$refs.tpresion1.equipoMedicoSeleccionado;
            let tpresion_arterial_detalles = this.$refs.tpresion2.toma_presion;
            let tipo_toma = this.$refs.tpresion2.seleccion_tipo;
            let fecha_hoy = new Date();
            fecha_hoy = fecha_hoy.getFullYear()+'/'+(fecha_hoy.getMonth()+1)+'/'+fecha_hoy.getDate();
            axios.post('/create_tpresion_arterial', {numero_de_identificacion: this.identificacion_paciente, equipo_medico: equipo_medico_seleccionado, fecha: fecha_hoy, tipo_toma_presion: tipo_toma, resultado: tpresion_arterial_detalles})
            .then(function(respuesta){
                console.log(respuesta);
                $('#modAgregarTPresionArt2').modal('hide');
                Swal.fire("¡Operación exitosa!",
	      				"Se ha registrado exitosamente la toma de presión arterial",
	      				"success");
            })
            .catch(function(error){
                $('#modAgregarTPresionArt2').modal('hide');
                Swal.fire("¡Error!",
	      				"Hubo un error al intentar realizar la operación.",
	      				"success");
                console.log(error);
            });  

        },

        quitar_medicamento: function(medicamento) {
            this.medicamentos_seleccionados = this.medicamentos_seleccionados.filter(dato => dato.id !== medicamento[0]);
        }
    },

    watch:{
		busquedaMedicamento: function(){
			this.paginaMedicamento = 1;
			this.obtener_medicamentos();
		},
		paginaMedicamento: function() {
			this.obtener_medicamentos();
		}

	},

});
</script>

@endsection
