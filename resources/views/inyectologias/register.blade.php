@extends('base.app')

@section('migas')
    <h1>Inyectología</h1>
    <ol class="breadcrumb">
        <ol class="breadcrumb">
            <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li>Atención de Enfermería</li>
            <li>Inyectología</li>
            <li class="active">Registro de Inyectología</li>
        </ol>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Registro de Inyectologia</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" id="formulario" action="{{ route('create_inyectologia') }}" method="POST" data-entrada="">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if (session()->has('error_existencia') and  session('error_existencia'))
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> Error al ingresar, verifique que no se está tratando de
                                descontar para un medicamento un valor mayor al de su existencia.
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body" id="nofountId" style="display: none">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>Importante!</strong> El Número de identificación, No esta registrado en la Base de
                            Datos, por favor, primero registra el paciente en
                            <a href="{{ route('new_paciente')}}" class="btn btn-link">Aqui</a>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha de registro:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="fecha"
                                       name="fecha" value="{{ date('m/d/Y')}}">
                            </div>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Número de identificación:</label>
                            @if (isset($numero_de_identificacion))
                                <input type="text" class="form-control" id="numero_de_identificacion"
                                       name="numero_de_identificacion" value="{{ $numero_de_identificacion }}"
                                       readonly>
                                <input type="hidden" name="input_cedula" value="{{ $numero_de_identificacion }}">
                            @else
                                <input type="text" class="form-control" id="numero_de_identificacion"
                                       name="numero_de_identificacion" placeholder="Cédula, Tarjeta de identidad">
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombre</label>
                            <input type="text" class="form-control" id="nombres" name="nombre" placeholder="Nombres"
                                   value="{{ old('nombres') }}">
                        </div>
                        <div class="form-group">
                            <label>Apellido</label>
                            <input type="text" class="form-control" id="apellidos" name="apellido"
                                   placeholder="Apellidos" value="{{ old('apellidos') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Medicamento:</label>
                            <div class="form-inline">
                                <div class="row">
                                    <div class="col-xs-9">
                                        <input type="hidden" id="medicamento" name="medicamento_inyectologia">
                                        <input type="text" class="form-control" style="width: 100%" id="medicamentoName"
                                               name="medicamento_inyectologia_name"
                                               placeholder="Medicamento administrado">
                                    </div>
                                    <div class="col-xs-3">
                                        <button id="seleccionarMedIny" type="button" class="btn btn-warning pull-right">
                                            <i class="fa fa-hand-pointer-o" aria-hidden="true"></i>
                                            Seleccionar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Orden Médica</label>
                            <input type="text" class="form-control" id="orden_medica" name="orden_medica_iny"
                                   placeholder="Orden medica">
                        </div>
                    </div>

                    <div class="col-md-12 box-header with-border">
                        <div class="row">
                            <div class="col-xs-6">
                                <h3 class="box-title" style="padding-top: 10px">Insumos utilizados</h3>
                            </div>
                            <div class="col-xs-6">
                                <button id="add_insumo_iny" type="button" class="btn btn-link  pull-right"><i
                                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                                </button>
                                <button id="remove_insumo_iny" type="button"
                                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                                        aria-hidden="true"></i> Quitar
                                </button>
                            </div>
                        </div>
                    </div>

                    <div class="insumos_usados_iny"></div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        @if (isset($numero_de_identificacion))
                            <button class="btn btn-primary pull-right margin-r-5" type="submit" formaction="{{ route('val_cedula') }}">
                                <i class="fa fa-ban" aria-hidden="true"></i> Cancelar
                            </button>
                        @else
                            <a class="btn btn-primary pull-right margin-r-5"
                               href="{{ route('view_inyectologias') }}">
                                <i class="fa fa-ban" aria-hidden="true"></i>
                                Cancelar
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modalInsumoMedico" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Selección de Insumo médico</h4>
                </div>
                <div class="modal-body">
                    <table id="lista_insumos_medicos" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Marca</th>
                            <th>Existencia</th>
                            <th>Fecha de vencimiento</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalMedicamento" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Selección de Medicamento</h4>
                </div>
                <div class="modal-body">
                    <table id="lista_medicamentos" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Laboratorio</th>
                            <th>Existencia</th>
                            <th>Presentacion</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        var added_medicamentos = new Array();

        $(document).ready(function () {

            //Evento para mostrar el nombre y apellido del paciente con su CC ya establecido
            if ($('#numero_de_identificacion').val() > 0) {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        console.log("consulta hecha " + json);
                        $('#nombres').val(json.nombres);
                        $('#apellidos').val(json.apellidos);
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            }

            //Date picker
            $('#fecha').datepicker({
                startDate: '-285d',
                endDate: '0d',
                autoclose: true
            });

            //Evento para mostrar el nombre y apellido del paciente al escribir su CC
            $('#numero_de_identificacion').blur(function () {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        if(json.nombres == '-' && json.apellidos == '-'){
                            $("#nofountId").show();
                            $('#numero_de_identificacion').val('');
                            $('#nombres').val('');
                            $('#apellidos').val('');
                        } else {
                            $('#nombres').val(json.nombres);
                            $('#apellidos').val(json.apellidos);
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            });

            //Funcion para añadir insumo medico en formulario inyectologia
            var numero_actual_campos_insumos_iny = 0;
            $('#add_insumo_iny').on('click', function () {

                if (numero_actual_campos_insumos_iny < 5)
                {
                    $('.insumos_usados_iny').append('<div class="row">'
                        + '<div class="col-md-6 margin-bottom">'
                        + '<div class="input-group" data-numero="' + numero_actual_campos_insumos_iny + '">'
                        + '<span class="input-group-addon">Descripción</span>'
                        + '<input type="hidden" class="form-control" id="insumo_usado_iny' + numero_actual_campos_insumos_iny + '" name="insumos_medicos[]" required>'
                        + '<input type="text" class="form-control readonly" id="insumo_usado_iny_name' + numero_actual_campos_insumos_iny + '" required>'
                        + '<span class="input-group-btn">'
                        + '<button id="seleccionarInsumoMedico' + numero_actual_campos_insumos_iny + '" type="button" class="btn btn-warning"><i class="fa fa-search-plus" aria-hidden="true"></i></button>'
                        + '</span>'
                        + '</div>'
                        + '</div>'

                        + '<div class="col-md-6 margin-bottom">'
                        + '<div class="input-group">'
                        + '<span class="input-group-addon">Cantidad</span>'
                        + '<input type="number" class="form-control" name="cantidad_im_iny[]" min="1" required>'
                        + '</div>'
                        + '</div>'
                        + '</div>'
                    );

                    $(".readonly").keydown(function(e){
                        e.preventDefault();
                    });

                    //Funcion de evento para mostrar ventana para seleccionar un insumo médico
                    $('#seleccionarInsumoMedico' + numero_actual_campos_insumos_iny).on('click', function () {

                        $('#formulario').attr("data-entrada", $(this).parents("div").attr("data-numero"));
                        if (added_medicamentos.length != 0) { //deshabilitar botones
                            for (var i = 0; i < added_medicamentos.length; i++) {
                                $('#' + added_medicamentos[i]).prop('disabled', true);
                            }
                        }
                        $('#modalInsumoMedico').modal("show"); //Muestra el cuadro para seleccionar
                        $('#lista_insumos_medicos').on('click', 'tbody tr td button', function () {

                            if (added_medicamentos.length != 0) { //Habilitar botones
                                for (var i = 0; i < added_medicamentos.length; i++) {
                                    if (added_medicamentos[i] == $('#insumo_usado_iny' + $('#formulario').attr("data-entrada")).val()) {
                                        $('#' + added_medicamentos[i]).prop('disabled', false);
                                        added_medicamentos.splice(i, 1);
                                    }
                                }
                            }

                            $('#insumo_usado_iny' + $('#formulario').attr("data-entrada")).val($(this).parent().children('p').text()); //Cambia el valor
                            $('#insumo_usado_iny_name' + $('#formulario').attr("data-entrada")).val($(this).parent().children('label').text()); //Pone el nombre
                            added_medicamentos.push($(this).parent().children('p').text());
                            $('#modalInsumoMedico').modal("hide");//Ocultar el cuadro para seleccionar
                        });
                    });

                    numero_actual_campos_insumos_iny++;
                }
            });

            //Funcion para quitar un registro de insumo medico en atencion en salud
            $('#remove_insumo_iny').on('click', function () {
                $('div.insumos_usados_iny > div').last().remove();
                numero_actual_campos_insumos_iny--;
            });

            $('#lista_insumos_medicos').DataTable({
                "drawCallback": function( settings ) { // Dibujar la tabla despues de haber seleccionado un medicamento con los medicamentos seleccionados deshabilitados
              if(added_medicamentos.length != 0){
              for(var i = 0; i < added_medicamentos.length; i++){
              $('#'+added_medicamentos[i]).prop('disabled', true);
              }
              }
              },
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "M",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_insumos_medicos_ajax_iny") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            //Evento que hace que se muestre la ventana para seleccionar un medicamento
            $('#seleccionarMedIny').on('click', function () {
                $('#modalMedicamento').modal("show"); //Muestra el cuadro para seleccionar
                $('#lista_medicamentos').on('click', 'tbody tr td button', function () {
                    $('#medicamento').val($(this).parent().children('p').text()); //Cambia el valor
                    $('#medicamentoName').val($(this).parent().children('label').text()); //Cambia el nombre
                    $('#modalMedicamento').modal("hide");//Ocultar el cuadro para seleccionar
                });
            });

            $('#lista_medicamentos').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "M",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_entrega_medicamentos_ajax") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });
        });
    </script>
@endsection
