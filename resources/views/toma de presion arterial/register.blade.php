@extends('base.app')

@section('migas')
    <h1>Toma de Presión Arterial</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Atención de Enfermería</li>
        <li>Toma de Presión</li>
        <li class="active">Registrar Toma de Presión Arterial</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Registro Toma de Presión Arterial</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_tpresion_arterial') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if (session()->has('error_existencia') and  session('error_existencia'))
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> Error al ingresar, verifique que no se está tratando de
                                descontar para un medicamento un valor mayor al de su existencia.
                            </div>
                        </div>
                    </div>
                @endif

                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body" id="nofountId" style="display: none">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>Importante!</strong> El Número de identificación, No esta registrado en la Base de
                            Datos, por favor, primero registra el paciente en
                            <a href="{{ route('new_paciente')}}" class="btn btn-link">Aqui</a>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha</label>
                            <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="fecha" name="fecha"
                                       value="{{ date('m/d/Y')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación:</label>
                            @if (isset($numero_de_identificacion))
                                <input type="text" class="form-control" id="numero_de_identificacion"
                                       name="numero_de_identificacion" value="{{ $numero_de_identificacion }}"
                                       readonly>
                                <input type="hidden" name="input_cedula" value="{{ $numero_de_identificacion }}">
                            @else
                                <input type="text" class="form-control" id="numero_de_identificacion"
                                       name="numero_de_identificacion" placeholder="Cédula, Tarjeta de identidad"
                                       value="{{ old('numero_de_identificacion') }}" required>
                            @endif
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombres</label>
                            <input type="text" class="form-control" id="nombres" name="nombre" placeholder="Nombres"
                                   value="{{ old('nombre') }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" id="apellidos" name="apellido"
                                   placeholder="Apellidos" value="{{ old('apellido') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Equipo médico:</label>
                            <div class="form-inline">
                                <div class="row">
                                    <div class="col-xs-9 col-md-9">
                                        <input type="hidden" id="equipo_medico" name="equipo_medico_as">
                                        <input type="text" class="form-control" style="width:100%;"
                                               id="equipo_medico_name"
                                               name="equipo_medico_name" placeholder="Equipo medico usado">
                                    </div>
                                    <div class="col-xs-3 col-md-3">
                                        <button id="seleccionarEqu" type="button" class="btn btn-warning pull-right"><i
                                                    class="fa fa-hand-pointer-o" aria-hidden="true"></i>
                                            Seleccionar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Resultado:</label>
                            <input type="text" class="form-control" id="resultado" name="resultado"
                                   placeholder="Resultados">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Tipo:</label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="normal_tp" name="tipo_toma_presion" value="Normal">
                                Normal
                            </label>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <label class="radio-inline">
                                <input type="radio" id="afinamiento_tp" name="tipo_toma_presion" value="Afinamiento">
                                Afinamiento
                            </label>
                        </div>
                    </div>

                    <div class="col-md-6" id="seccion_posicion_tp">
                        <div class="form-group">
                            <label>Posición:</label>
                            <input type="text" class="form-control" id="posicion" name="posicion_tp"
                                   placeholder="Posicion">
                        </div>
                    </div>

                    <div class="col-md-12" id="seccion_afinamiento_tp">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="de_pie_tp" value="De pie" name="afinamientos[]">
                                        De pie
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" id="msd_dp" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" id="msi_dp" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="sentado_tp" value="Sentado" name="afinamientos[]">
                                        Sentado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" id="msd_se" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" id="msi_se" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label class="checkbox-inline">
                                        <input type="checkbox" id="acostado_tp" value="Acostado" name="afinamientos[]">
                                        Acostado
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSD</span>
                                    <input type="text" class="form-control" id="msd_ac" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <div class="input-group">
                                    <span class="input-group-addon">MSI&nbsp;&nbsp;</span>
                                    <input type="text" class="form-control" id="msi_ac" name="">
                                    <span class="input-group-addon">(mmHg)</span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        @if (isset($numero_de_identificacion))
                            <button class="btn btn-primary pull-right margin-r-5" formaction="{{ route('val_cedula') }}">
                                <i class="fa fa-ban" aria-hidden="true"></i> Cancelar
                            </button>
                        @else
                            <a class="btn btn-primary pull-right margin-r-5"
                               href="{{ route('view_tpresion_arteriales') }}">
                                <i class="fa fa-ban" aria-hidden="true"></i>
                                Cancelar
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="modal fade" id="modalEquipoMedico" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Listado de Equipo Médico</h4>
                </div>
                <div class="modal-body">
                    <table id="lista_equipos_medicos" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Modelo</th>
                            <th>Tipo</th>
                            <th>Acciones</th>
                        </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">

        $(document).ready(function () {

            //Date picker
            $('#fecha').datepicker({
                startDate: '-285d',
                endDate: '0d',
                autoclose: true
            });

            //Evento para mostrar el nombre y apellido del paciente al escribir su CC
            $('#numero_de_identificacion').blur(function () {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        if (json.nombres == '-' && json.apellidos == '-') {
                            $("#nofountId").show();
                            $('#numero_de_identificacion').val('');
                            $('#nombres').val('');
                            $('#apellidos').val('');
                        } else {
                            $('#nombres').val(json.nombres);
                            $('#apellidos').val(json.apellidos);
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });

            });

            $('#numero_de_identificacion').focus(function () {
                $("#nofountId").hide();
            });

            //Evento para mostrar el nombre y apellido del paciente con su CC ya establecido
            if ($('#numero_de_identificacion').val() > 0) {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        console.log("consulta hecha " + json);
                        $('#nombres').val(json.nombres);
                        $('#apellidos').val(json.apellidos);
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            }

            //Funcion para mostrar campo posicion cuando se elige toma de presion arterial tipo normal
            $('#seccion_posicion_tp').hide();
            $('#seccion_afinamiento_tp').hide();

            $('#normal_tp').change(function () {
                $('#seccion_posicion_tp').show();
                $('#seccion_afinamiento_tp').hide();
            });

            //Funcion para mostrar campos afinamiento cuando se elige toma de presion arterial tipo afinamiento
            $('#afinamiento_tp').change(function () {
                $('#seccion_afinamiento_tp').show();
                $('#seccion_posicion_tp').hide();
            });

            //Funcion para mostrar u ocultar campos enseguida de campo de pie
            $('#msd_dp').hide();
            $('#msi_dp').hide();
            $('#de_pie_tp').change(function () {
                if ($(this).prop('checked')) {
                    $('#msd_dp').show();
                    $('#msi_dp').show();
                    $('#msd_dp').attr("name", "msd_tp[]");
                    $('#msi_dp').attr("name", "msi_tp[]");
                } else {
                    $('#msd_dp').hide();
                    $('#msi_dp').hide();
                    $('#msd_dp').attr("name", "");
                    $('#msi_dp').attr("name", "");
                }
            });

            //Funcion para mostrar u ocultar campos enseguida de campo sentado
            $('#msd_se').hide();
            $('#msi_se').hide();
            $('#sentado_tp').change(function () {
                if ($(this).prop('checked')) {
                    $('#msd_se').show();
                    $('#msi_se').show();
                    $('#msd_se').attr("name", "msd_tp[]");
                    $('#msi_se').attr("name", "msi_tp[]");
                } else {
                    $('#msd_se').hide();
                    $('#msi_se').hide();
                    $('#msd_se').attr("name", "");
                    $('#msi_se').attr("name", "");
                }
            });

            //Funcion para mostrar u ocultar campos enseguida de campo acostado
            $('#msd_ac').hide();
            $('#msi_ac').hide();
            $('#acostado_tp').change(function () {
                if ($(this).prop('checked')) {
                    $('#msd_ac').show();
                    $('#msi_ac').show();
                    $('#msd_ac').attr("name", "msd_tp[]");
                    $('#msi_ac').attr("name", "msi_tp[]");
                } else {
                    $('#msd_ac').hide();
                    $('#msi_ac').hide();
                    $('#msd_ac').attr("name", "");
                    $('#msi_ac').attr("name", "");
                }
            });

            //Evento que hace que se muestre la ventana para seleccionar un equipo medico
            $('#seleccionarEqu').on('click', function () {
                $('#modalEquipoMedico').modal("show"); //Muestra el cuadro para seleccionar
                $('#lista_equipos_medicos').on('click', 'tbody tr td button', function () {
                    $('#equipo_medico').val($(this).parent().children('p').text()); //Cambia el valor
                    $('#equipo_medico_name').val($(this).parent().children('label').text()); //Cambia el nombre
                    $('#modalEquipoMedico').modal("hide");//Ocultar el cuadro para seleccionar
                });
            });

            $('#lista_equipos_medicos').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_equipos_medicos_ajax_gl") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });
        });
    </script>
@endsection