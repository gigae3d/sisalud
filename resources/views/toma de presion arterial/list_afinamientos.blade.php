@extends('base.app')

@section('migas')
    <h1>Toma de Presión Arterial</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Atención de Enfermería</li>
        <li>Toma de Presión</li>
        <li class="active">Lista de Afinamientos</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Afinamientos para el
                    Paciente</h3>
            </div>
            <div class="box-body">
                <table id="lista_afinamientos" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Equipo médico</th>
                        <th>Usuario</th>
                        <th>Fecha</th>
                        <th>Tipo</th>
                        <th>Resultado</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    @if (!$tpresion_arterials->isEmpty())
                        @foreach ($tpresion_arterials as $registro)
                            @php
                                $i = 0;
                            @endphp
                            <tr>
                                <td>{{ $registro->id }}</td>
                                <td>{{ $registro->equipo_medico }}</td>
                                <td>{{ $registro->usuario }}</td>
                                <td>{{ $registro->fecha }}</td>
                                <td>{{ $registro->tipo }}</td>
                                <td>{{ $registro->resultado }}</td>
                                <td>{!! $options[$i] !!}</td>
                            </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                    @else
                        <tr>
                            <td colspan="7" style="text-align: center;"><label>No se encontraron resultados</label></td>
                        </tr>
                    @endif

                    </tbody>
                </table>
                {{ $tpresion_arterials->links() }}
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalAfinamiento" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                                aria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Información Adicional</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Afinamientos</h4>
                            <table id="lista_entrega_medicamentos" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>Postura</th>
                                    <th>MSD</th>
                                    <th>MSI</th>
                                </tr>
                                </thead>
                                <tbody class="afinamientos">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script type="text/javascript">

        $(document).ready(function () {
            //Mostrar informacion adicional de un medicamento
            $('#lista_afinamientos').on('click', 'tbody tr td div button', function () {
                $('.afinamientos').html('');
                var codigo = $(this).parent().children('p').text();//Obtener el id del registro de afinamiento

                $.ajax({
                    method: "GET",
                    url: "{{ route('get_afinamientos_reg') }}",
                    data: {id: codigo},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        var i;
                        $('.afinamientos').empty();
                        for (i = 0; i < json.afinamientos.length; i++) {
                            $('.afinamientos').append('<tr role="row" class="odd">'
                                + '<td>' + json.afinamientos[i].postura + '</td>'
                                + '<td>' + json.afinamientos[i].MSD + '</td>'
                                + '<td>' + json.afinamientos[i].MSI + '</td>'
                                + '</tr>'
                            );
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
                $('#modalAfinamiento').modal("show");
            });
        });
    </script>
@endsection