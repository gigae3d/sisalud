<div id="columna_vertebral" class="box-body tab-pane fade">

	          <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Inspección</h3>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_IN" name="selTodosIN" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Anormal_IN" name="selTodosIN" value="">
                        
                        </label>
                        </div>

              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Alineación en vista lateral</label>
                        <input type="hidden" name="inp[0]" value="Alineacion en vistal lateral">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_IN" name="inv[0]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_IN" name="inv[0]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="inhl[0]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Cifosis</label>
                        <input type="hidden" name="inp[1]" value="Cifosis">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_IN" name="inv[1]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_IN" name="inv[1]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="inhl[1]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Lordosis</label>
                        <input type="hidden" name="inp[2]" value="Lordosis">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_IN" name="inv[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_IN" name="inv[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="inhl[2]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Escoliosis</label>
                        <input type="hidden" name="inp[3]" value="Escoliosis">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_IN" name="inv[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_IN" name="inv[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="inhl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Disbalance muscular</label>
                        <input type="hidden" name="inp[4]" value="Disbalance muscular">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_IN" name="inv[4]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_IN" name="inv[4]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="inhl[4]">
                    </div>    
                </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Palpación</h3>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_PA" name="selTodosPA" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Anormal_PA" name="selTodosPA" value="">
                        
                        </label>
                        </div>

              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Espasmo muscular</label>
                        <input type="hidden" name="pap[0]" value="Espasmo muscular">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PA" name="pav[0]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_PA" name="pav[0]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="pahl[0]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolor en región dorsal</label>
                        <input type="hidden" name="pap[1]" value="Dolor en region dorsal">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PA" name="pav[1]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_PA" name="pav[1]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="pahl[1]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolor en región lumbar</label>
                        <input type="hidden" name="pap[2]" value="Dolor en region lumbar">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PA" name="pav[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_PA" name="pav[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="pahl[2]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolor irradiado a M. Inf.</label>
                        <input type="hidden" name="pap[3]" value="Dolor irradiado a M Inf">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PA" name="pav[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_PA" name="pav[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="pahl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Musculos paravertebrales</label>
                        <input type="hidden" name="pap[4]" value="Musculos paravertebrales">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PA" name="pav[4]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_PA" name="pav[4]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="pahl[4]">
                    </div>    
                </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Movilidad</h3>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_MV" name="selTodosMV" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Anormal_MV" name="selTodosMV" value="">
                        
                        </label>
                        </div>

              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Flexion</label>
                        <input type="hidden" name="mvp[0]" value="Flexion">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_MV" name="mvv[0]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_MV" name="mvv[0]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="mvhl[0]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Extensión</label>
                        <input type="hidden" name="mvp[1]" value="Extensión">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_MV" name="mvv[1]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_MV" name="mvv[1]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="mvhl[1]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Inclinaciones laterales</label>
                        <input type="hidden" name="mvp[2]" value="Inclinaciones laterales">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_MV" name="mvv[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_MV" name="mvv[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="mvhl[2]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Rotación</label>
                        <input type="hidden" name="mvp[3]" value="Rotación">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_MV" name="mvv[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_MV" name="mvv[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="mvhl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Test de Wells(flexibilidad)</label>
                        <input type="hidden" name="mvp[4]" value="Test de Wells">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_MV" name="mvv[4]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_MV" name="mvv[4]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="mvhl[4]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Test de Shober(flexibilidad)</label>
                        <input type="hidden" name="mvp[5]" value="Test de Shober">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_MV" name="mvv[5]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_MV" name="mvv[5]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="mvhl[5]">
                    </div>    
                </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Nuca y miembro superior</h3>
              </div>
              
              <div class="row">
              <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-size:19px; font-style: italic; text-decoration: underline;">Nuca</label>
                    </div>
              </div>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_NYS" name="selTodosNYS" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Anormal_NYS" name="selTodosNYS" value="">
                        
                        </label>
                        </div>

              </div>
              
              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Desviación(Cifosis)</label>
                        <input type="hidden" name="nsp[0]" value="Desviación(Cifosis)">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="nsv[0]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="nsv[0]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="nshl[0]">
                    </div>    
              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Contracción muscular</label>
                        <input type="hidden" name="nsp[1]" value="Contraccion muscular">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="nsv[1]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="nsv[1]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="nshl[1]">
                    </div>    
              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolor localizado</label>
                        <input type="hidden" name="nsp[2]" value="Dolor localizado">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="nsv[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="nsv[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="nshl[2]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolor irradiado a M. Sup.</label>
                        <input type="hidden" name="nsp[3]" value="Dolor irradiado a M Sup">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="nsv[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="nsv[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="nshl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Limitación de la movilidad</label>
                         <input type="hidden" name="nsp[4]" value="Limitacion de la movilidad">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="nsv[4]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="nsv[4]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="nshl[4]">
                    </div>    
                </div>
              
              <div class="row">
              <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-size:19px; font-style: italic; text-decoration: underline;">Hombros</label>
                    </div>
              </div>
              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Hombros al mismo nivel</label>
                         <input type="hidden" name="hmp[0]" value="Hombros al mismo nivel">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="hmv[0]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="hmv[0]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="hmhl[0]">
                    </div>    
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolor</label>
                        <input type="hidden" name="hmp[1]" value="Dolor">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="hmv[1]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="hmv[1]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="hmhl[1]">
                    </div>    
              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Fricción del manguito</label>
                        <input type="hidden" name="hmp[2]" value="Friccion del manguito">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="hmv[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="hmv[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="hmhl[2]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Limitación del movimiento</label>
                        <input type="hidden" name="hmp[3]" value="Limitación del movimiento">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="hmv[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="hmv[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="hmhl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Abducción+rotación</label>
                        <input type="hidden" name="hmp[4]" value="Abduccion+rotacion">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_NYS" name="hmv[4]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_NYS" name="hmv[4]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="hmhl[4]">
                    </div>    
                </div>

              <div class="row">
              <div class="col-md-6">
                    <div class="form-group form-inline">
                        <label style="font-size:19px; font-style: italic; text-decoration: underline;">Codos, </label>
                        <label style="font-size:19px; font-style: italic; text-decoration: underline;">Mano / Muñeca</label>
                    </div>
              </div>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_CMM" name="selTodosCMM" value="">
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Anormal_CMM" name="selTodosCMM" value="">
                        </label>
                        </div>

              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Edema</label>
                        <input type="hidden" name="cmmp[0]" value="Edema">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[0]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[0]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[0]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Cicatrices</label>
                        <input type="hidden" name="cmmp[1]" value="Cicatrices">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[1]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[1]" value="Anormal_CMM">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[1]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Quistes</label>
                        <input type="hidden" name="cmmp[2]" value="Quistes">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[2]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Atrofias</label>
                        <input type="hidden" name="cmmp[3]" value="Atrofias">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Desviaciones</label>
                        <input type="hidden" name="cmmp[4]" value="Desviaciones">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[4]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[4]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[4]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Gangliones</label>
                        <input type="hidden" name="cmmp[5]" value="Gangliones">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[5]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[5]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[5]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Codo de golfista</label>
                        <input type="hidden" name="cmmp[6]" value="Codo de golfista">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[6]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[6]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[6]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Callosidades</label>
                        <input type="hidden" name="cmmp[7]" value="Callosidades">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[7]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[7]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[7]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Codo de tenista</label>
                        <input type="hidden" name="cmmp[8]" value="Codo de tenista">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[8]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[8]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[8]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Tumefacción local</label>
                        <input type="hidden" name="cmmp[9]" value="Tumefaccion local">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[9]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[9]" value="Anormal_CMM">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[9]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Compresión del cubital</label>
                        <input type="hidden" name="cmmp[10]" value="Compresion del cubital">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[10]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[10]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[10]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Nudosidades palpables</label>
                        <input type="hidden" name="cmmp[11]" value="Nudosidades palpables">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[11]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[11]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[11]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Flexión/extensión del codo</label>
                        <input type="hidden" name="cmmp[12]" value="Flexion/extension del codo">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[12]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[12]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[12]">
                    </div>    
                </div>

                <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dolores localizados</label>
                        <input type="hidden" name="cmmp[13]" value="Dolores localizados">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[13]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[13]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[13]">
                    </div>    
                </div>

                    <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Movilidad</label>
                        <input type="hidden" name="cmmp[14]" value="Movilidad">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[14]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[14]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[14]">
                    </div>    
                </div>
              

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Resist a la ext de muñeca y dedos</label>
                        <input type="hidden" name="cmmp[15]" value="Resis a la ext de muñeca y dedos">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[15]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[15]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[15]">
                    </div>    
                </div>

              
                 <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Test de Phallen</label>
                        <input type="hidden" name="cmmp[16]" value="Test de Phallen">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[16]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[16]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[16]">
                    </div>    
                </div>

                 <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Test de Tinnel</label>
                        <input type="hidden" name="cmmp[17]" value="Test de Tinnel">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[17]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[17]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[17]">
                    </div>    
                </div>
              

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Signo del abanico o de pitres</label>
                        <input type="hidden" name="cmmp[18]" value="Signo del abanico o de pitres">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[18]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[18]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[18]">
                    </div>    
              </div>

              
              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Oponente del pulgar</label>
                        <input type="hidden" name="cmmp[19]" value="Oponente del pulgar">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[19]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[19]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[19]">
                    </div>    
              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Test de Finkelstein</label>
                        <input type="hidden" name="cmmp[20]" value="Test de Finkelstein">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_CMM" name="cmmv[20]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_CMM" name="cmmv[20]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="cmhl[20]">
                    </div>    
                </div>
              

              <div class="col-md-12">
                      <h3 class="box-title">Descripción y ampliación de hallazgos:
                      </h3> 
                    <div class="box-body pad">
                    <textarea id="deampl_CV" name="deampl_CV" rows="10" cols="80">
                            Descripcion
                        </textarea>
              </div>
              </div>

</div>