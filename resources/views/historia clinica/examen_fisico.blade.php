<div id="examen_fisico" class="box-body tab-pane fade">

            <div class="row">
		      <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Examen físico</h3>
              </div>
              </div>

              <div class="row">
              <div class="col-md-4">
		                <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">Constitución:</label>
	                    <input type="text" class="form-control" id="constitucion" name="constitucion">
                        </div>
              </div>
 
              <div class="col-md-5">
                        <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">Dominancia:</label>
                        </label>
                        <label class="radio-inline"> 
                        <input type="radio" name="dominancia" value="Diestro">
                        Diestro
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="dominancia" value="Zurdo">
                        Zurdo
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="dominancia" value="Ambidiestro">
                        Ambidiestro
                        </label>  
                        </div>
              </div>


              </div>

              <div class="row">
              <div class="col-md-6">
		                <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">T.A.:</label>
	                    <input type="text" class="form-control" id="t_a" name="t_a">
                        <label style="font-size:16px; font-style: italic;">mmHg</label>
                        </div>
              </div>

              <div class="col-md-6">
		                <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">F.C.:</label>
	                    <input type="text" class="form-control" id="frecuencia_cardiaca" name="frecuencia_cardiaca">
	                    <label style="font-size:16px; font-style: italic;">x MIN</label>
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-4">
                        <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">Peso:</label>
                        <input type="text" class="form-control" id="peso" name="peso">
                        <label style="font-size:16px; font-style: italic;">Kg</label>
                        </div>
              </div>

              <div class="col-md-4">
                        <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">Talla</label>
                        <input type="text" class="form-control" id="talla" name="talla">
                        <label style="font-size:16px; font-style: italic;">mts</label>
                        </div>
              </div>

              <div class="col-md-4">
                        <div class="form-group form-inline">
                        <label style="font-size:16px; font-style: italic;">IMC:</label>
                        <input type="text" class="form-control" id="imc" name="imc">
                        </div>
              </div>
              </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Examen Físico 2</h3>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_EF" name="selTodosMO" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Anormal_EF" name="selTodosMO" value="">
                        
                        </label>
                        </div>

              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Cabeza</label>
                        <input type="hidden" name="efp[0]" value="Cabeza">
                    </div>
                    
                    <div class="col-md-1">    
                    <label class="radio-inline"> 
                    <input type="radio" class="Normal_EF" name="efv[0]" value="Normal">
                    N
                    </label>
                    </div>
                    <div class="col-md-1">
                    <label class="radio-inline"> 
                    <input type="radio" class="Anormal_EF" name="efv[0]" value="Anormal">
                    AN
                    </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[0]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Pulmones</label>
                        <input type="hidden" name="efp[1]" value="Pulmones">
                    </div>
                    
                    <div class="col-md-1">    
                    <label class="radio-inline"> 
                    <input type="radio" class="Normal_EF" name="efv[1]" value="Normal">
                    N
                    </label>
                    </div>
                    <div class="col-md-1">
                    <label class="radio-inline"> 
                    <input type="radio" class="Anormal_EF" name="efv[1]" value="Anormal">
                    AN
                    </label>
                    </div>
                    <div class="col-md-4 form-inline">
                    <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                    <input type="text" class="form-control" name="efhl[1]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Ojos</label>
                        <input type="hidden" name="efp[2]" value="Ojos">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[2]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[2]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[2]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Cardiaco</label>
                        <input type="hidden" name="efp[3]" value="Cardiaco">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[3]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[3]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[3]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Conjuntiva</label>
                        <input type="hidden" name="efp[4]" value="Conjuntiva">
                    </div>
                    
                    <div class="col-md-1">    
                    <label class="radio-inline"> 
                    <input type="radio" class="Normal_EF" name="efv[4]" value="Normal">
                    N
                    </label>
                    </div>
                    <div class="col-md-1">
                    <label class="radio-inline"> 
                    <input type="radio" class="Anormal_EF" name="efv[4]" value="Anormal">
                    AN
                    </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[4]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Circulatorio</label>
                        <input type="hidden" name="efp[5]" value="Circulatorio">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[5]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[5]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[5]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Reflejo pupilar</label>
                        <input type="hidden" name="efp[6]" value="Reflejo pupilar">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[6]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[6]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[6]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Abdomen</label>
                        <input type="hidden" name="efp[7]" value="Abdomen">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[7]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[7]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[7]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Fondo de ojo</label>
                        <input type="hidden" name="efp[8]" value="Fondo de ojo">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[8]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[8]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[8]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Genitales exterior</label>
                        <input type="hidden" name="efp[9]" value="Genitales exterior">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[9]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[9]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[9]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Oidos</label>
                        <input type="hidden" name="efp[10]" value="Oidos">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[10]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[10]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[10]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Miembros inferiores</label>
                        <input type="hidden" name="efp[11]" value="Miembros inferiores">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[11]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">
                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[11]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[11]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Otoscopia</label>
                        <input type="hidden" name="efp[12]" value="Otoscopia">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[12]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">
                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[12]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[12]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Miembros superiores</label>
                        <input type="hidden" name="efp[13]" value="Miembros superiores">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[13]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[13]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[13]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Nariz</label>
                        <input type="hidden" name="efp[13]" value="Nariz">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[13]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[13]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[13]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Sistema nervioso</label>
                        <input type="hidden" name="efp[14]" value="Sistema nervioso">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[14]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[14]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[14]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Boca</label>
                        <input type="hidden" name="efp[15]" value="Boca">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[15]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[15]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[15]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Estado mental</label>
                        <input type="hidden" name="efp[16]" value="Estado mental">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[16]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[16]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[16]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Lengua</label>
                        <input type="hidden" name="efp[17]" value="Lengua">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[17]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[17]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[17]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Pares craneales</label>
                        <input type="hidden" name="efp[18]" value="Pares craneales">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[18]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[18]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[18]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Dentadura</label>
                        <input type="hidden" name="efp[19]" value="Dentadura">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[19]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[19]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[19]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Reflejos</label>
                        <input type="hidden" name="efp[20]" value="Reflejos">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[20]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[20]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[20]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Faringe</label>
                        <input type="hidden" name="efp[21]" value="Faringe">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[21]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[21]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[21]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Motilidad</label>
                        <input type="hidden" name="efp[22]" value="Motilidad">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[22]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[22]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[22]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Amigdalas</label>
                        <input type="hidden" name="efp[23]" value="Amigdalas">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[23]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[23]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[23]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Sensibilidad</label>
                        <input type="hidden" name="efp[24]" value="Sensibilidad">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[24]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[24]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[24]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Cuello</label>
                        <input type="hidden" name="efp[25]" value="Cuello">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[25]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[25]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[25]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Tono muscular</label>
                        <input type="hidden" name="efp[26]" value="Tono muscular">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[26]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[26]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[26]">
                    </div>    
                    </div>

                    <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Columna</label>
                        <input type="hidden" name="efp[27]" value="Columna">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[27]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[27]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[27]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Fuerza muscular</label>
                        <input type="hidden" name="efp[28]" value="Fuerza muscular">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[28]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[28]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[28]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Torax</label>
                        <input type="hidden" name="efp[29]" value="Torax">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[29]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[29]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[29]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Marcha</label>
                        <input type="hidden" name="efp[30]" value="Marcha">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[30]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[30]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[30]">
                    </div>    
                    </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Senos</label>
                        <input type="hidden" name="efp[31]" value="Senos">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[31]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[31]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[31]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Piel y faneras</label>
                        <input type="hidden" name="efp[32]" value="Piel y faneras">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_EF" name="efv[32]" value="Normal">
                        N
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="Anormal_EF" name="efv[32]" value="Anormal">
                        AN
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="efhl[32]">
                    </div>    
                    </div>

              <div class="col-md-12">
                      <h3 class="box-title">Descripción y ampliación de hallazgos:
                      </h3> 
                    <div class="box-body pad">
                        <textarea id="descripcion_ao" name="deampl_EF" rows="10" cols="80">
                            Descripcion
                        </textarea>
              </div>
              </div>

</div>