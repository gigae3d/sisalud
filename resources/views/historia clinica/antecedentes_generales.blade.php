<div id="antecedentes_generales" class="box-body tab-pane fade">
    <div class="box-header with-border">
        <h3 class="box-title">Antecedentes familiares</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2 col-xs-12"></div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" id="Si_AF" name="selTodosAF" value="">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" id="No_AF" name="selTodosAF" value="">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" id="NoSabe_AF" name="selTodosAF" value="">
            </div>
            <div class="col-md-4 col-xs-12"></div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>HTA</label>
                <input type="hidden" name="afp[0]" value="HTA">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[0]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[0]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[0]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[0]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Infarto</label>
                <input type="hidden" name="afp[1]" value="Infarto">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[1]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[1]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[1]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[1]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>ACV</label>
                <input type="hidden" name="afp[2]" value="ACV">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[2]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[2]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[2]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[2]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Alergia</label>
                <input type="hidden" name="afp[3]" value="Alergia">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[3]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[3]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[3]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[3]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Asma</label>
                <input type="hidden" name="afp[4]" value="Asma">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[4]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[4]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[4]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[4]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Diabetes</label>
                <input type="hidden" name="afp[5]" value="Diabetes">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[5]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[5]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[5]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[5]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Osteomusculares</label>
                <input type="hidden" name="afp[6]" value="Osteomusculares">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[6]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[6]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[6]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[6]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>TBC</label>
                <input type="hidden" name="afp[7]" value="TBC">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[7]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[7]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[7]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[7]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Artritis</label>
                <input type="hidden" name="afp[8]" value="Artritis">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[8]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[8]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[8]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[8]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Enf. Mental</label>
                <input type="hidden" name="afp[9]" value="Enf Mental">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[9]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[9]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[9]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[9]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Cancer</label>
                <input type="hidden" name="afp[10]" value="Cancer">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[10]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[10]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[10]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[10]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-2  col-xs-12">
                <label>Convulsiones</label>
                <input type="hidden" name="afp[11]" value="Convulsiones">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AF" name="afv[11]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AF" name="afv[11]" value="No" required>
                No
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="NoSabe_AF" name="afv[11]" value="No sabe" required>
                No sabe
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Parentesco</label>
                <input type="text" class="form-control" name="afpr[11]" value=" ">
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Antecedentes personales</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12"></div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" id="Si_AP" name="selTodosAP" value="">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" id="No_AP" name="selTodosAP" value="">
            </div>
            <div class="col-md-4 col-xs-12"></div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Migraña</label>
                <input type="hidden" name="app[0]" value="Migraña">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[0]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[0]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[0]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Def. Visual</label>
                <input type="hidden" name="app[1]" value="Def Visual">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[1]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[1]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[1]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Sordera</label>
                <input type="hidden" name="app[2]" value="Sordera">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[2]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[2]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[2]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Otitis media</label>
                <input type="hidden" name="app[3]" value="Otitis media">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[3]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[3]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[3]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Sinusitis</label>
                <input type="hidden" name="app[4]" value="Sinusitis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[4]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[4]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[4]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Tinitus</label>
                <input type="hidden" name="app[5]" value="Tinitus">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[5]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[5]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[5]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Convulsiones</label>
                <input type="hidden" name="app[6]" value="Convulsiones">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[6]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[6]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[6]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>HTA</label>
                <input type="hidden" name="app[7]" value="HTA">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[7]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[7]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[7]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Enf. Cardiaca</label>
                <input type="hidden" name="app[8]" value="Enf Cardiaca">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[8]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[8]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[8]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Hepatitis</label>
                <input type="hidden" name="app[9]" value="Hepatitis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[9]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[9]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[9]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Enf. Tiroides</label>
                <input type="hidden" name="app[10]" value="Enf Tiroides">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[10]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[10]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[10]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Diabetes</label>
                <input type="hidden" name="app[11]" value="Diabetes">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[11]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[11]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[11]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Hernia inguinal</label>
                <input type="hidden" name="app[12]" value="Hernia inguinal">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[12]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[12]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[12]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Hernia umbilical</label>
                <input type="hidden" name="app[13]" value="Hernia umbilical">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[13]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[13]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[13]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Varicocele</label>
                <input type="hidden" name="app[14]" value="Varicocele">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[14]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[14]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[14]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Dermatitis</label>
                <input type="hidden" name="app[15]" value="Dermatitis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[15]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[15]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[15]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Artritis</label>
                <input type="hidden" name="app[16]" value="Artritis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[16]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[16]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[16]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Bronquitis</label>
                <input type="hidden" name="app[17]" value="Bronquitis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[17]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[17]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[17]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Asma</label>
                <input type="hidden" name="app[18]" value="Asma">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[18]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[18]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[18]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>TBC</label>
                <input type="hidden" name="app[19]" value="TBC">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[19]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[19]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[19]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Enf. Acidopept</label>
                <input type="hidden" name="app[20]" value="Enf Acidopept">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[20]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[20]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[20]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Colitis</label>
                <input type="hidden" name="app[21]" value="Colitis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[21]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[21]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[21]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Colelitiasis</label>
                <input type="hidden" name="app[22]" value="Colelitiasis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[22]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[22]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[22]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Urolitiasis</label>
                <input type="hidden" name="app[23]" value="Urolitiasis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[23]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[23]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[23]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Inf.
                    Urinaria</label>
                <input type="hidden" name="app[24]" value="Inf Urinaria">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[24]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[24]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[24]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Venereas</label>
                <input type="hidden" name="app[25]" value="Venereas">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[25]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[25]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[25]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Colesterol
                    alto</label>
                <input type="hidden" name="app[26]" value="Colesterol alto">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[26]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[26]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[26]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Cancer</label>
                <input type="hidden" name="app[27]" value="Cancer">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[27]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[27]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[27]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Alteraciones
                    hormonales</label>
                <input type="hidden" name="app[28]" value="Alteraciones hormonales">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[28]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[28]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[28]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Lumbago
                    crónico</label>
                <input type="hidden" name="app[29]" value="Lumbago crónico">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[29]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[29]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[29]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Tunel carpiano</label>
                <input type="hidden" name="app[30]" value="Tunel carpiano">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[30]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[30]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[30]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Varices piernas</label>
                <input type="hidden" name="app[31]" value="Varices piernas">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[31]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[31]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[31]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Trombosis</label>
                <input type="hidden" name="app[32]" value="Trombosis">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[32]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[32]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[32]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Gota</label>
                <input type="hidden" name="app[33]" value="Gota">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="Si_AP" name="apv[33]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" class="No_AP" name="apv[33]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aphc[33]">
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title"> Antecedentes Dermatológicos</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12"></div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" id="Si_AD" name="selTodosAD" value="">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" id="No_AD" name="selTodosAD" value="">
            </div>
            <div class="col-md-4 col-xs-12"></div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Eczema</label>
                <input type="hidden" name="adp[0]" value="Eczema">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AD" name="adv[0]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AD" name="adv[0]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="adhc[0]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4  col-xs-12">
                <label>Rinitis</label>
                <input type="hidden" name="adp[1]" value="Rinitis">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AD" name="adv[1]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AD" name="adv[1]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="adhc[1]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4  col-xs-12">
                <label>Dermatitis atópica</label>
                <input type="hidden" name="adp[2]" value="Dermatitis atopica">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AD" name="adv[2]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AD" name="adv[2]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="adhc[2]">
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Antecedentes Osteomusculares</h3>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12"></div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" id="Si_AO" name="selTodosAO" value="">
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" id="No_AO" name="selTodosAO" value="">
            </div>
            <div class="col-md-4 col-xs-12"></div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <label>Fracturas</label>
                <input type="hidden" name="aop[0]" value="Fracturas">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AO" name="aov[0]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AO" name="aov[0]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aohc[0]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4  col-xs-12">
                <label>Cirugías osteomusculares</label>
                <input type="hidden" name="aop[1]" value="Cirugias osteomusculares">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AO" name="aov[1]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AO" name="aov[1]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aohc[1]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4  col-xs-12">
                <label>Sintomas a nivel de columna vertebral</label>
                <input type="hidden" name="aop[2]" value="Sintomas a nivel de columna vertebral">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AO" name="aov[2]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AO" name="aov[2]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aohc[2]">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4  col-xs-12">
                <label>Sintomas osteomusculares</label>
                <input type="hidden" name="aop[3]" value="Sintomas osteomusculares">
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="Si_AO" name="aov[3]" value="Si" required>
                Si
            </div>
            <div class="col-md-2 col-xs-4">
                <input type="radio" class="No_AO" name="aov[3]" value="No" required>
                No
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <label>Hace cuanto</label>
                <input type="text" class="form-control" name="aohc[3]">
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Descripción:</h3>
    </div>

    <div class="col-md-12">
        <div class="box-body pad">
            <textarea id="descripcion_ao" name="descripcion_ao" rows="10">
                Descripcion
            </textarea>
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <div class="row">
            <div class="col-xs-6">
                <h3 class="box-title">Antecedentes Quirúrgicos</h3>
            </div>
            <div class="col-xs-6">
                <button id="add_ant_quirurgico" type="button" class="btn btn-link  pull-right"><i
                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                </button>
                <button id="remove_ant_quirurgico" type="button"
                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                        aria-hidden="true"></i> Quitar
                </button>
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header ant_quirurgicos"></div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <div class="row">
            <div class="col-xs-6">
                <h3 class="box-title">Ant. Traumáticos (accidentes)</h3>
            </div>
            <div class="col-xs-6">
                <button id="add_ant_traumatico" type="button" class="btn btn-link  pull-right"><i
                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                </button>
                <button id="remove_ant_traumatico" type="button"
                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                        aria-hidden="true"></i> Quitar
                </button>
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header ant_traumaticos"></div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Secuelas:</h3>
    </div>

    <div class="col-md-12">
        <div class="box-body pad">
            <textarea id="descripcion_ao" name="secuelas_ao" rows="10">
                Descripcion
            </textarea>
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Describa los medicamentos, drogas o vitaminas que toma en la actualidad:</h3>
    </div>

    <div class="col-md-12">
        <div class="box-body pad">
            <textarea id="descripcion_ao" name="medicamentos_ao" rows="10">
                Descripcion
            </textarea>
        </div>
    </div>

    <div class="col-md-12" style="margin-top: 25px">
        <div class="form-group">
            <label>Es usted alergico a algún medicamento, sustancia o comida?</label>
            <input type="text" class="form-control" id="alergias" name="alergias"
                   placeholder="Describa, si tiene, sus alergias">
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Inmunización</h3>
    </div>

    <div class="col-md-12" style="margin-top: 5px">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="form-group" style="padding: 3px 0;">
                    <label>
                        <input type="checkbox" id="hepatitis_i" value="Hepatitis B" name="inmunizaciones[]">
                        Hepatitis B
                    </label>
                </div>
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <div class="form-group">
                    <label>Fecha</label>
                    <div class="input-group date">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="date" class="form-control" id="fecim_h" name="fecim_h">
                    </div>
                </div>
            </div>
            <div class="col-md-4 form-inline">
                <label>Dosis</label>
                <input type="text" class="form-control" id="dosim_h" name="dosim_h">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="form-group" style="padding: 3px 0;">
                    <label>
                        <input type="checkbox" id="tetano_i" value="Tetano" name="inmunizaciones[]">
                        Tétano
                    </label>
                </div>
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <div class="form-group">
                    <label>Fecha</label>
                    <div class="input-group date">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="date" class="form-control" id="fecim_t" name="fecim_t">
                    </div>
                </div>
            </div>
            <div class="col-md-4 form-inline">
                <label>Dosis</label>
                <input type="text" class="form-control" id="dosim_t" name="dosim_t">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 col-xs-12">
                <div class="form-group" style="padding: 3px 0;">
                    <label>
                        <input type="checkbox" id="influenza_in" value="Influenza" name="inmunizaciones[]">
                        Influenza
                    </label>
                </div>
            </div>
            <div class="col-md-4 form-inline col-xs-12">
                <div class="form-group">
                    <label>Fecha</label>
                    <div class="input-group date">
                        <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                        <input type="date" class="form-control" id="fecim_in" name="fecim_in">
                    </div>
                </div>
            </div>
            <div class="col-md-4 form-inline">
                <label>Dosis</label>
                <input type="text" class="form-control" id="dosim_in" name="dosim_in">
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group">
            <label>Otras</label>
            <input type="text" class="form-control" id="otras_inmunizacion" name="otras_inmunizacion"
                   placeholder="Otras inmunizaciones">
        </div>
    </div>

    <div class="col-md-12 box-header with-border" style="margin-top: 25px">
        <h3 class="box-title">Antecedentes Gineco-obstétricos</h3>
    </div>

    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Registrar Antecedentes gineco-obstétricos?:</label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="ant_gineco_obs" value="Si">
                        Si
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="ant_gineco_obs" value="No">
                        No
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Menarquia</label>
                <input type="text" class="form-control" id="menarquia" name="menarquia">
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Ciclos</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="dias_C1">
                    </div>
                    <div class="col-md-2 text-center">
                        <label>X</label>
                    </div>
                    <div class="col-md-5">
                        <input type="text" class="form-control" name="dias_C2">
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>FUM</label>
                <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input type="date" class="form-control" name="FUM">
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                        <label>Dismenorrea</label>
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="dismenorrea" value="Si">
                        Si
                    </div>
                    <div class="col-md-6">
                        <input type="radio" name="dismenorrea" value="No">
                        No
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>FPP</label>
                <div class="input-group date">
                    <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                    <input type="date" class="form-control" id="fpp" name="fpp">
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <label>Paridad</label>
        </div>

        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label>G</label>
                        <input type="number" ass="form-control" name="paridadg">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>P</label>
                        <input type="number" ass="form-control" name="paridadp">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>A</label>
                        <input type="number" ass="form-control" name="paridada">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>C</label>
                        <input type="number" ass="form-control" name="paridadc">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>E</label>
                        <input type="number" ass="form-control" name="paridade">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>M</label>
                        <input type="number" ass="form-control" name="paridadm">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>V</label>
                        <input type="number" ass="form-control" name="paridadv">
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Planificación</label>
                    <label class="radio-inline">
                        <input type="radio" name="planificacion" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="planificacion" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Método</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control" id="metodo_planificacion" name="metodo_planificacion">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Citología reciente</label>
                    <label class="radio-inline">
                        <input type="radio" name="citologia_reciente" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="citologia_reciente" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Fecha</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="date" class="form-control" id="fecha_citologia" name="fecha_citologia">
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Resultado</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control" id="resultado_citologia" name="resultado_citologia">
                </div>
            </div>
        </div>

        <div class="col-md-12 box-header with-border">
            <h3 class="box-title">Habitos tóxicos</h3>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Fumador</label>
                    <label class="radio-inline">
                        <input type="radio" name="fumador" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="fumador" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Ex fumador</label>
                    <label class="radio-inline">
                        <input type="radio" name="exfumador" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="exfumador" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Años de suspensión del hábito</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" id="asuspension" name="asuspension_h">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Años de fumador</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" id="afumador" name="afumador">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>Cigarrillos por día</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="number" ass="form-control" id="cigarrillos_dia" name="cigarrillos_dia">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Toma licor habitualmente?</label>
                    <label class="radio-inline">
                        <input type="radio" name="tomalicorh" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="tomalicorh" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Frecuencia del hábito</label>
                    <label class="radio-inline">
                        <input type="radio" name="frecuencia_habito" value="Diario">
                        Diario
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="frecuencia_habito" value="Semanal">
                        Semanal
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="frecuencia_habito" value="Quincenal">
                        Quincenal
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="frecuencia_habito" value="Mensual">
                        Mensual
                        </label>

                    <label class="radio-inline">
                        <input type="radio" name="frecuencia_habito" value="Ocasional">
                        Ocasional
                    </label>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Tipo de licor</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="text" class="form-control" id="tlicor" name="tlicor">
                </div>
            </div>
            <div class="col-md-5">
                <div class="form-group">
                    <label>¿Ha tenido problemas con la bebida?</label>
                    <label class="radio-inline">
                        <input type="radio" name="pbebida" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="pbebida" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>¿Exbebedor?</label>
                    <label class="radio-inline">
                        <input type="radio" name="exbebedor" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="exbebedor" value="No">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>Años suspendido</label>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <input type="number" ass="form-control" id="asuspendido" name="asuspendido">
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <label>¿Otros tóxicos?</label>
                    <label class="radio-inline">
                        <input type="radio" name="otoxicos" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="otoxicos" value="No">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-4">
                <div class="form-group">
                    <label>Medicamentos regularmente</label>
                    <label class="radio-inline">
                        <input type="radio" name="med_reg_ht" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="med_reg_ht" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Cual:</label>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <input type="text" class="form-control" name="cual_med_reg">
                </div>
            </div>
        </div>

        <div class="col-md-12 box-header with-border">
            <h3 class="box-title"> Factores predisponentes para riesgo ergonómico</h3>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label>Sedentarismo</label>
                    <label class="radio-inline">
                        <input type="radio" name="sedentarismo" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="sedentarismo" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Deportes de raqueta(Tenis/Squash)</label>
                    <label class="radio-inline">
                        <input type="radio" name="deportes_raqueta" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="deportes_raqueta" value="No">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Sobrepeso</label>
                    <label class="radio-inline">
                        <input type="radio" name="sobrepeso" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="sobrepeso" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-9">
                <div class="form-group">
                    <label>Manualidades(Pintar, bordar, tejer, instrumento musical, bricolaje, etc)</label>
                    <label class="radio-inline">
                        <input type="radio" name="manualidades" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="manualidades" value="No">
                        No
                    </label>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-3">
                <div class="form-group form-inline">
                    <label>Deporte</label>
                    <label class="radio-inline">
                        <input type="radio" name="deporte" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="deporte" value="No">
                        No
                    </label>
                </div>
            </div>
            <div class="col-md-1">
                <div class="form-group">
                    <label>Cual:</label>
                </div>
            </div>
            <div class="col-md-3">
                <div class="form-group">
                    <input type="text" class="form-control" id="cdeporte" name="cdeporte">
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label>Pasatiempos(Jardinería, coser, tejer, pintar, instrumento de música, manualidades, oficios
                        domésticos)</label>
                    <label class="radio-inline">
                        <input type="radio" name="pasatiempos" value="Si">
                        Si
                    </label>
                    <label class="radio-inline">
                        <input type="radio" name="pasatiempos" value="No">
                        No
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>
