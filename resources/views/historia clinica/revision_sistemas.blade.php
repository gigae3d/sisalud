<div id="revision_sistemas" class="box-body tab-pane fade">

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Sistema nervioso:</label>
	                    <input type="text" class="form-control" name="sistema_nervioso_rs">
                        </div>
              </div>
              </div>
             
             <div class="row">
             <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Insomnio:</label>
	                    <input type="text" class="form-control"  name="insomnio_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Dificultad para concentrarse:</label>
	                    <input type="text" class="form-control" name="dif_p_concentrarse_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Desinteres para realizar actividades:</label>
	                    <input type="text" class="form-control" name="des_real_act_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Tensión muscular, cansancio, miedo:</label>
	                    <input type="text" class="form-control" name="tension_muscular_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Ojos, epifora, esfuerzo visual:</label>
	                    <input type="text" class="form-control" name="ojos_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">ORL:</label>
	                    <input type="text" class="form-control" name="ORL_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Respiratorio(Disnea, hiperventilación, etc):</label>
	                    <input type="text" class="form-control" name="respiratorio_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Cardiovascular:</label>
	                    <input type="text" class="form-control" name="cardiovascular_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Digestivo:</label>
	                    <input type="text" class="form-control" name="digestivo_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group ">
                        <label style="font-size:16px; font-style: italic;">Piel y faneras:</label>
	                    <input type="text" class="form-control" name="piel_faneras_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Musculoesquelético:</label>
	                    <input type="text" class="form-control" name="musculoesqueletico_rs">
                        </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-12">
		                <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Genito urinario:</label>
	                    <input type="text" class="form-control" name="genito_urinario_rs">
                        </div>
              </div>
              </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">En el último año ha tenido dolor o molestia a nivel de:</h3>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Si_MO" name="selTodosMO" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="No_MO" name="selTodosMO" value="">
                        
                        </label>
                        </div>

              </div>

                    <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Nuca, cuello</label>
                        <input type="hidden" name="map[0]" value="Nuca cuello">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[0]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[0]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[0]">
                    </div>    
                    </div>

                    <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Hombro derecho</label>
                        <input type="hidden" name="map[1]" value="Hombro derecho">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[1]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[1]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[1]">
                    </div>    
                    </div>

                     <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Hombro izquierdo</label>
                        <input type="hidden" name="map[2]" value="Hombro izquierdo">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[2]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[2]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[2]">
                    </div>    
                    </div>

                     <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Codo derecho</label>
                        <input type="hidden" name="map[3]" value="Codo derecho">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[3]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[3]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[3]">
                    </div>    
                    </div>

                     <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Codo izquierdo</label>
                        <input type="hidden" name="map[4]" value="Codo izquierdo">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[4]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[4]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[4]">
                    </div>    
                    </div>

                     <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Mano, muñeca derecha</label>
                        <input type="hidden" name="map[5]" value="Mano muñeca derecha">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[5]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[5]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[5]">
                    </div>    
                    </div>

                

                
                    <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Mano, muñeca izquierda</label>
                        <input type="hidden" name="map[6]" value="Mano muñeca izquierda">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[6]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[6]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[6]">
                    </div>    
                    </div>

                        <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Columna dorsal</label>
                        <input type="hidden" name="map[7]" value="Columna dorsal">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[7]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[7]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[7]">
                    </div>    
                    </div>

                        <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Columna lumbar</label>
                        <input type="hidden" name="map[8]" value="Columna lumbar">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[8]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[8]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[8]">
                    </div>    
                    </div>

                        <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Rodillas</label>
                        <input type="hidden" name="map[9]" value="Rodillas">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[9]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[9]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[9]">
                    </div>    
                    </div>

                        <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Miembros inferiores</label>
                        <input type="hidden" name="map[10]" value="Miembros inferiores">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[10]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[10]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[10]">
                    </div>    
                    </div>

                        <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Varices</label>
                        <input type="hidden" name="map[11]" value="Varices">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Si_MO" name="mav[11]" value="Si">
                        Si
                        </label>
                    </div>
                    <div class="col-md-1">

                        <label class="radio-inline"> 
                        <input type="radio" class="No_MO" name="mav[11]" value="No">
                        No
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Descripción</label>
                        <input type="text" class="form-control" name="macd[11]">
                    </div>    
                    </div>


                

               <div class="col-md-12">
		              <h3 class="box-title">Seguimiento y evolución de casos:
		              </h3>	
		            <div class="box-body pad">
	                    <textarea id="descripcion_sev" name="seguimiento_casos" rows="10" cols="80">
	                    	Descripcion
	                    </textarea>
		       </div>
		       </div>

              

</div>