@extends('base.app')

@section('migas')
    <h1>Historia Clínica Ocupacional</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Historia Cinica Ocupacional</li>
        <li class="active">Crear Registro a Historia Clínica Ocupacional</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Nuevo Registro a Historia Clínica Ocupacional</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_consulta_medica') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="trabajador" value="{{ $trabajador }}">
                <div class="box-body">
                    <div class="col-md-12">
                        <ul class="nav nav-tabs nav-justified">
                            <li role="presentation" class="active"><a data-toggle="tab" href="#informacion_ocupacional">Información
                                    Ocupacional</a></li>
                            <li role="presentation"><a data-toggle="tab" href="#antecedentes_laborales">Antecedentes
                                    Laborales</a>
                            </li>
                            <li role="presentation"><a data-toggle="tab" href="#antecedentes_generales">Antecedentes
                                    Generales</a></li>
                            <li role="presentation"><a data-toggle="tab" href="#revision_sistemas">Enfermedad<br/>Actual</a>
                            </li>
                            <li role="presentation"><a data-toggle="tab" href="#examen_fisico">Examen<br/>Físico</a></li>
                            <li role="presentation"><a data-toggle="tab" href="#columna_vertebral">Columna<br/>vertebral</a>
                            </li>
                            <li role="presentation"><a data-toggle="tab" href="#evaluacion_mental">Evaluacion del Estado
                                    Mental</a>
                            </li>
                            <li role="presentation"><a data-toggle="tab" href="#concepto_aptitud">Concepto de
                                    Aptitud</a>
                            </li>
                        </ul>
                    </div>
                </div>

                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-12">
                        <div class="tab-content">
                            @include('historia clinica.informacion_ocupacional')
                            @include('historia clinica.antecedentes_laborales')
                            @include('historia clinica.antecedentes_generales')
                            @include('historia clinica.revision_sistemas')
                            @include('historia clinica.examen_fisico')
                            @include('historia clinica.columna_vertebral')
                            @include('historia clinica.evaluacion_mental')
                            @include('historia clinica.concepto_aptitud')
                        </div>
                    </div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar</button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_h_clinicas_ocupacionales', ['id' => $trabajador]) }}"><i class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            //Calcular indice de masa corporal
            $('#peso').keyup(function (event) {
                var imc = $('#peso').val() / ($('#talla').val() * 2);
                imc = imc.toFixed(3);
                $('#imc').val(imc);
            });

            $('#talla').keyup(function (event) {
                var imc = $('#peso').val() / ($('#talla').val() * 2);
                imc = imc.toFixed(2);
                $('#imc').val(imc);
            });

            //Campo otra actividad informacion ocupacional
            $('#campo_otra_actividad').hide();
            $('#otra_actividad').change(function () {
                if ($(this).prop('checked')) {
                    $('#campo_otra_actividad').show();
                    $('#campo_otra_actividad').attr("name", "actividades_que_realiza[]");
                } else {
                    $('#campo_otra_actividad').hide();
                    $('#campo_otra_actividad').attr("name", "");
                }
            });

            //Campo otra accion formulario información ocupacional
            $('#campo_otra_accion').hide();
            $('#otra_accion').change(function () {
                if ($(this).prop('checked')) {
                    $('#campo_otra_accion').show();
                    $('#campo_otra_accion').attr("name", "acciones_que_realiza[]");
                } else {
                    $('#campo_otra_accion').hide();
                    $('#campo_otra_accion').attr("name", "");
                }
            });

            //Funcion para añadir una nueva exposición a factores de riesgo y antecedentes laborales
            var numero_actual_campos_riesgo = 0;
            $('#add_exp_riesgo_laboral').on('click', function () {

                if (numero_actual_campos_riesgo < 6)
                {
                    $('.exp_riesgo_laborales').append('<div class="row">'
                        + '<div class="col-md-6">'
                            + '<div class="form-group">'
                                + '<label>Empresa:</label>'
                                + '<input type="text" class="form-control"  name="empresas[]" placeholder="Nombre de la empresa" required>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="form-group">'
                                + '<label>EPP:</label>'
                                + '<input type="text" class="form-control"  name="epp[]" placeholder="Elementos de protección personal">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="form-group">'
                                + '<label>Cargo:</label>'
                                + '<input type="text" class="form-control"  name="cargo[]" placeholder="Cargo en la empresa">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="form-group">'
                                + '<label>Tiempo:</label>'
                                + '<input type="text" class="form-control"  name="tiempo[]" placeholder="Tiempo en la empresa">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="row form-group">'
                                + '<div class="col-xs-4">'
                                    + '<label>Estado laboral:</label>'
                                + '</div>'
                                + '<div class="col-xs-4">'
                                    + '<input type="radio" name="estado_laboral[' + numero_actual_campos_riesgo + ']" value="Anterior">'
                                    + '  Anterior'
                                + '</div>'
                                + '<div class="col-xs-4">'
                                    + '<input type="radio" name="estado_laboral[' + numero_actual_campos_riesgo + ']" value="Actual">'
                                    + '  Actual'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="row form-group">'
                                + '<div class="col-xs-4">'
                                    + '<label>Riesgo:</label>'
                                + '</div>'
                                + '<div class="col-xs-4">'
                                    + '<input type="checkbox" value="riesgo">'
                                + '</div>'
                                + '<div class="col-xs-4">&nbsp;</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-12">'
                            + '<div class="row form-group">'
                                + '<div class="col-xs-12 col-md-2">'
                                    + '<label>Factor de riesgo:</label>'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Loc]" value="Loc">'
                                    + '  Loc'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Fis]" value="Fis">'
                                    + '  Fis'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Qui]" value="Qui">'
                                    + '  Qui'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Bio]" value="Bio">'
                                    + '  Bio'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Erg]" value="Erg">'
                                    + '  Erg'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Mec]" value="Mec">'
                                    + '  Mec'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Psic]" value="Psic">'
                                    + '  Psic'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" name="factores_de_riesgo_AL[' + numero_actual_campos_riesgo + '][Elec]" value="Elec">'
                                    + '  Elec'
                                + '</div>'
                                + '<div class="col-xs-6 col-md-2">'
                                    + '<input type="checkbox" value="">'
                                    + '  Otro'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '</div>'
                        +'<hr/>');
                    numero_actual_campos_riesgo++;
                }
            });

            //Funcion para quitar un campo de exposición a factores de riesgo y antecedentes laborales
            $('#remove_exp_riesgo_laboral').on('click', function () {
                $('div.exp_riesgo_laborales > div').last().remove();
                numero_actual_campos_riesgo--;
            });

            //Funcion para añadir un nuevo registro a antecedentes de accidentes de trabajo
            var n_a_antecedentes_acciden = 0;
            $('#add_ant_acci').on('click', function () {

                if (n_a_antecedentes_acciden < 6)
                {
                    $('.ant_accidentes_trabajo').append('<div><div class="row">'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Fecha:</label>'
                                + '<div class="input-group date">'
                                    + '<div class="input-group-addon"><i class="fa fa-calendar"></i></div>'
                                    + '<input type="date"  class="form-control" name="fecha_aat[]">'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Empresa:</label>'
                                + '<input type="text" class="form-control" name="empresa_aat[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Causa:</label>'
                                + '<input type="text" class="form-control" name="causa_aat[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Tipo de lesión:</label>'
                                + '<input type="text" class="form-control" name="tlesion_aat[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Parte del cuerpo afectado:</label>'
                                + '<input type="text" class="form-control" name="parte_aat[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Dias de incapacidad:</label>'
                                + '<input type="number" class="form-control" name="dincapacidad_aat[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="form-group">'
                                + '<label>Secuelas:</label>'
                                + '<input type="text" class="form-control" name="secuelas_aat[]">'
                            + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<hr/>');
                    n_a_antecedentes_acciden++;
                }
            });

            //Funcion para quitar un registro de antecedentes de accidentes de trabajo
            $('#remove_ant_acci').on('click', function () {
                $('div.ant_accidentes_trabajo > div').last().remove();
                n_a_antecedentes_acciden--;
            });

            //Funcion para añadir un nuevo registro de enfermedad profesional
            var n_enf_profesional = 0;
            $('#add_enf_profesional').on('click', function () {

                if (n_enf_profesional < 6)
                {
                    $('.enf_profesional').append('<div><div class="row">'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Fecha:</label>'
                                + '<div class="input-group date">'
                                    + '<div class="input-group-addon"><i class="fa fa-calendar"></i></div>'
                                    + '<input type="date" class="form-control"  name="fecha_lep[]">'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>Empresa:</label>'
                                + '<input type="text" class="form-control" name="empresa_lep[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-6">'
                            + '<div class="form-group">'
                                + '<label>Diagnóstico:</label>'
                                + '<input type="text" class="form-control" name="diagnostico_lep[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>¿Indemnización?</label>'
                                + '<input type="text" class="form-control" name="indemnizacion_lep[]">'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3">'
                            + '<div class="form-group">'
                                + '<label>¿Reubicación?</label>'
                                + '<input type="text" class="form-control" name="reubicacion_lep[]">'
                            + '</div>'
                        + '</div>'
                        + '</div>'
                        + '<hr/>');
                    n_enf_profesional++;
                }
            });

            //Funcion para quitar un registro de enfermedad profesional
            $('#remove_enf_profesional').on('click', function () {
                $('div.enf_profesional > div').last().remove();
                n_enf_profesional--;
            });

            //Funcion para añadir un nuevo registro de antecedentes quirúrgicos
            var n_ant_quirurgicos = 0;
            $('#add_ant_quirurgico').on('click', function () {

                if (n_ant_quirurgicos < 3)
                {
                    $('.ant_quirurgicos').append('<div class="row">'
                        + '<div class="col-md-9" col-xs-12>'
                            + '<div class="form-group">'
                                + '<label>Antecedente:</label>'
                                + '<input type="text" class="form-control" name="antecedente_quirurgico[]" required>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3 col-xs-12">'
                            + '<div class="form-group">'
                                + '<label>Fecha:</label>'
                                + '<div class="input-group date">'
                                    + '<div class="input-group-addon"><i class="fa fa-calendar"></i></div>'
                                    + '<input type="date" class="form-control" name="fecha_aq[]" required>'
                                + '</div>'
                            + '</div>'
                        + '</div>'
                        + '</div>');
                    n_ant_quirurgicos++;
                }
            });

            //Funcion para quitar un nuevo registro de antecedentes quirurgicos
            $('#remove_ant_quirurgico').on('click', function () {
                $('div.ant_quirurgicos > div').last().remove();
                n_ant_quirurgicos--;
            });

            //Funcion para añadir un nuevo registro de antecedentes traumáticos
            var n_ant_traumaticos = 0;
            $('#add_ant_traumatico').on('click', function () {

                if (n_ant_traumaticos < 3)
                {
                    $('.ant_traumaticos').append('<div class="row">'
                        + '<div class="col-md-9 col-xs-12">'
                            + '<div class="form-group">'
                                + '<label>Lesión:</label>'
                                + '<input type="text" class="form-control" name="antecedente_traumatico[]" required>'
                            + '</div>'
                        + '</div>'
                        + '<div class="col-md-3 col-xs-12">'
                            + '<div class="form-group">'
                                + '<label>Causa:</label>'
                                + '<input type="text" class="form-control" name="causa_at[]" required>'
                            + '</div>'
                        + '</div>'
                        + '</div>');
                    n_ant_traumaticos++;
                }
            });

            //Funcion para quitar un nuevo registro de antecedentes traumaticos
            $('#remove_ant_traumatico').on('click', function () {
                $('div.ant_traumaticos > div').last().remove();
                n_ant_traumaticos--;
            });

            //Funcion para añadir un nuevo registro de paraclínico
            var n_paraclinicos = 0;
            $('#add_paraclinico').on('click', function () {
                if (n_paraclinicos < 6) {
                    $('.paraclinicos').append('<div class="row">'
                        + '<div class="col-md-3">'
                        + '<div class="form-group">'
                        + '<label style="font-size:16px; font-style: italic;">Nombre del paraclínico:</label>'
                        + '<input type="text" class="form-control" name="nombre_para[]" required>'
                        + '</div>'
                        + '</div>'

                        + '<div class="col-md-2">'
                        + '<div class="form-group">'
                        + '<label style="font-size:16px; font-style: italic;">Fecha:</label>'
                        + '<input type="date" class="form-control" name="fecha_para[]" required>'
                        + '</div>'
                        + '</div>'

                        + '<div class="col-md-7">'
                        + '<div class="form-group">'
                        + '<label style="font-size:16px; font-style: italic;">Resultado:</label>'
                        + '<input type="text" class="form-control" name="resultado_para[]" required>'
                        + '</div>'
                        + '</div>'
                        + '</div>');
                    n_paraclinicos++;
                }
            });

            //Funcion para quitar un nuevo registro de paraclinico
            $('#remove_paraclinico').on('click', function () {
                $('div.paraclinicos > div').last().remove();
                n_paraclinicos--;
            });

            //Funcion para añadir un nuevo registro de diagnostico
            var n_diagnosticos = 0;
            $('#add_diagnostico').on('click', function () {
                if (n_diagnosticos < 6) {
                    $('.diagnosticos').append('<div class="row">'
                        + '<div class="col-md-1">'
                        + '<div class="form-group">'
                        + '<label style="font-size:16px; font-style: italic;">' + n_diagnosticos + '.</label>'
                        + '</div>'
                        + '</div>'

                        + '<div class="col-md-11">'
                        + '<div class="form-group">'
                        + '<input type="text" class="form-control" name="diagnosticos[]" required>'
                        + '</div>'
                        + '</div>'
                        + '</div>');
                    n_diagnosticos++;
                }
            });

            //Funcion para quitar un nuevo registro de diagnostico
            $('#remove_diagnostico').on('click', function () {
                $('div.diagnosticos > div').last().remove();
                n_diagnosticos--;
            });

            //Campo otro elemento formulario antecedentes laborales
            $('#campo_otro_elemento').hide();
            $('#otro_elemento').change(function () {
                if ($(this).prop('checked')) {
                    $('#campo_otro_elemento').show();
                } else {
                    $('#campo_otro_elemento').hide();
                }
            });

            //Campo otro ingreso pveso concepto de aptitud
            $('#campo_ing_pveso').hide();

            $('#otro_ing_pveso').change(function () {
                if ($(this).prop('checked')) {
                    $('#campo_ing_pveso').show();
                    $('#campo_ing_pveso').attr("name", "ingreso_pveso[]");
                } else {
                    $('#campo_ing_pveso').hide();
                    $('#campo_ing_pveso').attr("name", "");
                }
            });

            //Funcion para seleccionar todos SI en antecedentes familiares
            $('#Si_AF').change(function () {
                $('.Si_AF').prop('checked', true);
            });

            //Función para seleccionar todos NO en antecedentes familiares
            $('#No_AF').change(function () {
                $('.No_AF').prop('checked', true);
            });

            //Función para seleccionar todos NO SABE en antecedentes familiares
            $('#NoSabe_AF').change(function () {
                $('.NoSabe_AF').prop('checked', true);
            });

            $('#Si_AP').change(function () {
                $('.Si_AP').prop('checked', true);
            });

            $('#No_AP').change(function () {
                $('.No_AP').prop('checked', true);
            });

            $('#Si_AD').change(function () {
                $('.Si_AD').prop('checked', true);
            });

            $('#No_AD').change(function () {
                $('.No_AD').prop('checked', true);
            });

            $('#Si_AO').change(function () {
                $('.Si_AO').prop('checked', true);
            });

            $('#No_AO').change(function () {
                $('.No_AO').prop('checked', true);
            });

            //Funcion para seleccionar todos SI en "Ha tenido molestias a nivel de"
            $('#Si_MO').change(function () {
                $('.Si_MO').prop('checked', true);
            });

            //Funcion para seleccionar todos NO en "Ha tenido molestias a nivel de"
            $('#No_MO').change(function () {
                $('.No_MO').prop('checked', true);
            });

            //Función para seleccionar todos NORMAL en "examen fisico"
            $('#Normal_EF').change(function () {
                $('.Normal_EF').prop('checked', true);
            });

            //Función para seleccionar todos ANORMAL en "examen fisico"
            $('#Anormal_EF').change(function () {
                $('.Anormal_EF').prop('checked', true);
            });

            //Funcion para seleccionar todos NORMAL en "inspeccion"
            $('#Normal_IN').change(function () {
                $('.Normal_IN').prop('checked', true);
            });

            //Funcion para seleccionar todos ANORMAL en "inspeccion"
            $('#Anormal_IN').change(function () {
                $('.Anormal_IN').prop('checked', true);
            });

            //Funcion para seleccionar todos NORMAL en "palpacion"
            $('#Normal_PA').change(function () {
                $('.Normal_PA').prop('checked', true);
            });

            //Funcion para seleccionar todos ANORMAL en "palpacion"
            $('#Anormal_PA').change(function () {
                $('.Anormal_PA').prop('checked', true);
            });

            //Funcion para seleccionar todos NORMAL en "nuca y miembro superior"
            $('#Normal_NYS').change(function () {
                $('.Normal_NYS').prop('checked', true);
            });

            //Funcion para seleccionar todos ANORMAL en "nuca y miembro superior"
            $('#Anormal_NYS').change(function () {
                $('.Anormal_NYS').prop('checked', true);
            });

            //Funcion para seleccionar todos NORMAL en "movilidad"
            $('#Normal_MV').change(function () {
                $('.Normal_MV').prop('checked', true);
            });

            //Funcion para seleccionar todos ANORMAL en "movilidad"
            $('#Anormal_MV').change(function () {
                $('.Anormal_MV').prop('checked', true);
            });

            //Funcion para seleccionar todos NORMAL en "Codos, mano/muñeca"
            $('#Normal_CMM').change(function () {
                $('.Normal_CMM').prop('checked', true);
            });

            //Funcion para seleccionar todos ANORMAL en "Codos, mano/muñeca"
            $('#Anormal_CMM').change(function () {
                $('.Anormal_CMM').prop('checked', true);
            });

            //Funcion para seleccionar todos NORMAL en "Procesos(Estado mental)"
            $('#Normal_PR').change(function () {
                $('.Normal_PR').prop('checked', true);
            });

            //Funcion para seleccionar todos DISFUNCION en "Procesos(Estado mental)"
            $('#Disfuncion_PR').change(function () {
                $('.Disfuncion_PR').prop('checked', true);
            });

            //Mostrar u ocultar campos de fecha y dosis para Hepatitis B
            $('#fecim_h').hide();
            $('#dosim_h').hide();

            $('#hepatitis_i').change(function () {
                if ($(this).prop('checked')) {
                    $('#fecim_h').show();
                    $('#dosim_h').show();
                    $('#fecim_h').attr("name", "fecinmun[]");
                    $('#dosim_h').attr("name", "dosinmun[]");
                } else {
                    $('#fecim_h').hide();
                    $('#dosim_h').hide();
                    $('#fecim_h').attr("name", "");
                    $('#dosim_h').attr("name", "");
                }
            });

            //Mostrar u ocultar campos de fecha y dosis para Tetano
            $('#fecim_t').hide();
            $('#dosim_t').hide();

            $('#tetano_i').change(function () {
                if ($(this).prop('checked')) {
                    $('#fecim_t').show();
                    $('#dosim_t').show();
                    $('#fecim_t').attr("name", "fecinmun[]");
                    $('#dosim_t').attr("name", "dosinmun[]");
                } else {
                    $('#fecim_t').hide();
                    $('#dosim_t').hide();
                    $('#fecim_t').attr("name", "");
                    $('#dosim_t').attr("name", "");
                }
            });

            //Mostrar u ocultar campos de fecha y dosis para Influenza
            $('#fecim_in').hide();
            $('#dosim_in').hide();

            $('#influenza_in').change(function () {
                if ($(this).prop('checked')) {
                    $('#fecim_in').show();
                    $('#dosim_in').show();
                    $('#fecim_in').attr("name", "fecinmun[]");
                    $('#dosim_in').attr("name", "dosinmun[]");
                } else {
                    $('#fecim_in').hide();
                    $('#dosim_in').hide();
                    $('#fecim_in').attr("name", "");
                    $('#dosim_in').attr("name", "");
                }
            });

            //Mostrar objetos
            $('#mostrar_empresas').on('click', function () {
                var empresas = $('.empresas');
                for (var x in empresas) {
                    console.log(empresas[x].value);
                }

            });

            //Campo de texto descripcion antecedentes osteomusculares
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('descripcion_ao')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto descripcion secuelas
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('secuelas_ao')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto descripcion medicamentos
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('medicamentos_ao')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto descripcion resultados audiometria
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('descripcion_ra')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto descripcion resultados examen visual
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('descripcion_rev')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto seguimiento y evolución de casos
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('descripcion_sev')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto descripción y ampliacion de hallazgos examen fisico
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('deampl_EF')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto descripcion y ampliacion de hallazgos columna vertebral
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('deampl_CV')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Campo de texto recomendaciones medicas de concepto de aptitud
            $(function () {
                // Replace the <textarea id="observaciones"> with a CKEditor
                // instance, using default configuration.
                CKEDITOR.replace('recomendaciones_medicas')
                //bootstrap WYSIHTML5 - text editor
                $('.textarea').wysihtml5()
            });

            //Date picker
            $('#fecha_historia, .fecha_form').datepicker({
                startDate: '-285d',
                endDate: '0d',
                autoclose: true
            });
        });
    </script>
@endsection
