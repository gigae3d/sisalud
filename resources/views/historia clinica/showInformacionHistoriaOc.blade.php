@extends('base.app')

@section('migas')
    <h1>Historia Clinica Ocupacional</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Historia Clinica Ocupacional</li>
        <li class="active">Consulta Historia Clinica Ocupacional</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="col-md-12 box-header with-border">
                <h3 class="box-title">Bienestar Institucional y Gestión Humana</h3>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12 box-header">&nbsp;</div>
                </div>
                <div class="panel panel-primary">
                    <div class="panel-heading">Información general de Historia Clínica</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Tipo:</label><br>
                                <label style="font-size:16px;"> {{$historia_ocupacional->tipo_historia}}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Fecha del
                                    examen:</label><br>
                                <label style="font-size:16px;"> {{$historia_ocupacional->fecha}}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Ciudad:</label><br>
                                <label style="font-size:16px;"> {{$historia_ocupacional->ciudad}}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Información del trabajador</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Numero de
                                    identificación:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->numero_de_identificacion }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Nombres:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->nombres }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Apellidos:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->apellidos }}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Fecha de
                                    nacimiento:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->fecha_nacimiento }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Género:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->genero }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Edad:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->edad }}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Lugar de
                                    nacimiento:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->lugar_nacimiento }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Procedencia:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->procedencia }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Residencia:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->residencia }}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Escolaridad:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->escolaridad }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Profesión u
                                    oficio:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->prof_oficio }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Estado
                                    civil:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->estado_civil }}</label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">EPS:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->eps }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">ARP
                                    Anterior:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->arp_anterior }}</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Pensiones:</label><br>
                                <label style="font-size:16px;"> {{ $trabajador->pensiones }}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="text-align: center;">Información ocupacional</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header">&nbsp;</div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Información ocupacional</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Antiguedad en la
                                    empresa:</label><br>
                                <label style="font-size:16px;"> {{ $informacion_ocupacional[0]->antiguedad_en_empresa }}
                                    meses </label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Nombre cargo
                                    actual o a desempeñar:</label>
                                <label style="font-size:16px;"> {{ $informacion_ocupacional[0]->nombre_cargo_actual }}  </label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Antiguedad en el
                                    cargo:</label> <br>
                                <label style="font-size:16px;"> {{ $informacion_ocupacional[0]->antiguedad_en_cargo }}
                                    meses </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Sección:</label><br>
                                <label style="font-size:16px;">{{ $informacion_ocupacional[0]->seccion }} </label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Turno:</label><br>
                                <label style="font-size:16px;">{{ $informacion_ocupacional[0]->turno }} </label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Descripción del
                                    cargo:</label><br>
                                <label style="font-size:16px;">{{ $informacion_ocupacional[0]->descripcion_del_cargo }} </label>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Herramientas
                                    utilizadas:</label><br>
                                <label style="font-size:16px;">{{ $informacion_ocupacional[0]->equipos_herramientas_utilizadas }} </label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Materias primas o
                                    insumos usados:</label><br>
                                <label style="font-size:16px;">{{ $informacion_ocupacional[0]->materias_primas_usadas }} </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic; font-weight: bold;">Acciones
                                    realizadas:</label><br>
                                @foreach ($realiza_accion as $accion)
                                    <label style="font-size:16px;"> {{ $accion->tipo }}</label><br>
                                @endforeach
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic;">Actividades realizadas:</label><br>
                                @foreach ($realiza_actividad as $actividad)
                                    <label style="font-size:16px;"> {{ $actividad->tipo }}</label><br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="text-align: center;">Exposición a factores de riesgo y antecedentes
                            laborales</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header">&nbsp;</div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Exposición a factores de riesgo y antecedentes laborales</div>
                    <div class="panel-body">
                        @if(!$factores_de_riesgo_AL->isEmpty())
                            @foreach ($factores_de_riesgo_AL as $factor_de_riesgo)
                                <div class="row">
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">Empresa:</label><br>
                                        <label style="font-size:16px;"> {{ $factor_de_riesgo->empresa }}</label><br>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">Estado laboral:</label><br>
                                        <label style="font-size:16px;"> {{ $factor_de_riesgo->estado_laboral }}</label><br>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">Cargo:</label><br>
                                        <label style="font-size:16px;"> {{ $factor_de_riesgo->cargo }}</label><br>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">Tiempo:</label><br>
                                        <label style="font-size:16px;"> {{ $factor_de_riesgo->tiempo }}</label><br>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">EPP:</label><br>
                                        <label style="font-size:16px;"> {{ $factor_de_riesgo->EPP }}</label><br>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="text-align: center;">Uso de elementos de protección personal</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header">&nbsp;</div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Uso de elementos de protección personal</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic;">En el cargo o empresa
                                    anterior:</label><br>
                                <label style="font-size:16px; font-style: italic;">{{$elementos_proteccion_p[0]->en_cargo_empresa_ant}}</label><br>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic;">Examen periodico:</label><br>
                                <label style="font-size:16px; font-style: italic;">{{$elementos_proteccion_p[0]->examen_periodico}}</label><br>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px; font-style: italic;">Elementos usados:</label><br>
                                @foreach ($elem_usados as $elem_usado)
                                    <label style="font-size:16px;"> {{ $elem_usado->elemento }}</label><br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="text-align: center;">Antecedentes de accidentes de trabajo</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header">&nbsp;</div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes de accidentes de trabajo</div>
                    <div class="panel-body">
                        @if(!$ant_acci_trabajo->isEmpty())
                            @foreach ($ant_acci_trabajo as $ant_acci_t)
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Fecha:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->fecha}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Empresa:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->empresa}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Causa:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->causa}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Tipo de lesión:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->tipo_de_lesion}}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Parte del cuerpo
                                                afectado:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->parte_del_cuerpo}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Dias de
                                                incapacidad:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->dias_de_incapacidad}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Secuelas:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$ant_acci_t->secuelas}}</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Enfermedad profesional</div>
                    <div class="panel-body">
                        @if(!$enfer_profesionales->isEmpty())
                            @foreach($enfer_profesionales as $enf_pro)
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Fecha:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$enf_pro->fecha}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Empresa:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$enf_pro->empresa}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Diagnóstico:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$enf_pro->diagnostico}}</label>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">¿Indemnización?:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$enf_pro->indemnizacion}}</label>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">¿Reubicación?:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$enf_pro->reubicacion}}</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes familiares</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px;">Enfermedad</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Presente</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Parentesco</label>
                            </div>
                        </div>

                        @foreach ($ant_familiares as $ant_familiar)
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_familiar->enfermedad}}</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_familiar->presente}}</label>
                                </div>
                                <div class="col-md-4 ">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_familiar->parentesco}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes personales</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px;">Patología</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Presente</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Hace cuanto</label>
                            </div>
                        </div>

                        @foreach ($ant_personales as $ant_personal)
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_personal->patologia}}</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_personal->presente}}</label>
                                </div>
                                <div class="col-md-4 ">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_personal->hace_cuanto}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="text-align: center;">Antecedentes dermatológicos</h3>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header">&nbsp;</div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes dermatológicos</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px;">Patología</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Presente</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Hace cuanto</label>
                            </div>
                        </div>

                        @foreach ($ant_dermatologicos as $ant_dermatologico)
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_dermatologico->patologia}}</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_dermatologico->presente}}</label>
                                </div>
                                <div class="col-md-4 ">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_dermatologico->hace_cuanto}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes osteomusculares</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px;">Patología</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Presente</label>
                            </div>
                            <div class="col-md-4">
                                <label style="font-size:16px;">Hace cuanto</label>
                            </div>
                        </div>

                        @foreach ($ant_osteomusculares as $ant_osteomuscular)
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_osteomuscular->patologia}}</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_osteomuscular->presente}}</label>
                                </div>
                                <div class="col-md-4 ">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_osteomuscular->hace_cuanto}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes quirúrgicos</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <label style="font-size:16px;">Antecedente</label>
                            </div>
                            <div class="col-md-6">
                                <label style="font-size:16px;">Fecha</label>
                            </div>
                        </div>

                        @foreach ($ant_quirurgicos as $ant_qui)
                            <div class="row">
                                <div class="col-md-6">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_qui->antecedente}}</label>
                                </div>
                                <div class="col-md-6">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_qui->fecha}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes traumáticos</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <label style="font-size:16px;">Lesión</label>
                            </div>
                            <div class="col-md-8">
                                <label style="font-size:16px;">Causa</label>
                            </div>
                        </div>

                        @foreach ($ant_traumaticos as $ant_trau)
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_trau->antecedente}}</label>
                                </div>
                                <div class="col-md-8">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_trau->causa}}</label>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Secuelas</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    {!!$historia_ocupacional->secuelas!!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Describa los medicamentos, drogas o vitaminas que toma en la actualidad
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p style="font-size:18px;">
                                    {!!$historia_ocupacional->mdv_act!!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Es usted alergico a algún medicamento, sustancia o comida?</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    {{$historia_ocupacional->alergico_algun_medicamento}}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Inmunizaciones</div>
                    <div class="panel-body">
                        @if(!$inmunizaciones->isEmpty())
                            <div class="row">
                                <div class="col-md-4">
                                    <label style="font-size:16px;">Inmunización</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size:16px;">Fecha</label>
                                </div>
                                <div class="col-md-4">
                                    <label style="font-size:16px;">Dosis</label>
                                </div>
                            </div>

                            @foreach ($inmunizaciones as $inmunizacion)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" style="padding: 3px 0;">
                                            <label style="font-size:16px; font-style: italic;">
                                                {{ $inmunizacion->inmunizacion }}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">Fecha:</label>
                                        <label style="font-size:16px; font-style: italic;"> {{ $inmunizacion->fecha }} </label>
                                    </div>
                                    <div class="col-md-4">
                                        <label style="font-size:16px; font-style: italic;">Dosis:</label>
                                        <label style="font-size:16px; font-style: italic;">
                                            {{ $inmunizacion->dosis }}</label>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Antecedentes gineco-obstétricos</div>
                    <div class="panel-body">
                        @if(!$ant_gineco->isEmpty())
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Menarquia:</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label style="font-size:16px; font-style: italic;">{{ $ant_gineco[0]->menarquia }}</label>
                                </div>
                                <div class="col-md-6 form-inline">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Ciclos:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->ciclos}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">FUM:</label>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->fecha_ultima_m}}</label>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Dismenorrea:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->dismenorrea}}</label>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-2">
                                    <label style="font-size:20px; font-style: italic;">Paridad</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">G:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_g}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">P:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_p}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">A:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_a}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">C:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_c}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">E:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_e}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">M:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_m}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group form-inline">
                                        <label style="font-size:16px; font-style: italic;">V:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->paridad_v}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">FPP:</label>
                                    </div>
                                </div>
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->FPP}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Planificación:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->planificacion}}</label>

                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Método:</label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->metodo}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Citología reciente:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->citologia_reciente}}</label>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Fecha:</label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->fecha_citologia}}</label>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Resultado:</label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$ant_gineco[0]->resultado_citologia}}</label>
                                    </div>
                                </div>
                            </div>
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Hábitos tóxicos</div>
                    <div class="panel-body">

                        @if(!$habitos_toxicos->isEmpty())

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Fumador:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->fumador}}</label>
                                    </div>
                                </div>


                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Ex fumador:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->exfumador}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Años de suspensión del
                                            hábito:</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->asuspension_h}}</label>
                                    </div>
                                </div>

                            </div>

                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Años de fumador:</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->adefumador}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Cigarrillos por día:</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->cigpordia}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Toma licor
                                            habitualmente?:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->tomalicorh}}</label>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Frecuencia del
                                            hábito:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->frecuenciahab}}</label>

                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Tipo de licor:</label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->tipolicor}}</label>
                                    </div>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">¿Ha tenido problemas con la
                                            bebida?:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->problemas_con_bebida}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">¿Exbebedor?:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->exbebedor}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Años suspendido:</label>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->exasuspendido}}</label>
                                    </div>
                                </div>

                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">¿Otros tóxicos?:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->otros_toxicos}}</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Medicamentos
                                            regularmente:</label>
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->med_regularmente}}</label>
                                    </div>
                                </div>

                                <div class="col-md-1">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">Cual:</label>
                                    </div>
                                </div>

                                <div class="col-md-4">
                                    <div class="form-group">
                                        <label style="font-size:16px; font-style: italic;">{{$habitos_toxicos[0]->cual_med_reg}}</label>
                                    </div>
                                </div>
                            </div>


                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Factores predisponentes para riesgo ergonómico</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Sedentarismo:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->sedentarismo}}</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Deportes de
                                        raqueta(Tenis/Squash):</label>
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->deportes_de_raqueta}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Sobrepeso:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->sobrepeso}}</label>
                                </div>
                            </div>

                            <div class="col-md-9">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Manualidades(Pintar, bordar,
                                        tejer, instrumento musical, bricolaje, etc):</label>
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->manualidades}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">Deporte:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->deporte}}</label>
                                </div>
                            </div>

                            <div class="col-md-1">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Cual:</label>
                                </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->cual_deporte}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Pasatiempos(Jardinería, coser,
                                        tejer, pintar, instrumento de música, manualidades, oficios domésticos):</label>
                                    <label style="font-size:16px; font-style: italic;">{{$factores_predisponentes[0]->pasatiempos}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Revisión por sistemas y enfermedad actual</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Sistema nervioso:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->sistema_nervioso}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Insomnio:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->insonmio}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Dificultad para
                                        concentrarse:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->d_para_concentrarse}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Desinteres para realizar
                                        actividades:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->desp_realizar_act}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Tensión muscular, cansancio,
                                        miedo:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->tmuscular}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Ojos, epifora, esfuerzo
                                        visual:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->ojos}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">ORL:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->orl}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Respiratorio(Disnea,
                                        hiperventilación, etc):</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->respiratorio}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Cardiovascular:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->cardiovascular}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Digestivo:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->digestivo}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group ">
                                    <label style="font-size:16px; font-style: italic;">Piel y faneras:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->piel_faneras}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Musculoesquelético:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->musculoesqueletico}}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">Genito urinario:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$revision_sistemas[0]->genito_urinario}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">En el último año ha tenido dolor o molestia a nivel de</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Patología</th>
                                        <th>Respuesta</th>
                                        <th>Descripción</th>
                                    </tr>
                                    @foreach ($molestias_u_a as $mol_u_a)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$mol_u_a->patologia}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$mol_u_a->presente}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$mol_u_a->descripcion}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Seguimiento y evolución de casos</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">{!!$historia_ocupacional->seg_evolucion_casos!!}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="panel panel-primary">
                    <div class="panel-heading">Examen físico</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">Constitución:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->constitucion}}</label>
                                </div>
                            </div>

                            <div class="col-md-5">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">Dominancia:</label>
                                    </label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->dominancia}}</label>
                                </div>
                            </div>


                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">T.A.:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->TA}}</label>
                                    <label style="font-size:16px; font-style: italic;">mmHg</label>
                                </div>
                            </div>

                            <div class="col-md-6">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">F.C.:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->frec_cardiaca}}</label>
                                    <label style="font-size:16px; font-style: italic;">x MIN</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">Peso:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->Peso}}</label>
                                    <label style="font-size:16px; font-style: italic;">Kg</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">Talla</label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->Talla}}</label>
                                    <label style="font-size:16px; font-style: italic;">mts</label>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group form-inline">
                                    <label style="font-size:16px; font-style: italic;">IMC:</label>
                                    <label style="font-size:16px; font-style: italic;">{{$examen_fisico[0]->IMC}}</label>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Detalle examen físico</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Organo</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($detalles_examen_fisico as $det_examen)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$det_examen->organo}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$det_examen->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$det_examen->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Descripción y ampliación de hallazgos</div>
                    <div class="panel-body">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-size:16px; font-style: italic;">{!!$historia_ocupacional->desc_ampl_hallazgos_ef!!}</label>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Inspección</div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Inspección</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($inspeccion as $insp)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$insp->inspeccion}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$insp->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$insp->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Palpación</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Palpación</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($palpacion as $palp)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$palp->palpacion}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$palp->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$palp->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Movilidad</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Movilidad</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($movilidad as $movil)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$movil->movilidad}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$movil->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$movil->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title" style="text-align: center;">Codos, mano, muñeca:</h3>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Nuca</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Descripción</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($nuca as $nu)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$nu->descripcion}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$nu->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$nu->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Hombros</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Descripción</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($hombros as $hom)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$hom->descripcion}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$hom->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$hom->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Codos, Mano, Muñeca</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Descripción</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($cod_mano_mun as $cmm)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$cmm->descripcion}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$cmm->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$cmm->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Descripción y ampliación de hallazgos</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">{!!$historia_ocupacional->desc_ampl_hallazgos_cv!!}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Evaluación del estado mental</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-bordered">
                                    <tr>
                                        <th>Proceso</th>
                                        <th>Estado</th>
                                        <th>Hallazgo</th>
                                    </tr>
                                    @foreach ($pro_ev as $pro)
                                        <tr>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$pro->proceso}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$pro->estado}}</label>
                                            </td>
                                            <td>
                                                <label style="font-size:16px; font-style: italic;">{{$pro->hallazgo}}</label>
                                            </td>
                                        </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Audiometría</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">{!!$historia_ocupacional->resultados_audiometria!!}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Examen visual</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">{!!$historia_ocupacional->examen_visual!!}</label>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label style="font-size:16px; font-style: italic;">¿Usa lentes?: </label>
                                    <label style="font-size:16px; font-style: italic;">{{$historia_ocupacional->usa_lentes}}</label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Otros paraclínicos</div>
                    <div class="panel-body">

                        @if(!$otros_paraclinicos->isEmpty())

                            @foreach ($otros_paraclinicos as $otro_par)
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Nombre del
                                                paraclínico:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$otro_par->nombre_paraclinico}}</label>
                                        </div>
                                    </div>

                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Fecha:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$otro_par->fecha}}</label>
                                        </div>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">Resultado:</label>
                                            <label style="font-size:16px; font-style: italic;">{{$otro_par->resultado}}</label>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Diagnósticos</div>
                    <div class="panel-body">

                        @if(!$diagnosticos->isEmpty())
                            @php
                                $ndiag = 1;
                            @endphp
                            @foreach ($diagnosticos as $diagnostico)
                                <div class="row">
                                    <div class="col-md-1">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">{{$ndiag}}.</label>
                                        </div>
                                    </div>

                                    <div class="col-md-11">
                                        <div class="form-group">
                                            <label style="font-size:16px; font-style: italic;">{{$diagnostico->diagnostico}}</label>
                                        </div>
                                    </div>
                                </div>
                                @php
                                    $ndiag++;
                                @endphp
                            @endforeach

                        @else
                            <div class="row">
                                <div class="col-md-12">
                                    <p>No se registraron datos</p>
                                </div>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Concepto de aptitud</div>
                    <div class="panel-body">

                        <div class="col-md-12">
                            <div class="form-group">
                                <label style="font-size:16px; font-style: italic;">{{$concepto_aptitud[0]->aptitud}}</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-size:16px; font-style: italic;">Examen de egreso normal:</label>
                                <label style="font-size:16px; font-style: italic;">{{$concepto_aptitud[0]->examen_egreso_normal}}</label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-group">
                                <label style="font-size:16px; font-style: italic;">Examen periódico
                                    satisfactorio:</label>
                                <label style="font-size:16px; font-style: italic;">{{$concepto_aptitud[0]->examen_p_satisfactorio}}</label>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Recomendaciones médicas</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    {!!$historia_ocupacional->recomendaciones_medicas!!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel panel-primary">
                    <div class="panel-heading">Ingreso PVESO</div>
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-md-4">
                                @foreach ($ingreso_pveso as $ingr_pv)
                                    <label style="font-size:16px;"> {{ $ingr_pv->tipo }}</label><br>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

            </div> <!-- cierre box-body -->


        </div>
    </div>

@endsection