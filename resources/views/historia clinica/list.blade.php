@extends('base.app')

@section('migas')
    <h1>Historia Clinica Ocupacional</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Historia Clinica Ocupacional</li>
        <li class="active">Lista Historias Clinicas Ocupacionales</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Historias Clinicas
                    Ocupacionales</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('reg_hist_clinica_oc', ['trabajador'=>$trabajador]) }}" class="btn btn-success"><i
                                class="fa fa-plus-circle"></i>&nbsp;Crear Registro</a>
                </div>
            </div>
            <div class="box-body">
                @if(session()->has('exito'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok"></span> {{ session('exito') }}
                    </div>
                @endif
                <table id="lista_historias_clinicas" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Tipo de historia</th>
                        <th>Fecha</th>
                        <th>Ciudad</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lista_historias_clinicas').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenar de manera Ascendente",
                        "sSortDescending": ": Ordenar de manera Descendente"
                    }
                },
                "ajax": {
                    "url": '{{ route("list_h_clinicas_ocupacionales_ajax",["id"=>$trabajador]) }}',
                    dataSrc: ''
                },
                "deferRender": false,
                "processing": false,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true,
                "paging": false,
                "info": false
            });
        });
    </script>
@endsection