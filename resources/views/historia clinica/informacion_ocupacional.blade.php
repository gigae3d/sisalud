<div id="informacion_ocupacional" class="box-body tab-pane fade in active">
    <div class="box-header with-border">
        <h3 class="box-title">Información General Historia Clínica</h3>
    </div>

    <div class="box-body">
        <div class="col-md-6">
            <label>Fecha</label>
            <div class="input-group date">
                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                <input type="text" class="form-control" id="fecha_historia" name="fecha_historia" required>
            </div>
        </div>

        <div class="col-md-6">
            <label>Ciudad</label>
            <input type="text" class="form-control" name="ciudad_historia" required>
        </div>

        <div class="col-md-12" style="margin-top: 15px">
            <div class="row">
                <div class="col-md-3 col-xs-12 ">
                    <label>Tipo:</label>
                </div>
                <div class="col-xs-6 col-md-2">
                    <input type="radio" name="tipo_historia" value="Ingreso" required>
                    Ingreso
                </div>
                <div class="col-xs-6 col-md-2">
                    <input type="radio" name="tipo_historia" value="Retiro" required>
                    Retiro
                </div>
                <div class="col-xs-6 col-md-2">
                    <input type="radio" name="tipo_historia" value="Periodico" required>
                    Periodico
                </div>
                <div class="col-xs-6 col-md-3">
                    <input type="radio" name="tipo_historia" value="Postincapacidad" required>
                    Postincapacidad
                </div>
            </div>
        </div>
    </div>

    <div class="box-header with-border">
        <h3 class="box-title">Información Ocupacional</h3>
    </div>

    <div class="box-body">
        <div class="col-md-6">
            <div class="form-group">
                <label>Antiguedad en la empresa:</label>
                <input type="number" class="form-control" id="antiguedad" name="antiguedad_en_empresa"
                       placeholder="En meses" required>
            </div>
            <div class="form-group">
                <label>Nombre del cargo actual o a desempeñar:</label>
                <input type="text" class="form-control" id="nombre_cargo_actual"
                       name="nombre_cargo_actual" placeholder="Cargo actual o a desempeñar">
            </div>
            <div class="form-group">
                <label>Antiguedad en cargo:</label>
                <input type="number" class="form-control" id="antiguedad_en_cargo" name="antiguedad_en_cargo"
                       placeholder="Escriba cuanto lleva en el cargo actual">
            </div>
            <div class="form-group">
                <label>Sección:</label>
                <input type="text" class="form-control" id="seccion" name="seccion"
                       placeholder="Sección en la que labora">
            </div>
            <div class="form-group">
                <label>Turno: </label>
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <input type="radio" name="turno" value="Diurno" required>
                        Diurno
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="radio" name="turno" value="Nocturno" required>
                        Nocturno
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="radio" name="turno" value="Rotatorio" required>
                        Rotatorio
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label>Descripción del cargo:</label>
                <input type="text" class="form-control" id="descripcion_del_cargo" name="descripcion_del_cargo"
                       placeholder="Describa brevemente el cargo">
            </div>
            <div class="form-group">
                <label>Las actividades las realiza:</label>
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="actividades_que_realiza[]" value="De pie">
                        De pie
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="actividades_que_realiza[]" value="Sentado">
                        Sentado
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="actividades_que_realiza[]" value="Caminando">
                        Caminando
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="actividades_que_realiza[]" value="Arrodillado">
                        Arrodillado
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="actividades_que_realiza[]" value="Inclinado">
                        Inclinado
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" id="otra_actividad">
                        Otro
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="campo_otra_actividad" name="" class="form-control">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Herramientas utilizadas:</label>
                <input type="text" class="form-control" id="equipos_herramientas_utilizadas"
                       name="equipos_herramientas_utilizadas" placeholder="Herramientas que usa en el trabajo">
            </div>
            <div class="form-group">
                <label>Materias primas o insumos usados:</label>
                <input type="text" class="form-control" id="materias_primas_usadas" name="materias_primas_usadas"
                       placeholder="Materias primas o insumos usados">
            </div>
            <div class="form-group">
                <label>Acciones que realiza:</label>
                <div class="row">
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="acciones_que_realiza[]" value="Alcanzar">
                        Alcanzar
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="acciones_que_realiza[]" value="Halar">
                        Halar
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="acciones_que_realiza[]" value="Empujar">
                        Empujar
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="acciones_que_realiza[]" value="Levantar">
                        Levantar
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" name="acciones_que_realiza[]" value="Arrastrar">
                        Arrastrar
                    </div>
                    <div class="col-xs-6 col-md-4">
                        <input type="checkbox" id="otra_accion" value="">
                        Otro
                    </div>
                    <div class="col-md-12">
                        <input type="text" id="campo_otra_accion" name="" class="form-control">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>