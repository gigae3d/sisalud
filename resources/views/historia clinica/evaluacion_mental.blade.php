<div id="evaluacion_mental" class="box-body tab-pane fade">

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Evaluacion del estado mental</h3>
              </div>

              <div class="col-md-12">
                    <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Procesos</label>
                    </div>
              </div>

              <div class="row">
                        <div class="col-md-offset-2 col-md-1">
                        <label >
                        <input type="radio" id="Normal_PR" name="selTodosPR" value="">
                        
                        </label>
                        </div>

                        <div class="col-md-1">
                        <label > 
                        <input type="radio" id="Disfuncion_PR" name="selTodosPR" value="">
                        
                        </label>
                        </div>

              </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Orientación</label>
                        <input type="hidden" name="prp[0]" value="Orientacion">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="prv[0]" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">
                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="prv[0]" value="Disfuncion">
                        Disfunción
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="prhl[0]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Atención concentración</label>
                         <input type="hidden" name="prp[1]" value="Atención concentracion">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="prv[1]" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">

                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="prv[1]" value="Disfuncion">
                        Disfunción
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="prhl[1]">
                    </div>    
               </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Sensopercepción</label>
                         <input type="hidden" name="prp[2]" value="Sensopercepción">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="prv[2]" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">

                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="prv[2]" value="Disfuncion">
                        Disfunción
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="prhl[2]">
                    </div>    
               </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Memoria</label>
                         <input type="hidden" name="prp[3]" value="Memoria">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="prv[3]" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">

                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="prv[3]" value="Disfuncion">
                        Disfunción
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="prhl[3]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Pensamiento</label>
                         <input type="hidden" name="prp[4]" value="Pensamiento">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="prv[4]" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">

                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="prv[4]" value="Disfuncion">
                        Disfunción
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="prhl[4]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Lenguaje</label>
                        <input type="hidden" name="prp[5]" value="Lenguaje">
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="prv[5]" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">

                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="prv[5]" value="Disfuncion">
                        Disfunción
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="prhl[5]">
                    </div>    
                </div>

              <div class="row">
                    <div class="col-md-2">
                        <label style="font-size:16px; font-style: italic;">Concepto</label>
                    </div>
                    
                    <div class="col-md-1">    
                        <label class="radio-inline"> 
                        <input type="radio" class="Normal_PR" name="concepto_PR" value="Normal">
                        Normal
                        </label>
                    </div>
                    <div class="col-md-2">

                        <label class="radio-inline"> 
                        <input type="radio" class="Disfuncion_PR" name="concepto_PR" value="Anormal">
                        Anormal
                        </label>
                    </div>
                    <div class="col-md-4 form-inline">
                        <label style="font-size:16px; font-style: italic;">Hallazgo</label>
                        <input type="text" class="form-control" name="hallazgo_conc_PR">
                    </div>    
                </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Audiometría dB</h3>
              </div>

              <div class="col-md-12">
                      <h3 class="box-title">Resultado:
                      </h3> 
                    <div class="box-body pad">
                        <textarea id="descripcion_ra" name="resultado_audiometria" rows="10" cols="80">
                            Descripcion
                        </textarea>
              </div>
              </div>

              <div class="col-md-12 box-header with-border">
              <h3 class="box-title" style="text-align: center;">Examen visual</h3>
              </div>

              <div class="row">
              <div class="col-md-12">
                      <h3 class="box-title">Resultado:
                      </h3> 
                    <div class="box-body pad">
                        <textarea id="descripcion_rev" name="resultado_examen_visual" rows="10" cols="80">
                            Descripcion
                        </textarea>
                    </div>
              </div>
              </div>

              <div class="row">
              <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">¿Usa lentes?</label>
                        <label class="radio-inline"> 
                        <input type="radio" name="usa_lentes" value="Si">
                        Si
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="usa_lentes" value="No">
                        No
                        </label>
                    </div>
              </div>
              </div>

        <div class="paraclinicos">
        <div class="row">
        <div class="col-md-12 box-header with-border">
        <h3 class="box-title" style="text-align: center;">Otros paraclínicos</h3>
        </div>
        </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
            <button id="add_paraclinico" type="button" class="btn btn-primary">Añadir</button>
            <button id="remove_paraclinico" type="button" class="btn btn-danger">Quitar</button>
            </div>
        </div>
        
        <div class="diagnosticos">
        <div class="row">
        <div class="col-md-12 box-header with-border">
        <h3 class="box-title" style="text-align: center;">Diagnósticos</h3>
        </div>
        </div>
        </div>

        <div class="row">
            <div class="col-md-12" style="text-align: center;">
            <button id="add_diagnostico" type="button" class="btn btn-primary">Añadir</button>
            <button id="remove_diagnostico" type="button" class="btn btn-danger">Quitar</button>
            </div>
        </div>

</div>