<div id="antecedentes_laborales" class="box-body tab-pane fade">
    <div class="box-header with-border">
        <h3 class="box-title">Exposición a factores de riesgo y antecedentes laborales</h3>
    </div>
    <div class="col-md-12" style="height: 25px">
        <button id="add_exp_riesgo_laboral" type="button" class="btn btn-link  pull-right"><i
                    class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
        </button>
        <button id="remove_exp_riesgo_laboral" type="button"
                class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                aria-hidden="true"></i> Quitar
        </button>
    </div>

    <div class="col-md-12 with-border exp_riesgo_laborales"></div>

    <div class="col-md-12 box-header with-border">
        <h3 class="box-title">Uso de elementos de protección personal</h3>
    </div>

    <div class="col-md-6" style="margin-top: 15px">
        <div class="row">
            <div class="col-xs-12 col-md-8">
                <label>En el cargo o empresa anterior(Ingreso-egreso):</label>
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" name="cargo_empresa_anterior_ep" value="Si">&nbsp;&nbsp;Si
            </div>
            <div class="col-md-2 col-xs-6">
                <input type="radio" name="cargo_empresa_anterior_ep" value="No">&nbsp;&nbsp;No
            </div>
        </div>
    </div>

    <div class="col-md-6" style="margin-top: 15px">
        <div class="row">
            <div class="col-xs-12 col-md-4">
                <label>Examen periódico:</label>
            </div>
            <div class="col-md-4 col-xs-6">
                <input type="radio" name="examen_periodico_ep" value="Si" required>&nbsp;&nbsp;Si
            </div>
            <div class="col-md-4 col-xs-6">
                <input type="radio" name="examen_periodico_ep" value="No" required>&nbsp;&nbsp;No
            </div>
        </div>
    </div>

    <div class="col-md-12" style="margin: 15px 0">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <label>Elementos:</label>
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Casco">&nbsp;&nbsp;Casco
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Gafas">&nbsp;&nbsp;Gafas
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Tapaboca">&nbsp;&nbsp;Tapaboca
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Protectores auditivos">&nbsp;&nbsp;Protectores
                auditivos
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Respirador">&nbsp;&nbsp;Respirador
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Overol">&nbsp;&nbsp;Overol
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Guantes">&nbsp;&nbsp;Guantes
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" name="elementos_pr[]" value="Botas">&nbsp;&nbsp;Botas
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="checkbox" id="otro_elemento">&nbsp;&nbsp;Otros
            </div>
            <div class="col-md-3 col-xs-6">
                <input type="text" id="campo_otro_elemento" class="form-control" name="valor_otro_elemento">
            </div>
        </div>
    </div>

    <div class="col-md-12 box-header with-border">
        <h3 class="box-title">Antecedentes de accidentes de trabajo</h3>
    </div>

    <div class="col-md-12" style="margin-top: 15px">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <label>Ha sufrido accidentes de trabajo?</label>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <input type="radio" name="ha_sufrido_at" value="Si">&nbsp;&nbsp;Si
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <input type="radio" name="ha_sufrido_at" value="No">&nbsp;&nbsp;No
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <button id="add_ant_acci" type="button" class="btn btn-link  pull-right"><i
                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                </button>
                <button id="remove_ant_acci" type="button"
                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                        aria-hidden="true"></i> Quitar
                </button>
            </div>
        </div>
    </div>

    <div class="col-md-12 with-border ant_accidentes_trabajo"></div>

    <div class="col-md-12 box-header with-border">
        <h3 class="box-title">Enfermedad profesional</h3>
    </div>

    <div class="col-md-12" style="margin-top: 15px">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-md-8 col-xs-12">
                        <label>Ha sufrido enfermedad profesional?:</label>
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <input type="radio" name="ha_sufrido_ep" value="Si">&nbsp;&nbsp;Si
                    </div>
                    <div class="col-md-2 col-xs-6">
                        <input type="radio" name="ha_sufrido_ep" value="No">&nbsp;&nbsp;No
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <button id="add_enf_profesional" type="button" class="btn btn-link  pull-right"><i
                            class="fa fa-plus-circle" aria-hidden="true"></i> Añadir
                </button>
                <button id="remove_enf_profesional" type="button"
                        class="btn btn-link  pull-right  margin-r-5"><i class="fa fa-minus-circle"
                                                                        aria-hidden="true"></i> Quitar
                </button>
            </div>
        </div>
    </div>

    <div class="col-md-12 with-border enf_profesional"> </div>
</div>