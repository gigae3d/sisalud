@extends('base.app')

@section('migas')
    <h1>Historia Clinica Ocupacional</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Historia Clinica Ocupacional</li>
        <li class="active">Editar Trabajador</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="col-md-12 box-header with-border">
                <h3 class="box-title">Crear Trabajador</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('edit_trabajador') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" name="id" value="{{ $trabajador->id }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Número de identificación:</label>
                            <input type="text" class="form-control" id="numero_de_identificacion"
                                   name="numero_de_identificacion" value="{{ $trabajador->numero_de_identificacion }}"
                                   disabled>
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Nombres:</label>
                            <input type="text" class="form-control" id="nombres"
                                   name="nombres" value="{{ $trabajador->nombres }}">
                        </div>
                        <div class="form-group">
                            <label>Apellidos:</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"
                                   value="{{ $trabajador->apellidos }}">
                        </div>
                        <div class="form-group">
                            <label>Fecha de nacimiento:</label>
                            <div class="input-group date">
                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                <input type="text" class="form-control" id="fecha_de_nacimiento"
                                       name="fecha_nacimiento" value="{{ $trabajador->fecha_nacimiento }}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Género:</label>
                            <select class="form-control" name="genero">
                                <option value="{{ $trabajador->genero }}">
                                    @if($trabajador->genero == 'M')
                                        Masculino
                                    @else
                                        Femenino
                                    @endif
                                </option>
                                <option value="M">Masculino</option>
                                <option value="F">Femenino</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Edad:</label>
                            <input type="number" class="form-control" name="edad" value="{{ $trabajador->edad }}">
                        </div>
                        <div class="form-group">
                            <label>Lugar de nacimiento:</label>
                            <input type="text" class="form-control" name="lugar_nacimiento"
                                   value="{{ $trabajador->lugar_nacimiento }}">
                        </div>
                        <div class="form-group">
                            <label>Procedencia:</label>
                            <input type="text" class="form-control" name="procedencia"
                                   value="{{ $trabajador->procedencia }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Residencia:</label>
                            <input type="text" class="form-control" name="residencia"
                                   value="{{ $trabajador->residencia }}">
                        </div>
                        <div class="form-group">
                            <label>Profesión u oficio:</label>
                            <input type="text" class="form-control" name="prof_oficio"
                                   value="{{ $trabajador->prof_oficio }}">
                        </div>
                        <div class="form-group">
                            <label>Pensiones:</label>
                            <input type="text" class="form-control" name="pensiones"
                                   value="{{ $trabajador->pensiones }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>EPS:</label>
                            <input type="text" class="form-control" name="eps" value="{{ $trabajador->eps }}">
                        </div>
                        <div class="form-group">
                            <label>ARP Anterior:</label>
                            <input type="text" class="form-control" name="arp_anterior"
                                   value="{{ $trabajador->arp_anterior }}">
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-xs-12 col-md-2">
                                <label>Escolaridad: </label>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="escolaridad" value="Analfabeta" required>
                                Analfabeta
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="escolaridad" value="Primaria" required>
                                Primaria
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="escolaridad" value="Secundaria" required>
                                Secundaria
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="escolaridad" value="Tecnico" required>
                                Técnico
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="escolaridad" value="Universidad" required>
                                Universidad
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12" style="margin-top: 15px">
                        <div class="row">
                            <div class="col-xs-12 col-md-2">
                                <label>Estado civil: </label>
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="estado_civil" value="Solter@" required>
                                Solter@
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="estado_civil" value="Casad@" required>
                                Casad@
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="estado_civil" value="Union Libre" required>
                                Unión Libre
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="estado_civil" value="Viud@" required>
                                Viud@
                            </div>
                            <div class="col-xs-4 col-md-2">
                                <input type="radio" name="estado_civil" value="Separad@" required>
                                Separad@
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <a class="btn btn-primary pull-right margin-r-5"
                           href="{{ route('view_trabajadores') }}">
                            <i class="fa fa-ban" aria-hidden="true"></i>
                            Cancelar
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#fecha_de_nacimiento').datepicker({
                startDate: '-285d',
                endDate: '0d',
                autoclose: true
            });
        });
    </script>
@endsection