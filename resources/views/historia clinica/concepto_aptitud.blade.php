<div id="concepto_aptitud" class="box-body tab-pane fade">

	    <div class="col-md-12 box-header with-border">
        <h3 class="box-title" style="text-align: center;">Concepto de aptitud</h3>
        </div>

        <div class="col-md-12">
                    <div class="form-group">
                        <label class="radio-inline"> 
                        <input type="radio" name="concepto_aptitud" value="Apto">
                        Apto
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="concepto_aptitud" value="No Apto">
                        No Apto
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="concepto_aptitud" value="Apto para el cargo con restricciones para su labor">
                        Apto para el cargo con restricciones para su labor
                        </label>

                        <label class="radio-inline">
                        <input type="radio" name="concepto_aptitud" value="Requiere nueva valorizacion">
                        Requiere nueva valorización
                        </label>
                    </div>
        </div>

        <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Examen de egreso normal</label>
                        <label class="radio-inline"> 
                        <input type="radio" name="examen_enormal" value="Si">
                        Si
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="examen_enormal" value="No">
                        No
                        </label>
                    </div>
        </div>

        <div class="col-md-6">
                    <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Examen periódico satisfactorio</label>
                        <label class="radio-inline"> 
                        <input type="radio" name="epsatisfactorio" value="Si">
                        Si
                        </label>

                        <label class="radio-inline"> 
                        <input type="radio" name="epsatisfactorio" value="No">
                        No
                        </label>
                    </div>
        </div>

        <div class="col-md-12">
                      <h3 class="box-title">Recomendaciones médicas:
                      </h3> 
                    <div class="box-body pad">
                        <textarea id="recomendaciones_medicas" name="recomendaciones_medicas" rows="10" cols="80">
                            Descripcion
                        </textarea>
                   </div>
        </div>

        <div class="col-md-12">
                    <div class="form-group">
                        <label style="font-size:16px; font-style: italic;">Ingreso PVESO</label>
                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Visual">
                        Visual
                        </label>

                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Auditivo">
                        Auditivo
                        </label>

                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Respiratorio">
                        Respiratorio
                        </label>

                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Ergonómico">
                        Ergonómico
                        </label>

                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Sust. Quimicas">
                        Sust. Quimicas
                        </label>

                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Cardiovascular">
                        Cardiovascular
                        </label>

                        <label class="checkbox-inline"> 
                        <input type="checkbox" name="ingreso_pveso[]" value="Psicosocial">
                        Psicosocial
                        </label>
                    </div>
        </div>

        <div class="col-md-12">
                    <div class="form-group">
                        <label class="checkbox-inline"> 
                        <input type="checkbox" id="otro_ing_pveso" value="">
                        Otro
                        </label>
                        <input type="text" class="form-control" id="campo_ing_pveso" name="otro_ing_pveso">
                    </div>
        </div>


</div>