@extends('base.app')

@section('migas')
    <h1>Planificación Familiar</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Planificación Familiar</li>
        <li>Asesorias en Salud Sexual</li>
        <li class="active">Crear Asesoría en salud sexual</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="col-md-12 box-header with-border">
                <h3 class="box-title">Crear Registro de Asesoría en Salud Sexual</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_asesoria_se') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body" id="nofountId" style="display: none">
                    <div class="col-md-12">
                        <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                        aria-hidden="true">&times;</span></button>
                            <strong>Importante!</strong> El Número de identificación, No esta registrado en la Base de
                            Datos, por favor, primero registra el paciente en
                            <a href="{{ route('new_paciente')}}" class="btn btn-link">Aqui</a>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fecha:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="fecha"
                                       name="fecha" value="{{ date('m/d/Y')}}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Número de identificación:</label>
                            <input type="text" class="form-control" id="numero_de_identificacion"
                                   name="numero_de_identificacion" placeholder="Cédula, Tarjeta de identidad">
                            <!-- /.input group -->
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombres:</label>
                            <input type="text" class="form-control" id="nombres" name="nombres" placeholder="Nombres">
                        </div>
                        <div class="form-group">
                            <label>Apellidos:</label>
                            <input type="text" class="form-control" id="apellidos" name="apellidos"
                                   placeholder="Apellidos">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Asesoría:</label>
                            <input type="text" class="form-control" id="asesoria" name="asesoria"
                                   placeholder="Asesoria">
                        </div>
                    </div>
                    <!-- /.box -->
                    <!-- /.box-body -->
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right"><i class="fa fa-floppy-o"
                                                                                    aria-hidden="true"></i> Guardar
                        </button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_asesorias_se') }}"><i
                                    class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                    <!-- /.box-footer -->
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {

            //Date picker
            $('#fecha').datepicker({
                startDate: '-285d',
                endDate: '0d',
                autoclose: true
            });

            $('#numero_de_identificacion').focus(function () {
                $("#nofountId").hide();
            });

            //Evento para mostrar el nombre y apellido del paciente al escribir su CC
            $('#numero_de_identificacion').blur(function () {
                $.ajax({
                    method: "GET",
                    url: "{{ route('get_nomape_paciente') }}",
                    data: {numero_de_identificacion: $('#numero_de_identificacion').val()},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        if (json.nombres == '-' && json.apellidos == '-') {
                            $("#nofountId").show();
                            $('#numero_de_identificacion').val('');
                            $('#nombres').val('');
                            $('#apellidos').val('');
                        } else {
                            $('#nombres').val(json.nombres);
                            $('#apellidos').val(json.apellidos);
                        }
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            });
        });
    </script>
@endsection