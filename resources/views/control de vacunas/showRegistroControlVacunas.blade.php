@extends('base.app')

@section('migas')
    <h1>Control de Vacunas</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Control de vacunas</li>
        <li class="active">Registro Control de Vacunas</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="col-md-12 box-header with-border">
                <h3 class="box-title"></h3>
                Registro datos personales y vacunas de estudiantes de internado rotatorio
            </div>
            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Numero de identificación: <label> {{ $estudiante->numero_de_identificacion }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Expedida en: <label> {{ $estudiante->d_expedida_en }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Tipo: <label> {{ $estudiante->tipo }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        Apellidos: <label> {{ $estudiante->apellidos }}  </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        Nombres: <label> {{ $estudiante->nombres }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        Pregrado de: <label> {{ $estudiante->pregrado }}  </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        Semestre: <label> {{ $estudiante->semestre }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Fecha de nacimiento: <label> {{ $estudiante->fecha_de_nacimiento }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Lugar de nacimiento: <label> {{ $estudiante->lugar_de_nacimiento }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Género: <label> {{ $estudiante->genero }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Grupo sanguíneo: <label> {{ $estudiante->grupo_sanguineo }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        RH: <label> {{ $estudiante->RH }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Dirección: <label> {{ $estudiante->direccion }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Barrio: <label> {{ $estudiante->barrio }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Ciudad: <label> {{ $estudiante->ciudad }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Teléfono fijo <label> {{ $estudiante->telefono_fijo }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Celular: <label> {{ $estudiante->celular }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Correo electrónico: <label> {{ $estudiante->correo_electronico }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-12 box-header with-border">
                    <h3 class="box-title">Antecedentes de salud personal</h3>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Enfermedades previas: <label> {{ $estudiante->enfermedades_previas }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Antecedentes alérgicos: <label> {{ $estudiante->antecedentes_alergicos }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Medicamentos que toma: <label> {{ $estudiante->medicamentos_tomados }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-12 box-header with-border">
                    <h3 class="box-title">Esquema de vacunas</h3>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <label>Vacuna</label>
                </div>
                <div class="col-md-4">
                    <label>Numero de dosis aplicadas</label>
                </div>
                <div class="col-md-4">
                    <label>Fecha de la última dosis</label>
                </div>
            </div>

            @foreach ($esquemas_vacunas as $vacuna)

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ $vacuna->vacuna }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ $vacuna->n_dosis_aplicadas }}
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            {{ $vacuna->fecha_ultima_dosis }}
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="box-body">
                <div class="col-md-12">
                    <div class="form-group">
                        <label>Prueba de PPD</label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <div class="form-group">
                        Resultado: <label> {{ $estudiante->pr_ppd_resultado }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Fecha: <label> {{ $estudiante->pr_ppd_fecha }}  </label>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        Laboratorio: <label> {{ $estudiante->pr_ppd_laboratorio }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-12 box-header with-border">
                    <h3 class="box-title">Titulación de anticuerpos</h3>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-3">
                    <label>Anticuerpos</label>
                </div>
                <div class="col-md-3">
                    <label>Resultado</label>
                </div>
                <div class="col-md-3">
                    <label>Laboratorio</label>
                </div>
                <div class="col-md-3">
                    <label>Fecha</label>
                </div>
            </div>

            @foreach ($titulacion_anticuerpos as $titant)
                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ $titant->anticuerpos }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ $titant->resultado }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ $titant->laboratorio }}
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            {{ $titant->fecha }}
                        </div>
                    </div>
                </div>
            @endforeach

            <div class="box-body">
                <div class="col-md-12 box-header with-border">
                    <h3 class="box-title">Sistema de aseguramiento</h3>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-6">
                    <div class="form-group">
                        Seguridad Social (EPS): <label> {{ $estudiante->EPS }}  </label>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        Conoce el reglamento de prácticas?: <label> {{ $estudiante->conoce_regl_pr }}  </label>
                    </div>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-6">
                    <label>En caso de emergencia avisar a:</label>
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    Nombres y apellidos:
                </div>
                <div class="col-md-4">
                    Dirección:
                </div>
                <div class="col-md-4">
                    Teléfono:
                </div>
            </div>

            <div class="box-body">
                <div class="col-md-4">
                    <label> {{ $estudiante->eme_nomyape }}  </label>
                </div>
                <div class="col-md-4">
                    <label> {{ $estudiante->eme_direccion }}  </label>
                </div>
                <div class="col-md-4">
                    <label> {{ $estudiante->eme_telefono }}  </label>
                </div>
            </div>
            <div class="box-body">
                <div class="col-md-12 box-footer">
                    <a class="btn btn-primary pull-right margin-r-5"
                       href="{{ route('view_registro_est') }}">
                        <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
                        Regresar
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection