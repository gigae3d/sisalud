@extends('base.app')

@section('migas')
<h1>
Módulo de Control de Vacunas
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Control de vacunas</li>
</ol>
@endsection

@section('contenido')

<div class="col-xs-12 col-md-12">
	<div class="box box-success">
        <div class="box-header with-border">
          	<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Estudiantes registrados</h3>
			{{--<div class="box-tools pull-right">
		        <a href="{{ route('new_reg_control_vacuna')}}" class="btn btn-success"><i class="fa fa-list"></i>&nbsp;Crear nuevo registro</a>
	      	</div>--}}
		</div>
        <div class="box-body">

	

        <h3 class="box-title">Filtrar estudiantes
		</h3>

     <form role="form" action="{{ route('filtrar_estudiantes') }}" method="POST">

         <input type="hidden" name="_token" value="{{ csrf_token() }}">

        <div class="row">
         <div class="col-md-3">
         <div class="form-group">
         <label style="font-size:16px; font-style: italic;">Programa:</label>

         <select class="form-control" id="programa" name="programa">
	                  	  <option value=""></option>
                          <option value="Enfermeria">Enfermeria</option>
                          <option value="Medicina">Medicina</option>
	                  	</select>
         </div>
         </div>

         <div class="col-md-3">
         	<div class="form-group">
            <label style="font-size:16px; font-style: italic;">Semestre:</label>

            <div class="form-inline">

            <select class="form-control" name="semestre">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
					<option value="11">11</option>
					<option value="12">12</option>
            </select>
            <button type="submit" class="btn btn-info pull-right">Consultar</button>
            </div>

            </div>

         </div>
         </div>

     </form>

			<table id="lista_estudiantes_reg" class="table table-bordered table-striped" width="100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Pregrado</th>
						<th>Semestre</th>
						<th>Fecha</th>
						<th>Celular</th>
						<th>Correo</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('#lista_estudiantes_reg').DataTable( {
			"language": {
			    "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			},
	        "ajax": '{{ route("list_estudiantes_reg_ajax") }}',
	        "deferRender":    false,
	        "processing": 	  true,
	        "scrollCollapse": true,
	        "scrollX":       true,
	        "scroller":       true,
	        "stateSave":      true,
          	"serverSide": 	true
	    } );
	});
</script>

@endsection