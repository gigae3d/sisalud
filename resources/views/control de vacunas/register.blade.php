@extends('base.app')

@section('migas')
    <h1>Control de Vacunas</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Control de vacunas</li>
        <li class="active">Registro Control de Vacunas</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="col-md-12 box-header with-border">
                <h3 class="box-title"></h3>
                Registro datos personales y vacunas de estudiantes
            </div>
            <form role="form" action="{{ route('create_reg_control_vacuna') }}" method="POST"
                  enctype="multipart/form-data">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Numero de identificación</label>
                            <input type="number" class="form-control" name="numero_de_identificacion"
                                   placeholder="Cédula, tarjeta de identidad">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Expedida en:</label>
                            <input type="text" class="form-control" name="exped_identificacion"
                                   placeholder="Lugar de expedicion del documento">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Tipo:</label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_identificacion" value="CC">
                                        CC
                                    </label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_identificacion" value="CE">
                                        CE
                                    </label>
                                </div>
                                <div class="col-md-4 col-xs-4">
                                    <label class="radio-inline">
                                        <input type="radio" name="tipo_identificacion" value="TI">
                                        TI
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Apellidos</label>
                            <input type="text" class="form-control" name="apellidos" placeholder="Apellidos">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombres</label>
                            <input type="text" class="form-control" name="nombres" placeholder="Nombres">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Pregrado de:</label>
                            <select class="form-control" name="pregrado">
                                @if ($programas)
                                    <option value="">- Seleccione -</option>
                                    @foreach($programas as $programa)
                                        <option value="{{ $programa->id }}">{{ $programa->name }}</option>
                                    @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Semestre</label>
                            <select class="form-control" name="semestre">
                                <option value="">- Seleccione -</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha de nacimiento</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="fecha_de_nacimiento" class="form-control"
                                       name="fecha_de_nacimiento">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Lugar de nacimiento</label>
                            <input type="text" class="form-control" name="lugar_nacimiento"
                                   placeholder="Ciudad, departamento y país">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Género:</label>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="genero" value="Masculino">
                                        Masculino
                                    </label>
                                </div>
                                <div class="col-md-6 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="genero" value="Femenino">
                                        Femenino
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Grupo sanguíneo</label>
                            <input type="text" class="form-control" name="grupo_sanguineo">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>RH</label>
                            <input type="text" class="form-control" name="rh">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Direccion</label>
                            <input type="text" class="form-control" name="direccion" placeholder="Dirección">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Barrio</label>
                            <input type="text" class="form-control" name="barrio" placeholder="Barrio">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Ciudad</label>
                            <input type="text" class="form-control" name="ciudad" placeholder="Ciudad">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Teléfono fijo</label>
                            <input type="number" class="form-control" name="telefono_fijo"
                                   placeholder="Cédula, tarjeta de identidad">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Celular</label>
                            <input type="number" class="form-control" name="celular"
                                   placeholder="Cédula, tarjeta de identidad">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Correo electrónico</label>
                            <input type="email" class="form-control" name="correo_electronico"
                                   placeholder="YEscriba su correo electrónico por favor">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-header with-border" style="margin-top: 15px">
                        <h3 class="box-title">Antecedentes de salud personal</h3>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Enfermedades previas</label>
                            <input type="text" class="form-control" name="enfermedades_previas"
                                   placeholder="Enfermedades previas">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Antecedentes alérgicos</label>
                            <input type="text" class="form-control" name="ant_alergicos">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Medicamentos que toma</label>
                            <input type="text" class="form-control" name="med_tomados"
                                   placeholder="Medicamentos que toma">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title">Esquema de vacunas</h3>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <label>Vacuna</label>
                    </div>
                    <div class="col-md-4">
                        <label>Numero de dosis aplicadas</label>
                    </div>
                    <div class="col-md-4">
                        <label>Fecha de la última dosis</label>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Varicela</label>
                            <input type="hidden" name="vacuna[0]" value="Varicela">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <input type="checkbox" id="aplicaVaricela">
                            <label>Tiene anticuerpos</label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <input type="number" id="dosisVaricela" class="form-control" name="n_dosis_aplicadas[0]" required>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="fechaVaricela" class="form-control fechas_vacunas" name="fechas_ultima_dosis[0]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Hepatitis A</label>
                            <input type="hidden" name="vacuna[1]" value="Hepatitis A">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[1]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[1]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Hepatitis B</label>
                            <input type="hidden" name="vacuna[2]" value="Hepatitis B">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[2]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[2]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>MMR (Papera, rubeola,
                                sarampión</label>
                            <input type="hidden" name="vacuna[3]" value="MMR (Papera, rubeola, sarampion)">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[3]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[3]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Tétanos-Difteria</label>
                            <input type="hidden" name="vacuna[4]" value="Tétanos-Difteria">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[4]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[4]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Influenza</label>
                            <input type="hidden" name="vacuna[5]" value="Influenza">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[5]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[5]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Neumococo</label>
                            <input type="hidden" name="vacuna[6]" value="Neumococo">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[6]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[6]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Meningitis</label>
                            <input type="hidden" name="vacuna[7]" value="Meningitis">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[7]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[7]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>DPTa
                                <input type="hidden" name="vacuna[8]" value="DPTa"></label>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <input type="number" class="form-control" name="n_dosis_aplicadas[8]" required>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control fechas_vacunas" name="fechas_ultima_dosis[8]" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title">Prueba de PPD</h3>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Resultado:</label>
                            <input type="text" class="form-control" name="resultado_ppd"
                                   placeholder="Resultado PPD">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Fecha:</label>
                            <div class="input-group date">
                                <div class="input-group-addon">
                                    <i class="fa fa-calendar"></i>
                                </div>
                                <input type="text" id="fecha_ppd" class="form-control" name="fecha_ppd" required>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Laboratorio:</label>
                            <input type="text" class="form-control" name="laboratorio_ppd"
                                   placeholder="Laboratorio">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title">Titulación de Anticuerpos</h3>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-3">
                        <label>Anticuerpos</label>
                    </div>
                    <div class="col-md-3">
                        <label>Varicela</label>
                        <input type="hidden" name="anticuerpos[0]" value="Varicela">
                        <!--<div class="form-group">
                            <input type="checkbox" id="aplicaVaricelaAnti">
                            <label>Tiene anticuerpos</label>
                        </div>-->
                    </div>
                    <div class="col-md-3">
                        <label>Hepatitis A</label>
                        <input type="hidden" name="anticuerpos[1]" value="Hepatitis A">
                    </div>
                    <div class="col-md-3">
                        <label>Hepatitis B</label>
                        <input type="hidden" name="anticuerpos[2]" value="Hepatitis B">
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Resultado:</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="resulVaricelaAnti" class="form-control" name="resultados_ant[0]" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="resultados_ant[1]" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="resultados_ant[2]" required>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Laboratorio:</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="labVaricelaAnti" class="form-control" name="laboratorio_ant[0]" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="laboratorio_ant[1]" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control" name="laboratorio_ant[2]" required>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Fecha:</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <input type="text" id="fechaVaricelaAnti" class="form-control fechas_vacunas" name="fecha_ant[0]" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control fechas_vacunas" name="fecha_ant[1]" required>
                    </div>
                    <div class="col-md-3">
                        <input type="text" class="form-control fechas_vacunas" name="fecha_ant[2]" required>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title">Sistema de aseguramiento</h3>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Seguridad Social (EPS)</label>
                            <input type="text" class="form-control" name="eps" placeholder="Entidad">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label>Conoce el reglamento de prácticas?</label>
                                </div>
                                <div class="col md-6 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="conoce_reglamento" value="Si">
                                        Si
                                    </label></div>
                                <div class="col md-6 col-xs-6">
                                    <label class="radio-inline">
                                        <input type="radio" name="conoce_reglamento" value="No">
                                        No
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-header with-border">
                        <h3 class="box-title">En caso de emergencia avisar a:</h3>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <label>Nombres y apellidos</label>
                    </div>
                    <div class="col-md-4">
                        <label>Dirección</label>
                    </div>
                    <div class="col-md-4">
                        <label>Teléfono</label>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="nomape_eme">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="direccion_eme">
                    </div>
                    <div class="col-md-4">
                        <input type="text" class="form-control" name="telefono_eme">
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-offset-4 col-md-4">
                        <div class="form-group">
                            <h3 for="archivoAnexo">Subir archivo anexo</h3>
                            <input type="file" id="archivoAnexo" name="archivo">
                        </div>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12">
                        <label>Favor anexar los siguientes documentos en
                            un solo archivo en formato PDF:</label><br>
                        <label>1. Carné estudiantil (Pegar
                            imagen).</label><br>
                        <label>2. Cedula de ciudadanía (Pegar imagen por
                            ambos lados).</label><br>
                        <label>3. Certificado expedido por la EPS (Pegar
                            imagen) con fecha vigente a la del semestre a cursar, NO ACEPTAN el certificado del
                            FOSYGA.</label><br>
                        <label>4. Carné de Vacunas (Pegar imagen por
                            ambos lados).</label><br>
                        <label>5. Reportes de los anticuerpos (Pegar
                            imagen).</label><br>
                        <label>6. Resultado prueba de PPD (pegar
                            imagen).</label>
                    </div>
                </div>

                <div class="box-body">
                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <a class="btn btn-primary pull-right margin-r-5"
                           href="{{ route('view_registro_est') }}">
                            <i class="fa fa-ban" aria-hidden="true"></i>
                            Cancelar
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        /*function readURL(input) {
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $("#foto").attr("src", e.target.result);
                };
                reader.readAsDataURL(input.files[0]);
            }
        }*/

        $(document).ready(function () {
            //Validación del tipo de archivo
            $('#archivoAnexo').change(function (){
                var file = this.files[0];
                var anexo = file.type;
                var anexoSize = file.size;
                var match = ["application/pdf"];
                
                if(!(anexo == match[0] && anexoSize < 20000000)){
                    alert("Por favor asegurese de que se está tratando de anexar un archivo en formato PDF y con un tamaño menor a 20MB");
                    $(this).val("");
                    return false;
                }

            });

            //Date picker
            $('#fecha_de_nacimiento').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $('#fecha_ppd').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $('.fechas_vacunas').datepicker({
                dateFormat: 'yy-mm-dd'
            });

            $('#aplicaVaricela').change(function () {
                if ($(this).prop('checked')) {
                    $('#dosisVaricela').val(0);
                    $('#dosisVaricela').prop("disabled", true);
                    $('#fechaVaricela').prop("disabled", true);
                } else{
                    $('#dosisVaricela').prop("disabled", false);
                    $('#fechaVaricela').prop("disabled", false);
                }
            });

            $('#aplicaVaricelaAnti').change(function () {
                if ($(this).prop('checked')) {
                    $('#resulVaricelaAnti').val("No aplica");
                    $('#resulVaricelaAnti').prop("disabled", true);
                    $('#labVaricelaAnti').prop("disabled", true);
                    $('#labVaricelaAnti').val("No aplica");
                    $('#fechaVaricelaAnti').prop("disabled", true);
                } else{
                    $('#resulVaricelaAnti').val("");
                    $('#labVaricelaAnti').val("");
                    $('#resulVaricelaAnti').prop("disabled", false);
                    $('#labVaricelaAnti').prop("disabled", false);
                    $('#fechaVaricelaAnti').prop("disabled", false);
                }
            });

            $('#cambiar').change(function () {
                readURL(this);
            });
        });
    </script>
@endsection
