@extends('base.app')

@section('migas')
<h1>
Módulo de control de vacunas
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Control de vacunas</li>
</ol>
@endsection

@section('contenido')

<div class="col-xs-12 col-md-12">
	<div class="box box-success">
        <div class="box-header with-border">
          	<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Estudiantes Registrados</h3>
		</div>
        <div class="box-body">
			<table id="lista_estudiantes" class="table table-bordered table-striped" width="100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Nombres</th>
						<th>Apellidos</th>
						<th>Pregrado</th>
						<th>Semestre</th>
						<th>Fecha</th>
						<th>Celular</th>
						<th>Correo</th>
						<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
                
                @if (!$estudiantes->isEmpty())
                @foreach ($estudiantes as $estudiante)
                @php 
                $i = 0;
                @endphp
                <tr>
                <td>{{ $estudiante->id }}</td>
                <td>{{ $estudiante->nombres }}</td>
                <td>{{ $estudiante->apellidos }}</td>
                <td>{{ $estudiante->pregrado }}</td>
                <td>{{ $estudiante->semestre }}</td>
                <td>{{ $estudiante->fecha_diligenciamiento }}</td>
                <td>{{ $estudiante->celular }}</td>
                <td>{{ $estudiante->correo_electronico }}</td>
                <td>{!! $options[$i] !!}</td>
                </tr>
                @php
                $i++;
                @endphp
                @endforeach
                @else
                <tr>
                <td colspan="9" style="text-align: center;"> <label >No se encontraron resultados</label> </td>
                </tr>
                @endif

				</tbody>
			</table>

            {{ $estudiantes->links() }}
		</div>
	</div>
</div>

@endsection

@section('js')

@endsection