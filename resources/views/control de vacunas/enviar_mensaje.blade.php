@extends('base.app')

@section('migas')
<h1>
Control de vacunas
<small>Enviar mensaje</small>
</h1>
<ol class="breadcrumb">
<li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i>Control de vacunas</a></li>
<li class="active">Mensaje</li>
</ol>
@endsection

@section('contenido')

<div class="col-md-12">
	<div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Enviar Mensaje</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->

            <div class="box-body">
            <form role="form" action="{{ route('create_mensaje') }}" method="POST">

              <input type="hidden" name="_token" value="{{ csrf_token() }}">

            @if ($errors->any()) 

		    <div class="alert alert-danger" role="alert">
		    <i class="fa fa-close" aria-hidden="true"></i> 
		    Error al tratar de realizar la operación.        
		      <ul>             
		        @foreach ($errors->all() as $error)                 
		        <li>{{ $error }}</li>             
		        @endforeach         
		      </ul>     
		    </div> 
		    @endif
            		
	         <div class="row">    
	         <div class="col-md-12">
	         <div class="form-group">
	         <label>Estudiante</label>
	          <input type="text" class="form-control" id="estudiante" name="estudiante" value="{{ $id }}" readonly>

	         </div>
	         </div>
	         </div>

             <div class="row">
	              <div class="col-md-12">
		              <h3 class="box-title">Escriba su Mensaje
		              </h3>	
		            <div class="box-body pad">
	                    <textarea id="mensaje" name="mensaje" rows="10" cols="130"></textarea>
		            </div>
		           </div>
		     </div>
		          	<!-- /.box -->
	              		<!-- /.box-body -->
	              <div class="col-md-12 box-footer">
	                <a class="btn btn-warning" href="{{ route('list_estudiantes_reg') }}">Cancelar</a>
	                <button type="submit" class="btn btn-info pull-right">Enviar</button>
	              </div>
	              <!-- /.box-footer -->
	              </form>
          		</div>
            
          </div>
</div>

@endsection

@section('js')

<script type="text/javascript">

	/*$(function () {
	    CKEDITOR.replace('observaciones')
	    //bootstrap WYSIHTML5 - text editor
	    $('.textarea').wysihtml5()
	  });
	//Date picker*/
</script>

@endsection