@extends('base.app')

@section('migas')
<h1>
Módulo de Control de Vacunas
</h1>
<ol class="breadcrumb">
<li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Home</a></li>
<li class="active">Control de vacunas</li>
</ol>
@endsection

@section('contenido')

<div class="col-xs-12 col-md-12">
	<div class="box box-success">
        <div class="box-header with-border">
          	<h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Mensajes</h3>
		</div>
        <div class="box-body">
			<table id="list_mensajes_est" class="table table-bordered table-striped" width="100%">
				<thead>
					<tr>
						<th>Id</th>
						<th>Remitente</th>
						<th>Mensaje</th>
						<th>Fecha</th>
					</tr>
				</thead>
				<tbody>
				</tbody>
			</table>
		</div>
	</div>
</div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('#list_mensajes_est').DataTable( {
			"language": {
			    "sProcessing":     "Procesando...",
			    "sLengthMenu":     "Mostrar _MENU_ registros",
			    "sZeroRecords":    "No se encontraron resultados",
			    "sEmptyTable":     "Ningún dato disponible en esta tabla",
			    "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
			    "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
			    "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
			    "sInfoPostFix":    "",
			    "sSearch":         "Buscar:",
			    "sUrl":            "",
			    "sInfoThousands":  ",",
			    "sLoadingRecords": "Cargando...",
			    "oPaginate": {
			        "sFirst":    "Primero",
			        "sLast":     "Último",
			        "sNext":     "Siguiente",
			        "sPrevious": "Anterior"
			    },
			    "oAria": {
			        "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
			        "sSortDescending": ": Activar para ordenar la columna de manera descendente"
			    }
			},
	        "ajax": {"url":'{{ route("list_mensajes_ajax",["id"=>$usuario]) }}',dataSrc:''},
	        "deferRender":    false,
	        "processing": 	  false,
	        "scrollCollapse": true,
	        "scrollX":       true,
	        "scroller":       true,
	        "stateSave":      true,
          	"serverSide": 	true,
          	"paging" : false,
          	"info" : false
	    } );
	});
</script>

@endsection