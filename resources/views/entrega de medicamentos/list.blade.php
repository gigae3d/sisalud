@extends('base.app')

@section('migas')
    <h1>Entregas de Medicamentos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Entregas de Medicamentos</li>
        <li class="active">Lista de Entregas de Medicamentos</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Entregas Medicamentos</h3>
                <div class="box-tools pull-right">
                    <a href="{{ route('new_entrega_med_w')}}" class="btn btn-success"><i class="fa fa-plus-circle"></i>&nbsp;Crear
                        Entrega</a>
                </div>
            </div>
            <div class="box-body">
                @if(session()->has('exito'))
                    <div class="alert alert-info alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">&times;</span></button>
                        <span class="glyphicon glyphicon-ok"></span> {{ session('exito') }}
                    </div>
                @endif
                <table id="lista_entrega_medicamentos" class="table table-bordered table-striped" width="100%">
                    <thead>
                    <tr>
                        <th>Id</th>
                        <th>Numero de identificación</th>
                        <th>Usuario que registra</th>
                        <th>Fecha</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalEntregaMed" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog " role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span ria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Información Adicional</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <h4>Medicamentos entregados:</h4>
                            <table id="lista_entrega_medicamentos" class="table table-bordered table-striped" width="100%">
                                <thead>
                                <tr>
                                    <th>Código</th>
                                    <th>Nombre</th>
                                    <th>Cantidad</th>
                                </tr>
                                </thead>
                                <tbody class="medicamentos_ent">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            $('#lista_entrega_medicamentos').DataTable({
                "language": {
                    "sProcessing": "Cargando...",
                    "sLengthMenu": "Ver _MENU_ registros",
                    "sZeroRecords": "No se encontraron resultados.",
                    "sEmptyTable": "No se encontraron resultados.",
                    "sInfo": "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                    "sInfoEmpty": "",
                    "sInfoFiltered": "",
                    "sInfoPostFix": "",
                    "sSearch": "Buscar:",
                    "sUrl": "",
                    "sInfoThousands": ",",
                    "sLoadingRecords": "Cargando...",
                    "oPaginate": {
                        "sFirst": "Primero",
                        "sLast": "Último",
                        "sNext": "Siguiente",
                        "sPrevious": "Anterior"
                    },
                    "oAria": {
                        "sSortAscending": ": Ordenarvde manera Ascendente",
                        "sSortDescending": ": Ordenar manera Descendente"
                    }
                },
                "ajax": '{{ route("list_reg_entrega_medicamentos_ajax") }}',
                "deferRender": false,
                "processing": true,
                "scrollCollapse": true,
                "scrollX": true,
                "scroller": true,
                "stateSave": true,
                "serverSide": true
            });

            //Mostrar informacion adicional de una entrega de medicamentos
            $('#lista_entrega_medicamentos').on('click', 'tbody tr td div button', function ()
            {
                $(".medicamentos_ent").html('');
                var codigo = $(this).parent().children('p').text();//Obtener el id del registro de entrega de medicamento

                $.ajax({
                    method: "GET",
                    url: "{{ route('get_medicamentos_entr') }}",
                    data: {id: codigo},
                    dataType: "JSON"
                })
                    .done(function (json) {
                        var i;
                        $('.medicamentos_ent').empty();
                        for (i = 0; i < json.medicamentos.length; i++) {
                            $('.medicamentos_ent').append('<tr role="row" class="odd">'
                                + '<td>' + json.medicamentos[i].medicamento + '</td>'
                                + '<td>' + json.nombres[i] + '</td>'
                                + '<td>' + json.medicamentos[i].cantidad + '</td>'
                                + '</tr>'
                            );
                        }

                        $('#modalEntregaMed').modal("show");
                    })
                    .fail(function (xhr, status, errorThrown) {
                        console.log("Error: " + errorThrown);
                        console.log("Status: " + status);
                        console.dir(xhr);
                    });
            });
        });
    </script>
@endsection