@extends('base.menupaciente')

@section('migas')
    <h1>Entregas de Medicamentos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Entregas de Medicamentos</li>
        <li class="active">Lista de Entregas de Medicamentos</li>
    </ol>
@endsection

@section('contenido')

<div id="contenedor_vue">

<div class="col-md-12">
	   <div class="box box-success">

		 <div class="box-header with-border">
		 <div class="form-inline" >
		  <label>Buscar:</label>
          <input class="form-control" type="text" id="buscar" v-model="busquedaEntregaMedicamento">
         </div>
                <div class="box-tools pull-right">
                    <!-- <a href="{{ route('new_Medicamento')}}" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Medicamento</a>-->
					<!--<a v-on:click="modal_reg_entrega_medicamento" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Entrega Medicamento</a>--> 
                </div>
         </div>
			

		<div class="box-body">
    	
        <table class="table table-hover">
            <thead>
                <th>Usuario que registra</th>
                <th>Fecha</th>
                <th>Acciones</th>
            </thead>
            
            <tbody>
                <tr>
                
                </tr>
            </tbody>

        </table>

        <div class="pull-right">
		<label>Página @{{pagina}} de @{{numeroDePaginas}}</label>
		<button v-if="pagina == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		<button v-else type="button" class="btn btn-link " @click="pagina--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		<button v-if="pagina == numeroDePaginas || numeroDePaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
        <button v-else type="button" class="btn btn-link " @click="pagina++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		</div>

        </div>

</div>

@endsection

@section('js')

<script src="{{ asset('js/entrega_medicamentos/entrega_medicamentos.js') }}"></script>

@endsection