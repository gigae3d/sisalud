@extends('base.app')

@section('migas')
    <h1>Equipos Médicos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Equipos Médicos</li>
        <li class="active">Lista Equipos Médicos</li>
    </ol>
@endsection

@section('contenido')

<div id="contenedor_vue">

    <div class="col-md-12">
        <div class="box box-success">

        <div class="box-header with-border">
		 <div class="form-inline" >
		  <label>Buscar:</label>
          <input class="form-control" type="text" id="buscar" v-model="busquedaEquipoMedico">
         </div>
                <div class="box-tools pull-right">
                    <!-- <a href="{{ route('new_Medicamento')}}" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Medicamento</a>-->
					      <a v-on:click="modal_reg_equipo_medico" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp;Crear Equipo medico</a>
                </div>
         </div>

         <div class="box-body">
         <table class="table table-hover">
        	<thead>
          	<th>Nombre</th>
          	<th>Marca</th>
            <th>Modelo</th>
          	<th>Tipo</th>
            <th>Existencia</th>
            <th>Numero de serie</th>
            <th>Acciones</th>
        	</thead>

        <tbody v-if="equipos_medicos.length == 0">
				<tr v-if="equipos_medicos.length == 0">
				<td colspan="8">No se encontraron resultados...</td>
				<tr>
				</tbody>
				<tbody v-else>
                <tr v-for="equipo in equipos_medicos">
                     <td>@{{equipo.nombre}}</td>
                     <td>@{{equipo.marca}}</td>
                     <td>@{{equipo.modelo}}</td>
				     <td>@{{equipo.tipo}}</td>
				     <td>@{{equipo.existencia}}</td>
				     <td>@{{equipo.numero_de_serie}}</td>
				     <td>
				     <div style="display: flex;">
				     <!--<button type="button" class="btn btn-link "><span class="glyphicon glyphicon-pencil" style="color:#219F08;" aria-hidden="true" @click="modal_edi_equipo_medico(equipo.id)"></span></button>-->
				     </div>
				     </td>
               </tr>
        </tbody>
    	</table>

        <div class="pull-right">
		<label>Página @{{pagina}} de  @{{numeroDePaginas}}</label>
		<button v-if="pagina == 1" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		<button v-else type="button" class="btn btn-link " @click="pagina--"><span class="glyphicon glyphicon-chevron-left" aria-hidden="true" ></span></button>
		<button v-if="pagina == numeroDePaginas || numeroDePaginas == 0" type="button" class="btn btn-link " disabled><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
        <button v-else type="button" class="btn btn-link " @click="pagina++"><span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span></button>
		</div>

         </div>

        </div>
    </div>


<div class="modal fade" id="modAgregarEquipoMedico" tabindex="-1" role="dialog"> <!-- inicio modal agregar equipo medico -->
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Registro de Equipo Medico</h4>
      </div>
      <div class="modal-body">
        
				<div class="row">
					<div class="col-md-6">
					<div class="form-group">
              <label>Nombre</label>
              <input type="text" class="form-control" id="nombre" v-model="nuevo_equipomedico.nombre" placeholder="Nombre"
              >
							
          </div>

					<div class="form-group">
            <label>Marca:</label>
            <input type="text" class="form-control" id="marca" v-model="nuevo_equipomedico.marca" placeholder="Marca">
          </div>

					<div class="form-group">
              <label>Modelo:</label>
              <input type="text" class="form-control" id="modelo" v-model="nuevo_equipomedico.modelo" placeholder="Modelo"
              >
          </div>

					<div class="form-group">
               <label>Tipo</label>
              <input type="text" class="form-control" id="tipo" v-model="nuevo_equipomedico.tipo"
                placeholder="Tipo">
           </div>
					</div>						
					
					<div class="col-md-6">
					<div class="form-group">
          <label>Existencia</label>
          <input type="number" class="form-control" id="existencia" v-model="nuevo_equipomedico.existencia"
                 placeholder="Existencia" min="1">
          </div>

					<div class="form-group">
              <label>Numero de serie</label>
              <input type="text" class="form-control" id="numero_de_serie" v-model="nuevo_equipomedico.numero_serie"
                     placeholder="Numero de serie" >
          </div>

		 </div>

      </div>
      <div class="modal-footer">
	    <label v-show="campos_nuevo_equipom_vacio" style="color:red;">*Debe llenar todos los campos.</label>
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="button" class="btn btn-primary" v-on:click="registrar_equipo_medico" :disabled="campos_nuevo_equipom_vacio">Guardar</button>
      </div>
    </div>
  </div>
</div>
</div> <!--Fin modal agregar equipo medico-->  


</div>


@endsection


@section('js')

<script src="{{ asset('js/equipos_medicos/equipos_medicos.js') }}"></script>
<script src="{{ asset('js/sweetalert2.js') }}"></script>


@endsection