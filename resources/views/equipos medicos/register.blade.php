@extends('base.app')

@section('migas')
    <h1>Equipos Médicos</h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li>Inventario</li>
        <li>Equipos Médicos</li>
        <li class="active">Crear Equipo Médico</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Crear Equipo Médico</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form role="form" action="{{ route('create_equipo_medico') }}" method="POST">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                @if ($errors->any())
                    <div class="box-body">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissible ml-5" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span
                                            aria-hidden="true">&times;</span></button>
                                <strong>Importante!</strong> La solicitud no se pudo procesar, revisa los siguientes
                                errores:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="box-body">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Nombre:</label>
                            <input type="text" class="form-control" id="nombre" name="nombre"
                                   placeholder="Nombre del equipo" value="{{ old('nombre') }}">
                            <!-- /.input group -->
                        </div>
                        <div class="form-group">
                            <label>Marca:</label>
                            <input type="text" class="form-control" id="marca"
                                   name="marca" placeholder="Marca del equipo" value="{{ old('marca') }}">
                        </div>
                        <div class="form-group">
                            <label>Modelo:</label>
                            <input type="text" class="form-control" id="modelo" name="modelo" placeholder="Modelo"
                                   value="{{ old('modelo') }}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label style="font-size:16px; font-style: italic;">Tipo:</label>
                            <input type="text" class="form-control" id="tipo" name="tipo"
                                   placeholder="Especifique el tipo" value="{{ old('tipo') }}">
                        </div>
                        <div class="form-group">
                            <label>Existencia:</label>
                            <input type="number" class="form-control" id="existencia" name="existencia"
                                   placeholder="Existencia" min="1" value="{{ old('existencia') }}">
                        </div>
                        <div class="form-group">
                            <label>Numero de serie:</label>
                            <input type="text" class="form-control" id="numero_de_serie" name="numero_de_serie"
                                   placeholder="Numero de serie" value="{{ old('numero_de_serie') }}">
                        </div>
                    </div>

                    <div class="col-md-12 box-footer">
                        <button type="submit" class="btn btn-success pull-right">
                            <i class="fa fa-floppy-o" aria-hidden="true"></i> Guardar
                        </button>
                        <a class="btn btn-primary pull-right margin-r-5" href="{{ route('view_equipos_medicos') }}"><i
                                    class="fa fa-ban" aria-hidden="true"></i> Cancelar</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection  



