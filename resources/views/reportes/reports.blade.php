@extends('base.app')

@section('migas')
    <h1>Reportes </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('Home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Reportes</li>
    </ol>
@endsection

@section('contenido')
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Reportes</h3>
            </div>
            <div class="box-body">
            @if (Auth::user()->role == 'UControlVacunas')
                <div class="col-md-3 col-xs-6 margin-bottom">
                    <a class="btn btn-block btn-accion" id="reporteVacunas" data-url="{{ route('filtro_reporte_vacunas') }}">
                        <i class="fa fa-stethoscope" aria-hidden="true"></i><br>Cuadro Vacunas
                    </a>
                </div>
            @endif

            @if (Auth::user()->role == 'Enfermero')
                <div class="col-md-3 col-xs-6 margin-bottom">
                    <a class="btn btn-block btn-accion" href="{{ route('rep_medicamentos') }}">
                        <i class="fa fa-medkit" aria-hidden="true"></i></span><br>Medicamentos
                    </a>
                </div>

                <div class="col-md-3 col-xs-6 margin-bottom">
                    <a class="btn btn-block btn-accion" href="{{ route('rep_anticonceptivos') }}">
                        <i class="fa fa-plus" aria-hidden="true"></i></span><br>Anticonceptivos
                    </a>
                </div>

                <div class="col-md-3 col-xs-6 margin-bottom">
                    <a class="btn btn-block btn-accion" href="{{ route('rep_insumos_medicos') }}">
                        <i class="fa fa-user-md" aria-hidden="true"></i></span><br>Insumos Medicos
                    </a>
                </div>

                <div class="col-md-3 col-xs-6 margin-bottom">
                    <a class="btn btn-block btn-accion" href="{{ route('rep_equipos_medicos') }}">
                        <i class="fa fa-list" aria-hidden="true"></i></span><br>Equipos Medicos
                    </a>
                </div>
            @endif
            </div>
        </div>
    </div>
    <input type="hidden" id="_token" name="_token" value="{{ csrf_token() }}">

    <div class="modal fade in" id="modalFiltrosVacunas" tabindex="-1" role="dialog" aria-labelledby="modalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close"  id="closeModal" data-dismiss="modal" aria-label="Close">
                        <span ria-hidden="true">&times</span></button>
                    <h4 class="modal-title" id="modalLabel">Filtros</h4>
                </div>
                <div class="modal-body" id="filtroBody">
                    {{--Filtros creados--}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            callFiltrosVacunas();
            var $modal = $('#closeModal');
            $modal.click(function () {
                var $closeModal = $('#modalFiltrosVacunas');
                closeModal($closeModal);
            })
        });

        function callFiltrosVacunas() {
            var $filtroVacunas = $('#reporteVacunas');
            var $contenedorModal = $('#modalFiltrosVacunas');
            var $url = $('#reporteVacunas').data('url');
            var $token = $('#_token');

            $filtroVacunas.click(function () {
                $contenedorModal.show();
                $.post($url, $token).done(function (data) {
                    $contenedorModal.find('#filtroBody').html($(data));
                });
            })
        }

        function closeModal($modal) {
            $modal.click(function () {
                $modal.hide();
            })
        }
    </script>
@endsection
