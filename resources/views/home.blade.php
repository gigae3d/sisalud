@extends('base.app')

@section('migas')
    <h1>
        Bienvenido
        <small>Página principal</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="javascript:void(0);"><i class="fa fa-dashboard"></i> Página principal</a></li>
        <li class="active">Menú</li>
    </ol>
@endsection

@section('contenido')

@if (Auth::user()->role == 'Enfermero')
    <div class="box-body">
        <div class="col-md-4 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-primary">
                <div class="inner">
                    <h3>Pacientes</h3>
                    <p>Atención de enfermería</p>
                </div>
                <div class="icon">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <a href="{{ route('validate') }}" class="small-box-footer">
                    Validar <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-md-4 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-green">
                <div class="inner">
                    <h3>Inventario</h3>
                    <p>Medicamentos</p>
                </div>
                <div class="icon">
                    <i class="fa fa-medkit" aria-hidden="true"></i>
                </div>
                <a href="{{ route('view_medicamentos') }}" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-md-4 col-xs-12">
            <!-- small box -->
            <div class="small-box bg-yellow">
                <div class="inner">
                    <h3>Planificación</h3>
                    <p>Anticonceptivos</p>
                </div>
                <div class="icon">
                    <i class="ion ion-person-add"></i>
                </div>
                <a href="{{ route('view_medicamentos_antic') }}" class="small-box-footer">
                    Ver <i class="fa fa-arrow-circle-right"></i>
                </a>
            </div>
        </div>
        <!-- ./col -->
    </div>

    <div class="box-body">
        <div class="col-md-6 col-xs-12">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-bar-chart" aria-hidden="true"></i>&nbsp;Servicios
                        Prestados</h3>
                </div>
                <div class="box-body chart-responsive">
                    <div class="col-md-1"></div>
                    <div class="col-md-10" id="bar-chart" style="height: 300px;"></div>
                    <div class="col-md-1"></div>
                </div>
            </div>
        </div>


        <div class="col-md-6 col-xs-12">
            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Medicamentos con menor
                        existencia</h3>
                </div>
                <div class="box-body" style="height: 320px;">
                    <table id="lista_medicamentos" class="table table-bordered table-striped" width="100%">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Laboratorio</th>
                            <th>Existencia</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($medicamentos_existencia as $value)
                            <tr>
                                <td>{{ $value->id }}</td>
                                <td>{{ $value->nombre }}</td>
                                <td>{{ $value->laboratorio }}</td>
                                <td>{{ $value->existencia }}</td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    @endif

@if (Auth::user()->role == 'Estudiante')
<div class="col-xs-12 col-md-12">
  <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Registro del estudiante</h3>
      <div class="box-tools pull-right">
        @if (Auth::user()->num_records == 0)
        <a href="{{ route('new_reg_control_vacuna')}}" class="btn btn-success"><i class="fa fa-list"></i>&nbsp;Crear registro de control de vacunas</a>
        @else
            <a href="{{ route('new_reg_control_vacuna')}}" class="btn btn-success disabled"><i class="fa fa-list"></i>&nbsp;Crear registro de control de vacunas</a>
            @endif
          </div>
    </div>
        <div class="box-body">
      <table id="list_registro_est" class="table table-bordered table-striped" width="100%">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Pregrado</th>
            <th>Semestre</th>
            <th>Fecha</th>
            <th>Celular</th>
            <th>Correo</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>

@endif

@if (Auth::user()->role == 'UControlVacunas')


<div class="col-xs-12 col-md-12">
  <div class="box box-success">
        <div class="box-header with-border">
            <h3 class="box-title"><i class="fa fa-list" aria-hidden="true"></i>&nbsp;Lista de Estudiantes registrados</h3>
      {{--<div class="box-tools pull-right">
            <a href="{{ route('new_reg_control_vacuna')}}" class="btn btn-success"><i class="fa fa-list"></i>&nbsp;Crear nuevo registro</a>
          </div>--}}
    </div>
        <div class="box-body">

        <h3 class="box-title">Filtrar estudiantes
    </h3>

     <form role="form" action="{{ route('filtrar_estudiantes') }}" method="POST">

         <input type="hidden" name="_token" value="{{ csrf_token() }}">
            
        <div class="row">
         <div class="col-md-3">
         <div class="form-group">
         <label style="font-size:16px; font-style: italic;">Programa:</label>
         
         <select class="form-control" id="programa" name="programa">
                        <option value=""></option>  
                          <option value="Enfermeria">Enfermeria</option>
                          <option value="Medicina">Medicina</option>
                      </select>
         </div>
         </div>

         <div class="col-md-3">
          <div class="form-group">
            <label style="font-size:16px; font-style: italic;">Semestre:</label>
            
            <div class="form-inline">
            
            <select class="form-control" name="semestre">
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
            </select>
            <button type="submit" class="btn btn-info pull-right">Consultar</button>
            </div>

            </div>

         </div>
         </div> 

     </form>

      <table id="lista_estudiantes_reg" class="table table-bordered table-striped" width="100%">
        <thead>
          <tr>
            <th>Id</th>
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Pregrado</th>
            <th>Semestre</th>
            <th>Fecha</th>
            <th>Celular</th>
            <th>Correo</th>
            <th>Acciones</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
    </div>
  </div>
</div>

@endif


@endsection

@if (Auth::user()->role == 'Enfermero')
@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            //Grafico para estadisticas
            var day_data = [
                {"period": "Presión Arterial", "Registros": "{{ $presion_arterial }}"},
                {"period": "Glucometrias", "Registros": "{{ $glucometrias }}"},
                {"period": "Inyectologías", "Registros": "{{ $inyectologias }}"},
                {"period": "Atenc. Salud", "Registros": "{{ $atencion_salud }}"},
                {"period": "Ent. Medicamento", "Registros": "{{ $medicamento }}"}
            ];
            Morris.Bar({
                element: 'bar-chart',
                data: day_data,
                xkey: 'period',
                ykeys: ['Registros'],
                labels: ['Registros'],
                xLabelAngle: 30
            });
        });
    </script>
@endsection
@endif

@if (Auth::user()->role == 'UControlVacunas')
@section('js')

<script type="text/javascript">
  $(document).ready(function() {
    $('#lista_estudiantes_reg').DataTable( {
      "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
          "ajax": '{{ route("list_estudiantes_reg_ajax") }}',
          "deferRender":    false,
          "processing":     true,
          "scrollCollapse": true,
          "scrollX":       true,
          "scroller":       true,
          "stateSave":      true,
            "serverSide":   true
      } );
  });
</script>

@endsection
@endif


@if (Auth::user()->role == 'Estudiante')
@section('js')

<script type="text/javascript">
  $(document).ready(function() {
    $('#list_registro_est').DataTable( {
      "language": {
          "sProcessing":     "Procesando...",
          "sLengthMenu":     "Mostrar _MENU_ registros",
          "sZeroRecords":    "No se encontraron resultados",
          "sEmptyTable":     "Ningún dato disponible en esta tabla",
          "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
          "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
          "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
          "sInfoPostFix":    "",
          "sSearch":         "Buscar:",
          "sUrl":            "",
          "sInfoThousands":  ",",
          "sLoadingRecords": "Cargando...",
          "oPaginate": {
              "sFirst":    "Primero",
              "sLast":     "Último",
              "sNext":     "Siguiente",
              "sPrevious": "Anterior"
          },
          "oAria": {
              "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
              "sSortDescending": ": Activar para ordenar la columna de manera descendente"
          }
      },
          "ajax": {"url":'{{ route("list_registro_est_ajax",["id"=>Auth::user()->id]) }}',dataSrc:''},
          "deferRender":    false,
          "processing":     false,
          "scrollCollapse": true,
          "scrollX":       true,
          "scroller":       true,
          "stateSave":      true,
            "serverSide":   true,
            "paging" : false,
            "info" : false
      } );
  });
</script>

@endsection
@endif
